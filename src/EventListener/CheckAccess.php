<?php

namespace App\EventListener;

use App\Entity\UserPi;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class CheckAccess
{
    private $router;
    private $security;
    private $flashBagService;
    private $session;

    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        Security $security,
        FlashBagInterface $flashBagService,
        SessionInterface $session
    ) {
        $this->router          = $urlGenerator;
        $this->security        = $security;
        $this->flashBagService = $flashBagService;
        $this->session         = $session;
    }

    public function onKernelRequest(RequestEvent $event)
    {

        $route = 'admin_change_password';

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        if (!empty($user) && $user->getEstado() == 'P' && $event->getRequest()->attributes->get('_route') != $route) {
            $url = $this->router->generate($route);
            $response = new RedirectResponse($url);
            $this->flashBagService->add('error', 'Debe cambiar su contraseña');
            $event->setResponse($response);
        }

        return;
    }
}
