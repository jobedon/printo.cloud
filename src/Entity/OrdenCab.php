<?php

namespace App\Entity;

use App\Repository\OrdenCabRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use NumberFormatter;

/**
 * OrdenCab
 *
 * @ORM\Table(name="orden_cab", indexes={@ORM\Index(name="FK_ORDCAB_USER", columns={"user_id"})})
 * @ORM\Entity(repositoryClass=OrdenCabRepository::class)
 */
class OrdenCab
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="no_orden", type="string", length=50, nullable=true)
     */
    private $noOrden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="no_ticket", type="string", length=50, nullable=true)
     */
    private $noTicket;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="total_items", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalItems = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="total_envio", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalEnvio = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="subtotal", type="float", precision=10, scale=0, nullable=true)
     */
    private $subtotal = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="total_iva", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalIva = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="total_pagar", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalPagar = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parent_id_wd_xerox", type="string", length=100, nullable=true)
     */
    private $parentIdWdXerox;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parent_id_wd_mimaki", type="string", length=100, nullable=true)
     */
    private $parentIdWdMimaki;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parent_id_wd_electro", type="string", length=100, nullable=true)
     */
    private $parentIdWdElectro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="requiere_envio", type="string", length=1, nullable=true)
     */
    private $requiereEnvio = 'N';

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion_id", type="string", length=100, nullable=true)
     */
    private $direccionId = 'N';

    /**
     * @var string|null
     *
     * @ORM\Column(name="metodo_pago", type="string", length=25, nullable=true)
     */
    private $metodoPago = 'N';

    /**
     * @var UserPi
     *
     * @ORM\ManyToOne(targetEntity="UserPi")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity=FilesOrdenCab::class, inversedBy="ordenCab", cascade={"persist", "remove"})
     */
    private $files;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $comprobantePath;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
        $this->setNoOrden($this->getIdByDate());
        $this->setFiles(new FilesOrdenCab());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNoOrden(): ?string
    {
        return $this->noOrden;
    }

    public function setNoOrden(?string $noOrden): self
    {
        $this->noOrden = $noOrden;

        return $this;
    }

    public function getNoTicket(): ?string
    {
        return $this->noTicket;
    }

    public function setNoTicket(?string $noTicket): self
    {
        $this->noTicket = $noTicket;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacionNow(): self
    {
        $this->fechaActualizacion = new \DateTime("now");

        return $this;
    }

    public function setFechaActualizacionArg(\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function __call($method, $arguments)
    {
        if ($method == 'setFechaActualizacion') {
            if (count($arguments) == 0) {
                return call_user_func_array(array($this, 'setFechaActualizacionNow'), $arguments);
            } else if (count($arguments) == 1) {
                return call_user_func_array(array($this, 'setFechaActualizacionArg'), $arguments);
            }
        }
    }

    public function getUser(): ?UserPi
    {
        return $this->user;
    }

    public function setUser(?UserPi $user): self
    {
        $this->user = $user;

        return $this;
    }



    public function getTotalItems(): ?float
    {
        return $this->totalItems;
    }

    public function setTotalItems(float $totalItems): self
    {
        $this->totalItems = round($totalItems, 2);

        return $this;
    }

    public function getTotalEnvio(): ?float
    {
        return $this->totalEnvio;
    }

    public function setTotalEnvio(float $totalEnvio): self
    {
        $this->totalEnvio = round($totalEnvio, 2);

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): self
    {
        $this->subtotal = round($subtotal, 2);

        return $this;
    }

    public function getTotalIva(): ?float
    {
        return $this->totalIva;
    }

    public function setTotalIva(float $totalIva): self
    {
        $this->totalIva = round($totalIva, 2);

        return $this;
    }

    public function getTotalPagar(): ?float
    {
        return $this->totalPagar;
    }

    public function setTotalPagar(float $totalPagar): self
    {
        $this->totalPagar = round($totalPagar, 2);

        return $this;
    }

    public function getParentIdWdXerox(): ?string
    {
        return $this->parentIdWdXerox;
    }

    public function setParentIdWdXerox(?string $parentIdWdXerox): self
    {
        $this->parentIdWdXerox = $parentIdWdXerox;

        return $this;
    }

    public function getParentIdWdMimaki(): ?string
    {
        return $this->parentIdWdMimaki;
    }

    public function setParentIdWdMimaki(?string $parentIdWdMimaki): self
    {
        $this->parentIdWdMimaki = $parentIdWdMimaki;

        return $this;
    }

    public function getParentIdWdElectro(): ?string
    {
        return $this->parentIdWdElectro;
    }

    public function setParentIdWdElectro(?string $parentIdWdElectro): self
    {
        $this->parentIdWdElectro = $parentIdWdElectro;

        return $this;
    }

    public function setFechaActualizacion(): self
    {
        $this->fechaActualizacion = new \DateTime("now");
        return $this;
    }

    public function getRequiereEnvio(): ?string
    {
        return $this->requiereEnvio;
    }

    public function setRequiereEnvio(?string $requiereEnvio): self
    {
        $this->requiereEnvio = $requiereEnvio;

        return $this;
    }

    public function getDireccionId(): ?string
    {
        return $this->direccionId;
    }

    public function setDireccionId(?string $direccionId): self
    {
        $this->direccionId = $direccionId;

        return $this;
    }

    public function getMetodoPago(): ?string
    {
        return $this->requiereEnvio;
    }

    public function setMetodoPago(?string $metodoPago): self
    {
        $this->metodoPago = $metodoPago;

        return $this;
    }

    public function resetPrecios()
    {
        $this->setTotalItems(0);
        $this->setTotalEnvio(0);
        $this->setSubtotal(0);
        $this->setTotalIva(0);
        $this->setTotalPagar(0);
        $this->setRequiereEnvio('N');
        $this->setMetodoPago('');
        $this->setDireccionId('N');
    }

    public function getIdByDate()
    {
        $now = (new \DateTime("now"))->format('Ymd') . '-' . uniqid();
        return $now;
    }

    public function getFiles(): ?FilesOrdenCab
    {
        return $this->files;
    }

    public function setFiles(?FilesOrdenCab $files): self
    {
        $this->files = $files;

        return $this;
    }

    public function getArrayData()
    {
        $fmt = numfmt_create('en_US', NumberFormatter::CURRENCY);
        $currency = "USD";

        $totalItems = numfmt_format_currency($fmt, $this->getTotalItems(), $currency);
        $totalIva   = numfmt_format_currency($fmt, $this->getTotalIva(), $currency);
        $totalPagar = numfmt_format_currency($fmt, $this->getTotalPagar(), $currency);
        return [
            'totalItems' => $totalItems,
            'totalIva'   => $totalIva,
            'totalPagar' => $totalPagar,
        ];
    }

    public function getComprobantePath(): ?string
    {
        return $this->comprobantePath;
    }

    public function setComprobantePath(?string $comprobantePath): self
    {
        $this->comprobantePath = $comprobantePath;

        return $this;
    }
}
