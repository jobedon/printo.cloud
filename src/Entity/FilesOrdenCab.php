<?php

namespace App\Entity;

use App\Repository\FilesOrdenCabRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FilesOrdenCabRepository::class)
 */
class FilesOrdenCab
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta Xerox en mesa de trabajo"})
     */
    private $parentIdXeroxWs;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta Mimaki en mesa de trabajo"})
     */
    private $parentIdMimakiWs;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta Electrocorte en mesa de trabajo"})
     */
    private $parentIdElectroWs;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta Xerox en carpeta Tickets"})
     */
    private $parentIdXeroxTk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta Mimaki en carpeta Tickets"})
     */
    private $parentIdMimakiTk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta Electrocorte en carpeta Tickets"})
     */
    private $parentIdElectroTk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta principal en carpeta Tickets"})
     */
    private $parentIdTk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=3, options={"default":"A"})
     */
    private $status = 'A';

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity=OrdenCab::class, mappedBy="files", cascade={"persist", "remove"})
     */
    private $ordenCab;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParentIdXeroxWs(): ?string
    {
        return $this->parentIdXeroxWs;
    }

    public function setParentIdXeroxWs(?string $parentIdXeroxWs): self
    {
        $this->parentIdXeroxWs = $parentIdXeroxWs;

        return $this;
    }

    public function getParentIdMimakiWs(): ?string
    {
        return $this->parentIdMimakiWs;
    }

    public function setParentIdMimakiWs(?string $parentIdMimakiWs): self
    {
        $this->parentIdMimakiWs = $parentIdMimakiWs;

        return $this;
    }

    public function getParentIdElectroWs(): ?string
    {
        return $this->parentIdElectroWs;
    }

    public function setParentIdElectroWs(?string $parentIdElectroWs): self
    {
        $this->parentIdElectroWs = $parentIdElectroWs;

        return $this;
    }

    public function getParentIdXeroxTk(): ?string
    {
        return $this->parentIdXeroxTk;
    }

    public function setParentIdXeroxTk(?string $parentIdXeroxTk): self
    {
        $this->parentIdXeroxTk = $parentIdXeroxTk;

        return $this;
    }

    public function getParentIdMimakiTk(): ?string
    {
        return $this->parentIdMimakiTk;
    }

    public function setParentIdMimakiTk(?string $parentIdMimakiTk): self
    {
        $this->parentIdMimakiTk = $parentIdMimakiTk;

        return $this;
    }

    public function getParentIdElectroTk(): ?string
    {
        return $this->parentIdElectroTk;
    }

    public function setParentIdElectroTk(?string $parentIdElectroTk): self
    {
        $this->parentIdElectroTk = $parentIdElectroTk;

        return $this;
    }

    public function getParentIdTk(): ?string
    {
        return $this->parentIdTk;
    }

    public function setParentIdTk(?string $parentIdTk): self
    {
        $this->parentIdTk = $parentIdTk;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrdenCab(): ?OrdenCab
    {
        return $this->ordenCab;
    }

    public function setOrdenCab(?OrdenCab $ordenCab): self
    {
        $this->ordenCab = $ordenCab;

        // set (or unset) the owning side of the relation if necessary
        $newFiles = null === $ordenCab ? null : $this;
        if ($ordenCab->getFiles() !== $newFiles) {
            $ordenCab->setFiles($newFiles);
        }

        return $this;
    }
}
