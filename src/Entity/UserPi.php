<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * UserPi
 *
 * @ORM\Table(name="user_pi", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_42DF0630E7927C74", columns={"email"})})
 * @ORM\Entity
 */
class UserPi implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=180, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="ruc", type="string", length=20, nullable=false)
     */
    private $ruc;

    /**
     * @ORM\Column(name="roles", type="json", nullable=false)
     */
    private $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido", type="string", length=100, nullable=true)
     */
    private $apellido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="razon_social", type="string", length=100, nullable=true)
     */
    private $razonSocial;

    /**
     * @var string|null
     *
     * @ORM\Column(name="crm_user_id", type="string", length=50, nullable=true)
     */
    private $crmUserId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=true, options={"default"="P"})
     */
    private $estado = 'P';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\OneToMany(targetEntity=UserPlanHistory::class, mappedBy="user")
     */
    private $userPlanHistories;

    /**
     * @ORM\OneToMany(targetEntity=Direccion::class, mappedBy="user")
     */
    private $direccions;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
        $this->setRoles(['ROLE_USER']);
        $this->userPlanHistories = new ArrayCollection();
        $this->direccions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRuc(): ?string
    {
        return $this->ruc;
    }

    public function setRuc(string $ruc): self
    {
        $this->ruc = $ruc;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(?string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getRazonSocial(): ?string
    {
        return $this->razonSocial;
    }

    public function setRazonSocial(?string $razonSocial): self
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    public function getCrmUserId(): ?string
    {
        return $this->crmUserId;
    }

    public function setCrmUserId(?string $crmUserId): self
    {
        $this->crmUserId = $crmUserId;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(?\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getSalt()
    {
        
    }

    public function getUsername()
    {
        return $this->getRuc();
    }

    public function eraseCredentials()
    {

    }

    /**
     * @return Collection|UserPlanHistory[]
     */
    public function getUserPlanHistories(): Collection
    {
        return $this->userPlanHistories;
    }

    public function addUserPlanHistory(UserPlanHistory $userPlanHistory): self
    {
        if (!$this->userPlanHistories->contains($userPlanHistory)) {
            $this->userPlanHistories[] = $userPlanHistory;
            $userPlanHistory->setUser($this);
        }

        return $this;
    }

    public function removeUserPlanHistory(UserPlanHistory $userPlanHistory): self
    {
        if ($this->userPlanHistories->contains($userPlanHistory)) {
            $this->userPlanHistories->removeElement($userPlanHistory);
            // set the owning side to null (unless already changed)
            if ($userPlanHistory->getUser() === $this) {
                $userPlanHistory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Direccion[]
     */
    public function getDireccions(): Collection
    {
        return $this->direccions;
    }

    public function addDireccion(Direccion $direccion): self
    {
        if (!$this->direccions->contains($direccion)) {
            $this->direccions[] = $direccion;
            $direccion->setUser($this);
        }

        return $this;
    }

    public function removeDireccion(Direccion $direccion): self
    {
        if ($this->direccions->contains($direccion)) {
            $this->direccions->removeElement($direccion);
            // set the owning side to null (unless already changed)
            if ($direccion->getUser() === $this) {
                $direccion->setUser(null);
            }
        }

        return $this;
    }

}
