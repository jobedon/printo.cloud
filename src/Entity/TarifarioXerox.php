<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TarifarioXerox
 *
 * @ORM\Table(name="tarifario_xerox", indexes={@ORM\Index(name="FK_SUBCAT_TARIFXEROX", columns={"subcategoria_producto_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TarifarioXeroxRepository")
 */
class TarifarioXerox
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="nivel", type="integer", nullable=false)
     */
    private $nivel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion_nivel", type="string", length=100, nullable=true)
     */
    private $descripcionNivel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion_cantidad", type="string", length=100, nullable=true)
     */
    private $descripcionCantidad;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad_min", type="integer", nullable=false)
     */
    private $cantidadMin = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad_max", type="integer", nullable=false)
     */
    private $cantidadMax = '0';

    /**
     * @var float|null
     *
     * @ORM\Column(name="precio_tiro", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioTiro;

    /**
     * @var float|null
     *
     * @ORM\Column(name="precio_tiro_retiro", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioTiroRetiro;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    /**
     * @ORM\ManyToOne(targetEntity="SubcategoriaProducto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subcategoria_producto_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $subcategoriaProducto;

    /**
     * @ORM\ManyToOne(targetEntity=CategoriaProducto::class, inversedBy="tarifarioXeroxes")
     */
    private $categoriaProducto;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNivel(): ?int
    {
        return $this->nivel;
    }

    public function setNivel(int $nivel): self
    {
        $this->nivel = $nivel;

        return $this;
    }

    public function getDescripcionNivel(): ?string
    {
        return $this->descripcionNivel;
    }

    public function setDescripcionNivel(?string $descripcionNivel): self
    {
        $this->descripcionNivel = $descripcionNivel;

        return $this;
    }

    public function getDescripcionCantidad(): ?string
    {
        return $this->descripcionCantidad;
    }

    public function setDescripcionCantidad(?string $descripcionCantidad): self
    {
        $this->descripcionCantidad = $descripcionCantidad;

        return $this;
    }

    public function getCantidadMin(): ?int
    {
        return $this->cantidadMin;
    }

    public function setCantidadMin(int $cantidadMin): self
    {
        $this->cantidadMin = $cantidadMin;

        return $this;
    }

    public function getCantidadMax(): ?int
    {
        return $this->cantidadMax;
    }

    public function setCantidadMax(int $cantidadMax): self
    {
        $this->cantidadMax = $cantidadMax;

        return $this;
    }

    public function getPrecioTiro(): ?float
    {
        return $this->precioTiro;
    }

    public function setPrecioTiro(?float $precioTiro): self
    {
        $this->precioTiro = $precioTiro;

        return $this;
    }

    public function getPrecioTiroRetiro(): ?float
    {
        return $this->precioTiroRetiro;
    }

    public function setPrecioTiroRetiro(?float $precioTiroRetiro): self
    {
        $this->precioTiroRetiro = $precioTiroRetiro;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getSubcategoriaProducto(): ?SubcategoriaProducto
    {
        return $this->subcategoriaProducto;
    }

    public function setSubcategoriaProducto(?SubcategoriaProducto $subcategoriaProducto): self
    {
        $this->subcategoriaProducto = $subcategoriaProducto;

        return $this;
    }

    public function getCategoriaProducto(): ?CategoriaProducto
    {
        return $this->categoriaProducto;
    }

    public function setCategoriaProducto(?CategoriaProducto $categoriaProducto): self
    {
        $this->categoriaProducto = $categoriaProducto;

        return $this;
    }


}
