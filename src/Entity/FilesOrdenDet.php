<?php

namespace App\Entity;

use App\Repository\FilesOrdenDetRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FilesOrdenDetRepository::class)
 */
class FilesOrdenDet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta item Xerox/Mimaki en mesa de trabajo"})
     */
    private $parentIdImpresionWs;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta item Electrocorte en mesa de trabajo"})
     */
    private $parentIdElectroWs;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"file Id item Xerox/Mimaki en mesa de trabajo"})
     */
    private $fileIdImpresionWs;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"file Id item Electrocorte en mesa de trabajo"})
     */
    private $fileIdElectroWs;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta item Xerox/Mimaki en carpeta Tickets"})
     */
    private $parentIdImpresionTk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"Parent Id carpeta item Electrocorte en carpeta Tickets"})
     */
    private $parentIdElectroTk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"file Id item Xerox/Mimaki en carpeta Tickets"})
     */
    private $fileIdImpresionTk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, 
     *             options={"comment":"file Id item Electrocorte en carpeta Tickets"})
     */
    private $fileIdElectroTk;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=3, options={"default":"A"})
     */
    private $status = 'A';

    /**
     * @ORM\Column(type="datetime", options={"default":"CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity=OrdenDet::class, mappedBy="files", cascade={"persist", "remove"})
     */
    private $ordenDet;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $folderId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $fileIdImpresion;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $fileIdElectrocorte;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParentIdImpresionWs(): ?string
    {
        return $this->parentIdImpresionWs;
    }

    public function setParentIdImpresionWs(?string $parentIdImpresionWs): self
    {
        $this->parentIdImpresionWs = $parentIdImpresionWs;

        return $this;
    }

    public function getParentIdElectroWs(): ?string
    {
        return $this->parentIdElectroWs;
    }

    public function setParentIdElectroWs(?string $parentIdElectroWs): self
    {
        $this->parentIdElectroWs = $parentIdElectroWs;

        return $this;
    }

    public function getFileIdImpresionWs(): ?string
    {
        return $this->fileIdImpresionWs;
    }

    public function setFileIdImpresionWs(string $fileIdImpresionWs): self
    {
        $this->fileIdImpresionWs = $fileIdImpresionWs;

        return $this;
    }

    public function getFileIdElectroWs(): ?string
    {
        return $this->fileIdElectroWs;
    }

    public function setFileIdElectroWs(?string $fileIdElectroWs): self
    {
        $this->fileIdElectroWs = $fileIdElectroWs;

        return $this;
    }

    public function getFileIdImpresionTk(): ?string
    {
        return $this->fileIdImpresionTk;
    }

    public function setFileIdImpresionTk(?string $fileIdImpresionTk): self
    {
        $this->fileIdImpresionTk = $fileIdImpresionTk;

        return $this;
    }

    public function getFileIdElectroTk(): ?string
    {
        return $this->fileIdElectroTk;
    }

    public function setFileIdElectroTk(?string $fileIdElectroTk): self
    {
        $this->fileIdElectroTk = $fileIdElectroTk;

        return $this;
    }

    public function getParentIdImpresionTk(): ?string
    {
        return $this->parentIdImpresionTk;
    }

    public function setParentIdImpresionTk(?string $parentIdImpresionTk): self
    {
        $this->parentIdImpresionTk = $parentIdImpresionTk;

        return $this;
    }

    public function getParentIdElectroTk(): ?string
    {
        return $this->parentIdElectroTk;
    }

    public function setParentIdElectroTk(?string $parentIdElectroTk): self
    {
        $this->parentIdElectroTk = $parentIdElectroTk;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrdenDet(): ?OrdenDet
    {
        return $this->ordenDet;
    }

    public function setOrdenDet(?OrdenDet $ordenDet): self
    {
        $this->ordenDet = $ordenDet;

        // set (or unset) the owning side of the relation if necessary
        $newFiles = null === $ordenDet ? null : $this;
        if ($ordenDet->getFiles() !== $newFiles) {
            $ordenDet->setFiles($newFiles);
        }

        return $this;
    }

    public function getFolderId(): ?string
    {
        return $this->folderId;
    }

    public function setFolderId(?string $folderId): self
    {
        $this->folderId = $folderId;

        return $this;
    }

    public function getFileIdImpresion(): ?string
    {
        return $this->fileIdImpresion;
    }

    public function setFileIdImpresion(?string $fileIdImpresion): self
    {
        $this->fileIdImpresion = $fileIdImpresion;

        return $this;
    }

    public function getFileIdElectrocorte(): ?string
    {
        return $this->fileIdElectrocorte;
    }

    public function setFileIdElectrocorte(?string $fileIdElectrocorte): self
    {
        $this->fileIdElectrocorte = $fileIdElectrocorte;

        return $this;
    }
}
