<?php

namespace App\Entity;

use App\Repository\UserPlanHistoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserPlanHistoryRepository::class)
 */
class UserPlanHistory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Plan::class, inversedBy="userPlanInicialHistories")
     */
    private $planInicial;

    /**
     * @ORM\ManyToOne(targetEntity=Plan::class, inversedBy="userPlanFinalHistories")
     */
    private $planFinal;

    /**
     * @ORM\ManyToOne(targetEntity=UserPi::class, inversedBy="userPlanHistories")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"default"="A"})
     */
    private $status = 'A';

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $subtotal;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $discount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlanInicial(): ?Plan
    {
        return $this->planInicial;
    }

    public function setPlanInicial(?Plan $planInicial): self
    {
        $this->planInicial = $planInicial;

        return $this;
    }

    public function getPlanFinal(): ?Plan
    {
        return $this->planFinal;
    }

    public function setPlanFinal(?Plan $planFinal): self
    {
        $this->planFinal = $planFinal;

        return $this;
    }

    public function getUser(): ?UserPi
    {
        return $this->user;
    }

    public function setUser(?UserPi $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(?float $subtotal): self
    {
        $this->subtotal = round($subtotal, 2);

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): self
    {
        $this->discount = round($discount, 2);

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(?float $total): self
    {
        $this->total = round($total, 2);

        return $this;
    }
}
