<?php

namespace App\Entity;

use App\Repository\OrdenDetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use NumberFormatter;

/**
 * OrdenDet
 *
 * @ORM\Table(name="orden_det", indexes={@ORM\Index(name="FK_ORDCAB_SUBCAT", columns={"subcategoria_producto_id"}), @ORM\Index(name="FK_ORDCAB_ORDDET", columns={"orden_cab_id"})})
 * @ORM\Entity(repositoryClass=OrdenDetRepository::class)
 */
class OrdenDet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_id", type="integer", nullable=true)
     */
    private $productoId;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=15, nullable=true)
     */
    private $sku;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre_producto", type="string", length=100, nullable=true)
     */
    private $nombreProducto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=600, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacion", type="string", length=300, nullable=true)
     */
    private $observacion;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="float", precision=10, scale=0, nullable=true, options={"default"=0})
     */
    private $cantidad = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="ancho", type="float", precision=10, scale=0, nullable=true)
     */
    private $ancho;

    /**
     * @var float
     *
     * @ORM\Column(name="alto", type="float", precision=10, scale=0, nullable=true)
     */
    private $alto;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=false)
     */
    private $precio;

    /**
     * @var float
     *
     * @ORM\Column(name="precio_base", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioBase = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precioBaseReal;

    /**
     * @var float
     *
     * @ORM\Column(name="precio_corte", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioCorte = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="precio_laminado", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioLaminado = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_corte", type="string", length=20, nullable=true)
     */
    private $tipoCorte;

    /**
     * @var string
     *
     * @ORM\Column(name="laminado", type="string", length=1, nullable=false, options={"default"="N"})
     */
    private $laminado = 'N';

    /**
     * @var string
     *
     * @ORM\Column(name="electrocorte", type="string", length=1, nullable=false, options={"default"="N"})
     */
    private $electrocorte = 'N';

    /**
     * @var string
     *
     * @ORM\Column(name="ojales", type="string", length=1, nullable=false, options={"default"="N"})
     */
    private $ojales = 'N';

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad_ojales", type="integer", nullable=true, options={"default"=0})
     */
    private $cantidadOjales = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="especificacion_ojales", type="string", length=300, nullable=true)
     */
    private $especificacionOjales;

    /**
     * @var float
     *
     * @ORM\Column(name="precio_ojales", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioOjales = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_laminado", type="string", length=20, nullable=true)
     */
    private $tipoLaminado;

    /**
     * @var string
     *
     * @ORM\Column(name="corte_rigido", type="string", length=1, nullable=false, options={"default"="N"})
     */
    private $corteRigido = 'N';

    /**
     * @var float
     *
     * @ORM\Column(name="precio_corte_rigido", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioCorteRigido = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_impresion", type="string", length=10, nullable=true)
     */
    private $tipoImpresion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta_impresion", type="string", length=150, nullable=true)
     */
    private $rutaImpresion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta_electrocorte", type="string", length=150, nullable=true)
     */
    private $rutaElectrocorte;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parent_id_wd_impresion", type="string", length=150, nullable=true)
     */
    private $parentIdWdImpresion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parent_id_wd_electro", type="string", length=150, nullable=true)
     */
    private $parentIdWdElectro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="file_id_wd_impresion", type="string", length=150, nullable=true)
     */
    private $fileIdWdImpresion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="file_id_wd_electro", type="string", length=150, nullable=true)
     */
    private $fileIdWdElectro;

    /**
     * @var string
     *
     * @ORM\Column(name="subida_impresion", type="string", length=1, nullable=true, options={"default"="N"})
     */
    private $subidaImpresion = 'N';

    /**
     * @var string
     *
     * @ORM\Column(name="subida_electro", type="string", length=1, nullable=true, options={"default"="N"})
     */
    private $subidaElectro = 'N';

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\ManyToOne(targetEntity="OrdenCab")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="orden_cab_id", referencedColumnName="id")
     * })
     */
    private $ordenCab;

    /**
     * @ORM\ManyToOne(targetEntity="SubcategoriaProducto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subcategoria_producto_id", referencedColumnName="id")
     * })
     */
    private $subcategoriaProducto;

    /**
     * @ORM\OneToOne(targetEntity=FilesOrdenDet::class, inversedBy="ordenDet", cascade={"persist", "remove"})
     */
    private $files;

    /**
     * @ORM\ManyToOne(targetEntity=Producto::class, inversedBy="ordenDets")
     */
    private $producto;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $noTicket;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $itemOrder;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $lineItemId;

    /**
     * @ORM\OneToMany(targetEntity=OrdenDetCaracteristica::class, mappedBy="ordenDet")
     */
    private $ordenDetCaracteristicas;

    /**
     * @ORM\ManyToOne(targetEntity=PaqueteEnvio::class, inversedBy="ordenDets")
     */
    private $paqueteEnvio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $imgPathRef;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
        $this->setFiles(new FilesOrdenDet());
        $this->ordenDetCaracteristicas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductoId(): ?string
    {
        return $this->productoId;
    }

    public function setProductoId(string $productoId): self
    {
        $this->productoId = $productoId;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getNombreProducto(): ?string
    {
        return $this->nombreProducto;
    }

    public function setNombreProducto(?string $nombreProducto): self
    {
        $this->nombreProducto = $nombreProducto;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getCantidad(): ?float
    {
        return $this->cantidad;
    }

    public function setCantidad(float $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getAncho(): ?float
    {
        return $this->ancho;
    }

    public function setAncho(float $ancho): self
    {
        $this->ancho = $ancho;

        return $this;
    }

    public function getAlto(): ?float
    {
        return $this->alto;
    }

    public function setAlto(float $alto): self
    {
        $this->alto = $alto;

        return $this;
    }

    public function getPrecioBase(): ?float
    {
        return $this->precioBase;
    }

    public function setPrecioBase(float $precioBase): self
    {
        $this->precioBase = $precioBase;

        return $this;
    }

    public function getPrecioCorte(): ?float
    {
        return $this->precioCorte;
    }

    public function setPrecioCorte(float $precioCorte): self
    {
        $this->precioCorte = $precioCorte;

        return $this;
    }

    public function getPrecioLaminado(): ?float
    {
        return $this->precioLaminado;
    }

    public function setPrecioLaminado(float $precioLaminado): self
    {
        $this->precioLaminado = $precioLaminado;

        return $this;
    }

    public function getTipoCorte(): ?string
    {
        return $this->tipoCorte;
    }

    public function setTipoCorte(?string $tipoCorte): self
    {
        $this->tipoCorte = $tipoCorte;

        return $this;
    }

    public function getLaminado(): ?string
    {
        return $this->laminado;
    }

    public function setLaminado(string $laminado): self
    {
        $this->laminado = $laminado;

        return $this;
    }

    public function getElectrocorte(): ?string
    {
        return $this->electrocorte;
    }

    public function setElectrocorte(string $electrocorte): self
    {
        $this->electrocorte = $electrocorte;

        return $this;
    }

    public function getOjales(): ?string
    {
        return $this->ojales;
    }

    public function setOjales(string $ojales): self
    {
        $this->ojales = $ojales;

        return $this;
    }

    public function getEspecificacionOjales(): ?String
    {
        return $this->especificacionOjales;
    }

    public function setEspecificacionOjales(String $especificacionOjales): self
    {
        $this->especificacionOjales = $especificacionOjales;

        return $this;
    }

    public function getPrecioOjales(): ?float
    {
        return $this->precioOjales;
    }

    public function setPrecioOjales(float $precioOjales): self
    {
        $this->precioOjales = $precioOjales;

        return $this;
    }

    public function getCantidadOjales(): ?int
    {
        return $this->cantidadOjales;
    }

    public function setCantidadOjales(int $cantidadOjales): self
    {
        $this->cantidadOjales = $cantidadOjales;

        return $this;
    }

    public function getTipoLaminado(): ?string
    {
        return $this->tipoLaminado;
    }

    public function setTipoLaminado(?string $tipoLaminado): self
    {
        $this->tipoLaminado = $tipoLaminado;

        return $this;
    }

    public function getCorteRigido(): ?string
    {
        return $this->corteRigido;
    }

    public function setCorteRigido(string $corteRigido): self
    {
        $this->corteRigido = $corteRigido;

        return $this;
    }

    public function getPrecioCorteRigido(): ?float
    {
        return $this->precioCorteRigido;
    }

    public function setPrecioCorteRigido(float $precioCorteRigido): self
    {
        $this->precioCorteRigido = $precioCorteRigido;

        return $this;
    }

    public function getTipoImpresion(): ?string
    {
        return $this->tipoImpresion;
    }

    public function setTipoImpresion(?string $tipoImpresion): self
    {
        $this->tipoImpresion = $tipoImpresion;

        return $this;
    }

    public function getRutaImpresion(): ?string
    {
        return $this->rutaImpresion;
    }

    public function setRutaImpresion(?string $rutaImpresion): self
    {
        $this->rutaImpresion = $rutaImpresion;

        return $this;
    }

    public function getRutaElectrocorte(): ?string
    {
        return $this->rutaElectrocorte;
    }

    public function setRutaElectrocorte(?string $rutaElectrocorte): self
    {
        $this->rutaElectrocorte = $rutaElectrocorte;

        return $this;
    }

    public function getParentIdWdImpresion(): ?string
    {
        return $this->parentIdWdImpresion;
    }

    public function setParentIdWdImpresion(?string $parentIdWdImpresion): self
    {
        $this->parentIdWdImpresion = $parentIdWdImpresion;

        return $this;
    }

    public function getParentIdWdElectro(): ?string
    {
        return $this->parentIdWdElectro;
    }

    public function setParentIdWdElectro(?string $parentIdWdElectro): self
    {
        $this->parentIdWdElectro = $parentIdWdElectro;

        return $this;
    }

    public function getFileIdWdElectro(): ?string
    {
        return $this->fileIdWdElectro;
    }

    public function setFileIdWdElectro(?string $fileIdWdElectro): self
    {
        $this->fileIdWdElectro = $fileIdWdElectro;

        return $this;
    }

    public function getFileIdWdImpresion(): ?string
    {
        return $this->fileIdWdImpresion;
    }

    public function setFileIdWdImpresion(?string $fileIdWdImpresion): self
    {
        $this->fileIdWdImpresion = $fileIdWdImpresion;

        return $this;
    }

    public function getSubidaImpresion(): ?string
    {
        return $this->subidaImpresion;
    }

    public function setSubidaImpresion(string $subidaImpresion): self
    {
        $this->subidaImpresion = $subidaImpresion;

        return $this;
    }

    public function getSubidaElectro(): ?string
    {
        return $this->subidaElectro;
    }

    public function setSubidaElectro(string $subidaElectro): self
    {
        $this->subidaElectro = $subidaElectro;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getOrdenCab(): ?OrdenCab
    {
        return $this->ordenCab;
    }

    public function setOrdenCab(?OrdenCab $ordenCab): self
    {
        $this->ordenCab = $ordenCab;

        return $this;
    }

    public function getSubcategoriaProducto(): ?SubcategoriaProducto
    {
        return $this->subcategoriaProducto;
    }

    public function setSubcategoriaProducto(?SubcategoriaProducto $subcategoriaProducto): self
    {
        $this->subcategoriaProducto = $subcategoriaProducto;

        return $this;
    }

    public function getArrayData()
    {
        //$categoria    = $this->getSubcategoriaProducto()->getCategoriaProducto()->getTipoProducto()->getCategoria();
        $categoria = $this->getProducto()->getCategoriaProducto()->getCategoria();
        //$subcategoria = $this->getSubcategoriaProducto()->getSubcategoria();

        $cantidad = $this->getCantidad();
        if ($categoria == 'MIMAKI') {
            $cantidad .= ' m2';
        }
        $fmt = numfmt_create('en_US', NumberFormatter::CURRENCY);
        $currency = "USD";
        $preciounit = $this->getPrecio() / $this->getCantidad();
        $fmtPrecioUnit = numfmt_format_currency($fmt, $preciounit, $currency);

        $fmtSubtotal = numfmt_format_currency($fmt, $this->getPrecio(), $currency);
        $fmtBase = ($this->getPrecioBase() == 0) ? '' : numfmt_format_currency($fmt, $this->getPrecioBase(), $currency);
        $fmtCorte = ($this->getPrecioCorte() == 0) ? '' : numfmt_format_currency($fmt, $this->getPrecioCorte(), $currency);
        $fmtLaminado = ($this->getPrecioLaminado() == 0) ? '' : numfmt_format_currency($fmt, $this->getPrecioLaminado(), $currency);
        $fmtOjales = ($this->getPrecioOjales() == 0) ? '' : numfmt_format_currency($fmt, $this->getPrecioOjales(), $currency);
        $fmtRigido = ($this->getPrecioCorteRigido() == 0) ? '' : numfmt_format_currency($fmt, $this->getPrecioCorteRigido(), $currency);

        $nombreProducto = $this->getTitleProducto();
        $nombreSubProducto = $this->getProducto()->getNombreProducto();

        return [
            'id'             => $this->getId(),
            'nombreproducto' => $nombreProducto,
            'nombresubproducto' => $nombreSubProducto,
            'descripcion'    => $this->getDescripcion(),
            'categoria'      => $categoria,
            //'subcategoria'   => $subcategoria,
            'cantidad'       => $cantidad,
            'subtotal'       => $fmtSubtotal,
            'preciounit'     => $fmtPrecioUnit,
            'preciobase'     => $fmtBase,
            'preciocorte'    => $fmtCorte,
            'preciolaminado' => $fmtLaminado,
            'preciorigido'   => $fmtRigido,
            'precioojales'   => $fmtOjales
        ];
    }

    public function getArrayItem()
    {

        return [
            'id'               => $this->getId(),
            'sku'              => $this->getSku(),
            'productId'        => $this->getProductoId(),
            // 'subcategoriaId'   => $this->getSubcategoriaProducto()->getId(),
            // 'subcategoriaName' => $this->getSubcategoriaProducto()->getSubcategoria(),
            'categoriaId'      => $this->getProducto()->getProducto()->getId(),
            'categoriaName'    => $this->getProducto()->getMaquina(),
            'cantidad'         => $this->getCantidad(),
            'ancho'            => $this->getAncho(),
            'alto'             => $this->getAlto(),
            'electrocorte'     => ($this->getElectrocorte() == 'S') ? true : false,
            'laminado'         => ($this->getLaminado() == 'S') ? true : false,
            'ojales'           => ($this->getOjales() == 'S') ? true : false,
            'tipocorte'        => $this->getTipoCorte(),
            'tipolaminado'     => $this->getTipoLaminado(),
            'tipoimpresion'    => $this->getTipoImpresion(),
            'cantidadojales'   => $this->getCantidadOjales(),
            'especifiojales'   => $this->getEspecificacionOjales(),
            'corteRecto'       => ($this->getCorteRigido() == 'S') ? true : false,
            'tieneFileImpr'    => (!empty($this->getRutaImpresion())) ? true : false,
            'tieneFileElec'    => (!empty($this->getRutaElectrocorte())) ? true : false,
            'subidaImpresion'  => ($this->getSubidaImpresion() == 'S') ? true : false,
            'subidaElectro'    => ($this->getSubidaElectro() == 'S') ? true : false,
            'observacion'      => $this->getObservacion(),
            'alias'            => $this->getAlias(),
        ];
    }

    public function getFiles(): ?FilesOrdenDet
    {
        return $this->files;
    }

    public function setFiles(?FilesOrdenDet $files): self
    {
        $this->files = $files;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    public function getNoTicket(): ?string
    {
        return $this->noTicket;
    }

    public function setNoTicket(?string $noTicket): self
    {
        $this->noTicket = $noTicket;

        return $this;
    }

    public function getItemOrder(): ?int
    {
        return $this->itemOrder;
    }

    public function setItemOrder(?int $itemOrder): self
    {
        $this->itemOrder = $itemOrder;

        return $this;
    }

    public function getLineItemId(): ?string
    {
        return $this->lineItemId;
    }

    public function setLineItemId(?string $lineItemId): self
    {
        $this->lineItemId = $lineItemId;

        return $this;
    }

    public function getPrecioBaseReal(): ?float
    {
        return $this->precioBaseReal;
    }

    public function setPrecioBaseReal(?float $precioBaseReal): self
    {
        $this->precioBaseReal = $precioBaseReal;

        return $this;
    }

    /**
     * @return Collection|OrdenDetCaracteristica[]
     */
    public function getOrdenDetCaracteristicas(): Collection
    {
        return $this->ordenDetCaracteristicas;
    }

    public function addOrdenDetCaracteristica(OrdenDetCaracteristica $ordenDetCaracteristica): self
    {
        if (!$this->ordenDetCaracteristicas->contains($ordenDetCaracteristica)) {
            $this->ordenDetCaracteristicas[] = $ordenDetCaracteristica;
            $ordenDetCaracteristica->setOrdenDet($this);
        }

        return $this;
    }

    public function removeOrdenDetCaracteristica(OrdenDetCaracteristica $ordenDetCaracteristica): self
    {
        if ($this->ordenDetCaracteristicas->contains($ordenDetCaracteristica)) {
            $this->ordenDetCaracteristicas->removeElement($ordenDetCaracteristica);
            // set the owning side to null (unless already changed)
            if ($ordenDetCaracteristica->getOrdenDet() === $this) {
                $ordenDetCaracteristica->setOrdenDet(null);
            }
        }

        return $this;
    }

    public function getPaqueteEnvio(): ?PaqueteEnvio
    {
        return $this->paqueteEnvio;
    }

    public function setPaqueteEnvio(?PaqueteEnvio $paqueteEnvio): self
    {
        $this->paqueteEnvio = $paqueteEnvio;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getTitleProducto(){
        if (!empty($this->getAlias())) {
            return $this->getAlias();
        }elseif (!empty($this->getProducto()->getProducto())) {
            return $this->getProducto()->getProducto()->getNombreProducto();
        }
        return $this->getProducto()->getNombreProducto();
    }

    public function getImgPathRef(): ?string
    {
        return $this->imgPathRef;
    }

    public function setImgPathRef(?string $imgPathRef): self
    {
        $this->imgPathRef = $imgPathRef;

        return $this;
    }

}
