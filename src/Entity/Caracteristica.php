<?php

namespace App\Entity;

use App\Repository\CaracteristicaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="caracteristica")
 * @ORM\Entity(repositoryClass=CaracteristicaRepository::class)
 */
class Caracteristica
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $val1;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $val2;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $val3;

    /**
     * @ORM\Column(type="string", length=3, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @ORM\Column(name="fecha_creacion", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\OneToMany(targetEntity=ProductoCaracteristica::class, mappedBy="caracteristica")
     */
    private $productoCaracteristicas;

    /**
     * @ORM\OneToMany(targetEntity=OrdenDetCaracteristica::class, mappedBy="caracteristica")
     */
    private $ordenDetCaracteristicas;

    public function __construct()
    {
        $this->productoCaracteristicas = new ArrayCollection();
        $this->ordenDetCaracteristicas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    /**
     * @return Collection|ProductoCaracteristica[]
     */
    public function getProductoCaracteristicas(): Collection
    {
        return $this->productoCaracteristicas;
    }

    public function addProductoCaracteristica(ProductoCaracteristica $productoCaracteristica): self
    {
        if (!$this->productoCaracteristicas->contains($productoCaracteristica)) {
            $this->productoCaracteristicas[] = $productoCaracteristica;
            $productoCaracteristica->setCaracteristica($this);
        }

        return $this;
    }

    public function removeProductoCaracteristica(ProductoCaracteristica $productoCaracteristica): self
    {
        if ($this->productoCaracteristicas->contains($productoCaracteristica)) {
            $this->productoCaracteristicas->removeElement($productoCaracteristica);
            // set the owning side to null (unless already changed)
            if ($productoCaracteristica->getCaracteristica() === $this) {
                $productoCaracteristica->setCaracteristica(null);
            }
        }

        return $this;
    }

    public function getVal1(): ?string
    {
        return $this->val1;
    }

    public function setVal1(?string $val1): self
    {
        $this->val1 = $val1;

        return $this;
    }

    public function getVal2(): ?string
    {
        return $this->val2;
    }

    public function setVal2(?string $val2): self
    {
        $this->val2 = $val2;

        return $this;
    }

    public function getVal3(): ?string
    {
        return $this->val3;
    }

    public function setVal3(?string $val3): self
    {
        $this->val3 = $val3;

        return $this;
    }

    /**
     * @return Collection|OrdenDetCaracteristica[]
     */
    public function getOrdenDetCaracteristicas(): Collection
    {
        return $this->ordenDetCaracteristicas;
    }

    public function addOrdenDetCaracteristica(OrdenDetCaracteristica $ordenDetCaracteristica): self
    {
        if (!$this->ordenDetCaracteristicas->contains($ordenDetCaracteristica)) {
            $this->ordenDetCaracteristicas[] = $ordenDetCaracteristica;
            $ordenDetCaracteristica->setCaracteristica($this);
        }

        return $this;
    }

    public function removeOrdenDetCaracteristica(OrdenDetCaracteristica $ordenDetCaracteristica): self
    {
        if ($this->ordenDetCaracteristicas->contains($ordenDetCaracteristica)) {
            $this->ordenDetCaracteristicas->removeElement($ordenDetCaracteristica);
            // set the owning side to null (unless already changed)
            if ($ordenDetCaracteristica->getCaracteristica() === $this) {
                $ordenDetCaracteristica->setCaracteristica(null);
            }
        }

        return $this;
    }
}
