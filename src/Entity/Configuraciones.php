<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Configuraciones
 *
 * @ORM\Table(name="configuraciones")
 * @ORM\Entity(repositoryClass="App\Repository\ConfiguracionesRepository")
 */
class Configuraciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=true)
     */
    private $codigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mapping_name", type="string", length=50, nullable=true)
     */
    private $mappingName;

    /**
     * @var float|null
     *
     * @ORM\Column(name="valorDouble", type="float", precision=10, scale=0, nullable=true)
     */
    private $valordouble;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valorString", type="string", length=50, nullable=true)
     */
    private $valorstring;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(?string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getMappingName(): ?string
    {
        return $this->mappingName;
    }

    public function setMappingName(?string $mappingName): self
    {
        $this->codigo = $mappingName;

        return $this;
    }

    public function getValordouble(): ?float
    {
        return $this->valordouble;
    }

    public function setValordouble(?float $valordouble): self
    {
        $this->valordouble = $valordouble;

        return $this;
    }

    public function getValorstring(): ?string
    {
        return $this->valorstring;
    }

    public function setValorstring(?string $valorstring): self
    {
        $this->valorstring = $valorstring;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }
}
