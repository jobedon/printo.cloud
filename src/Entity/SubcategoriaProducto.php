<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubcategoriaProducto
 *
 * @ORM\Table(name="subcategoria_producto", indexes={@ORM\Index(name="FK_SUB_CATPROD", columns={"categoria_producto_id"})})
 * @ORM\Entity
 */
class SubcategoriaProducto
{
    /**
     * @var int
     * 
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subcategoria", type="string", length=100, nullable=false)
     */
    private $subcategoria;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @var \CategoriaProducto
     *
     * @ORM\ManyToOne(targetEntity="CategoriaProducto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria_producto_id", referencedColumnName="id")
     * })
     */
    private $categoriaProducto;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubcategoria(): ?string
    {
        return $this->subcategoria;
    }

    public function setSubcategoria(string $subcategoria): self
    {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getCategoriaProducto(): ?CategoriaProducto
    {
        return $this->categoriaProducto;
    }

    public function setCategoriaProducto(?CategoriaProducto $categoriaProducto): self
    {
        $this->categoriaProducto = $categoriaProducto;

        return $this; 
    }


}
