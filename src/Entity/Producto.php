<?php

namespace App\Entity;

use App\Repository\ProductoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductoRepository::class)
 */
class Producto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $nombreProducto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $sku;

    /**
     * @ORM\Column(type="string", length=3, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @ORM\Column(name="fecha_creacion", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\ManyToOne(targetEntity=Producto::class, inversedBy="productos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     * })
     */
    private $producto;

    /**
     * @ORM\OneToMany(targetEntity=Producto::class, mappedBy="producto")
     */
    private $productos;

    /**
     * @ORM\ManyToOne(targetEntity=CategoriaProducto::class, inversedBy="productos")
     */
    private $categoriaProducto;

    /**
     * @ORM\OneToMany(targetEntity=ProductoCaracteristica::class, mappedBy="producto")
     */
    private $productoCaracteristicas;

    /**
     * @ORM\OneToOne(targetEntity=TarifarioMimaki::class, mappedBy="producto", cascade={"persist", "remove"})
     */
    private $tarifarioMimaki;

    /**
     * @ORM\OneToMany(targetEntity=ProductoFavorito::class, mappedBy="producto")
     */
    private $productoFavoritos;

    /**
     * @ORM\OneToMany(targetEntity=OrdenDet::class, mappedBy="producto")
     */
    private $ordenDets;

    /**
     * @ORM\ManyToOne(targetEntity=Catalogo::class)
     */
    private $catalogo;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $imgPath;

    public function __construct()
    {
        $this->productos = new ArrayCollection();
        $this->productoCaracteristicas = new ArrayCollection();
        $this->productoFavoritos = new ArrayCollection();
        $this->ordenDets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreProducto(): ?string
    {
        return $this->nombreProducto;
    }

    public function setNombreProducto(?string $nombreProducto): self
    {
        $this->nombreProducto = $nombreProducto;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getProducto(): ?self
    {
        return $this->producto;
    }

    public function setProducto(?self $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(self $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setProducto($this);
        }

        return $this;
    }

    public function removeProducto(self $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            // set the owning side to null (unless already changed)
            if ($producto->getProducto() === $this) {
                $producto->setProducto(null);
            }
        }

        return $this;
    }

    public function getCategoriaProducto(): ?CategoriaProducto
    {
        return $this->categoriaProducto;
    }

    public function setCategoriaProducto(?CategoriaProducto $categoriaProducto): self
    {
        $this->categoriaProducto = $categoriaProducto;

        return $this;
    }

    /**
     * @return Collection|ProductoCaracteristica[]
     */
    public function getProductoCaracteristicas(): Collection
    {
        return $this->productoCaracteristicas;
    }

    public function addProductoCaracteristica(ProductoCaracteristica $productoCaracteristica): self
    {
        if (!$this->productoCaracteristicas->contains($productoCaracteristica)) {
            $this->productoCaracteristicas[] = $productoCaracteristica;
            $productoCaracteristica->setProducto($this);
        }

        return $this;
    }

    public function removeProductoCaracteristica(ProductoCaracteristica $productoCaracteristica): self
    {
        if ($this->productoCaracteristicas->contains($productoCaracteristica)) {
            $this->productoCaracteristicas->removeElement($productoCaracteristica);
            // set the owning side to null (unless already changed)
            if ($productoCaracteristica->getProducto() === $this) {
                $productoCaracteristica->setProducto(null);
            }
        }

        return $this;
    }

    public function getTarifarioMimaki(): ?TarifarioMimaki
    {
        return $this->tarifarioMimaki;
    }

    public function setTarifarioMimaki(?TarifarioMimaki $tarifarioMimaki): self
    {
        $this->tarifarioMimaki = $tarifarioMimaki;

        // set (or unset) the owning side of the relation if necessary
        $newProducto = null === $tarifarioMimaki ? null : $this;
        if ($tarifarioMimaki->getProducto() !== $newProducto) {
            $tarifarioMimaki->setProducto($newProducto);
        }

        return $this;
    }

    /**
     * @return Collection|ProductoFavorito[]
     */
    public function getProductoFavoritos(): Collection
    {
        return $this->productoFavoritos;
    }

    public function addProductoFavorito(ProductoFavorito $productoFavorito): self
    {
        if (!$this->productoFavoritos->contains($productoFavorito)) {
            $this->productoFavoritos[] = $productoFavorito;
            $productoFavorito->setProducto($this);
        }

        return $this;
    }

    public function removeProductoFavorito(ProductoFavorito $productoFavorito): self
    {
        if ($this->productoFavoritos->contains($productoFavorito)) {
            $this->productoFavoritos->removeElement($productoFavorito);
            // set the owning side to null (unless already changed)
            if ($productoFavorito->getProducto() === $this) {
                $productoFavorito->setProducto(null);
            }
        }

        return $this;
    }

    function getMaquina()
    {
        return ($this->getProducto() != null) ? $this->getProducto()->getDescripcion() : null;
    }

    function getProductoFinal()
    {
        if (empty($this->getProducto())) {
            return $this->getNombreProducto();
        } else {
            return $this->getProducto()->getNombreProducto();
        }
        return "";
    }

    /**
     * @return Collection|OrdenDet[]
     */
    public function getOrdenDets(): Collection
    {
        return $this->ordenDets;
    }

    public function addOrdenDet(OrdenDet $ordenDet): self
    {
        if (!$this->ordenDets->contains($ordenDet)) {
            $this->ordenDets[] = $ordenDet;
            $ordenDet->setProducto($this);
        }

        return $this;
    }

    public function removeOrdenDet(OrdenDet $ordenDet): self
    {
        if ($this->ordenDets->contains($ordenDet)) {
            $this->ordenDets->removeElement($ordenDet);
            // set the owning side to null (unless already changed)
            if ($ordenDet->getProducto() === $this) {
                $ordenDet->setProducto(null);
            }
        }

        return $this;
    }

    public function getCatalogo(): ?Catalogo
    {
        return $this->catalogo;
    }

    public function setCatalogo(?Catalogo $catalogo): self
    {
        $this->catalogo = $catalogo;

        return $this;
    }

    public function getImgPath(): ?string
    {
        return $this->imgPath;
    }

    public function setImgPath(?string $imgPath): self
    {
        $this->imgPath = $imgPath;

        return $this;
    }
}
