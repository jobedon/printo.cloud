<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ZohoTokens
 *
 * @ORM\Table(name="zoho_tokens", indexes={@ORM\Index(name="FK_ZOTO_TIZO", columns={"tipo_zoho_id"})})
 * @ORM\Entity
 */
class ZohoTokens
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="access_token", type="string", length=100, nullable=false)
     */
    private $accessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_modificacion", type="datetime", nullable=true)
     */
    private $fechaModificacion;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    /**
     * @var \TipoZoho
     *
     * @ORM\ManyToOne(targetEntity="TipoZoho")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_zoho_id", referencedColumnName="id")
     * })
     */
    private $tipoZoho;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaModificacion(): ?\DateTimeInterface
    {
        return $this->fechaModificacion;
    }

    public function setFechaModificacion(?\DateTimeInterface $fechaModificacion): self
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    public function getTipoZoho(): ?TipoZoho
    {
        return $this->tipoZoho;
    }

    public function setTipoZoho(?TipoZoho $tipoZoho): self
    {
        $this->tipoZoho = $tipoZoho;

        return $this;
    }


}
