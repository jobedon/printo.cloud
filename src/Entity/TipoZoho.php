<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoZoho
 *
 * @ORM\Table(name="tipo_zoho")
 * @ORM\Entity
 */
class TipoZoho
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="zoho", type="string", length=50, nullable=false)
     */
    private $zoho;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getZoho(): ?string
    {
        return $this->zoho;
    }

    public function setZoho(string $zoho): self
    {
        $this->zoho = $zoho;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }


}
