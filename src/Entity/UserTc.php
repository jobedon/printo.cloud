<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserTc
 *
 * @ORM\Table(name="user_tc", indexes={@ORM\Index(name="FK_USER_TC", columns={"user_id"})})
 * @ORM\Entity
 */
class UserTc
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="registrationId", type="string", length=50, nullable=false)
     */
    private $registrationid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=true, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @var \UserPi
     *
     * @ORM\ManyToOne(targetEntity="UserPi")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRegistrationid(): ?string
    {
        return $this->registrationid;
    }

    public function setRegistrationid(string $registrationid): self
    {
        $this->registrationid = $registrationid;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(?\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getUser(): ?UserPi
    {
        return $this->user;
    }

    public function setUser(?UserPi $user): self
    {
        $this->user = $user;

        return $this;
    }


}
