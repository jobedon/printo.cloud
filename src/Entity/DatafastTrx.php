<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatafastTrx
 *
 * @ORM\Table(name="datafast_trx", indexes={
 *      @ORM\Index(name="FK_ORDEN_TRX", columns={"orden_cab_id"}),
 *      @ORM\Index(name="CHECKOUTID_TRX", columns={"checkout_id"})
 * })
 * @ORM\Entity
 */
class DatafastTrx
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=100, nullable=true)
     */
    private $transactionId;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_transaccion", type="string", length=15, nullable=false, options={"default"="Pendiente"})
     */
    private $estadoTransaccion = 'Pendiente';

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var string|null
     *
     * @ORM\Column(name="request", type="blob", length=65535, nullable=true)
     */
    private $request;

    /**
     * @var string|null
     *
     * @ORM\Column(name="response", type="blob", length=65535, nullable=true)
     */
    private $response;

    /**
     * @var string|null
     *
     * @ORM\Column(name="result_code", type="string", length=50, nullable=true)
     */
    private $resultCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @var OrdenCab|null
     *
     * @ORM\ManyToOne(targetEntity="OrdenCab")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="orden_cab_id", referencedColumnName="id")
     * })
     */
    private $ordenCab;

    /**
     * @var string
     *
     * @ORM\Column(name="error", type="string", length=300, nullable=true)
     */
    private $error;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=100, nullable=true)
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $checkoutId;

    /**
     * @ORM\Column(type="string", length=25, nullable=true, options={"default"="COMPRA ORDEN"})
     */
    private $nombreTransaccion = "COMPRA ORDEN";

    /**
     * @ORM\ManyToOne(targetEntity=UserPi::class)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $observacion;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function setTransactionId(string $transactionId): self
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    public function getEstadoTransaccion(): ?string
    {
        return $this->estadoTransaccion;
    }

    public function setEstadoTransaccion(string $estadoTransaccion): self
    {
        $this->estadoTransaccion = $estadoTransaccion;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setRequest($request): self
    {
        $this->request = $request;

        return $this;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse($response): self
    {
        $this->response = $response;

        return $this;
    }

    public function getResultCode(): ?string
    {
        return $this->resultCode;
    }

    public function setResultCode(?string $resultCode): self
    {
        $this->resultCode = $resultCode;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(): self
    {
        $this->fechaActualizacion = new \DateTime("now");

        return $this;
    }

    public function getOrdenCab(): ?OrdenCab
    {
        return $this->ordenCab;
    }

    public function setOrdenCab(?OrdenCab $ordenCab): self
    {
        $this->ordenCab = $ordenCab;

        return $this;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(?string $error): self
    {
        $this->error = $error;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getCheckoutId(): ?string
    {
        return $this->checkoutId;
    }

    public function setCheckoutId(?string $checkoutId): self
    {
        $this->checkoutId = $checkoutId;

        return $this;
    }

    public function getNombreTransaccion(): ?string
    {
        return $this->nombreTransaccion;
    }

    public function setNombreTransaccion(?string $nombreTransaccion): self
    {
        $this->nombreTransaccion = $nombreTransaccion;

        return $this;
    }

    public function getUser(): ?UserPi
    {
        return $this->user;
    }

    public function setUser(?UserPi $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

}
