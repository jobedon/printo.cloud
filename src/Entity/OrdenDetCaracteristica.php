<?php

namespace App\Entity;

use App\Repository\OrdenDetCaracteristicaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrdenDetCaracteristicaRepository::class)
 */
class OrdenDetCaracteristica
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=OrdenDet::class, inversedBy="ordenDetCaracteristicas")
     */
    private $ordenDet;

    /**
     * @ORM\ManyToOne(targetEntity=Caracteristica::class, inversedBy="ordenDetCaracteristicas")
     */
    private $caracteristica;

    /**
     * @ORM\Column(type="string", length=3, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $valor;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $val1;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $val2;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $val3;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdenDet(): ?OrdenDet
    {
        return $this->ordenDet;
    }

    public function setOrdenDet(?OrdenDet $ordenDet): self
    {
        $this->ordenDet = $ordenDet;

        return $this;
    }

    public function getCaracteristica(): ?Caracteristica
    {
        return $this->caracteristica;
    }

    public function setCaracteristica(?Caracteristica $caracteristica): self
    {
        $this->caracteristica = $caracteristica;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getValor(): ?string
    {
        return $this->valor;
    }

    public function setValor(?string $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getVal1(): ?string
    {
        return $this->val1;
    }

    public function setVal1(?string $val1): self
    {
        $this->val1 = $val1;

        return $this;
    }

    public function getVal2(): ?string
    {
        return $this->val2;
    }

    public function setVal2(?string $val2): self
    {
        $this->val2 = $val2;

        return $this;
    }

    public function getVal3(): ?string
    {
        return $this->val3;
    }

    public function setVal3(?string $val3): self
    {
        $this->val3 = $val3;

        return $this;
    }
}
