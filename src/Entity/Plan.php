<?php

namespace App\Entity;

use App\Repository\PlanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlanRepository::class)
 */
class Plan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $zohoPlanCode;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $printoPlanCode;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $priority;

    /**
     * @ORM\OneToMany(targetEntity=UserPlanHistory::class, mappedBy="planInicial")
     */
    private $userPlanInicialHistories;

    /**
     * @ORM\OneToMany(targetEntity=UserPlanHistory::class, mappedBy="planFinal")
     */
    private $userPlanFinalHistories;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime("now"));
        $this->userPlanInicialHistories = new ArrayCollection();
        $this->userPlanFinalHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getZohoPlanCode(): ?string
    {
        return $this->zohoPlanCode;
    }

    public function setZohoPlanCode(?string $zohoPlanCode): self
    {
        $this->zohoPlanCode = $zohoPlanCode;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getPrintoPlanCode(): ?string
    {
        return $this->printoPlanCode;
    }

    public function setPrintoPlanCode(?string $printoPlanCode): self
    {
        $this->printoPlanCode = $printoPlanCode;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection|UserPlanHistory[]
     */
    public function getUserPlanInicialHistories(): Collection
    {
        return $this->userPlanInicialHistories;
    }

    public function addUserPlanInicialHistory(UserPlanHistory $userPlanInicialHistory): self
    {
        if (!$this->userPlanInicialHistories->contains($userPlanInicialHistory)) {
            $this->userPlanInicialHistories[] = $userPlanInicialHistory;
            $userPlanInicialHistory->setPlanInicial($this);
        }

        return $this;
    }

    public function removeUserPlanInicialHistory(UserPlanHistory $userPlanInicialHistory): self
    {
        if ($this->userPlanInicialHistories->contains($userPlanInicialHistory)) {
            $this->userPlanInicialHistories->removeElement($userPlanInicialHistory);
            // set the owning side to null (unless already changed)
            if ($userPlanInicialHistory->getPlanInicial() === $this) {
                $userPlanInicialHistory->setPlanInicial(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPlanHistory[]
     */
    public function getUserPlanFinalHistories(): Collection
    {
        return $this->userPlanFinalHistories;
    }

    public function addUserPlanFinalHistory(UserPlanHistory $userPlanFinalHistory): self
    {
        if (!$this->userPlanFinalHistories->contains($userPlanFinalHistory)) {
            $this->userPlanFinalHistories[] = $userPlanFinalHistory;
            $userPlanFinalHistory->setPlanFinal($this);
        }

        return $this;
    }

    public function removeUserPlanFinalHistory(UserPlanHistory $userPlanFinalHistory): self
    {
        if ($this->userPlanFinalHistories->contains($userPlanFinalHistory)) {
            $this->userPlanFinalHistories->removeElement($userPlanFinalHistory);
            // set the owning side to null (unless already changed)
            if ($userPlanFinalHistory->getPlanFinal() === $this) {
                $userPlanFinalHistory->setPlanFinal(null);
            }
        }

        return $this;
    }
}
