<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TarifarioMimaki
 *
 * @ORM\Table(name="tarifario_mimaki", indexes={@ORM\Index(name="FK_SUBCAT_TARIFMIMAKI", columns={"subcategoria_producto_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TarifarioMimakiRepository")
 */
class TarifarioMimaki
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion_producto", type="string", length=150, nullable=true)
     */
    private $descripcionProducto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="producto_id_zoho", type="string", length=50, nullable=true)
     */
    private $productoIdZoho;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=true)
     */
    private $precio = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="precio_plus", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioPlus = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="precio_recto_metro2", type="float", precision=10, scale=0, nullable=true)
     */
    private $precioRectoMetro2 = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=50, nullable=true)
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(name="anchos_disponibles", type="string", length=300, nullable=true)
     */
    private $anchosDisponibles;

    /**
     * @var string
     *
     * @ORM\Column(name="altos_disponibles", type="string", length=300, nullable=true)
     */
    private $altosDisponibles;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @var \SubcategoriaProducto
     *
     * @ORM\ManyToOne(targetEntity="SubcategoriaProducto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subcategoria_producto_id", referencedColumnName="id")
     * })
     */
    private $subcategoriaProducto;

    /**
     * @ORM\OneToOne(targetEntity=Producto::class, inversedBy="tarifarioMimaki", cascade={"persist", "remove"})
     */
    private $producto;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precioClienteFinal;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precioPrintoFree;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precioPrintoProfesional;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcionProducto(): ?string
    {
        return $this->descripcionProducto;
    }

    public function setDescripcionProducto(?string $descripcionProducto): self
    {
        $this->descripcionProducto = $descripcionProducto;

        return $this;
    }

    public function getProductoIdZoho(): ?string
    {
        return $this->productoIdZoho;
    }

    public function setProductoIdZoho(?string $productoIdZoho): self
    {
        $this->productoIdZoho = $productoIdZoho;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getPrecioPlus(): ?float
    {
        return $this->precioPlus;
    }

    public function setPrecioPlus(float $precioPlus): self
    {
        $this->precioPlus = $precioPlus;

        return $this;
    }

    public function getPrecioRectoMetro2(): ?float
    {
        return $this->precioRectoMetro2;
    }

    public function setPrecioRectoMetro2(float $precioRectoMetro2): self
    {
        $this->precioRectoMetro2 = $precioRectoMetro2;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getAnchosDisponibles(): ?array
    {
        return explode(",",$this->anchosDisponibles);
    }

    public function setAnchosDisponibles(string $anchosDisponibles): self
    {
        $this->anchosDisponibles = $anchosDisponibles;

        return $this;
    }

    public function getAltosDisponibles(): ?string
    {
        return $this->altosDisponibles;
    }

    public function setAltosDisponibles(string $altosDisponibles): self
    {
        $this->altosDisponibles = $altosDisponibles;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getSubcategoriaProducto(): ?SubcategoriaProducto
    {
        return $this->subcategoriaProducto;
    }

    public function setSubcategoriaProducto(?SubcategoriaProducto $subcategoriaProducto): self
    {
        $this->subcategoriaProducto = $subcategoriaProducto;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    public function getPrecioClienteFinal(): ?float
    {
        return $this->precioClienteFinal;
    }

    public function setPrecioClienteFinal(?float $precioClienteFinal): self
    {
        $this->precioClienteFinal = $precioClienteFinal;

        return $this;
    }

    public function getPrecioPrintoFree(): ?float
    {
        return $this->precioPrintoFree;
    }

    public function setPrecioPrintoFree(?float $precioPrintoFree): self
    {
        $this->precioPrintoFree = $precioPrintoFree;

        return $this;
    }

    public function getPrecioPrintoProfesional(): ?float
    {
        return $this->precioPrintoProfesional;
    }

    public function setPrecioPrintoProfesional(?float $precioPrintoProfesional): self
    {
        $this->precioPrintoProfesional = $precioPrintoProfesional;

        return $this;
    }


}
