<?php

namespace App\Entity;

use App\Repository\PaqueteEnvioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaqueteEnvioRepository::class)
 */
class PaqueteEnvio
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=UserPi::class)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $observacion;

    /**
     * @ORM\ManyToOne(targetEntity=DatafastTrx::class)
     */
    private $datafastTrx;

    /**
     * @ORM\Column(type="string", length=3, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\OneToMany(targetEntity=OrdenDet::class, mappedBy="paqueteEnvio")
     */
    private $ordenDets;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
        $this->ordenDets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?UserPi
    {
        return $this->user;
    }

    public function setUser(?UserPi $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getDatafastTrx(): ?DatafastTrx
    {
        return $this->datafastTrx;
    }

    public function setDatafastTrx(?DatafastTrx $datafastTrx): self
    {
        $this->datafastTrx = $datafastTrx;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    /**
     * @return Collection|OrdenDet[]
     */
    public function getOrdenDets(): Collection
    {
        return $this->ordenDets;
    }

    public function addOrdenDet(OrdenDet $ordenDet): self
    {
        if (!$this->ordenDets->contains($ordenDet)) {
            $this->ordenDets[] = $ordenDet;
            $ordenDet->setPaqueteEnvio($this);
        }

        return $this;
    }

    public function removeOrdenDet(OrdenDet $ordenDet): self
    {
        if ($this->ordenDets->contains($ordenDet)) {
            $this->ordenDets->removeElement($ordenDet);
            // set the owning side to null (unless already changed)
            if ($ordenDet->getPaqueteEnvio() === $this) {
                $ordenDet->setPaqueteEnvio(null);
            }
        }

        return $this;
    }
}
