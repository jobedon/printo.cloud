<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppError
 *
 * @ORM\Table(name="app_error")
 * @ORM\Entity
 */
class AppError
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="aplicacion", type="string", length=50, nullable=true)
     */
    private $aplicacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="proceso", type="string", length=100, nullable=true)
     */
    private $proceso;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codigo", type="integer", nullable=true)
     */
    private $codigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="error", type="string", length=500, nullable=true)
     */
    private $error;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=true, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAplicacion(): ?string
    {
        return $this->aplicacion;
    }

    public function setAplicacion(?string $aplicacion): self
    {
        $this->aplicacion = $aplicacion;

        return $this;
    }

    public function getProceso(): ?string
    {
        return $this->proceso;
    }

    public function setProceso(?string $proceso): self
    {
        $this->proceso = $proceso;

        return $this;
    }

    public function getCodigo(): ?int
    {
        return $this->codigo;
    }

    public function setCodigo(?int $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(?string $error): self
    {
        $this->error = $error;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(?\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }


}
