<?php

namespace App\Entity;

use App\Repository\DireccionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DireccionRepository::class)
 */
class Direccion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nombres;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $apellidos;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $razonSocial;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $direccion1;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $direccion2;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefono1;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefono2;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ciudad;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $provincia;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ciudadId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $correo;

    /**
     * @ORM\Column(type="string", length=3, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\ManyToOne(targetEntity=UserPi::class, inversedBy="direccions")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=15, nullable=true, options={"default"="DESTINO"})
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $latitud;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $longitud;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(?string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getRazonSocial(): ?string
    {
        return $this->razonSocial;
    }

    public function setRazonSocial(?string $razonSocial): self
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    public function getDireccion1(): ?string
    {
        return $this->direccion1;
    }

    public function setDireccion1(?string $direccion1): self
    {
        $this->direccion1 = $direccion1;

        return $this;
    }

    public function getDireccion2(): ?string
    {
        return $this->direccion2;
    }

    public function setDireccion2(?string $direccion2): self
    {
        $this->direccion2 = $direccion2;

        return $this;
    }

    public function getTelefono1(): ?string
    {
        return $this->telefono1;
    }

    public function setTelefono1(?string $telefono1): self
    {
        $this->telefono1 = $telefono1;

        return $this;
    }

    public function getTelefono2(): ?string
    {
        return $this->telefono2;
    }

    public function setTelefono2(?string $telefono2): self
    {
        $this->telefono2 = $telefono2;

        return $this;
    }

    public function getCiudad(): ?string
    {
        return $this->ciudad;
    }

    public function setCiudad(?string $ciudad): self
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(?string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getCiudadId(): ?int
    {
        return $this->ciudadId;
    }

    public function setCiudadId(?int $ciudadId): self
    {
        $this->ciudadId = $ciudadId;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(?string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(?\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getUser(): ?UserPi
    {
        return $this->user;
    }

    public function setUser(?UserPi $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getFullName()
    {
        $nombre = (empty($this->getRazonSocial())) ? $this->getNombres() . ' ' . $this->getApellidos() : $this->getRazonSocial();
        return $nombre;
    }

    public function __toString()
    {
        $nombre = (empty($this->getRazonSocial())) ? $this->getNombres() . ' ' . $this->getApellidos() : $this->getRazonSocial();

        return $nombre . ' - ' . $this->getCiudad() . ', ' .  $this->getProvincia() . ' - ' . $this->getDireccion1();

    }

    public function getLatitud(): ?string
    {
        return $this->latitud;
    }

    public function setLatitud(?string $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?string
    {
        return $this->longitud;
    }

    public function setLongitud(?string $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }


}
