<?php

namespace App\Entity;

use App\Repository\ProductoCaracteristicaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="producto_caracteristica")
 * @ORM\Entity(repositoryClass=ProductoCaracteristicaRepository::class)
 */
class ProductoCaracteristica
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $valor;

    /**
     * @ORM\Column(type="string", length=3, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @ORM\Column(name="fecha_creacion", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @ORM\ManyToOne(targetEntity=Producto::class, inversedBy="productoCaracteristicas")
     */
    private $producto;

    /**
     * @ORM\ManyToOne(targetEntity=Caracteristica::class, inversedBy="productoCaracteristicas")
     */
    private $caracteristica;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValor(): ?string
    {
        return $this->valor;
    }

    public function setValor(string $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    public function getCaracteristica(): ?Caracteristica
    {
        return $this->caracteristica;
    }

    public function setCaracteristica(?Caracteristica $caracteristica): self
    {
        $this->caracteristica = $caracteristica;

        return $this;
    }
}
