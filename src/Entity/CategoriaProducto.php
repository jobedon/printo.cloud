<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\TipoProducto;

/**
 * CategoriaProducto
 *
 * @ORM\Table(name="categoria_producto", indexes={@ORM\Index(name="FK_CATPROD_TIPOPROD", columns={"tipo_producto_id"})})
 * @ORM\Entity
 */
class CategoriaProducto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=100, nullable=false)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=3, nullable=false, options={"default"="A"})
     */
    private $estado = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    /**
     * @var TipoProducto
     *
     * @ORM\ManyToOne(targetEntity="TipoProducto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_producto_id", referencedColumnName="id")
     * })
     */
    private $tipoProducto;

    /**
     * @ORM\OneToMany(targetEntity=TarifarioXerox::class, mappedBy="categoriaProducto")
     */
    private $tarifarioXeroxes;

    /**
     * @ORM\OneToMany(targetEntity=Producto::class, mappedBy="categoriaProducto")
     */
    private $productos;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime("now"));
        $this->tarifarioXeroxes = new ArrayCollection();
        $this->productos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoria(): ?string
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getTipoProducto(): ?TipoProducto
    {
        return $this->tipoProducto;
    }

    public function setTipoProducto(?TipoProducto $tipoProducto): self
    {
        $this->tipoProducto = $tipoProducto;

        return $this;
    }

    /**
     * @return Collection|TarifarioXerox[]
     */
    public function getTarifarioXeroxes(): Collection
    {
        return $this->tarifarioXeroxes;
    }

    public function addTarifarioXerox(TarifarioXerox $tarifarioXerox): self
    {
        if (!$this->tarifarioXeroxes->contains($tarifarioXerox)) {
            $this->tarifarioXeroxes[] = $tarifarioXerox;
            $tarifarioXerox->setCategoriaProducto($this);
        }

        return $this;
    }

    public function removeTarifarioXerox(TarifarioXerox $tarifarioXerox): self
    {
        if ($this->tarifarioXeroxes->contains($tarifarioXerox)) {
            $this->tarifarioXeroxes->removeElement($tarifarioXerox);
            // set the owning side to null (unless already changed)
            if ($tarifarioXerox->getCategoriaProducto() === $this) {
                $tarifarioXerox->setCategoriaProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Producto[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setCategoriaProducto($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            // set the owning side to null (unless already changed)
            if ($producto->getCategoriaProducto() === $this) {
                $producto->setCategoriaProducto(null);
            }
        }

        return $this;
    }


}
