<?php

namespace App\Repository;

use App\Entity\ProductoCaracteristica;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductoCaracteristica|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductoCaracteristica|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductoCaracteristica[]    findAll()
 * @method ProductoCaracteristica[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoCaracteristicaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductoCaracteristica::class);
    }

}
