<?php

namespace App\Repository;

use App\Entity\Direccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Direccion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Direccion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Direccion[]    findAll()
 * @method Direccion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DireccionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Direccion::class);
    }

    public function saveDireccion($parametros)
    {
        $dir = new Direccion();
        $dir->setNombres($parametros['nombres']);
        $dir->setApellidos($parametros['apellidos']);
        $dir->setRazonSocial($parametros['razonsocial']);
        $dir->setDireccion1($parametros['direccion1']);
        $dir->setTelefono1($parametros['telefono1']);
        $dir->setProvincia($parametros['provincia']);
        $dir->setCiudad($parametros['ciudad']);
        $dir->setCiudadId($parametros['ciudadId']);
        $dir->setUser($parametros['user']);
        $this->getEntityManager()->persist($dir);
        $this->getEntityManager()->flush();
    }

    

}
