<?php

namespace App\Repository;

use App\Entity\TarifarioElectrocorteXerox;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TarifarioElectrocorteXeroxRepository extends ServiceEntityRepository
{

    private $session;

    public function __construct(
        ManagerRegistry $registry,
        SessionInterface $session
    ) {
        $this->session = $session;
        parent::__construct($registry, TarifarioElectrocorteXerox::class);
    }

    public function findTarifa($item, $electrocorte)
    {

        $dql = "SELECT t 
                FROM App:TarifarioElectrocorteXerox t 
                WHERE t.estado = 'A' 
                AND :cantidad BETWEEN t.cantidadMin AND t.cantidadMax";

        $categoriaProducto = $item->getProducto()->getCategoriaProducto()->getCategoria();

        if ($categoriaProducto == 'Adhesivos') {
            $cantidad = $electrocorte['adhesivo'];
        } else {
            $cantidad = $electrocorte['cartulina'];
        }

        $qc = $this->getEntityManager()->createQuery($dql);
        $qc->setParameter('cantidad', $cantidad);

        $result = $qc->getResult();

        if (empty($result)) {
            return null;
        }

        $tarifa = $result[0];

        $subs = $this->session->get("userSubscription");
        if (!empty($subs) && in_array($subs['plan_name'], ['Profesional', 'Profesional Plus'])) {
            if ($tarifa->getNivel() < 7) {
                $dql = "SELECT t 
                    FROM App:TarifarioElectrocorteXerox t 
                    WHERE t.estado = 'A' 
                    AND t.nivel = :nivel";
                $qc = $this->getEntityManager()->createQuery($dql);
                $qc->setParameter('nivel', 7);

                $result2 = $qc->getResult();

                if (empty($result2)) {
                    return null;
                }

                $tarifa = $result2[0];
            }
        }

        if ($categoriaProducto == 'Adhesivos') {
            return $tarifa->getPrecioAdhesivo();
        } else {
            return $tarifa->getPrecioCartulina();
        }
    }
    
}
