<?php

namespace App\Repository;

use App\Entity\FilesOrdenDet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FilesOrdenDet|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilesOrdenDet|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilesOrdenDet[]    findAll()
 * @method FilesOrdenDet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilesOrdenDetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilesOrdenDet::class);
    }

}
