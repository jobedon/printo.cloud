<?php

namespace App\Repository;

use App\Entity\Configuraciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ConfiguracionesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configuraciones::class);
    }

    public function getConfigDouble($codigo)
    {
        $config = $this->findOneBy([
            'estado' => 'A',
            'codigo' => $codigo
        ]);
        return $config->getValordouble();
    }

    public function getConfigString($codigo)
    {
        $config = $this->findOneBy([
            'estado' => 'A',
            'codigo' => $codigo
        ]);
        return $config->getValorString();
    }

    public function getConfigMapping($mappingName)
    {
        $config = $this->findOneBy([
            'estado' => 'A',
            'mappingName' => $mappingName
        ]);
        return $config->getValorString();
    }

    
}
