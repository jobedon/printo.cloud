<?php

namespace App\Repository;

use App\Entity\TarifarioXerox;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TarifarioXeroxRepository extends ServiceEntityRepository
{
    private $session;

    public function __construct(
        ManagerRegistry $registry,
        SessionInterface $session
    ) {
        $this->session = $session;
        parent::__construct($registry, TarifarioXerox::class);
    }

    public function findTarifa($item, $dataRef)
    {

        $dql = "SELECT t 
                FROM App:TarifarioXerox t 
                WHERE t.estado = 'A' 
                AND t.categoriaProducto = :cat 
                AND :cantidad BETWEEN t.cantidadMin AND t.cantidadMax";

        $cantidad = $dataRef[$item->getProducto()->getCategoriaProducto()->getId()];

        $qc = $this->getEntityManager()->createQuery($dql);
        $qc->setParameter('cat',   $item->getProducto()->getCategoriaProducto());
        $qc->setParameter('cantidad', $cantidad);

        $result = $qc->getResult();

        if (empty($result)) {
            return null;
        }

        $tarifa = $result[0];

        // Validar precio por suscripcion
        $subs = $this->session->get("userSubscription");
        if (isset($subs['plan_name']) && in_array($subs['plan_name'], ['Profesional', 'Profesional Plus'])) {
            if ($result[0]->getNivel() < 7) {
                $dql = "SELECT t 
                    FROM App:TarifarioXerox t 
                    WHERE t.estado = 'A' 
                    AND t.categoriaProducto = :cat 
                    AND t.nivel = :nivel";
                $qc = $this->getEntityManager()->createQuery($dql);
                $qc->setParameter('cat',   $item->getProducto()->getCategoriaProducto());
                $qc->setParameter('nivel', 7);

                $result2 = $qc->getResult();

                if (empty($result2)) {
                    return null;
                }

                $tarifa = $result2[0];
            }
        }

        if ($item->getTipoImpresion() == "TIRO") {
            return $tarifa->getPrecioTiro();
        } elseif ($item->getTipoImpresion() == "T/R") {
            return $tarifa->getPrecioTiroRetiro();
        }
    }
}
