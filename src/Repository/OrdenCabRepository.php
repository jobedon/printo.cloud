<?php

namespace App\Repository;

use App\Entity\OrdenCab;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrdenCab|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrdenCab|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrdenCab[]    findAll()
 * @method OrdenCab[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdenCabRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrdenCab::class);
    }
}
