<?php

namespace App\Repository;

use App\Entity\OrdenDet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @method OrdenDet|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrdenDet|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrdenDet[]    findAll()
 * @method OrdenDet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdenDetRepository extends ServiceEntityRepository
{
    private $session;

    public function __construct(ManagerRegistry $registry,SessionInterface $session)
    {
        parent::__construct($registry, OrdenDet::class);
        $this->session = $session;
    }

    public function getDetallesPorTickets($parametros)
    {
        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $tickets = $parametros['tickets'];
        $user    = $parametros['user'];

        $dql = "SELECT o 
                FROM App:OrdenDet o,
                     App:OrdenCab c
                WHERE o.ordenCab = c
                AND o.estado = :estado 
                AND o.noTicket IN (:tickets)
                AND c.user = :user";

        $qc = $this->getEntityManager()->createQuery($dql);
        $qc->setParameter("estado", $estadoOrdenCab);
        $qc->setParameter("tickets", $tickets);
        $qc->setParameter("user", $user);

        $result = $qc->getResult();

        return $result;
    }

    public function getPesoTotalPorItems($parametros)
    {

        $items = $parametros['items'];
        $user  = $parametros['user'];

        $dql = "SELECT SUM(odc.valor)
                FROM App:OrdenDet od,
                     App:OrdenCab oc,
                     App:OrdenDetCaracteristica odc,
                     App:Caracteristica c
                WHERE od.ordenCab = oc
                AND odc.caracteristica = c
                AND odc.ordenDet = od
                AND c.nombre = :caracteristica
                AND od.estado = 'A' 
                AND oc.estado IN (:estados)
                AND odc.estado = 'A'
                AND od IN (:items)
                AND oc.user = :user";

        $qc = $this->getEntityManager()->createQuery($dql);
        $qc->setParameter("items", $items);
        $qc->setParameter("caracteristica", 'peso');
        $qc->setParameter("estados", ['A', 'PF', 'I']);
        $qc->setParameter("user", $user);

        $result = $qc->getSingleScalarResult();

        return $result;
    }

    public function getPesoTotalTickets($parametros)
    {
        $tickets = $parametros['tickets'];
        $user    = $parametros['user'];

        $dql = "SELECT SUM(odc.valor)
                FROM App:OrdenDet od,
                     App:OrdenCab oc,
                     App:OrdenDetCaracteristica odc,
                     App:Caracteristica c
                WHERE od.ordenCab = oc
                AND odc.caracteristica = c
                AND odc.ordenDet = od
                AND c.nombre = :caracteristica
                AND od.estado = 'A' 
                AND oc.estado = 'PF'
                AND odc.estado = 'A'
                AND od.noTicket IN (:tickets)
                AND oc.user = :user";

        $qc = $this->getEntityManager()->createQuery($dql);
        $qc->setParameter("tickets", $tickets);
        $qc->setParameter("caracteristica", 'peso');
        $qc->setParameter("user", $user);

        $result = $qc->getSingleScalarResult();

        return $result;
    }

    public function actualizarEnvioItems($parametros)
    {

        $tickets      = $parametros['tickets'];
        $user         = $parametros['user'];
        $paqueteEnvio = $parametros['paqueteEnvio'];

        $dql = "UPDATE App:OrdenDet od
                    SET od.paqueteEnvio = :paqueteEnvio
                WHERE 
                    od.noTicket IN (:tickets)
                AND EXISTS (
                    SELECT 
                        1 
                    FROM 
                        App:OrdenCab oc
                    WHERE
                        od.ordenCab = oc
                        AND oc.user = :user
                )";

        $qc = $this->getEntityManager()->createQuery($dql);
        $qc->setParameter('tickets', $tickets);
        $qc->setParameter('user', $user);
        $qc->setParameter('paqueteEnvio', $paqueteEnvio);

        $result = $qc->getResult();

        return $result;
    }
}
