<?php

namespace App\Repository;

use App\Entity\FilesOrdenCab;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FilesOrdenCab|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilesOrdenCab|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilesOrdenCab[]    findAll()
 * @method FilesOrdenCab[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilesOrdenCabRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilesOrdenCab::class);
    }

}
