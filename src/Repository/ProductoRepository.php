<?php

namespace App\Repository;

use App\Entity\Producto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Producto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Producto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Producto[]    findAll()
 * @method Producto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Producto::class);
    }

    public function getMaterialesPorProducto($user)
    {
        $dql = "SELECT
                    PR.id, 
                    PR.nombreProducto, 
                    PR.sku, 
                    PR2.nombreProducto AS categoria,
                    'productos' as tipo
                FROM 
                    App:Producto          PR, 
                    App:CategoriaProducto CT, 
                    App:Producto          PR2
                WHERE 
                    PR.producto = PR2
                    AND PR.categoriaProducto = CT
                    AND PR.producto IS NOT NULL
                    AND PR.id NOT IN (
                        SELECT 
                            PR3.id
                        FROM 
                            App:Producto         PR3, 
                            App:ProductoFavorito PF, 
                            App:Producto         PR4
                        WHERE PR3 = PF.producto
                        AND PR3.producto = PR4
                        AND PF.user = :user
                        AND PF.estado = 'A'
                    )";

        $qc = $this->getEntityManager()->createQuery($dql);

        $qc->setParameter('user', $user);

        $result = $qc->getResult();

        return $result;

    }

    public function getMaterialesFavoritos($user)
    {
        $dql = "SELECT 
                    PR3.id,
                    PR3.nombreProducto, 
                    PR3.sku, 
                    PR4.nombreProducto as categoria, 
                    'productosFav' as tipo
                FROM 
                    App:Producto         PR3, 
                    App:ProductoFavorito PF, 
                    App:Producto         PR4
                WHERE PR3 = PF.producto
                AND PR3.producto = PR4
                AND PF.user = :user
                AND PF.estado = 'A'";

        $qc = $this->getEntityManager()->createQuery($dql);

        $qc->setParameter('user', $user);

        $result = $qc->getResult();

        return $result;
    }

}
