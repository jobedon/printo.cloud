<?php

namespace App\Repository;

use App\Entity\TarifarioMimaki;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TarifarioMimakiRepository extends ServiceEntityRepository
{
    private $session;

    public function __construct(
        ManagerRegistry $registry,
        SessionInterface $session
    ) {
        $this->session = $session;
        parent::__construct($registry, TarifarioMimaki::class);
    }

    public function findTarifa($item)
    {
        $subs = $this->session->get("userSubscription");

        $tarfMimaki = $this->findOneBy([
            'estado'   => 'A',
            'producto' => $item->getProducto()
        ]);

        $precio = 0;

        if (!empty($subs) && in_array($subs['plan_name'], ['Profesional'])) {
            $precio = $tarfMimaki->getPrecioPrintoProfesional();
        } else {
            $precio = $tarfMimaki->getPrecioPrintoFree();
        }

        return $precio;
    }

    public function findTarifaCorteRecto($item)
    {
        $tarfMimaki = $this->findOneBy([
            'estado'   => 'A',
            'producto' => $item->getProducto()
        ]);
        return $tarfMimaki->getPrecioRectoMetro2();
    }
    
}
