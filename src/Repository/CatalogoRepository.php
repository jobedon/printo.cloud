<?php

namespace App\Repository;

use App\Entity\Catalogo;
use App\Entity\Producto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Catalogo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Catalogo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Catalogo[]    findAll()
 * @method Catalogo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatalogoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Catalogo::class);
    }

    public function getProductosPorCatalogo($parametros)
    {

        $catalogoNombre = $parametros['catalogoNombre'];

        $em = $this->getEntityManager();

        /**@var null|CatalogoRepository $cataRepo */
        $cataRepo = $em->getRepository(Catalogo::class);

        $catalogo = $cataRepo->findOneBy([
            'nombre' => $catalogoNombre,
            'estado' => 'A'
        ]);

        /**@var null|ProductoRepository $prodRepo */
        $prodRepo = $em->getRepository(Producto::class);

        $productos = $prodRepo->findBy([
            'catalogo' => $catalogo
        ]);

        return $productos;

    }

}
