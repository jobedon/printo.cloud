<?php

namespace App\Repository;

use App\Entity\PaqueteEnvio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaqueteEnvio|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaqueteEnvio|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaqueteEnvio[]    findAll()
 * @method PaqueteEnvio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaqueteEnvioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaqueteEnvio::class);
    }

    // /**
    //  * @return PaqueteEnvio[] Returns an array of PaqueteEnvio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaqueteEnvio
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
