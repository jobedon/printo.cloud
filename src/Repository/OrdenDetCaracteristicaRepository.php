<?php

namespace App\Repository;

use App\Entity\OrdenDetCaracteristica;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrdenDetCaracteristica|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrdenDetCaracteristica|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrdenDetCaracteristica[]    findAll()
 * @method OrdenDetCaracteristica[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdenDetCaracteristicaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrdenDetCaracteristica::class);
    }

    // /**
    //  * @return OrdenDetCaracteristica[] Returns an array of OrdenDetCaracteristica objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrdenDetCaracteristica
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
