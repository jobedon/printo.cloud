<?php

namespace App\Repository;

use App\Entity\Caracteristica;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Caracteristica|null find($id, $lockMode = null, $lockVersion = null)
 * @method Caracteristica|null findOneBy(array $criteria, array $orderBy = null)
 * @method Caracteristica[]    findAll()
 * @method Caracteristica[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaracteristicaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Caracteristica::class);
    }

}
