<?php

namespace App\Services;

use App\Entity\TipoZoho;
use App\Entity\ZohoTokens;
use Doctrine\ORM\EntityManagerInterface;

class TokenService
{

    private $entityManager;

    private $zohoCrmService;

    private $zohoBooksService;

    private $zohoDeskService;

    private $zohoSubService;

    private $zohoWorkDriveService;

    private $zohoUploadService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ZohoCrmService $zohoCrmService,
        ZohoBooksService $zohoBooksService,
        ZohoDeskService $zohoDeskService,
        ZohoSubscriptionsService $zohoSubService,
        ZohoWorkDriveService $zohoWorkDriveService,
        ZohoUploadService $zohoUploadService
    ) {
        $this->entityManager        = $entityManager;
        $this->zohoCrmService       = $zohoCrmService;
        $this->zohoBooksService     = $zohoBooksService;
        $this->zohoDeskService      = $zohoDeskService;
        $this->zohoSubService       = $zohoSubService;
        $this->zohoWorkDriveService = $zohoWorkDriveService;
        $this->zohoUploadService    = $zohoUploadService;
    }

    public function getToken($tipoToken)
    {

        $tokensRepo   = $this->entityManager->getRepository(ZohoTokens::class);
        $tipoZohoRepo = $this->entityManager->getRepository(TipoZoho::class);

        $tipoZoho = $tipoZohoRepo->findOneBy([
            'zoho'   => $tipoToken,
            'estado' => 'A'
        ]);

        $token = $tokensRepo->findOneBy([
            'estado'   => 'A',
            'tipoZoho' => $tipoZoho
        ]);

        if (isset($token) && $token != null) {
            $fechaCrea  = $token->getFechaCreacion();
            $fechaAhora = new \DateTime();
            $diferencia = $fechaAhora->getTimestamp() - $fechaCrea->getTimestamp();

            // Si ha pasado mas de una hora
            if ($diferencia < 3500) {
                return $token->getAccessToken();;
            } else {
                $token->setEstado("I");
            }
        }

        //Pedir nuevo token desde CRM
        switch ($tipoToken) {
            case 'CRM':
                $newToken = $this->zohoCrmService->getToken();
                break;
            case 'BOOKS':
                $newToken = $this->zohoBooksService->getToken();
                break;
            case 'DESK':
                $newToken = $this->zohoDeskService->getToken();
                break;
            case 'SUBSCRIPTIONS':
                $newToken = $this->zohoSubService->getToken();
                break;
            case 'WORKDRIVE':
                $newToken = $this->zohoWorkDriveService->getToken();
                break;
            case 'UPLOAD':
                $newToken = $this->zohoUploadService->getToken();
                break;
            default:
                # code...
                break;
        }

        // Ingresar en tabla
        $zohoTokens = new ZohoTokens();
        $zohoTokens->setAccessToken($newToken);
        $zohoTokens->setTipoZoho($tipoZoho);
        $this->entityManager->persist($zohoTokens);
        $this->entityManager->flush();

        return $zohoTokens->getAccessToken();
    }

    public function getCrmToken()
    {
        return $this->getToken("CRM");
    }

    public function getBooksToken()
    {
        return $this->getToken("BOOKS");
    }

    public function getDeskToken()
    {
        return $this->getToken("DESK");
    }

    public function getSubscriptionsToken()
    {
        return $this->getToken("SUBSCRIPTIONS");
    }

    public function getWorkDriveToken()
    {
        return $this->getToken("WORKDRIVE");
    }

    public function getUploadToken()
    {
        return $this->getToken("UPLOAD");
    }
}
