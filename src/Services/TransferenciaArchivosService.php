<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class TransferenciaArchivosService
{
    private $tokenService;

    private $em;

    private $security;

    private $session;

    private $zohoDeskService;

    public function __construct(
        TokenService $tokenService,
        EntityManagerInterface $em,
        Security $security,
        SessionInterface $session,
        ZohoDeskService $zohoDeskService
    ) {
        $this->tokenService     = $tokenService;
        $this->em               = $em;
        $this->security         = $security;
        $this->session          = $session;
        $this->zohoDeskService = $zohoDeskService;
    }

    public function transferirArchivos($saleOrder)
    {

        $token = $this->tokenService->getDeskToken();

        // obtengo ticket
        $ticket = $this->zohoDeskService->searchTicketBySaleOrder($token, $saleOrder);


    }

}