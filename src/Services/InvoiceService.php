<?php

namespace App\Services;

use App\Entity\Configuraciones;
use App\Entity\DatafastTrx;
use App\Entity\UserPi;
use App\Entity\UserTc;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class InvoiceService
{
    private $tokenService;
    private $zohoBooksService;
    /** @var SessionInterface|null $session */
    private $em;
    private $security;
    private $params;
    private $util;
    private $session;
    private $logger;
    private $dataFastService;

    private $successCodes = array('000.100.112', '000.100.110', '000.000.000', '000.200.100');

    public function __construct(
        SessionInterface $session,
        TokenService $tokenService,
        ZohoBooksService $zohoBooksService,
        UtilsService $util,
        EntityManagerInterface $em,
        Security $security,
        ContainerBagInterface $params,
        LoggerInterface $logger,
        DataFastService $dataFastService
    ) {
        $this->session          = $session;
        $this->tokenService     = $tokenService;
        $this->zohoBooksService = $zohoBooksService;
        $this->util             = $util;
        $this->em               = $em;
        $this->security         = $security;
        $this->params           = $params;
        $this->logger           = $logger;
        $this->dataFastService  = $dataFastService;
    }

    public function getUnpaidInvoices()
    {
        $token = $this->tokenService->getBooksToken();
        $userBooksZoho = $this->session->get('userBooksZoho');

        $invoices = $this->zohoBooksService->getUnpaidInvoices($token, [
            'customer_id' => $userBooksZoho['contact_id']
        ]);

        return $invoices;
    }

    public function getCheckoutId(array $parametros)
    {

        $token = $parametros['token'];

        $rubros = $parametros['rubros'];

        $total = (float)$rubros['total'];

        /** INI Logica TARJETAS DE CREDITO */
        $userTcRepo = $this->em->getRepository(UserTc::class);

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $tcs = $userTcRepo->findBy([
            'user' => $user,
            'estado' => 'A'
        ]);

        $tarjetas = [];
        $idx = 0;
        foreach ($tcs as $tc) {
            $tarjetas['registrations[' . $idx . '].id'] = $tc->getRegistrationid();
            $idx++;
        }
        $parametros['tarjetas'] = $tarjetas;
        /** FIN Logica TARJETAS DE CREDITO */


        $trxId = md5(uniqid(rand(), true));
        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $givenName  = (!empty($user->getNombre())) ? $user->getNombre() : '';
        $middleName = (!empty($user->getNombre())) ? $user->getNombre() : '';
        $surname    = (!empty($user->getApellido())) ? $user->getApellido() : '';
        if (!empty($user->getRazonSocial())) {
            $givenName = $user->getRazonSocial();
            $middleName = 'Prueba';
            $surname = $user->getRazonSocial();
        }

        $userBooksZoho = $this->session->get('userBooksZoho');
        $phone = $userBooksZoho['mobile'];

        $userCrmZoho = $this->session->get('userCrmZoho');

        if (isset($userCrmZoho['Direcci_n_de_facturaci_n']) && !empty($userCrmZoho['Direcci_n_de_facturaci_n'])) {
            $billingAddress = $userCrmZoho['Direcci_n_de_facturaci_n'];
        } else {
            $billingAddress = "Sin Dirección de Facturación";
        }

        $shippingAddress = 'Sin dirección Entrega';
        $shippingCountry = 'EC';
        $billingCountry = 'EC';

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        $rubroIva = $confRepo->getConfigDouble('PORCENTAJE_IVA');
        $subtotal = $total / (1 + $rubroIva / 100);
        $valorIva = $subtotal * ($rubroIva / 100);

        $data = array();

        $data["entityId"] = $this->params->get('df.entity.id');
        $data["amount"] = number_format(round($total, 2), 2, '.', '');
        $data["currency"] = 'USD';
        $data["paymentType"] = 'DB';

        $data["merchantTransactionId"] = $trxId;
        $data["customer.givenName"] = $givenName;
        $data["customer.middleName"] = $middleName;
        $data["customer.surname"] = $surname;
        $data["customer.ip"] = $this->util->getUserIP();
        $data["customer.merchantCustomerId"] = $user->getCrmUserId();
        $data["customer.email"] = $user->getEmail();
        $data["customer.phone"] = $phone;
        $data["shipping.street1"] = $shippingAddress;
        $data["shipping.country"] = $shippingCountry;
        $data["billing.street1"] = $billingAddress;
        $data["billing.country"] = $billingCountry;
        $data["customer.identificationDocType"] = $this->params->get("df.identification.doc.type");
        $data["customer.identificationDocId"] = substr($user->getRuc(), 0, 10);

        $detalleRubros = $rubros['detalle'];
        for ($i = 0; $i < count($detalleRubros); $i++) {
            $nameItem     = $detalleRubros[$i]['invoiceid'];
            $descItem     = 'Pago factura';
            $priceItem    = $detalleRubros[$i]['value'];
            $quantityItem = 1;
            if ($priceItem != 0) {
                $data["cart.items[" . $i . "].name"]        = $nameItem;
                $data["cart.items[" . $i . "].description"] = $descItem;
                $data["cart.items[" . $i . "].price"]       = number_format(round($priceItem, 2), 2, '.', '');
                $data["cart.items[" . $i . "].quantity"]    = $quantityItem;
            }
        }
        $subtotal = number_format(round($subtotal, 2), 2, '.', '');
        $valorIva = number_format(round($valorIva, 2), 2, '.', '');

        $data["customParameters[SHOPPER_VAL_BASE0]"]   = '0.00';
        $data["customParameters[SHOPPER_VAL_BASEIMP]"] = $subtotal;
        $data["customParameters[SHOPPER_VAL_IVA]"]     = $valorIva;

        $data["customParameters[SHOPPER_MID]"]         = $this->params->get("df.shopper.mid");
        $data["customParameters[SHOPPER_TID]"]         = $this->params->get("df.shopper.tid");
        $data["customParameters[SHOPPER_ECI]"]         = $this->params->get("df.shopper.eci");
        $data["customParameters[SHOPPER_PSERV]"]       = $this->params->get("df.shopper.pserv");
        $data["customParameters[SHOPPER_VERSIONDF]"]   = 2;

        $data["risk.parameters[USER_DATA2]"]           = $this->params->get("df.business.name");

        if (($this->params->get('app.env') != 'prod')) {
            $data["testMode"] = "EXTERNAL";
        }

        if (isset($parametros['tarjetas']) && !empty($parametros['totalPagar'])) {
            //$data = array_merge($data, $parametros['tarjetas']);
        }

        $parametros["data"] = $data;

        try {

            // Crear TRX
            $dfTrx = new DatafastTrx();
            $dfTrx->setTipo("checkout");
            $dfTrx->setTransactionId($trxId);
            //$dfTrx->setOrdenCab($ordenCab);
            $dfTrx->setRequest(json_encode($data));
            $dfTrx->setObservacion(json_encode($rubros));
            $dfTrx->setUser($user);

            $this->em->persist($dfTrx);
            $this->em->flush();

            $checkout = $this->dataFastService->getCheckoutId($parametros);

            $resultCode = 'NO CODE ERROR';
            if (isset($checkout['result']) && isset($checkout['result']['code'])) {
                $resultCode = $checkout['result']['code'];
            }
            //Actualizar TRX
            $dfTrx->setResponse(json_encode($checkout));
            $dfTrx->setCheckoutId($checkout['id']);
            $dfTrx->setResultCode($resultCode);
            $dfTrx->setEstadoTransaccion("Generado");
            $dfTrx->setFechaActualizacion();
            $this->em->flush();

            if (!in_array($resultCode, $this->successCodes)) {
                $errorMessage = "";
                if (isset($responsePurchase['result']) && isset($responsePurchase['result']['description'])) {
                    $errorMessage = " - " . $responsePurchase['result']['description'];
                }
                return [
                    'error' => true,
                    'message' => 'Hubo un problema con la transacción' . $errorMessage
                ];
            }
        } catch (Exception $e) {
            $dfTrx->setFechaActualizacion();
            $dfTrx->setEstadoTransaccion("Error");
            $dfTrx->setError(substr($e->getMessage(), 0, 300));
            $this->em->flush();
            $this->logger->error(get_class($this) . ' - method getCheckoutId - Hubo un error checout inicial PRINTO - ' . $e->getMessage());
            return [
                'error' => true,
                'message' => 'Hubo un problema con Datafast'
            ];
        }

        return $checkout['id'];
    }

    public function purchaseOrder($parametros)
    {
        $idCheckout = $parametros['idPayment'];

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $dfRepo = $this->em->getRepository(DatafastTrx::class);
        $dfTrxCheckout = $dfRepo->findOneBy([
            'checkoutId' => $idCheckout
        ]);

        $rubros = json_decode($dfTrxCheckout->getObservacion());

        $data = array();
        $data["entityId"] = $this->params->get("df.entity.id");
        $parametros["data"] = $data;

        try {
            $dfTrx = new DatafastTrx();
            $dfTrx->setTipo("purchase");
            $dfTrx->setTransactionId($dfTrxCheckout->getTransactionId());
            $dfTrx->setRequest(json_encode($data));
            $dfTrx->setUser($user);
            $dfTrx->setObservacion($dfTrxCheckout->getObservacion());
            $this->em->persist($dfTrx);
            $this->em->flush();

            $responsePurchase = $this->dataFastService->purchaseOrder($parametros);


            $resultCode = 'NO CODE ERROR';
            if (isset($responsePurchase['result']) && isset($responsePurchase['result']['code'])) {
                $resultCode = $responsePurchase['result']['code'];
            }

            //Actualizar TRX
            $dfTrx->setResponse(json_encode($responsePurchase));
            $dfTrx->setResultCode($resultCode);
            $dfTrx->setEstadoTransaccion("Generado");
            $dfTrx->setFechaActualizacion();
            $this->em->flush();

            if (!in_array($resultCode, $this->successCodes)) {
                $errorMessage = "";
                if (isset($responsePurchase['result']) && isset($responsePurchase['result']['description'])) {
                    $errorMessage = " - " . $responsePurchase['result']['description'] . " - Código : " . $resultCode;
                }
                throw new Exception('Hubo un problema al procesar su compra' . $errorMessage);
            }

            // Registro de tarjetas de credito
            if (isset($responsePurchase['registrationId'])) {
                $userTcRepo = $this->em->getRepository(UserTc::class);
                $userTc = $userTcRepo->findOneBy([
                    'user' => $user,
                    'estado' => 'A',
                    'registrationid' => $responsePurchase['registrationId']
                ]);
                if (empty($userTc)) {
                    $userTc = new UserTc();
                    $userTc->setRegistrationid($responsePurchase['registrationId']);
                    $userTc->setUser($user);
                    $this->em->persist($userTc);
                    $this->em->flush();
                }
            }

            // OK para DATAFAST

            // Pagar facturas
            $invoices = $this->getUnpaidInvoices();
            $totalPagar = $rubros->total; // se ignora detalle y se recalcula del total
            $invoicesRequest = [];
            foreach ($invoices as $invoice) {
                if ($totalPagar >= 0) {
                    $valor = (float) $invoice['balance'];
                    if ($totalPagar < $valor) {
                        $valorInvoice = $totalPagar;
                    } else{
                        $valorInvoice = $valor;
                    }
                    $totalPagar -= $valor;
                    $invoicesRequest[] = [
                        'invoice_id' => $invoice['invoice_id'],
                        'amount_applied' => $valorInvoice
                    ];
                }
            }

            $userBooksZoho = $this->session->get('userBooksZoho');
            $merchantCustomerId  = $userBooksZoho['contact_id'];

            $parametros = [
                'customerId' => $merchantCustomerId,
                'amount' => $rubros->total,
                'referenceNumber' => $dfTrx->getTransactionId(),
                'invoices' => $invoicesRequest
            ];

            $dfTrx->setObservacion(json_encode($parametros));

            $token = $this->tokenService->getBooksToken();

            $resultPayInvoices = $this->zohoBooksService->payInvoices($token, $parametros);

            if (empty($resultPayInvoices)) {
                throw new Exception("Hubo un problema con el pago de sus facturas, comunicarse con soporte.", 1);
            }

            $this->em->flush();            

        } catch (Exception $e) {
            $dfTrx->setFechaActualizacion();
            $dfTrx->setEstadoTransaccion("error");
            $dfTrx->setError(substr($e->getMessage(), 0, 300));
            $this->em->flush();
            $this->logger->error(get_class($this) . ' - method purchaseOrder - Hubo un error Creacion de orden DATAFAST - ' . $e->getMessage());
            throw new Exception("Hubo un problema con Datafast : " . $e->getMessage(), 1);
        }

        return $resultPayInvoices;

    }
}
