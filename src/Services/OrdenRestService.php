<?php

namespace App\Services;

use App\Entity\OrdenCab;
use App\Entity\OrdenDet;
use App\Exceptions\TransferFileException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class OrdenRestService
{

    /** @var EntityManagerInterface|null $em */
    private $em;
    /** @var SessionInterface|null $session */
    private $session;
    /** @var TokenService|null $tokenService */
    private $tokenService;
    /** @var ZohoWorkDriveService|null $zohoWDService */
    private $zohoWDService;

    public function __construct(
        EntityManagerInterface $em,
        SessionInterface $session,
        TokenService $tokenService,
        ZohoWorkDriveService $zohoWDService
    )
    {
        $this->session       = $session;
        $this->em            = $em;
        $this->tokenService  = $tokenService;
        $this->zohoWDService = $zohoWDService;
    }


    public function transferFilesOrder($data)
    {

        $this->renameFiles($data);

        $response = $this->movePendienteToFinalizadas($data);

        return $response;

    }

    public function renameFiles($data)
    {

        $idZohoOrden  = $data['id_orden'];  // 2995742000000517092
        $numZohoOrden = $data['num_orden']; // TI-PI-000000148
        $tickets = $data['tickets'];        // []

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        /** @var OrdenCab|null $ordenCab */
        $ordenCab = $ordenCabRepo->findOneBy([
            'estado'  => 'P',
            'noOrden' => $numZohoOrden
        ]);

        if (empty($ordenCab)) {
            throw new TransferFileException("Orden ya procesada, o inexistente");
        }

        $cliente = $ordenCab->getUser()->getRazonSocial();

        foreach ($tickets as $ticket) {

            /** @var OrdenDet|null $item */
            $item = $ordenDetRepo->findOneBy([
                'lineItemId' => $ticket['line_item_id']
            ]);

            if (empty($item)) {
                throw new TransferFileException("Detalle de la Orden no encontrada");
            }

            $tokenWD = $this->tokenService->getWorkDriveToken();

            $newNameFolder = "#" . $ticket['num_ticket'] . " - " . $cliente . ' - ' . $numZohoOrden;

            $folderId = $item->getFiles()->getFolderId(); 

            if (!empty($folderId)) {
                $this->zohoWDService->renameFolder($tokenWD, $newNameFolder, $folderId);
            }

            $item->setNoTicket($ticket['num_ticket']);

        }

        $ordenCab->setEstado('I');
        $ordenCab->setFechaActualizacion();
        $this->em->flush();

    }

    public function movePendienteToFinalizadas($data)
    {
        $numZohoOrden = $data['num_orden']; // TI-PI-000000148
        $tickets      = $data['tickets'];   // []

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        /** @var OrdenCab|null $ordenCab */
        $ordenCab = $ordenCabRepo->findOneBy([
            'estado'  => 'I',
            'noOrden' => $numZohoOrden
        ]);

        if (empty($ordenCab)) {
            throw new TransferFileException("Orden no se encuentra finaizada");
        }

        $cliente = $ordenCab->getUser()->getRazonSocial();

        $response = [
            'id_orden' => $data['id_orden'],
            'num_orden' => $data['num_orden'],
        ];

        $ticketsResponse = [];
        foreach ($tickets as $ticket) {
            
            /** @var OrdenDet|null $item */
            $item = $ordenDetRepo->findOneBy([
                'lineItemId' => $ticket['line_item_id']
            ]);

            if (empty($item)) {
                throw new TransferFileException("Detalle de la Orden no encontrada");
            }

            $newNameFolder = "#" . $ticket['num_ticket'] . " - " . $cliente . ' - ' . $numZohoOrden;

            $tokenWD = $this->tokenService->getWorkDriveToken();

            $folderIdFinalizada = $this->zohoWDService->createFolder($tokenWD, $newNameFolder, 'TICKETS_FINALIZADOS');

            if (!empty($item->getFiles()->getFileIdImpresion())) {
                $fileIdImpresion = $this->zohoWDService->copyFolder($tokenWD, [
                    'folderSourceId' => $item->getFiles()->getFileIdImpresion(),
                    'folderTargetId' => $folderIdFinalizada,
                ]);
                $item->getFiles()->setFileIdImpresion($fileIdImpresion);
            }

            if (!empty($item->getFiles()->getFileIdElectrocorte())) {
                $fileIdElectrocorte = $this->zohoWDService->copyFolder($tokenWD, [
                    'folderSourceId' => $item->getFiles()->getFileIdElectrocorte(),
                    'folderTargetId' => $folderIdFinalizada,
                ]);
                $item->getFiles()->setFileIdElectrocorte($fileIdElectrocorte);
            }

            $this->zohoWDService->deleteFolders($tokenWD, [
                $item->getFiles()->getFolderId()
            ], true);

            $item->getFiles()->setFolderId($folderIdFinalizada);

            $ticketsResponse[] = [
                'id_ticket' => $ticket['id_ticket'],
                'num_ticket' => $ticket['num_ticket'],
                'line_item_id' => $ticket['line_item_id'],
                'id_carpeta' => $folderIdFinalizada,
                'maquina' => $item->getProducto()->getMaquina(),
            ];

        }

        $response['tickets'] = $ticketsResponse;

        $this->em->flush();

        return $response;

    }

}