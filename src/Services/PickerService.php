<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class PickerService
{

    private $client;
    private $token;
    private $urlRequest;

    public function __construct(HttpClientInterface $client, $token, $urlRequest)
    {
        $this->client     = $client;
        $this->token      = $token;
        $this->urlRequest = $urlRequest;
    }


    public function preCheckout($datos)
    {
        $response = $this->client->request(
            'POST',
            $this->urlRequest . '/preCheckout',
            [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Authorization' => 'Bearer ' . $this->token
                ],
                'json' => [
                    'latitude'  => $datos['latitude'],
                    'longitude' => $datos['longitude'],
                ]
            ]
        );
        

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        if (!isset($content['data']['deliveryFee'])) {
            return null;
        }

        return $content['data']['deliveryFee'];
    }
}
