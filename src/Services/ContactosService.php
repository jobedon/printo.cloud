<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ContactosService
{
    private $tokenService;
    private $zohoCrmService;
    private $session;

    public function __construct(
        TokenService $tokenService,
        ZohoCrmService $zohoCrmService,
        SessionInterface $session
    ) {
        $this->tokenService     = $tokenService;
        $this->zohoCrmService   = $zohoCrmService;
        $this->session          = $session;
    }

    public function obtenerContactos()
    {

        if ($this->session->has('contactos')) {
            return $this->session->get('contactos');
        }

        $userCrmZoho = $this->session->get('userCrmZoho');
        $token = $this->tokenService->getCrmToken();
        $contactos = $this->zohoCrmService->getContactosXUser($token, [
            'crmUserId' => $userCrmZoho['id']
        ]);

        $contactosFin = [];
        foreach ($contactos as $contacto) {

            $direccionesXContact = $this->zohoCrmService->getDireccionesXContact($token, [
                'contactId' => $contacto['id']
            ]);

            
            $direcciones = [];

            if (!empty($direccionesXContact)) {
                
                foreach ($direccionesXContact as $direccXContact) {
                    $direccion = $this->zohoCrmService->getDireccionXId($token, [
                        'direccionId' => $direccXContact['DireccionesEnvioVsContactosEnvio']['id']
                    ]);
                    $direcciones[$direccion['id']] = [
                        'id' => $direccion['id'],
                        'contactoId' => $contacto['id'],
                        'Name' => $direccion['Name'],
                        'Provincia_Destino' => $direccion['Provincia_Destino'],
                        'Ciudad_Destino1' => $direccion['Ciudad_Destino1'],
                        'Coordenadas' => $direccion['Coordenadas']
                    ];
                }
            }

            $contactosFin[$contacto['id']] = [
                'id' => $contacto['id'],
                'Name' => $contacto['Name'],
                'Apellidos_de_Contacto' => $contacto['Apellidos_de_Contacto'],
                'Celular' => $contacto['Celular'],
                'Identificaci_n' => $contacto['Identificaci_n'],
                'direcciones' => $direcciones
            ];
        }

        return $contactosFin;
    }

    public function saveContacto($parametros)
    {
        $userCrmZoho = $this->session->get('userCrmZoho');
        $parametros['crmUserId'] = $userCrmZoho['id'];
        $token = $this->tokenService->getCrmToken();
        $contacto = $this->zohoCrmService->newContactoPorUser($token, $parametros);
 
        $contactos = $this->obtenerContactos();

        $contactoNuevo = [
            'id'                    => $contacto['id'],
            'Name'                  => $parametros['Name'],
            'Apellidos_de_Contacto' => $parametros['Apellidos_de_Contacto'],
            'Celular'               => $parametros['Celular'],
            'Identificaci_n'        => $parametros['Identificaci_n'],
            'direcciones'           => []
        ];

        //array_unshift($contactos, $contactoNuevo);
        $contactos[$contacto['id']] = $contactoNuevo;
        $this->session->set('contactos', $contactos);

        return $contacto;
    }

    public function actualizarContacto($parametros)
    {
        $token = $this->tokenService->getCrmToken();
        $contacto = $this->zohoCrmService->updateContactoXId($token, $parametros);

        $contactos = $this->obtenerContactos();

        $contactos[$parametros['contactoId']]['Name'] = $parametros['Name'];
        $contactos[$parametros['contactoId']]['Apellidos_de_Contacto'] = $parametros['Apellidos_de_Contacto'];
        $contactos[$parametros['contactoId']]['Celular'] = $parametros['Celular'];
        $contactos[$parametros['contactoId']]['Identificaci_n'] = $parametros['Identificaci_n'];

        $this->session->set('contactos', $contactos);

        return $contacto;

    }

    public function deleteContacto($parametros)
    {
        $token = $this->tokenService->getCrmToken();
        $this->zohoCrmService->deleteContactoXId($token, [
            'contactoId' => $parametros['contactoId']
        ]);

        $contactos = $this->obtenerContactos();

        unset($contactos[$parametros['contactoId']]);
        $this->session->set('contactos', $contactos);

    }


}
