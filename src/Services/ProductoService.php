<?php

namespace App\Services;

use App\Entity\Configuraciones;
use App\Entity\OrdenCab;
use App\Entity\OrdenDet;
use App\Entity\Producto;
use App\Entity\ProductoFavorito;
use App\Entity\UserPi;
use App\Repository\OrdenDetRepository;
use App\Repository\ProductoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;
use Twig\Environment;

class ProductoService
{

    private $em;
    private $session;
    private $security;
    private $twig;
    private $slugger;
    private $imagesDirectory;

    public function __construct(
        EntityManagerInterface $em,
        SessionInterface $session,
        Security $security,
        Environment $twig,
        SluggerInterface $slugger,
        $imagesDirectory
    ) {
        $this->em       = $em;
        $this->session  = $session;
        $this->security = $security;
        $this->twig     = $twig;
        $this->slugger  = $slugger;

        $this->imagesDirectory = $imagesDirectory;
    }

    public function getConfigProducto($tipoProducto, $producto)
    {
        $config = [
            'MIMAKI' => null,
            'XEROX'  => null
        ];

        if ($tipoProducto == "MIMAKI") {
            $config['MIMAKI'] = $this->getConfigMimaki($producto);
        } elseif ($tipoProducto == "XEROX") {
            $config['XEROX'] = $this->getConfigXerox($producto);
        }

        return $config;
    }

    public function getConfigXerox($producto)
    {
        $caracteristicas = [];
        $caracteristicasProducto = $producto->getProductoCaracteristicas();
        foreach ($caracteristicasProducto as $carac) {
            $caracteristicas[$carac->getCaracteristica()->getNombre()] = $carac->getValor();
        }

        return [
            'caracteristicas' => $caracteristicas
        ];
    }

    public function getConfigMimaki($producto)
    {
        $corteRecto   = false;
        $electrocorte = false;
        $laminar      = false;
        $ojales       = false;

        $categoria = $producto->getCategoriaProducto()->getCategoria();

        if (in_array($categoria, ['Rigidos'])) {
            $corteRecto = true;
        }

        if (in_array($categoria, ['Viniles'])) {
            $electrocorte = true;
        }

        if (in_array($categoria, ['Viniles', 'Rigidos'])) {
            $laminar = true;
        }

        if (
            !in_array($categoria, ['Viniles', 'Rigidos']) ||
            (in_array($categoria, ['Rigidos']) &&
                str_contains($producto->getNombreProducto(), 'Sintra'))
        ) {
            $ojales = true;
        }

        $caracteristicas = [];
        $caracteristicasProducto = $producto->getProductoCaracteristicas();
        foreach ($caracteristicasProducto as $carac) {
            $caracteristicas[$carac->getCaracteristica()->getNombre()] = $carac->getValor();
        }

        return [
            'corteRecto'      => $corteRecto,
            'electrocorte'    => $electrocorte,
            'laminar'         => $laminar,
            'ojales'          => $ojales,
            'caracteristicas' => $caracteristicas
        ];
    }

    public function changeProductFavorite($parametros)
    {
        $productoId = $parametros['productoId'];

        $user = $this->security->getUser();

        $proFavRepo = $this->em->getRepository(ProductoFavorito::class);

        if (empty($productoId)) {
            return ['status' => 'error'];
        }

        $proRepo = $this->em->getRepository(Producto::class);

        $producto = $proRepo->find($productoId);

        $favorito = $proFavRepo->findOneBy([
            'producto' => $producto,
            'user'     => $user
        ]);

        if (empty($favorito)) {
            $productoFavorito = new ProductoFavorito();
            $productoFavorito->setProducto($producto);
            $productoFavorito->setUser($user);
            $this->em->persist($productoFavorito);
        } else {
            $estado = ($favorito->getEstado() == 'A') ? 'I' : 'A';
            $favorito->setFechaActualizacion(new \DateTime("now"));
            $favorito->setEstado($estado);
        }

        $this->em->flush();
    }

    public function editProduct($parametros)
    {
        $idItem = $parametros['idItem'];

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $ordenDetRep = $this->em->getRepository(OrdenDet::class);
        /** @var OrdenDet|null $ordenDet */
        $ordenDet = $ordenDetRep->find($idItem);

        if ($ordenDet->getOrdenCab()->getUser()->getId() != $user->getId()) {
            return [
                'error' => true,
                'msg'   => 'Transaccion no permitida'
            ];
        }

        return [
            'id'            => $ordenDet->getId(),
            'categoriaId'   => $ordenDet->getProducto()->getProducto()->getId(),
            'categoriaName' => $ordenDet->getProducto()->getProducto()->getNombreProducto(),
        ];
    }

    public function getMateriales()
    {
        $user = $this->security->getUser();
        $prodRepo = $this->em->getRepository(Producto::class);

        $materiales = $prodRepo->getMaterialesPorProducto($user);
        $favoritos  = $prodRepo->getMaterialesFavoritos($user);

        $materialesFavoritos = array_merge($favoritos, $materiales);

        $materialesCateg = [];
        foreach ($materialesFavoritos as $material) {
            $materialesCateg[$material['categoria']][$material['tipo']][] = [
                'nombreProducto' => $material['nombreProducto'],
                'sku'            => $material['sku'],
                'idProducto'     => $material['id']
            ];
        }

        return $materialesCateg;
    }

    public function obtenerInfoProducto($parametros)
    {
        $idProducto = $parametros['idProducto'];
        $idItem     = $parametros['idItem'];

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $item = [];
        if (!empty($idProducto)) {
            /**@var null|ProductoRepository $prodRepo */
            $prodRepo = $this->em->getRepository(Producto::class);
            $producto = $prodRepo->find($idProducto);
            $categoria = $producto->getNombreProducto();
        } else {
            /**@var null|OrdenDetRepository $ordenDetRepo*/
            $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
            $ordenDet = $ordenDetRepo->find($idItem);

            if ($ordenDet->getOrdenCab()->getUser()->getId() != $user->getId()) {
                return [
                    'error' => true,
                    'msg'   => 'Transaccion no permitida'
                ];
            }
            $item = [
                'id'         => $ordenDet->getId(),
                'idProducto' => $ordenDet->getProducto()->getId()
            ];
            $producto = $ordenDet->getProducto();
            $categoria = $producto->getProducto()->getNombreProducto();
        }

        $materiales = $this->session->get('materiales');

        return [
            'productos' => $materiales[$categoria],
            'categoria' => $idProducto,
            'item'      => $item
        ];
    }

    /**
     * @deprecated
     */
    public function changeCategory($parametros)
    {
        $categoria = $parametros['categoria'];
        $idItem    = $parametros['idItem'];

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $item = [];

        if (!empty($idItem)) {
            $ordenDetRep = $this->em->getRepository(OrdenDet::class);
            /** @var OrdenDet|null $ordenDet */
            $ordenDet = $ordenDetRep->find($idItem);

            if ($ordenDet->getOrdenCab()->getUser()->getId() != $user->getId()) {
                return [
                    'error' => true,
                    'msg'   => 'Transaccion no permitida'
                ];
            }
            $item = [
                'id'         => $ordenDet->getId(),
                'idProducto' => $ordenDet->getProducto()->getId()
            ];
        }

        $materiales = $this->session->get('materiales');

        return [
            'productos' => $materiales[$categoria],
            'categoria' => $categoria,
            'item'      => $item
        ];
    }


    public function cambioMaterial($parametros)
    {
        $nombreProducto = $parametros["nombreProducto"];
        $idMaterial     = $parametros["idMaterial"];
        $idItem         = $parametros['idItem'];
        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $proRepo = $this->em->getRepository(Producto::class);
        $producto = $proRepo->find($idMaterial);

        $proFavRepo = $this->em->getRepository(ProductoFavorito::class);

        $favorito = $proFavRepo->findOneBy([
            'producto' => $producto,
            'user'     => $user,
        ]);

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);

        if ($producto->getMaquina() == 'XEROX') {
            $extAllowImpresion = $confRepo->getConfigString('FILES_ALLOW_XEROX_IMPRESION');
            $extAllowElectroco = $confRepo->getConfigString('FILES_ALLOW_XEROX_ELECTROCORTE');
        } elseif ($producto->getMaquina() == 'MIMAKI') {
            $extAllowImpresion = $confRepo->getConfigString('FILES_ALLOW_MIMAKI_IMPRESION');
            $extAllowElectroco = $confRepo->getConfigString('FILES_ALLOW_MIMAKI_ELECTROCORTE');
        }

        $item = [];
        if (!empty($idItem)) {
            $ordenDetRep = $this->em->getRepository(OrdenDet::class);
            $ordenDet = $ordenDetRep->find($idItem);

            if ($ordenDet->getOrdenCab()->getUser()->getId() != $user->getId()) {
                return [
                    'error' => true,
                    'msg'   => 'Transaccion no permitida'
                ];
            }
            $item = $ordenDet->getArrayItem();
        }


        $features = $this->getConfigProducto($producto->getMaquina(), $producto);

        $htmlProducto = $this->obtenerTemplatePorProducto([
            'producto'          => $producto,
            'extAllowImpresion' => $extAllowImpresion,
            'extAllowElectroco' => $extAllowElectroco,
            'item'              => $item,
            'features'          => $features,
        ]);


        return [
            'htmlProducto' => $htmlProducto,
            // ELiminar
            'producto'          => $producto,
            'extAllowImpresion' => $extAllowImpresion,
            'extAllowElectroco' => $extAllowElectroco,
            'item'              => $item,
            'features'          => $features,
            // Eliminar
            'favorito'     => $favorito,
        ];
    }

    public function obtenerTemplatePorProducto($parametros)
    {
        $producto = $parametros['producto'];

        // Parametros para Xerox y Mimaki
        $extAllowImpresion = $parametros['extAllowImpresion'];
        $extAllowElectroco = $parametros['extAllowElectroco'];
        $item              = $parametros['item'];
        $features          = $parametros['features'];

        $html = "";
        switch ($producto->getProductoFinal()) {
            case 'Formatos Super A3':
                $html = $this->twig->render('admin/catalogo/productos/forms/xerox.html.twig', [
                    'producto'          => $producto,
                    'extAllowImpresion' => $extAllowImpresion,
                    'extAllowElectroco' => $extAllowElectroco,
                    'item'              => $item,
                    'features'          => $features,
                ]);
                break;
            case 'Gigantografias':
            case 'Gigantografías':
                $html = $this->twig->render('admin/catalogo/productos/forms/mimaki.html.twig', [
                    'producto'          => $producto,
                    'extAllowImpresion' => $extAllowImpresion,
                    'extAllowElectroco' => $extAllowElectroco,
                    'item'              => $item,
                    'features'          => $features,
                ]);
                break;

            default:
                break;
        }

        return $html;
    }

    public function getDescripcionProducto($parametros)
    {
        $alias         = $parametros['alias'];
        $producto      = $parametros['producto'];

        $tipoCorte     = $parametros['tipoCorte'];
        $tipoLaminado  = $parametros['tipoLaminado'];
        $tipoImpresion = $parametros['tipoImpresion'];
        $observacion   = $parametros['observacion'];

        $cantOjales    = $parametros['cantOjales'];
        $ancho         = $parametros['ancho'];
        $alto          = $parametros['alto'];

        $laminar       = $parametros['laminar'];
        $ojales        = $parametros['ojales'];
        $electrocorte  = $parametros['electrocorte'];
        $corteRigido   = $parametros['corteRigido'];

        $delimeter = "\n";
        $descripcion = '';

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);

        if ($producto->getMaquina() == 'XEROX') {
            $mapTipoCorte = $confRepo->getConfigMapping($tipoCorte);
            $descripcion .= 'Corte : ' . $mapTipoCorte . $delimeter;
            if ($laminar) {
                $mapLaminado = $confRepo->getConfigMapping($tipoLaminado);
                $descripcion .= 'Laminado : ' . $mapLaminado . $delimeter;
            }
            $mapTipoImpresion = $confRepo->getConfigMapping($tipoImpresion);
            $descripcion .= 'Impresion : ' . $mapTipoImpresion . $delimeter;
        } elseif ($producto->getMaquina() == 'MIMAKI') {
            $descripcion .= 'Medida: ' . $ancho . ' Ancho x ' . $alto . ' Alto' . $delimeter;
            if ($electrocorte) {
                $descripcion .= 'Requiere Electrocorte' . $delimeter;
            }
            if ($ojales) {
                $strOjales = ($cantOjales == 1) ? 'Ojal' : 'Ojales';
                $descripcion .= 'Requiere ' . $cantOjales . ' ' . $strOjales . $delimeter;
                //$descripcion .= 'Nota : ' . $especifojales . $delimeter;
            }
            if ($laminar) {
                $mapLaminado = $confRepo->getConfigMapping($tipoLaminado);
                $descripcion .= 'Laminado : ' . $mapLaminado . $delimeter;
            }
            if ($corteRigido) {
                $descripcion .= 'Requiere Corte Recto' . $delimeter;
            }
        }
        if (!empty($alias)) {
            $descripcion .= 'Referencia : ' .  $alias . $delimeter;
        }
        $descripcion .= 'Comentarios : ' . $observacion . $delimeter;


        return $descripcion;
    }


    public function uploadImgProd($parametros)
    {

        $file = $parametros['file'];
        $idDetalle = $parametros['idDetalle'];

        if ($file) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->imagesDirectory,
                    $newFilename
                );

                /** Se actualiza Item en el Detalle de la Orden */
                /**@var null|OrdenDetRepository $ordenDetRepo */
                $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
                $ordenDet     = $ordenDetRepo->find($idDetalle);

                $ordenDet->setImgPathRef("/" . $this->imagesDirectory . $newFilename);
                $this->em->flush();

                return $ordenDet->getImgPathRef();
            } catch (FileException $e) {
                dump($e);
            }
        }

        return;
    }
}
