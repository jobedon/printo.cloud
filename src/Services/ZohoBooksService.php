<?php

namespace App\Services;

use App\Exceptions\OrdenException;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoBooksService
{
    private $client;

    private $urlToken;

    private $urlRequest;

    private $clientId;

    private $secretId;

    private $refreshToken;

    private $env;

    private $branchId;

    private $appEnvTransfer;

    private $logger;

    private $orgId;

    public function __construct(
        HttpClientInterface $client,
        LoggerInterface $logger,
        $appEnvTransfer,
        $urlToken,
        $clientId,
        $secretId,
        $refreshToken,
        $urlRequest,
        $appEnv,
        $orgId,
        $branchId
    ) {
        $this->client         = $client;
        $this->urlToken       = $urlToken;
        $this->urlRequest     = $urlRequest;
        $this->clientId       = $clientId;
        $this->secretId       = $secretId;
        $this->refreshToken   = $refreshToken;
        $this->appEnvTransfer = $appEnvTransfer;
        $this->env            = $appEnv;
        $this->logger         = $logger;
        $this->orgId          = $orgId;
        $this->branchId       = $branchId;
    }

    public function getToken()
    {

        $response = $this->client->request(
            'POST',
            $this->urlToken,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => [
                    'refresh_token' => $this->refreshToken,
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secretId,
                    'grant_type'    => 'refresh_token',
                ]
            ]
        );

        $content = $response->toArray();

        return $content['access_token'];
    }

    public function getAllItem($token, $page)
    {
        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/items",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => [
                    'organization_id' => $this->orgId,
                    'page'            => $page
                ]
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content;
    }

    public function getUserBookFromCrm($token, $accountId)
    {
        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/contacts",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => [
                    'organization_id' => $this->orgId,
                    'zcrm_account_id' => $accountId,
                ]
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['contacts'][0];
    }

    public function getContactById($token, $contactId)
    {
        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/contacts/" . $contactId,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => [
                    'organization_id' => $this->orgId,
                ]
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['contact'];
    }

    public function generarOrden($token, $datos)
    {
        $method = "POST";
        $url = $this->urlRequest . "/salesorders";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'organization_id' => $this->orgId,
            ],
            'json' => [
                'branch_id'           => $this->branchId,
                'customer_id'         => $datos['customerId'],
                'customer_name'       => $datos['contactName'],
                "status"              => "open",
                // 'shipping_address_id' => $datos['direccionId'],
                'line_items'          => $datos['items'],
                'payment_terms'       => $datos['payment_terms'],
                'payment_terms_label' => $datos['payment_terms_label'],
                'custom_fields' => [
                    // [
                    //     'customfield_id' => '2244816000001772052', // Tipo de Envío cf_proveedor_de_env_o (ANTIGUO)
                    //     'value' => $datos['requiereEnvio'] // Retiro en Oficina, Picker Programado, Picker Inmediato, Otros
                    // ],
                    // [
                    //     'customfield_id' => '2244816000001877477', // Efectivo Picker Contra entrega cf_pago_contra_entrega_en_efec (ANTIGUO)
                    //     'value' => $datos['pagoMotorizado'] // SI - MAXIMO $50 USD, NO
                    // ],
                    // [
                    //     'customfield_id' => '2244816000000405012', // Crear Ticket cf_crear_ticket (ANTIGUO)
                    //     'value' => $datos['crearTicket'] // SI, NO
                    // ],
                    // [
                    //     'customfield_id' => '2244816000010235100', // Asignación de Ticket cf_asignaci_n_de_ticket (ANTIGUO)
                    //     'value' => 'Printo - Web' // Kevin Hernández, Kimberly Guzmán, Lorena Zambrano, ...
                    // ],
                    [
                        'customfield_id' => '2995742000000462074', // cf_vendedor_principal
                        'value' => 'Printo.Cloud'
                    ],
                    [
                        'customfield_id' => '2995742000000535982', // cf_ambiente
                        'value' => $this->appEnvTransfer
                    ],
                    // [
                    //     'customfield_id' => '2244816000003021013', // Sistema Externo cf_sistema_externo (ANTIGUO)
                    //     'value' => ($this->appEnableTransfer == 'true') ? '1' : '0' // 1, 0
                    // ],
                    [
                        'customfield_id' => '2995742000001894800', // Printo FP cf_printo_fp (ANTIGUO)
                        'value' => $datos['formapago'] // Efectivo al Mensajero, Pago en Oficina, Credito Directo, Datafast
                    ]
                ]
            ]
        ];


        if (isset($datos['direccionId']) && !empty($datos['direccionId'])) {
            $options['json']['shipping_address_id'] = $datos['direccionId'];
        } 
        
        $this->logger->info("TRACKING >>> generarOrden METHOD : " . $method . " | URL : " .  $url);
        $this->logger->info("TRACKING >>> generarOrden OPTIONS : " . json_encode($options));

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            throw new OrdenException($content["message"], OrdenException::CODE_ERROR_ZOHO_BOOKS);
        }

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['salesorder'];
    }

    public function getAllOrdenesPorCliente($token, $parametros)
    {

        $query = [
            'organization_id' => $this->orgId,
            'customer_id'     => $parametros['customer_id'],
            'status'          => $parametros['status']
        ];

        if (isset($parametros['cf_printo_fp'])) {
            $query['cf_printo_fp'] = $parametros['cf_printo_fp'];
        }

        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/salesorders",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => $query
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content;
    }

    public function getUnpaidInvoices($token, $parametros)
    {
        $parametros['status'] = 'unpaid';
        return $this->getInvoicesByStatus($token, $parametros);
    }

    public function getInvoicesByStatus($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/invoices";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'organization_id' => $this->orgId,
                'customer_id' => $parametros['customer_id'],
                'status' => $parametros['status'],
                'sort_column' => 'created_time',
                'sort_order'  => 'A'
            ]
        ];

        $response = $this->client->request($method, $url, $options);

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            throw new OrdenException($content["message"], OrdenException::CODE_ERROR_ZOHO_BOOKS);
        }

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['invoices'];

    }

    public function addFileToOrder($token, $parametros)
    {
        $orderId  = $parametros['orderId'];
        $filePath = $parametros['filePath'];
        $filePath = ($filePath[0] == '/') ? substr($filePath,1) : $filePath;

        $formFields = [
            'attachment' => DataPart::fromPath($filePath),
        ];

        $formData = new FormDataPart($formFields);
        $headerFile = $formData->getPreparedHeaders()->toArray();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
        ];
        $headers = array_merge($headers, $headerFile);

        $method = "POST";
        $url = $this->urlRequest . "/salesorders/" . $orderId . "/attachment";
        $options = [
            'headers' => $headers,
            'query' => [
                'organization_id' => $this->orgId,
            ],
            'body' => $formData->bodyToIterable(),
        ];

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        return;
    }

    public function confirmarOrden($token, $parametros)
    {
        $orderId  = $parametros['orderId'];

        $method = "POST";
        $url = $this->urlRequest . "/salesorders/" . $orderId . "/status/open";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'organization_id' => $this->orgId,
            ]
        ];

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        return;
    }

    public function payInvoices($token, $parametros)
    {
        $method = "POST";
        $url = $this->urlRequest . "/customerpayments";
        $json = [
            'customer_id' => $parametros['customerId'],
            'payment_mode' => 'creditcard',
            'amount' => $parametros['amount'],
            'date' => date('Y-m-d'),
            'reference_number' => $parametros['referenceNumber'],
            'description' => 'Pago realizado desde link de pagos',
            'invoices' => $parametros['invoices'],
            'exchange_rate' => '1',
            'bank_charges' => '0',
            'custom_fields' => []            
        ];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'organization_id' => '713920220',
            ],
            'json' => $json
        ];
        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            return null;
            //throw new OrdenException($content["message"], OrdenException::CODE_ERROR_ZOHO_BOOKS);
        }

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content;
    }
}
