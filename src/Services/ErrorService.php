<?php

namespace App\Services;

use App\Entity\AppError;
use Doctrine\ORM\EntityManagerInterface;

class ErrorService
{

    private $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
    }


    public function saveError($parametros)
    {

        $codigo = ($parametros['cod']) ? $parametros['cod'] : 404;

        $appError = new AppError();
        $appError->setAplicacion($parametros['app']);
        $appError->setProceso($parametros['proceso']);
        $appError->setCodigo($codigo);
        $appError->setError($parametros['error']);

        $this->em->persist($appError);
        $this->em->flush();

    }


}