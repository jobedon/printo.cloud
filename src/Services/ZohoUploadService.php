<?php

namespace App\Services;

use App\Exceptions\TransferFileException;
use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoUploadService
{
    private $client;

    private $urlToken;

    private $urlRequest;

    private $clientId;

    private $secretId;

    private $refreshToken;

    private $parentIdXerox;

    private $parentIdMimaki;

    private $parentIdElectrocorte;

    private $uploadDirectory;

    public function __construct(
        HttpClientInterface $client,
        $urlToken,
        $clientId,
        $secretId,
        $refreshToken,
        $urlRequest,
        $parentIdXerox,
        $parentIdMimaki,
        $parentIdElectrocorte,
        $uploadDirectory
    ) {
        $this->client               = $client;
        $this->urlToken             = $urlToken;
        $this->urlRequest           = $urlRequest;
        $this->clientId             = $clientId;
        $this->secretId             = $secretId;
        $this->refreshToken         = $refreshToken;
        $this->parentIdXerox        = $parentIdXerox;
        $this->parentIdMimaki       = $parentIdMimaki;
        $this->parentIdElectrocorte = $parentIdElectrocorte;
        $this->uploadDirectory      = $uploadDirectory;
    }

    public function getToken()
    {

        $response = $this->client->request(
            'POST',
            $this->urlToken,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => [
                    'refresh_token' => $this->refreshToken,
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secretId,
                    'grant_type'    => 'refresh_token',
                ]
            ]
        );

        $content = $response->toArray();

        return $content['access_token'];
    }

    public function createFolder($token, $folderName, $parent)
    {
        
    }

    public function uploadFile($token, $pathFile, $parentId)
    {
        
    }
}
