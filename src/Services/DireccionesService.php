<?php

namespace App\Services;

use App\Entity\Direccion;
use App\Entity\UserPi;
use App\Repository\ConfiguracionesRepository;
use App\Services\Externo\ServientregaService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use NumberFormatter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\User;

class DireccionesService
{
    private $tokenService;

    private $zohoCrmService;

    private $zohoBooksService;

    private $pickerService;

    private $em;

    private $security;

    private $session;

    private $seService;

    private $contactosService;

    public function __construct(
        TokenService $tokenService,
        ZohoCrmService $zohoCrmService,
        ZohoBooksService $zohoBooksService,
        PickerService $pickerService,
        EntityManagerInterface $em,
        Security $security,
        SessionInterface $session,
        ServientregaService $seService,
        ContactosService $contactosService
    ) {
        $this->tokenService     = $tokenService;
        $this->zohoCrmService   = $zohoCrmService;
        $this->zohoBooksService = $zohoBooksService;
        $this->pickerService    = $pickerService;
        $this->em               = $em;
        $this->security         = $security;
        $this->session          = $session;
        $this->seService        = $seService;
        $this->contactosService = $contactosService;
    }

    public function getContactZohoBook()
    {
        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $accountId = $user->getCrmUserId();

        $token = $this->tokenService->getBooksToken();
        $contact = $this->zohoBooksService->getUserBookFromCrm($token, $accountId);

        $contact = $this->zohoBooksService->getContactById($token, $contact['contact_id']);

        return $contact;
    }

    /**
     * @deprecated
     */
    public function saveDireccionOld($datos)
    {
        $token = $this->tokenService->getCrmToken();
        $idRelacionDireccion = $this->zohoCrmService->insertDireccionCrmBook($token, $datos);
        return $idRelacionDireccion;
    }

    public function saveDireccion($parametros)
    {
        $userCrmZoho = $this->session->get('userCrmZoho');
        $parametros['crmUserId'] = $userCrmZoho['id'];
        $token = $this->tokenService->getCrmToken();
        $direccion = $this->zohoCrmService->newDireccionPorUser($token, $parametros);


        $this->zohoCrmService->newDireccXContactXId($token, [
            'crmUserId' => $userCrmZoho['id'],
            'direccionId' => $direccion['id'],
            'contactoId' => $parametros['contactoId']
        ]);

        $contactos = $this->contactosService->obtenerContactos();

        $nuevaDireccion = [
            'id'                => $direccion['id'],
            'contactoId'        => $parametros['contactoId'],
            'Name'              => $parametros['Name'],
            'Coordenadas'       => $parametros['Coordenadas'],
            'Provincia_Destino' => $parametros['Provincia_Destino'],
            'Ciudad_Destino1'   => $parametros['Ciudad_Destino1']
        ];
        $contactos[$parametros['contactoId']]['direcciones'][$direccion['id']] = $nuevaDireccion;
        //array_unshift($contactos[ $parametros['contactoId']]['direcciones'], $nuevaDireccion);
        $this->session->set('contactos', $contactos);

        return $direccion;

    }

    public function actualizarDireccion($parametros)
    {
        $token = $this->tokenService->getCrmToken();
        $direccion = $this->zohoCrmService->updateDireccionXId($token, $parametros);

        $contactos = $this->contactosService->obtenerContactos();

        $contactos[$parametros['contactoId']]['direcciones'][$parametros['direccionId']]['Name'] = $parametros['Name'];
        $contactos[$parametros['contactoId']]['direcciones'][$parametros['direccionId']]['Coordenadas'] = $parametros['Coordenadas'];
        $contactos[$parametros['contactoId']]['direcciones'][$parametros['direccionId']]['Provincia_Destino'] = $parametros['Provincia_Destino'];
        $contactos[$parametros['contactoId']]['direcciones'][$parametros['direccionId']]['Ciudad_Destino1'] = $parametros['Ciudad_Destino1'];

        $this->session->set('contactos', $contactos);

        return $direccion;
    }

    /**
     * Obtiene la direccion de los usuarios
     * Puede ser de zoho o base interna
     * Falta definir por cxhernan y kevin
     */
    public function obtenerDireccion()
    {
        $userBooksZoho = $this->session->get('userBooksZoho');
        $direcciones = (isset($userBooksZoho['addresses']))? $userBooksZoho['addresses'] : null;

        if (empty($direcciones)) {
            return null;
        }
        
        return "";
    }

    /**
     * @deprecated
     */
    public function searchDireccion($idDireccion)
    {
        $userBooksZoho = $this->session->get('userBooksZoho');
        $direcciones = $userBooksZoho['addresses'];

        foreach ($direcciones as $dir) {
            if ($dir['address_id'] == $idDireccion) {
                return $dir;
            }
        }

        return null;
    }

    /**
     * @deprecated
     */
    public function searchDireccionPorNombre($address)
    {
        $userBooksZoho = $this->session->get('userBooksZoho');
        $direcciones = $userBooksZoho['addresses'];

        foreach ($direcciones as $dir) {
            if ($dir['address'] == $address) {
                return $dir;
            }
        }

        return null;
    }

    public function getCiudades()
    {
        $ciudades = $this->seService->getCiudades();
        $provincias = [];
        foreach ($ciudades as $ciudad) {
            $nombre = $ciudad['nombre'];
            $posIni = strrpos($nombre, "(");
            $posFin = strrpos($nombre, ')');
            $posIni2 = strpos($nombre, "(");
            $provincia = trim(substr($nombre,  $posIni + 1, $posFin - $posIni - 1));
            $nombreCiudad = trim(substr($nombre, 0, $posIni2));
            $provincias[$provincia][] = [
                //"id" => $ciudad['id'],
                "id" => $nombreCiudad,
                'text' => $nombreCiudad
            ];
        }
        return $provincias;
    }

    public function getDirecciones($parametros = [])
    {
        $contactos = $this->contactosService->obtenerContactos();
        if (empty($parametros)) {
            return $contactos;
        }
        return $contactos[$parametros['contactoId']];
    }

    public function deleteDireccion($parametros)
    {
        $token = $this->tokenService->getCrmToken();
        $this->zohoCrmService->deleteDireccionXId($token, [
            'direccionId' => $parametros['direccionId']
        ]);
        $contactos = $this->contactosService->obtenerContactos();
        unset($contactos[$parametros['contactoId']]['direcciones'][$parametros['direccionId']]);
        $this->session->set('contactos', $contactos);

    }

}
