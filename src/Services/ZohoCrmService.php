<?php

namespace App\Services;

use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoCrmService
{
    private $client;

    private $urlToken;

    private $urlRequest;

    private $clientId;

    private $secretId;

    private $refreshToken;

    public function __construct(HttpClientInterface $client, $urlToken, $clientId, $secretId, $refreshToken, $urlRequest)
    {
        $this->client       = $client;
        $this->urlToken     = $urlToken;
        $this->urlRequest   = $urlRequest;
        $this->clientId     = $clientId;
        $this->secretId     = $secretId;
        $this->refreshToken = $refreshToken;
    }

    public function getToken()
    {

        $response = $this->client->request(
            'POST',
            $this->urlToken,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => [
                    'refresh_token' => $this->refreshToken,
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secretId,
                    'grant_type'    => 'refresh_token',
                ]
            ]
        );

        $content = $response->toArray();

        return $content['access_token'];
    }

    public function getUserByRuc($RUC, $token)
    {
        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/accounts/search",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => [
                    'criteria' => '(RUC:equals:' . $RUC . ')',
                ]
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['data'][0];
    }

    public function insertDireccionCrmBook($token, $datos)
    {
        $direccionId = $this->insertDireccion($token, $datos);
        $datos['address_id'] = $direccionId;
        $relacionDireccionId = $this->insertClienteXDireccion($token, $datos);
        return $relacionDireccionId;
    }

    public function insertDireccion($token, $datos)
    {
        $response = $this->client->request(
            'POST',
            $this->urlRequest . "/direcciones",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'data' => [
                        [
                            'Name'        => $datos['address_name'],
                            'direcciones' => $datos['account_name'],
                            'Coordenadas' => $datos['coordenadas'],
                        ]
                    ]
                ]
            ]
        );

        $tmp = ['data' => [
            [
                'Name'        => $datos['address_name'],
                'direcciones' => $datos['account_name'],
                'Coordenadas' => $datos['coordenadas'],
            ]
        ]];

        $tmp2 = json_encode($tmp);

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        if (!isset($content['data'][0]['details']['id'])) {
            return null;
        }

        return $content['data'][0]['details']['id'];
    }

    public function insertClienteXDireccion($token, $datos)
    {
        $response = $this->client->request(
            'POST',
            $this->urlRequest . "/Clientes_X_Direcciones",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'data' =>
                    [
                        [
                            'direcciones' =>
                            [
                                'name' => $datos['account_name'],
                                'id'   => $datos['account_id'],
                            ],
                            'Direcciones_de_Envio' =>
                            [
                                'name' => $datos['address_name'],
                                'id'   => $datos['address_id'],
                            ],
                        ],
                    ]
                ]
            ]
        );
        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        if (!isset($content['data'][0]['details']['id'])) {
            return null;
        }

        return $content['data'][0]['details']['id'];
    }

    public function getContactByUser($accountId, $token)
    {
        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/contacts/search",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => [
                    'criteria' => '(Account_Name:equals:' . $accountId . ')',
                ]
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['data'][0];
    }

    /** CONTACTOS */

    public function getContactosXUser($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/Contactos_para_Envio/search";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'criteria' => '(Cliente_Principal.id:equals:' . $parametros['crmUserId'] . ')',
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            return [];
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'];
    }

    public function getContactoXId($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/Contactos_para_Envio" . $parametros['contactoId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0];
    }

    public function newContactoPorUser($token, $parametros)
    {
        $method = "POST";
        $url = $this->urlRequest . "/Contactos_para_Envio";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'data' => [
                    [
                        "Name" => $parametros['Name'],
                        "Apellidos_de_Contacto" => $parametros['Apellidos_de_Contacto'],
                        "Cliente_Principal" => [
                            "id" => $parametros['crmUserId']
                        ],
                        "Celular" => $parametros['Celular'],
                        "Identificaci_n" => $parametros['Identificaci_n']
                    ]
                ]
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0]['details'];
    }

    public function updateContactoXId($token, $parametros)
    {
        $method = "PUT";
        $url = $this->urlRequest . "/Contactos_para_Envio/" . $parametros['contactoId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'data' => [
                    [
                        "Name" => $parametros['Name'],
                        "Apellidos_de_Contacto" => $parametros['Apellidos_de_Contacto'],
                        "Celular" => $parametros['Celular'],
                        "Identificaci_n" => $parametros['Identificaci_n']
                    ]
                ]
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0]['details'];
    }

    public function deleteContactoXId($token, $parametros)
    {
        $method = "DELETE";
        $url = $this->urlRequest . "/Contactos_para_Envio/" . $parametros['contactoId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0]['details'];
    }

    /** DIRECCIONES */

    public function getDireccionesXUser($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/Direcciones_de_Env_o/search";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'criteria' => '(Cliente_Relacionado.id:equals:' . $parametros['crmUserId'] . ')',
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'];
    }

    public function getDireccionXId($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/Direcciones_de_Env_o/" . $parametros['direccionId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0];
    }

    public function newDireccionPorUser($token, $parametros)
    {
        $method = "POST";
        $url = $this->urlRequest . "/Direcciones_de_Env_o";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'data' => [
                    [
                        "Name" => $parametros['Name'],
                        "Provincia_Destino" => $parametros['Provincia_Destino'],
                        "Cliente_Relacionado" => [
                            "id" => $parametros['crmUserId']
                        ],
                        "Ciudad_Destino1" => $parametros['Ciudad_Destino1'],
                        "creado_o_editado_en_printo" => 1,
                        "Coordenadas" => $parametros['Coordenadas']
                    ]
                ]
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0]['details'];
    }

    public function updateDireccionXId($token, $parametros)
    {
        $method = "PUT";
        $url = $this->urlRequest . "/Direcciones_de_Env_o/" . $parametros['direccionId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'data' => [
                    [
                        "Name" => $parametros['Name'],
                        "Provincia_Destino" => $parametros['Provincia_Destino'],
                        "Ciudad_Destino1" => $parametros['Ciudad_Destino1'],
                        "creado_o_editado_en_printo" => 1,
                        "Coordenadas" => $parametros['Coordenadas']
                    ]
                ]
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0]['details'];
    }

    public function deleteDireccionXId($token, $parametros)
    {
        $method = "DELETE";
        $url = $this->urlRequest . "/Direcciones_de_Env_o/" . $parametros['direccionId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0]['details'];
    }

    /** RELACION CONTACTO X DIRECCIONES */

    public function getDireccXContactXUser($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/DireccXContacDeEnvio/search";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'criteria' => '(Direccion_relacionada_con.id:equals:' . $parametros['crmUserId'] . ')',
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'];
    }

    public function getDireccXContactXId($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/DireccXContacDeEnvio" . $parametros['direccXContacId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        $content = $response->toArray();
        if ($content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'];
    }

    public function getDireccionesXContact($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/DireccXContacDeEnvio/search";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'criteria' => '(Contacto_de_Envio_Relacionado.id:equals:' . $parametros['contactId'] . ')',
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            return null;
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'];
    }

    public function newDireccXContactXId($token, $parametros)
    {
        $method = "POST";
        $url = $this->urlRequest . "/DireccXContacDeEnvio";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'data' => [
                    [
                        "DireccionesEnvioVsContactosEnvio" => [
                            "id" => $parametros['direccionId']
                        ],
                        "Contacto_de_Envio_Relacionado" => [
                            "id" => $parametros['contactoId']
                        ],
                        "Direccion_relacionada_con" => [
                            "id" => $parametros['crmUserId']
                        ]
                    ]
                ]
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0]['details'];
    }

    public function deleteDireccXContact($token, $parametros)
    {
        $method = "DELETE";
        $url = $this->urlRequest . "/DireccXContacDeEnvio/" . $parametros['direccXContacId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];
        $response = $this->client->request($method, $url, $options);
        if (empty($response->getContent())) {
            throw new Exception('No hay respuesta por parte de Zoho CRM');
        }
        if ($response->getStatusCode() >= 300) {
            throw new Exception('Zoho CRM esta presentando inconvenientes -> ERROR ' . $response->getStatusCode());
        }
        $content = $response->toArray();
        if (isset($content['data'][0]['status']) &&  $content['data'][0]['status'] == 'error') {
            throw new Exception('Hubo problemas en el servidor');
        }
        return $content['data'][0]['details'];
    }
}
