<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoSubscriptionsService
{
    private $client;

    private $urlToken;

    private $urlRequest;

    private $clientId;

    private $secretId;

    private $refreshToken;

    private $orgId;

    private $prodId;

    private $churnMessageId;

    public function __construct(
        HttpClientInterface $client,
        $urlToken,
        $clientId,
        $secretId,
        $refreshToken,
        $urlRequest,
        $orgId,
        $prodId,
        $churnMessageId
    ) {
        $this->client         = $client;
        $this->urlToken       = $urlToken;
        $this->urlRequest     = $urlRequest;
        $this->clientId       = $clientId;
        $this->secretId       = $secretId;
        $this->refreshToken   = $refreshToken;
        $this->orgId          = $orgId;
        $this->prodId         = $prodId;
        $this->churnMessageId = $churnMessageId;
    }

    public function getToken()
    {

        $response = $this->client->request(
            'POST',
            $this->urlToken,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => [
                    'refresh_token' => $this->refreshToken,
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secretId,
                    'grant_type'    => 'refresh_token',
                ]
            ]
        );

        $content = $response->toArray();

        return $content['access_token'];
    }

    public function getSubByCustomerId($token, $customerId)
    {
        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/subscriptions",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => [
                    'filter_by' => 'SubscriptionStatus.ACTIVE',
                    'customer_id' => $customerId,
                    'organization_id' => $this->orgId
                ]
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        if (count($content['subscriptions']) > 0) {
            return $content['subscriptions'][0];
        }

        return $content['subscriptions'];
    }

    public function getAllSubscriptions($token)
    {
        $url = $this->urlRequest . "/plans";
        $method = 'GET';
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'organization_id' => $this->orgId,
                'filter_by' => 'PlanStatus.ACTIVE',
                'product_id' => $this->prodId
            ]
        ];

        $response = $this->client->request($method, $url, $options);

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['plans'];
    }

    public function updatePlan($token, $parametros)
    {
        $url = $this->urlRequest . "/subscriptions/" . $parametros['subscriptionId'];
        $method = 'PUT';
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'organization_id' => $this->orgId,
            ],
            'json' => [
                'plan' => [
                    'plan_code' => $parametros['planCode'],
                    'quantity' => 1
                ]
            ]
        ];

        $response = $this->client->request($method, $url, $options);

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['subscription'];
    }

    public function createSubscription($token, $parametros)
    {

        $contactpersons = [];

        foreach ($parametros['contactos'] as $contacto) {
            if (!empty($contacto['email'])) {
                $contactpersons[] = [
                    'contactperson_id' => $contacto['contactperson_id']
                ];
            }
        }

        $url = $this->urlRequest . '/subscriptions';
        $method = 'POST';
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'organization_id' => $this->orgId,
            ],
            'json' => [
                'customer_id' => $parametros['customerId'],
                'payment_terms' => 15,
                'payment_terms_label' => '15 Días',
                'contactpersons' => $contactpersons,
                'plan' => [
                    'plan_code' => $parametros['planCode'],
                    'quantity' => 1,
                ],
                'auto_collect' => false,
                'salesperson_id' => '2995742000000320421',
                'salesperson_name' => 'Kevin Hernández',
                // 'custom_fields' => [
                //     [
                //         'customfield_id' => "2244816000000152003",
                //         'value' => "TARJETA DE CRÉDITO"
                //     ]
                // ]
            ]
        ];

        if (!empty($parametros['couponCode'])) {
            $options['json']['coupon_code'] = $parametros['couponCode'];
        }

        if (isset($parametros['customPrice']) && $parametros['customPrice'] != null) {
            $options['json']['plan']['price'] = $parametros['customPrice'];
        }

        $response = $this->client->request($method, $url, $options);

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['subscription'];
    }

    public function getAllCupon($token)
    {
        $url = $this->urlRequest . "/coupons";
        $method = 'GET';
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'filter_by' => 'CouponStatus.ACTIVE',
                'product_id' => $this->prodId,
                'organization_id' => $this->orgId,
            ]
        ];

        $response = $this->client->request($method, $url, $options);

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['coupons'];
    }

    public function cancelSubscription($token, $parametros)
    {

        $jsonString = [
            'churn_message_id' => $this->churnMessageId
        ];

        $url = $this->urlRequest . "/subscriptions/" . $parametros['subscriptionId'] . "/cancel";
        $method = 'POST';
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer ' . $token
            ],
            'query' => [
                'organization_id' => $this->orgId,
            ],
            'body' => [
                //'JSONString' => json_encode($jsonString)
            ]
        ];

        $response = $this->client->request($method, $url, $options);

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            return null;
        }

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['subscription'];
    }

    public function getContactPersons($token, $parametros)
    {
        $url = $this->urlRequest . "/customers/" . $parametros['customerId'] . "/contactpersons";
        $method = 'GET';
        $options = [
            'headers' => [
                'Content-Type' => 'multipart/form-data',
                'Authorization' => 'Bearer ' . $token
            ],
            'query' => [
                'organization_id' => $this->orgId,
            ],
        ];

        $response = $this->client->request($method, $url, $options);

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            return null;
        }

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['contactpersons'];
    }
}
