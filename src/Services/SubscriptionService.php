<?php

namespace App\Services;

use App\Entity\Configuraciones;
use App\Entity\DatafastTrx;
use App\Entity\Plan;
use App\Entity\UserTc;
use App\Services\TokenService;
use App\Services\ZohoSubscriptionsService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class SubscriptionService
{

    private $zohoSubsService;

    private $tokenService;

    private $em;

    private $security;

    private $session;

    private $util;

    private $entityId;

    private $env;

    private $shopperMid;

    private $shopperTid;

    private $shopperEci;

    private $shopperPserv;

    private $logger;

    private $dataFastService;

    private $authService;

    private $successCodes = array('000.100.112', '000.100.110', '000.000.000', '000.200.100');

    public function __construct(
        TokenService $tokenService,
        ZohoSubscriptionsService $zohoSubsService,
        EntityManagerInterface $em,
        UtilsService $util,
        Security $security,
        SessionInterface $session,
        LoggerInterface $logger,
        DataFastService $dataFastService,
        AuthenticationService $authService,
        $entityId,
        $appEnv,
        $shopperMid,
        $shopperTid,
        $shopperEci,
        $shopperPserv
    ) {
        $this->zohoSubsService = $zohoSubsService;
        $this->dataFastService = $dataFastService;
        $this->tokenService    = $tokenService;
        $this->em              = $em;
        $this->security        = $security;
        $this->session         = $session;
        $this->util            = $util;
        $this->env             = $appEnv;
        $this->entityId        = $entityId;
        $this->shopperMid      = $shopperMid;
        $this->shopperTid      = $shopperTid;
        $this->shopperEci      = $shopperEci;
        $this->shopperPserv    = $shopperPserv;
        $this->logger          = $logger;
        $this->authService     = $authService;
    }

    public function getAllSubscriptions()
    {

        $token = $this->tokenService->getSubscriptionsToken();

        $suscripciones = $this->zohoSubsService->getAllSubscriptions($token);

        $suscripciones = $this->mapSuscripciones($suscripciones);

        return $suscripciones;
    }

    public function mapSuscripciones($suscripciones)
    {
        $mapSuscripciones = [];
        $planCodes = [];
        foreach ($suscripciones as $suscripcion) {
            $idx = $suscripcion['plan_code'];
            $period = '';
            if ($suscripcion['interval_unit'] == 'months' && $suscripcion['interval'] == '1') {
                $period = 'Mensual';
            } elseif ($suscripcion['interval_unit'] == 'months' && $suscripcion['interval'] == '3') {
                $period = 'Trimestral';
            } elseif ($suscripcion['interval_unit'] == 'months' && $suscripcion['interval'] == '6') {
                $period = 'Semestral';
            } elseif ($suscripcion['interval_unit'] == 'years' && $suscripcion['interval'] == '1') {
                $period = 'Anual';
            }
            // dump($idx . ' - ' . $period);
            $shortname = $suscripcion['name'] . '/' . $period;
            $name = $shortname;
            $mapSuscripciones[$idx]['price'] = round(($suscripcion['recurring_price']), 0);
            $mapSuscripciones[$idx]['code'] = $suscripcion['plan_code'];
            $mapSuscripciones[$idx]['name'] = $name;
            $mapSuscripciones[$idx]['shortName'] = 'Suscripción ' . $shortname;
            $mapSuscripciones[$idx]['interval'] = $suscripcion['interval'];
            $mapSuscripciones[$idx]['interval_unit'] = $suscripcion['interval_unit'];
            $planCodes[] = $idx;
        }

        $planRepo = $this->em->getRepository(Plan::class);
        $planes = $planRepo->findBy([
            'status' => 'A',
            'zohoPlanCode' => $planCodes
        ]);

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        $rubroIva = $confRepo->getConfigDouble('PORCENTAJE_IVA');

        $suscripciones = [];
        foreach ($planes as $key => $plan) {
            $idx = $plan->getPrintoPlanCode();
            $price = round($mapSuscripciones[$plan->getZohoPlanCode()]['price'] * (1 + ($rubroIva / 100)), 0);
            $suscripciones[$idx]['priceBase'] = $mapSuscripciones[$plan->getZohoPlanCode()]['price'];
            $suscripciones[$idx]['price'] = round(($price), 0);
            $suscripciones[$idx]['code'] = $mapSuscripciones[$plan->getZohoPlanCode()]['code'];
            $suscripciones[$idx]['name'] = $mapSuscripciones[$plan->getZohoPlanCode()]['name'] . '( $ ' . $price . ' USD)';
            $suscripciones[$idx]['shortName'] = $mapSuscripciones[$plan->getZohoPlanCode()]['shortName'];
            $suscripciones[$idx]['priority'] = $plan->getPriority();
            $suscripciones[$idx]['printoPlanCode'] = $plan->getPrintoPlanCode();
            $now = time();
            //$now = strtotime('2021-11-08');
            $expirationDate = date('Y-m-d', strtotime("+" . $mapSuscripciones[$plan->getZohoPlanCode()]['interval'] . " " . $mapSuscripciones[$plan->getZohoPlanCode()]['interval_unit'], $now));
            $suscripciones[$idx]['expirationDate'] = $expirationDate;
        }

        return $suscripciones;
    }

    public function calcularRubro($plan)
    {

        $this->authService->refreshSubscription();
        $mySubscription = $this->session->get('userSubscription');
        $priceCredit = 0;

        $activatedAt = null;
        $expireAt = null;
        $daysLeft = 0;
        $mySubsSelected = null;

        if (!empty($mySubscription)) {
            $suscripciones  = $this->getAllSubscriptions();

            $repoPlan = $this->em->getRepository(Plan::class);
            $currentPlan = $repoPlan->findOneBy([
                'zohoPlanCode' => $mySubscription['plan_code'],
                'status'       => 'A'
            ]);
            $mySubsSelected = $suscripciones[$currentPlan->getPrintoPlanCode()];
        }


        if (!empty($mySubscription)) {
            // Calcular valor a favor
            $expireAt = $mySubscription['expires_at'];
            $activatedAt = $mySubscription['activated_at'];
            $now = time();
            $daysLeft = round((strtotime($expireAt) - $now) / (60 * 60 * 24));
            $totalDays = round((strtotime($expireAt) - strtotime($activatedAt)) / (60 * 60 * 24));
            $priceCredit = round(($mySubsSelected['price'] / $totalDays) * $daysLeft);
        }

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        $rubroIva = $confRepo->getConfigDouble('PORCENTAJE_IVA');

        // Calcular valor a cobrar
        $finalPriceIva = round($plan['price'] - $priceCredit, 2);
        $finalPrice0   = round($finalPriceIva / (1 + ($rubroIva / 100)), 2);

        $cupon = $this->getCupon50();
        $priceCupon = 0;
        if (!empty($cupon)) {
            if ($cupon['discount_by'] == 'percentage') {
                $priceCupon = ($finalPrice0 * $cupon['discount_value']) / 100;
            }
        }

        $finalPrice0C   = round($finalPrice0 - $priceCupon, 2);
        $finalPriceIvaC = round(($finalPrice0C * (1 + ($rubroIva / 100))), 2);

        $prices = [
            'activated_at'   => $activatedAt,
            'expireAt'       => $expireAt,
            'daysLeft'       => $daysLeft,
            'newExpireAt'    => $plan['expirationDate'],
            'priceCredit'    => $priceCredit,
            'priceCupon'     => $priceCupon,
            'initPrice'      => $plan['price'],
            'finalPrice0C'   => $finalPrice0C,
            'finalPriceIvaC' => $finalPriceIvaC,
            'finalPrice0'    => $finalPrice0,
            'finalPriceIva'  => $finalPriceIva,
        ];

        return $prices;
    }

    public function getCheckoutId($parametros)
    {

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $userTcRepo = $this->em->getRepository(UserTc::class);
        $tcs = $userTcRepo->findBy([
            'user' => $user,
            'estado' => 'A'
        ]);

        $tarjetas = [];
        $idx = 0;
        foreach ($tcs as $tc) {
            $tarjetas['registrations[' . $idx . '].id'] = $tc->getRegistrationid();
            $idx++;
        }

        $parametros['tarjetas'] = $tarjetas;

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        $rubroIva = $confRepo->getConfigDouble('PORCENTAJE_IVA');



        $subs = $this->getAllSubscriptions();

        //Calcular monto 
        $plan = $subs[$parametros['planKey']];
        //$prices = $this->calcularRubro($plan);
        $amount = number_format($parametros['amount'], 2, '.', '');
        $nombreProducto = $plan['shortName'];
        $descriProducto = $plan['name'];
        $price = $amount;
        $quantity = 1;

        $userBooksZoho = $this->session->get('userBooksZoho');

        $trxId = md5(uniqid(rand(), true));
        $phone = $userBooksZoho['mobile'];
        $addressShipping = "Sin Dirección de Entrega";
        $addressBilling = "Sin Dirección de Facturación";
        $shippingCountry = "EC";
        $billingCountry = "EC";
        $identificationDocType = "IDCARD";
        $businessName = "WWW.PRINTO.CLOUD";

        $data = [];
        $data["entityId"] = $this->entityId;
        $data["amount"] = $amount;
        $data["currency"] = 'USD';
        $data["paymentType"] = 'DB';

        $data["merchantTransactionId"] = $trxId;
        $data["customer.givenName"] = $user->getRazonSocial();
        $data["customer.middleName"] = '';
        $data["customer.surname"] = $user->getRazonSocial();
        $data["customer.ip"] = $this->util->getUserIP();
        $data["customer.merchantCustomerId"] = $user->getCrmUserId();
        $data["customer.email"] = $user->getEmail();
        $data["customer.phone"] = $phone;
        $data["shipping.street1"] = $addressShipping;
        $data["shipping.country"] = $shippingCountry;
        $data["billing.street1"] = $addressBilling;
        $data["billing.country"] = $billingCountry;
        $data["customer.identificationDocType"] = $identificationDocType;
        $data["customer.identificationDocId"] = substr($user->getRuc(), 0, 10);

        $data["cart.items[0].name"] = $nombreProducto;
        $data["cart.items[0].description"] = $descriProducto;
        $data["cart.items[0].price"] = number_format($price, 2, '.', '');
        $data["cart.items[0].quantity"] = $quantity;

        $subtotal = $amount / (1 + $rubroIva / 100);
        $valorIva = $subtotal * ($rubroIva / 100);

        $subtotal = number_format(round($subtotal, 2), 2, '.', '');
        $valoriva = number_format(round($valorIva, 2), 2, '.', '');

        $data["customParameters[SHOPPER_VAL_BASE0]"] = "0.00";
        $data["customParameters[SHOPPER_VAL_BASEIMP]"] = $subtotal;
        $data["customParameters[SHOPPER_VAL_IVA]"] = $valoriva;

        $data["customParameters[SHOPPER_MID]"]   = $this->shopperMid;
        $data["customParameters[SHOPPER_TID]"]   = $this->shopperTid;
        $data["customParameters[SHOPPER_ECI]"]   = $this->shopperEci;
        $data["customParameters[SHOPPER_PSERV]"] = $this->shopperPserv;

        $data["customParameters[SHOPPER_VERSIONDF]"] = 2;

        $data["risk.parameters[USER_DATA2]"] = $businessName;

        if (($this->env != 'prod')) {
            $data["testMode"] = "EXTERNAL";
        }

        if (isset($parametros['tarjetas'])) {
            $data = array_merge($data, $parametros['tarjetas']);
        }

        $parametros["data"] = $data;

        try {

            // Crear TRX
            $dfTrx = new DatafastTrx();
            $dfTrx->setTipo("checkout");
            $dfTrx->setTransactionId($trxId);
            //$dfTrx->setOrdenCab($ordenCab);
            $dfTrx->setRequest(json_encode($data));
            $dfTrx->setNombreTransaccion("COMPRA SUSCRIPCION");
            $this->em->persist($dfTrx);
            $this->em->flush();

            $checkout = $this->dataFastService->getCheckoutId($parametros);

            $resultCode = 'NO CODE ERROR';
            if (isset($checkout['result']) && isset($checkout['result']['code'])) {
                $resultCode = $checkout['result']['code'];
            }

            if (!in_array($resultCode, $this->successCodes)) {
                $errorMessage = "";
                if (isset($responsePurchase['result']) && isset($responsePurchase['result']['description'])) {
                    $errorMessage = " - " . $responsePurchase['result']['description'];
                }
                return [
                    'error' => true,
                    'message' => 'Hubo un problema con la transacción' . $errorMessage
                ];
            }

            //Actualizar TRX
            $dfTrx->setResponse(json_encode($checkout));
            $dfTrx->setResultCode($resultCode);
            $dfTrx->setEstadoTransaccion("Generado");
            $dfTrx->setFechaActualizacion();
            $dfTrx->setCheckoutId((isset($checkout['id'])) ? $checkout['id'] : null);
            $this->em->flush();
        } catch (Exception $e) {
            $dfTrx->setFechaActualizacion();
            $dfTrx->setEstadoTransaccion("Error");
            $dfTrx->setError(substr($e->getMessage(), 0, 300));
            $this->em->flush();
            $this->logger->error(get_class($this) . ' - method getCheckoutId - Hubo un error checout inicial PRINTO - ' . $e->getMessage());
            return [
                'error' => true,
                'message' => 'Hubo un problema con Datafast'
            ];
        }

        return $checkout['id'];
    }

    public function purchaseOrder($parametros)
    {

        $checkoutId = $parametros['checkoutId'];

        // Buscar TRX
        $dfTrxRepo = $this->em->getRepository(DatafastTrx::class);
        $dfTrxCheckout = $dfTrxRepo->findOneBy([
            'estado' => 'A',
            'checkoutId' => $checkoutId
        ]);

        $data = array();
        $data["entityId"] = $this->entityId;
        $parametros["data"] = $data;


        try {
            // Crear TRX
            $dfTrx = new DatafastTrx();
            $dfTrx->setTipo("purchase");
            $dfTrx->setTransactionId($dfTrxCheckout->getTransactionId());
            $dfTrx->setRequest(json_encode($data));
            $this->em->persist($dfTrx);
            $this->em->flush();

            $responsePurchase = $this->dataFastService->purchaseOrder($parametros);

            $resultCode = 'NO CODE ERROR';
            if (isset($responsePurchase['result']) && isset($responsePurchase['result']['code'])) {
                $resultCode = $responsePurchase['result']['code'];
            }

            //Actualizar TRX
            $dfTrx->setResponse(json_encode($responsePurchase));
            $dfTrx->setResultCode($resultCode);
            $dfTrx->setEstadoTransaccion("Generado");
            $dfTrx->setFechaActualizacion();
            $this->em->flush();

            if (!in_array($resultCode, $this->successCodes)) {
                $errorMessage = "";
                if (isset($responsePurchase['result']) && isset($responsePurchase['result']['description'])) {
                    $errorMessage = " - " . $responsePurchase['result']['description'] . " - Código : " . $resultCode;
                }
                return [
                    'error' => true,
                    'message' => 'Hubo un problema al procesar su compra' . $errorMessage
                ];
            }

            // Registro de tarjetas de credito
            if (isset($responsePurchase['registrationId'])) {
                $userTcRepo = $this->em->getRepository(UserTc::class);
                $user = $this->security->getUser();
                $userTc = $userTcRepo->findOneBy([
                    'user' => $user,
                    'estado' => 'A',
                    'registrationid' => $responsePurchase['registrationId']
                ]);
                if (empty($userTc)) {
                    $userTc = new UserTc();
                    $userTc->setRegistrationid($responsePurchase['registrationId']);
                    $userTc->setUser($user);
                    $this->em->persist($userTc);
                    $this->em->flush();
                }
            }

            // Generación de Plan
            $subs = $this->subscribePlan(['checkout' => $parametros['checkout']]);
        } catch (Exception $e) {
            $error = substr($e->getMessage(), 0, 300);
            $dfTrx->setFechaActualizacion();
            $dfTrx->setEstadoTransaccion("Error");
            $dfTrx->setError($error);
            $this->em->flush();
            $this->logger->error(get_class($this) . ' - method purchaseOrder - Hubo un error Creacion de orden DATAFAST - ' . $e->getMessage() . " - " . $e->getLine());
            return [
                'error' => true,
                'message' => 'Hubo un problema con Datafast : ' . $error
            ];
        }

        return [
            'error' => false,
            'subscription' => $subs
        ];
    }

    public function subscribePlan($parametros)
    {
        $mySubscription = $this->session->get('userSubscription');
        $userZohoBooks  = $this->session->get('userBooksZoho');

        $checkout = $parametros['checkout'];

        $prices  = $checkout['prices'];
        $myPlan  = $checkout['myPlan'];
        $newPlan = $checkout['newPlan'];

        // Cupon habilitado vigente
        $cupon   = $this->getCupon50();
        $cupoCode = (!empty($cupon)) ? $cupon['coupon_code'] : null;


        // Cliente
        $customerId = $userZohoBooks['contact_id'];

        // Obtener contactos
        $token     = $this->tokenService->getSubscriptionsToken();
        $contactos = $this->zohoSubsService->getContactPersons($token, ['customerId' => $customerId]);

        // Data para los request
        $paramsCreate = [
            'customerId' => $customerId,
            'planCode' => $newPlan->getZohoPlanCode(),
            'contactos'  => $contactos,
            'couponCode' => $cupoCode
        ];

        $subs = null;
        // valida si no tiene plan activo
        if (empty($mySubscription)) {
            // crea un plan desde 0 con el cupon
            $token = $this->tokenService->getSubscriptionsToken();
            $subs  = $this->zohoSubsService->createSubscription($token, $paramsCreate);
        } else { // valida si tiene plan activo
            // cancela el viejo plan
            $token = $this->tokenService->getSubscriptionsToken();
            $cancel = $this->zohoSubsService->cancelSubscription($token, ['subscriptionId' => $mySubscription['subscription_id']]);
            if (empty($cancel)) {
                throw new Exception('Hubo un problema cancelando la subscripcion vigente');
            }
            // calcula variacion de dias
            // crea un plan desde 0 con el nuevo precio de variacion con el cupon
            $paramsCreate['customPrice'] = $prices['finalPrice0'];
            $token = $this->tokenService->getSubscriptionsToken();
            $subs  = $this->zohoSubsService->createSubscription($token, $paramsCreate);
        }
        return $subs;
    }

    public function getCupon50()
    {
        $token = $this->tokenService->getSubscriptionsToken();
        $cupones = $this->zohoSubsService->getAllCupon($token);

        if (!empty($cupones) and count($cupones) >= 1) {
            foreach ($cupones as $cupon) {
                if ($cupon['coupon_code'] == 'E-001') {
                    return $cupon;
                }
            }
        }

        return null;
    }
}
