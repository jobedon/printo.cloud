<?php

namespace App\Services\Externo;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ServientregaService
{
    private $client;
    private $logger;
    private $urlRequest;
    private $urlRequest2;
    private $user;
    private $pwd;
    private $regionLocal;
    private $provinciaLocal;
    private $ciudadLocal;

    public function __construct(
        HttpClientInterface $client,
        LoggerInterface $logger,
        $urlRequest,
        $urlRequest2,
        $user,
        $pwd,
        $regionLocal,
        $provinciaLocal,
        $ciudadLocal
    )
    {
        $this->client         = $client;
        $this->logger         = $logger;
        $this->urlRequest     = $urlRequest;
        $this->urlRequest2    = $urlRequest2;
        $this->user           = $user;
        $this->pwd            = $pwd;
        $this->regionLocal    = $regionLocal;
        $this->provinciaLocal = $provinciaLocal;
        $this->ciudadLocal    = $ciudadLocal;
    }


    public function getCiudades()
    {
        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/ciudades/['{$this->user}','{$this->pwd}']"
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content;
    }

    public function createGuia($parametros)
    {

    }

    public function generatePdf($parametros)
    {
        $guia = $parametros['guia'];
        $response = $this->client->request(
            'GET',
            $this->urlRequest2 . "/GuiasWeb/['{$guia}','{$this->user}','{$this->pwd}','1']"
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content;
    }

    public function getTarifasServientrega($parametros)
    {
        $regionLocal    = $this->regionLocal;
        $provinciaLocal = $this->provinciaLocal;
        $ciudadLocal    = $this->ciudadLocal;

        $region    = $parametros['region'];
        $provincia = $parametros['provincia'];
        $ciudad    = $parametros['ciudad'];

        // GALAPAGOS
        if ($provincia == 'GALAPAGOS') {
            $precioInicial = 10.5;
            $precioAdicional = 1.99;
        }

        // LOCAL
        elseif ($ciudad == $ciudadLocal && $provincia == $provinciaLocal && $region == $regionLocal) {
            $precioInicial = 1.51;
            $precioAdicional = 0.34;
        }

        // CANTONAL
        elseif ($ciudad != $ciudadLocal && $provincia == $provinciaLocal && $region == $regionLocal) {
            $precioInicial = 2.83;
            $precioAdicional = 0.52;
        }

        // PROVINCIAL
        elseif ($ciudad != $ciudadLocal && $provincia != $provinciaLocal && $region == $regionLocal) {
            $precioInicial = 3.14;
            $precioAdicional = 0.55;
        }

        // REGIONAL
        elseif ($ciudad != $ciudadLocal && $provincia != $provinciaLocal && $region != $regionLocal) {
            $precioInicial = 3.5;
            $precioAdicional = 0.69;
        }

        return [
            'precioInicial' => $precioInicial,
            'precioAdicional' => $precioAdicional
        ];

    }
    

}