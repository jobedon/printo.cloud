<?php

namespace App\Services;

use App\Entity\AppError;
use App\Exceptions\TransferFileException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoWorkDriveService
{
    private $client;

    private $urlToken;

    private $urlRequest;

    private $clientId;

    private $secretId;

    private $refreshToken;

    private $parentIdXerox;

    private $parentIdMimaki;

    private $parentIdElectrocorte;

    private $parentIdTkPendientes;

    private $parentIdTkFinalizadas;

    private $uploadDirectory;

    private $logger;

    private $errorService;

    private $em;

    public function __construct(
        HttpClientInterface $client,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        ErrorService $errorService,
        $urlToken,
        $clientId,
        $secretId,
        $refreshToken,
        $urlRequest,
        $parentIdXerox,
        $parentIdMimaki,
        $parentIdElectrocorte,
        $parentIdTkPendientes,
        $parentIdTkFinalizadas,
        $uploadDirectory
    ) {
        $this->client                = $client;
        $this->em                    = $em;
        $this->urlToken              = $urlToken;
        $this->urlRequest            = $urlRequest;
        $this->clientId              = $clientId;
        $this->secretId              = $secretId;
        $this->refreshToken          = $refreshToken;
        $this->parentIdXerox         = $parentIdXerox;
        $this->parentIdMimaki        = $parentIdMimaki;
        $this->parentIdElectrocorte  = $parentIdElectrocorte;
        $this->parentIdTkPendientes  = $parentIdTkPendientes;
        $this->parentIdTkFinalizadas = $parentIdTkFinalizadas;
        $this->uploadDirectory       = $uploadDirectory;
        $this->logger                = $logger;
        $this->errorService          = $errorService;
    }

    public function getToken()
    {

        $response = $this->client->request(
            'POST',
            $this->urlToken,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => [
                    'refresh_token' => $this->refreshToken,
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secretId,
                    'grant_type'    => 'refresh_token',
                ]
            ]
        );

        $content = $response->toArray();

        return $content['access_token'];
    }

    public function createFolder($token, $folderName, $parent)
    {
        switch ($parent) {
            case 'XEROX':
                $parentId = $this->parentIdXerox;
                break;
            case 'MIMAKI':
                $parentId = $this->parentIdMimaki;
                break;
            case 'ELECTROCORTE':
                $parentId = $this->parentIdElectrocorte;
                break;
            case 'TICKETS_PENDIENTES':
                $parentId = $this->parentIdTkPendientes;
                break;
            case 'TICKETS_FINALIZADOS':
                $parentId = $this->parentIdTkFinalizadas;
                break;
            default:
                $parentId = $parent;
                break;
        }

        $method = "POST";
        $url = $this->urlRequest . "/files";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'data' => [
                    'attributes' => [
                        "name"      => $folderName,
                        "parent_id" => $parentId
                    ],
                    'type' => 'files',
                ],
            ]
        ];

        $this->logger->info("TRACKING CREATE >>> " . get_class($this) . " METHOD : " . $method . " | URL : " .  $url);
        $this->logger->info("TRACKING CREATE >>> " . get_class($this) . " OPTIONS : " . json_encode($options));

        $response = $this->client->request(
            $method,
            $url,
            $options
        );
        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            $this->logger->info("TRACKING CREATE >>> " . get_class($this) . " RESPONSE : " . json_encode($content));
            throw new TransferFileException($content['errors'][0]['title'], TransferFileException::CODE_ERROR_ZOHO_WD);
        }

        if (empty($response->getContent())) {
            throw new TransferFileException('Hubo un error en la transferencia de archivos!', TransferFileException::CODE_ERROR_ZOHO_WD);
        }

        $content = $response->toArray();

        return $content['data']['id'];
    }

    public function deleteFolders($token, $folders, $trashed = false)
    {
        $datas = array();
        foreach ($folders as $folder) {
            $data = [
                'attributes' => [
                    'status' => 51
                ],
                'type' => 'files',
                'id' => $folder
            ];
            $datas[] = $data;
        }

        $method = "PATCH";
        $url = $this->urlRequest . "/files";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'data' => $datas
            ]
        ];

        $this->logger->info("TRACKING DELETE >>> " . get_class($this) . " METHOD : " . $method . " | URL : " .  $url);
        $this->logger->info("TRACKING DELETE >>> " . get_class($this) . " OPTIONS : " . json_encode($options));

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if ($response->getStatusCode() >= 300) {

            $this->errorService->saveError([
                'app'     => 'ZohoWorkDrive',
                'proceso' => 'deleteFolders',
                'cod'     => TransferFileException::CODE_ERROR_ZOHO_WD,
                'error'   => 'Problema al eliminar carpetas'
            ]);

            return;
        }


        if (!$trashed) {
            return;
        }

        foreach ($folders as $folder) {
            $this->trashFolder($token, [
                'folderId' => $folder
            ]);
        }

    }

    public function trashFolder($token, $parametros)
    {
        $method = "DELETE";
        $url = $this->urlRequest . "/files/" . $parametros['folderId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);

            $this->errorService->saveError([
                'app'     => 'ZohoWorkDrive',
                'proceso' => 'trashFolder',
                'cod'     => TransferFileException::CODE_ERROR_ZOHO_WD,
                'error'   => 'Problema al eliminar carpetas de la papelera'
            ]);

            return;
        }

    }

    public function renameFolder($token, $newNameFolder, $parentId)
    {
        $response = $this->client->request(
            'PATCH',
            $this->urlRequest . "/files/" . $parentId,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'json' => [
                    'data' => [
                        'attributes' => [
                            "name"      => $newNameFolder
                        ],
                        'type' => 'files',
                    ],
                ]
            ]
        );

        $this->logger->info("TRACKING RENAME >>> " . get_class($this));

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            throw new TransferFileException('Problema al eliminar carpetas', TransferFileException::CODE_ERROR_ZOHO_WD);
        }
    }

    public function uploadFile($token, $pathFile, $parentId)
    {
        $arrayPath = explode('/', $pathFile);
        $filename = end($arrayPath);

        $data = [
            'filename'  => $filename,
            'parent_id' => $parentId,
            'content'   => DataPart::fromPath($this->uploadDirectory . '/' . $pathFile),
        ];

        $formData = new FormDataPart($data);
        $formData->getHeaders()->addTextHeader('Authorization', 'Bearer ' . $token);

        $method = "POST";
        $url = $this->urlRequest . "/upload";
        $options = [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToIterable()
        ];

        $this->logger->info("TRACKING UPLOAD >>> " . get_class($this) . " METHOD : " . $method . " | URL : " .  $url);
        $this->logger->info("TRACKING UPLOAD >>> " . get_class($this) . " OPTIONS : " . json_encode($options['headers']));

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            throw new TransferFileException($content['errors'][0]['title'], TransferFileException::CODE_ERROR_ZOHO_WD);
        }

        if (empty($response->getContent())) {
            throw new TransferFileException('Hubo un error en la transferencia de archivos!', TransferFileException::CODE_ERROR_ZOHO_WD);
        }

        $content = $response->toArray();


        return null;
    }

    public function copyFolder($token, $parametros)
    {
        $method = "POST";
        $url = $this->urlRequest . "/files/" . $parametros['folderTargetId'] . '/copy';
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'data' => [
                    'attributes' => [
                        "resource_id"      => $parametros['folderSourceId']
                    ],
                    'type' => 'files',
                ],
            ]
        ];

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if ($response->getStatusCode() >= 300) {
            //$content = $response->toArray(false);

            $dataTmp = $parametros['folderSourceId'] . ' >>> ' . $parametros['folderTargetId'];

            $this->errorService->saveError([
                'app'     => 'ZohoWorkDrive',
                'proceso' => 'copyFolder',
                'cod'     => TransferFileException::CODE_ERROR_ZOHO_WD,
                'error'   => 'Problema al copiar carpetas : ' . $dataTmp
            ]);

            return;
        }
        
        $content = $response->toArray();

        return $content['data']['id'];

    }

    public function getInfoFileFolder($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/files/" . $parametros['fileFolderId'];
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);

            $this->errorService->saveError([
                'app'     => 'ZohoWorkDrive',
                'proceso' => 'getInfoFileFolder',
                'cod'     => TransferFileException::CODE_ERROR_ZOHO_WD,
                'error'   => 'Problema al copiar carpetas | ' . substr(json_encode($content), 0, 450)
            ]);

            return;
        }
        $content = $response->toArray();

        return $content['data']['attributes']['name'];

    }

    public function getListFilesFolders($token, $parametros)
    {
        $method = "GET";
        $url = $this->urlRequest . "/files/" . $parametros['folderId'] . '/files';
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ]
        ];

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);

            $this->errorService->saveError([
                'app'     => 'ZohoWorkDrive',
                'proceso' => 'getListFilesFolders',
                'cod'     => TransferFileException::CODE_ERROR_ZOHO_WD,
                'error'   => 'Problema al copiar carpetas | ' . substr(json_encode($content), 0, 450)
            ]);

            return;
        }
        $content = $response->toArray();

        return $content['data'];
    }

}
