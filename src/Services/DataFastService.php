<?php

namespace App\Services;

use App\Entity\UserPi;
use App\Entity\UserTc;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DataFastService
{

    private $urlRequest;

    private $entityId;

    private $authToken;

    private $client;

    private $env;

    private $logger;

    private $em;

    private $security;

    private $session;

    public function __construct(
        HttpClientInterface $client,
        LoggerInterface $logger,
        EntityManagerInterface $em,
        Security $security,
        SessionInterface $session,
        $urlRequest,
        $authToken,
        $entityId,
        $appEnv
    ) {
        $this->urlRequest   = $urlRequest;
        $this->entityId     = $entityId;
        $this->authToken    = $authToken;
        $this->client       = $client;
        $this->env          = $appEnv;
        $this->logger       = $logger;
        $this->em           = $em;
        $this->security     = $security;
        $this->session      = $session;
    }

    public function getCheckoutId($parametros)
    {
        $data = $parametros["data"];

        $url = $this->urlRequest . '/v1/checkouts';
        $method = 'POST';
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->authToken,
            ],
            'body' => $data
        ];

        $this->logger->info("TRACKING >>> getCheckoutId URL :" . $url);
        $this->logger->info("TRACKING >>> getCheckoutId METHOD :" . $method);
        $this->logger->info("TRACKING >>> getCheckoutId OPTIONS :" . json_encode($options));

        $response = $this->client->request($method, $url, $options);

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            $this->logger->error("TRACKING >>> getCheckoutId RESPONSE :", $content);
            return $content;
            //throw new OrdenException('mensaje', OrdenException::CODE_ERROR_ZOHO_BOOKS);
        }

        $content = $response->toArray();

        return $content;
    }

    public function purchaseOrder($parametros)
    {
        $data = $parametros["data"];
        $resourcePath = $parametros['resourcePath'];
        $url = $this->urlRequest . $resourcePath;
        $method = 'GET';
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->authToken,
            ],
            'verify_peer' => ($this->env == 'prod'),
            'query' => $data
        ];

        $response = $this->client->request($method, $url, $options);

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            return $content;
            //throw new OrdenException($content['result']['description'], OrdenException::CODE_ERROR_ZOHO_BOOKS);
        }

        $content = $response->toArray();

        return $content;
    }

    public function getTarjetasPorUsuario($parametros)
    {
        $user = $parametros['user'];
        $userTcRepo = $this->em->getRepository(UserTc::class);

        $tcs = $userTcRepo->findBy([
            'user' => $user,
            'estado' => 'A'
        ]);

        $tarjetas = [];
        $idx = 0;
        foreach ($tcs as $tc) {
            $tarjetas['registrations[' . $idx . '].id'] = $tc->getRegistrationid();
            $idx++;
        }

        return $tarjetas;

    }




}
