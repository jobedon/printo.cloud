<?php

namespace App\Services;

use App\Entity\UserPi;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Security;

class AuthenticationService
{

    private $entityManager;

    private $tokenService;

    private $zohoCrmService;

    private $zohoSubService;

    private $zohoBooksService;

    private $encoder;

    private $mailer;

    private $security;

    private $session;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        EntityManagerInterface $entityManager,
        TokenService $tokenService,
        ZohoCrmService $zohoCrmService,
        ZohoBooksService $zohoBooksService,
        ZohoSubscriptionsService $zohoSubService,
        MailerInterface $mailer,
        Security $security,
        SessionInterface $session
    ) {
        $this->encoder          = $encoder;
        $this->tokenService     = $tokenService;
        $this->entityManager    = $entityManager;
        $this->zohoCrmService   = $zohoCrmService;
        $this->zohoBooksService = $zohoBooksService;
        $this->zohoSubService   = $zohoSubService;
        $this->mailer           = $mailer;
        $this->security         = $security;
        $this->session          = $session;
    }

    public function refreshSubscription()
    {
        /** @var UserPi|null $user */
        $user = $this->security->getUser();
        $tokenBook = $this->tokenService->getBooksToken();
        $userBookZoho = $this->zohoBooksService->getUserBookFromCrm($tokenBook, $user->getCrmUserId());
        $tokenSubs = $this->tokenService->getSubscriptionsToken();
        $userSubscription = $this->zohoSubService->getSubByCustomerId($tokenSubs, $userBookZoho['contact_id']);
        $this->session->set("userSubscription", $userSubscription);
    }


    public function getUser($ruc)
    {

        $token = $this->tokenService->getCrmToken();

        $userZoho = $this->zohoCrmService->getUserByRuc($ruc, $token);

        $userRepo = $this->entityManager->getRepository(UserPi::class);

        $userDB = $userRepo->findOneBy([
            'ruc' => $ruc
        ]);

        if (!empty($userZoho)) {

            if (isset($userZoho['Account_Type']) && $userZoho['Account_Type'] == 'Distribuidor') {

                if (empty($userDB)) {
                    // No se encuentra registrado en DB
                    $userDB = $this->registerUser($ruc);
                }
                $token = $this->tokenService->getCrmToken();
                $contactZoho = $this->zohoCrmService->getContactByUser($userZoho['id'], $token);
                $userDB->setEmail($contactZoho['Email']);
                $this->entityManager->flush();

                // buscar Books
                $tokenBook = $this->tokenService->getBooksToken();
                $userBookZoho = $this->zohoBooksService->getUserBookFromCrm($tokenBook, $userDB->getCrmUserId());
                $tokenSubs = $this->tokenService->getSubscriptionsToken();
                $userSubscription = $this->zohoSubService->getSubByCustomerId($tokenSubs, $userBookZoho['contact_id']);


                // obtener subscripcion

                return [
                    'userDb'       => $userDB,
                    'userCrmZoho'  => $userZoho,
                    'userBookZoho' => $userBookZoho,
                    'userSubZoho'  => $userSubscription
                ];
            } else {
                throw new Exception('Usuario encontrado no es Distribuidor');
            }
        } else {
            throw new Exception('Usuario no identificado');
        }

        return [
            'userDb'      => $userDB,
            'userCrmZoho' => $userZoho
        ];
    }


    public function registerUser($ruc)
    {

        $token = $this->tokenService->getCrmToken();

        $userZoho = $this->zohoCrmService->getUserByRuc($ruc, $token);

        $userRepo = $this->entityManager->getRepository(UserPi::class);

        $userDB = $userRepo->findOneBy([
            'ruc' => $ruc
        ]);

        if (!empty($userZoho)) {

            if (isset($userZoho['Account_Type']) && $userZoho['Account_Type'] == 'Distribuidor') {

                if (!empty($userDB)) {

                    throw new Exception('Usuario ya registrado');
                } else {
                    // No se encuentra registrado en DB
                    $userPi = new UserPi();
                    $userPi->setRuc($ruc);
                    $userPi->setRazonSocial(isset($userZoho['Account_Name']) ? $userZoho['Account_Name'] : '');
                    $pwd = $this->encoder->encodePassword($userPi, $ruc);
                    $userPi->setPassword($pwd);
                    $userPi->setCrmUserId(isset($userZoho['id']) ? $userZoho['id'] : '');
                    $this->entityManager->persist($userPi);
                    $this->entityManager->flush();
                    return $userPi;
                }
            } else {
                throw new Exception('Usuario encontrado no es Distribuidor');
            }
        } else {
            throw new Exception('Usuario no identificado');
        }

        return $userDB;
    }

    public function forgotPwd($ruc)
    {
        $user = $this->getUser($ruc);
        $userDb = $user['userDb'];

        $email = $userDb->getEmail();

        $pwdTmp = uniqid();

        $pwd = $this->encoder->encodePassword($userDb, $pwdTmp);

        $userDb->setPassword($pwd);
        $userDb->setEstado('P');

        $this->entityManager->flush();

        $email = (new TemplatedEmail())
            ->from('no-reply@printo.cloud')
            ->to(new Address($email))
            ->subject('PRINTO : Recuperacion de Contraseña')

            // path of the Twig template to render
            ->htmlTemplate('emails/recovery-pwd.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'user' => $userDb,
                'pwd'    => $pwdTmp
            ]);
        
            
        $this->mailer->send($email);


        return $userDb;
    }
}
