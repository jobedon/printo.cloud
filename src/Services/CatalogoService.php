<?php
namespace App\Services;

use App\Entity\Catalogo;
use App\Entity\Producto;
use App\Repository\CatalogoRepository;
use App\Repository\ProductoRepository;
use Doctrine\ORM\EntityManagerInterface;

class CatalogoService
{   

    private $em;
    
    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
    }

    public function initCatalogo()
    {
        /**@var null|CatalogoRepository $cataRepo */
        $cataRepo = $this->em->getRepository(Catalogo::class);

        $productos = $cataRepo->getProductosPorCatalogo([
            'catalogoNombre' => 'General'
        ]);

        return $productos;        

    }

}