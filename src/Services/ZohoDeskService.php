<?php

namespace App\Services;

use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoDeskService
{
    private $client;

    private $urlToken;

    private $urlRequest;

    private $clientId;

    private $secretId;

    private $refreshToken;

    public function __construct(
        HttpClientInterface $client,
        $urlToken,
        $clientId,
        $secretId,
        $refreshToken,
        $urlRequest
    ) {
        $this->client       = $client;
        $this->urlToken     = $urlToken;
        $this->urlRequest   = $urlRequest;
        $this->clientId     = $clientId;
        $this->secretId     = $secretId;
        $this->refreshToken = $refreshToken;
    }

    public function getToken()
    {

        $response = $this->client->request(
            'POST',
            $this->urlToken,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => [
                    'refresh_token' => $this->refreshToken,
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secretId,
                    'grant_type'    => 'refresh_token',
                ]
            ]
        );

        $content = $response->toArray();

        return $content['access_token'];
    }

    public function searchTicketBySaleOrder($token, $saleOrder)
    {
        $response = $this->client->request(
            'GET',
            $this->urlRequest . "/tickets/search",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => [
                    'subject' => $saleOrder . '*',
                ]
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['data'][0];
    }

    public function getTicketsByStatus($token, $parametros)
    {
        $query = [
            'accountName' => $parametros['accountName'],
            'status'      => $parametros['status']
        ];

        $url = $this->urlRequest . "/tickets/search";
        $method = "GET";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => $query
        ];

        $response = $this->client->request($method, $url, $options);

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            throw new Exception(isset($content['message']) ? $content['message'] : 'Hubo un error');
        }
        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['data'];
    }
    
    public function getTicketsByContact($token, $parametros)
    {
        $query = [
            'contactId' => $parametros['contactId'],
            'status'    => $parametros['status'],
            'sortBy'   => '-createdTime'
        ];
        
        $url = $this->urlRequest . "/tickets/search";
        $method = "GET";
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => $query
        ];

        $response = $this->client->request($method, $url, $options);

        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            throw new Exception(isset($content['message']) ? $content['message'] : 'Hubo un error');
        }
        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['data'];

    }
}
