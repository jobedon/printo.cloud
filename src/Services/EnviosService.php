<?php

namespace App\Services;

use App\Entity\Configuraciones;
use App\Entity\DatafastTrx;
use App\Entity\Direccion;
use App\Entity\OrdenCab;
use App\Entity\OrdenDet;
use App\Entity\PaqueteEnvio;
use App\Entity\UserPi;
use App\Entity\UserTc;
use App\Repository\ConfiguracionesRepository;
use App\Repository\DireccionRepository;
use App\Repository\FilesOrdenCabRepository;
use App\Repository\OrdenCabRepository;
use App\Repository\OrdenDetRepository;
use App\Services\Externo\ServientregaService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Security;

class EnviosService
{

    private $zohoApiService;
    private $zohoDeskService;
    private $tokenService;
    private $session;
    private $em;
    private $seService;
    private $security;
    private $util;
    private $dataFastService;
    private $entityId;
    private $env;
    private $shopperMid;
    private $shopperTid;
    private $shopperEci;
    private $shopperPserv;
    private $logger;
    private $pagService;
    private $mailer;
    private $successCodes = array('000.100.112', '000.100.110', '000.000.000', '000.200.100');

    public function __construct(
        ZohoApiService $zohoApiService,
        ZohoDeskService $zohoDeskService,
        TokenService $tokenService,
        SessionInterface $session,
        EntityManagerInterface $em,
        ServientregaService $seService,
        Security $security,
        UtilsService $util,
        DataFastService $dataFastService,
        LoggerInterface $logger,
        PagosService $pagService,
        MailerInterface $mailer,
        $entityId,
        $appEnv,
        $shopperMid,
        $shopperTid,
        $shopperEci,
        $shopperPserv
    ) {
        $this->zohoApiService  = $zohoApiService;
        $this->zohoDeskService = $zohoDeskService;
        $this->tokenService    = $tokenService;
        $this->session         = $session;
        $this->em              = $em;
        $this->seService       = $seService;
        $this->security        = $security;
        $this->util            = $util;
        $this->dataFastService = $dataFastService;
        $this->entityId        = $entityId;
        $this->shopperMid      = $shopperMid;
        $this->shopperTid      = $shopperTid;
        $this->shopperEci      = $shopperEci;
        $this->shopperPserv    = $shopperPserv;
        $this->logger          = $logger;
        $this->pagService      = $pagService;
        $this->mailer          = $mailer;
    }

    public function getListadoTicketsListos()
    {
        $userBooksZoho  = $this->session->get('userBooksZoho');
        $contactIdBooks = $userBooksZoho['contact_id'];
        $contactIdDesk  = $this->zohoApiService->getContactIdDeskFromContactIBooks([
            'contactIdBooks' => $contactIdBooks
        ]);
        $token = $this->tokenService->getDeskToken();
        $ticketsZoho = $this->zohoDeskService->getTicketsByContact($token, [
            'contactId' => $contactIdDesk,
            'status' => 'DISEÑO,PRE-PRENSA,PRENSA,POS-PRENSA,PAUSADO,CALIDAD,EMPACADO'
        ]);
        $ticketsZoho = $this->filtrarTicketsPaquetesEnviados([
            'ticketsZoho' => $ticketsZoho
        ]);
        $ordenes = $this->categorizarTicketsByOrden([
            'ticketsZoho' => $ticketsZoho
        ]);

        return $ordenes;
    }

    public function filtrarTicketsPaquetesEnviados($parametros)
    {
        $ticketsZoho = $parametros['ticketsZoho'];

        /**@var null|OrdenDetRepository $ordenDetRepo */
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
        $tickets = [];
        foreach ($ticketsZoho as $ticketZoho) {
            $noTicket = $ticketZoho['ticketNumber'];
            $ordenDet = $ordenDetRepo->findOneBy([
                'noTicket' => $noTicket
            ]);
            if (!empty($ordenDet) && empty($ordenDet->getPaqueteEnvio())) {
                $tickets[] = $ticketZoho;
            }
        }
        return $tickets;
    }

    public function categorizarTicketsByOrden($parametros)
    {
        $ticketsZoho = $parametros['ticketsZoho'];
        $ordenesZoho = [];
        foreach ($ticketsZoho as $key => $ticketZoho) {
            $ordenesZoho[$ticketZoho['cf']['cf_orden_de_compra_asociada']][] = $ticketZoho;
        }
        return $ordenesZoho;
    }

    public function generatePdf($parametros)
    {
        return $this->seService->generatePdf($parametros);
    }


    /**
     * Realiza el calculo de tarifas (cambiar de envio)
     */
    public function calcularTarifas($parametros)
    {
        $tickets = $parametros['tickets'];
        $validaOrdenesCompletas = $parametros['validaOrdenesCompletas'];
        $metodoEnvio = $parametros['metodo'];
        $contactoId  = $parametros['contactoId'];
        $direccionId = $parametros['direccionId'];

        $user    = $this->security->getUser();

        /**@var null|OrdenDetRepository $ordenDetRepo */
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        $items = $ordenDetRepo->getDetallesPorTickets([
            'tickets' => $tickets,
            'user'    => $user
        ]);

        if (count($items) == 0) {
            throw new Exception("No existen tickets asociados");
        }

        $tarifas = $this->pagService->calcularTarifas([
            'items'       => $items,
            'validaOrdenesCompletas' => $validaOrdenesCompletas,
            'metodoEnvio' => $metodoEnvio,
            'contactoId'  => $contactoId,
            'direccionId' => $direccionId
        ]);

        return $tarifas;
        
    }

    

    /**
     * Obtiene checkoutId para DATAFAST
     */
    public function getCheckoutId($parametros)
    {

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $parametros['tarjetas'] = $this->dataFastService->getTarjetasPorUsuario([
            'user' => $user
        ]);

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        $rubroIva = $confRepo->getConfigDouble('PORCENTAJE_IVA');

        $userBooksZoho = $this->session->get('userBooksZoho');
        $userCrmZoho   = $this->session->get('userCrmZoho');

        $trxId = md5(uniqid(rand(), true));
        $phone = $userBooksZoho['mobile'];

        $addressShipping = "Entra a cliente"; // Preguntar a Kevin
        $addressBilling = "Sin Dirección de Facturación";
        $shippingCountry = "EC";
        $billingCountry = "EC";
        $identificationDocType = "IDCARD";
        $businessName = "WWW.PRINTO.CLOUD";
        $nombreProducto = "Envio de Productos por Servientrega";
        $descriProducto = "Se envia mercaderia de Printo.Cloud";
        $amount = $parametros['valorPagar'];
        $price = $amount;
        $quantity = 1;

        if (isset($userCrmZoho['Direcci_n_de_facturaci_n']) && !empty($userCrmZoho['Direcci_n_de_facturaci_n'])) {
            $addressBilling = $userCrmZoho['Direcci_n_de_facturaci_n'];
        } else {
            $addressBilling = "Sin Dirección de Facturación";
        }

        $data = [];
        $data["entityId"] = $this->entityId;
        $data["amount"] = number_format($amount, 2, '.', '');;
        $data["currency"] = 'USD';
        $data["paymentType"] = 'DB';

        $data["merchantTransactionId"] = $trxId;
        $data["customer.givenName"] = $user->getRazonSocial();
        $data["customer.middleName"] = '';
        $data["customer.surname"] = $user->getRazonSocial();
        $data["customer.ip"] = $this->util->getUserIP();
        $data["customer.merchantCustomerId"] = $user->getCrmUserId();
        $data["customer.email"] = $user->getEmail();
        $data["customer.phone"] = $phone;
        $data["shipping.street1"] = $addressShipping;
        $data["shipping.country"] = $shippingCountry;
        $data["billing.street1"] = $addressBilling;
        $data["billing.country"] = $billingCountry;
        $data["customer.identificationDocType"] = $identificationDocType;
        $data["customer.identificationDocId"] = substr($user->getRuc(), 0, 10);

        $data["cart.items[0].name"] = $nombreProducto;
        $data["cart.items[0].description"] = $descriProducto;
        $data["cart.items[0].price"] = number_format($price, 2, '.', '');
        $data["cart.items[0].quantity"] = $quantity;

        $subtotal = $amount / (1 + $rubroIva / 100);
        $valorIva = $subtotal * ($rubroIva / 100);

        $subtotal = number_format(round($subtotal, 2), 2, '.', '');
        $valoriva = number_format(round($valorIva, 2), 2, '.', '');

        $data["customParameters[SHOPPER_VAL_BASE0]"] = "0.00";
        $data["customParameters[SHOPPER_VAL_BASEIMP]"] = $subtotal;
        $data["customParameters[SHOPPER_VAL_IVA]"] = $valoriva;

        $data["customParameters[SHOPPER_MID]"]   = $this->shopperMid;
        $data["customParameters[SHOPPER_TID]"]   = $this->shopperTid;
        $data["customParameters[SHOPPER_ECI]"]   = $this->shopperEci;
        $data["customParameters[SHOPPER_PSERV]"] = $this->shopperPserv;

        $data["customParameters[SHOPPER_VERSIONDF]"] = 2;

        $data["risk.parameters[USER_DATA2]"] = $businessName;

        if (($this->env != 'prod')) {
            $data["testMode"] = "EXTERNAL";
        }

        if (isset($parametros['tarjetas'])) {
            $data = array_merge($data, $parametros['tarjetas']);
        }

        $parametros["data"] = $data;

        try {

            // Crear TRX
            $dfTrx = new DatafastTrx();
            $dfTrx->setTipo("checkout");
            $dfTrx->setTransactionId($trxId);
            $dfTrx->setNombreTransaccion("COMPRA ENVIOS");
            $dfTrx->setRequest(json_encode($data));
            $this->em->persist($dfTrx);
            $this->em->flush();

            $checkout = $this->dataFastService->getCheckoutId($parametros);

            $resultCode = 'NO CODE ERROR';
            if (isset($checkout['result']) && isset($checkout['result']['code'])) {
                $resultCode = $checkout['result']['code'];
            }
            //Actualizar TRX
            $dfTrx->setResponse(json_encode($checkout));
            $dfTrx->setResultCode($resultCode);
            $dfTrx->setEstadoTransaccion("Generado");
            $dfTrx->setFechaActualizacion();
            $this->em->flush();

            if (!in_array($resultCode, $this->successCodes)) {
                $errorMessage = "";
                if (isset($responsePurchase['result']) && isset($responsePurchase['result']['description'])) {
                    $errorMessage = " - " . $responsePurchase['result']['description'];
                }
                return [
                    'error' => true,
                    'message' => 'Hubo un problema con la transacción' . $errorMessage
                ];
            }
        } catch (Exception $e) {
            $dfTrx->setFechaActualizacion();
            $dfTrx->setEstadoTransaccion("Error");
            $dfTrx->setError(substr($e->getMessage(), 0, 300));
            $this->em->flush();
            $this->logger->error(get_class($this) . ' - method getCheckoutId - Hubo un error checout inicial PRINTO - ' . $e->getMessage());
            return [
                'error' => true,
                'message' => 'Hubo un problema con Datafast'
            ];
        }

        return $checkout['id'];
    }

    /**
     * Realiza la compra de Datafast "purchase action"
     */
    public function purchaseOrder($parametros)
    {
        $user = $this->security->getUser();

        $data = array();
        $data["entityId"] = $this->entityId;
        $parametros["data"] = $data;

        try {

            // Buscar TRX
            // $dfTrxRepo = $this->em->getRepository("App:DatafastTrx");
            // $dfTrxCheckout = $dfTrxRepo->findOneBy([
            //     'estado' => 'A',
            //     'ordenCab' => $ordenCab
            // ]);
            // Crear TRX
            $dfTrx = new DatafastTrx();
            $dfTrx->setTipo("purchase");
            //$dfTrx->setTransactionId($dfTrxCheckout->getTransactionId());
            $dfTrx->setNombreTransaccion("COMPRA ENVIOS");
            //$dfTrx->setOrdenCab($ordenCab);
            $dfTrx->setUser($user);
            $dfTrx->setRequest(json_encode($data));
            $this->em->persist($dfTrx);
            $this->em->flush();

            $responsePurchase = $this->dataFastService->purchaseOrder($parametros);

            $resultCode = 'NO CODE ERROR';
            if (isset($responsePurchase['result']) && isset($responsePurchase['result']['code'])) {
                $resultCode = $responsePurchase['result']['code'];
            }

            //Actualizar TRX
            $dfTrx->setResponse(json_encode($responsePurchase));
            $dfTrx->setResultCode($resultCode);
            $dfTrx->setEstadoTransaccion("Generado");
            $dfTrx->setFechaActualizacion();
            $this->em->flush();

            if (!in_array($resultCode, $this->successCodes)) {
                $errorMessage = "";
                if (isset($responsePurchase['result']) && isset($responsePurchase['result']['description'])) {
                    $errorMessage = " - " . $responsePurchase['result']['description'] . " - Código : " . $resultCode;
                }
                return [
                    'error' => true,
                    'message' => 'Hubo un problema al procesar su compra' . $errorMessage
                ];
            }

            // Registro de tarjetas de credito
            if (isset($responsePurchase['registrationId'])) {
                $userTcRepo = $this->em->getRepository(UserTc::class);
                $user = $this->security->getUser();
                $userTc = $userTcRepo->findOneBy([
                    'user' => $user,
                    'estado' => 'A',
                    'registrationid' => $responsePurchase['registrationId']
                ]);
                if (empty($userTc)) {
                    $userTc = new UserTc();
                    $userTc->setRegistrationid($responsePurchase['registrationId']);
                    $userTc->setUser($user);
                    $this->em->persist($userTc);
                    $this->em->flush();
                }
            }

            // OK para DATAFAST
            // Consumo Api para Empaquetado Cristhian
            $this->realizarEmpaqueTickets([
                'dfTrx' => $dfTrx,
                'tickets' => $parametros['tickets']
            ]);

            // Consumo Api Servientrega

        } catch (Exception $e) {
            $dfTrx->setFechaActualizacion();
            $dfTrx->setEstadoTransaccion("Error");
            $dfTrx->setError(substr($e->getMessage(), 0, 300));
            $this->em->flush();
            $this->logger->error(get_class($this) . ' - method purchaseOrder - Hubo un error Creacion de orden DATAFAST - ' . $e->getMessage());
            return [
                'error' => true,
                'message' => 'Hubo un problema con Datafast'
            ];
        }

        return [
            'error' => false
        ];
    }

    public function notificacionTemporal($parametros)
    {
        $tickets = $parametros['tickets'];
        /**@var UserPi|null $user */
        $user = $this->security->getUser();

        $email = (new TemplatedEmail())
            ->from('no-reply@printo.cloud')
            ->to(new Address($user->getEmail()))
            ->subject('PRINTO Notificacion Envio de Paquetes')

            // path of the Twig template to render
            ->htmlTemplate('emails/notificacion-envio-paquetes.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'tickets' => $tickets
            ]);
        
            
        $this->mailer->send($email);
    }

    /**
     * @var $parametros['tickets'] : Tickets del envio agrupado
     */
    public function realizarEmpaqueTickets($parametros)
    {

        $user = $this->security->getUser();
        $parametros['user'] = $user;

        // Realizar Empaque en Printo
        $this->empacarTickets($parametros);

        // Realizar Empaque en Zoho APi de Cris
        // $this->zohoApiService->desempaquetarCrearNuevosPaquetes($parametros);
        $this->notificacionTemporal($parametros);


        return;
    }

    /**
     * @var $parametros['tickets'] : Tickets del envio agrupado
     */
    public function empacarTickets($parametros)
    {
        $user    = $parametros['user'];
        $dfTrx   = $parametros['dfTrx'];
        $tickets = $parametros['tickets'];

        $paqueteEnvio = new PaqueteEnvio();
        $paqueteEnvio->setUser($user);
        $paqueteEnvio->setDatafastTrx($dfTrx);
        $paqueteEnvio->setObservacion(json_encode($tickets));
        $this->em->persist($paqueteEnvio);
        $this->em->flush();

        /**@var null|OrdenDetRepository $ordenDetRepo */
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
        $response = $ordenDetRepo->actualizarEnvioItems([
            'user'         => $user,
            'tickets'      => $tickets,
            'paqueteEnvio' => $paqueteEnvio
        ]);

        return $response;
    }
}
