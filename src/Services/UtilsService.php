<?php

namespace App\Services;

class UtilsService
{

    function hideEmailAddress($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            list($first, $last) = explode('@', $email);
            $first = str_replace(substr($first, '3'), str_repeat('*', strlen($first) - 3), $first);
            $last = explode('.', $last);
            $last_domain = str_replace(substr($last['0'], '1'), str_repeat('*', strlen($last['0']) - 1), $last['0']);
            $hideEmailAddress = $first . '@' . $last_domain . '.' . $last['1'];
            return $hideEmailAddress;
        }
    }

    function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    function getRegionPorProvincia($provincia)
    {

        if (
            $provincia == "ESMERALDAS" || $provincia == "MANABI" || $provincia == "GUAYAS" ||
            $provincia == "SANTA ELENA" || $provincia == "EL ORO" || $provincia == "LOR RIOS"
        ) {
            return "COSTA";
        } elseif (
            $provincia == "CARCHI" || $provincia == "IMBABURA" || $provincia == "PICHINCHA" ||
            $provincia == "SANTO DOMINGO" || $provincia == "COTOPAXI" || $provincia == "TUNGURAHUA" ||
            $provincia == "CHIMBORAZO" || $provincia == "BOLIVAR" || $provincia == "CANAR" ||
            $provincia == "AZUAY" || $provincia == "LOJA"
        ) {
            return "SIERRA";
        } elseif (
            $provincia == "SUCUMBIOS" || $provincia == "NAPO" || $provincia == "PASTAZA" ||
            $provincia == "ORELLANA" || $provincia == "MORONA SANTIAGO" || $provincia == "ZAMORA"
        ) {
            return "ORIENTE";
        } elseif ($provincia == "GALAPAGOS") {
            return "INSULAR";
        } 

        return "INSULAR";
    }
}
