<?php

namespace App\Services;

use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoInventoryService
{
    private $client;

    private $urlToken;

    private $urlRequest;

    private $clientId;

    private $secretId;

    private $refreshToken;

    public function __construct(
        HttpClientInterface $client,
        $urlToken,
        $clientId,
        $secretId,
        $refreshToken,
        $urlRequest
    ) {
        $this->client       = $client;
        $this->urlToken     = $urlToken;
        $this->urlRequest   = $urlRequest;
        $this->clientId     = $clientId;
        $this->secretId     = $secretId;
        $this->refreshToken = $refreshToken;
    }

    public function getToken()
    {

        $response = $this->client->request(
            'POST',
            $this->urlToken,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => [
                    'refresh_token' => $this->refreshToken,
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secretId,
                    'grant_type'    => 'refresh_token',
                ]
            ]
        );

        $content = $response->toArray();

        return $content['access_token'];
    }

    public function createPackage($token, $parametros)
    {

        $response = $this->client->request(
            'POST',
            $this->urlRequest . "/packages",
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
                'query' => [
                    'organization_id' => $this->orgId
                ],
                'json' => [
                    'package_number' => $parametros['package_number'],
                    'date' => date('Y-m-d'),
                    'line_items' => $parametros['line_items'],
                    
                ]
            ]
        );

        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content;


    }

    
}
