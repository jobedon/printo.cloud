<?php

namespace App\Services;

use Exception;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoApiService
{

    private $client;

    private $urlRequest;

    private $zapikey;

    private $authType;

    public function __construct(
        HttpClientInterface $client,
        $urlRequest,
        $zapikey,
        $authType
    ) {
        $this->client     = $client;
        $this->urlRequest = $urlRequest;
        $this->zapikey    = $zapikey;
        $this->authType   = $authType;
    }

    public function getContactIdDeskFromContactIBooks($parametros)
    {
        $contactIdBooks = $parametros['contactIdBooks'];
        $query = [
            'auth_type' => $this->authType,
            'zapikey'   => $this->zapikey
        ];

        $formFields = [
            'arguments' => json_encode([
                'contact_id_books' => $contactIdBooks
            ])
        ];

        $formData = new FormDataPart($formFields);

        $url = $this->urlRequest . "/functions/get_contact_id_desk_from_contact_id_books/actions/execute";
        $method = "POST";
        $options = [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'query' => $query,
            'body'  => $formData->bodyToIterable(),
        ];

        $response = $this->client->request($method, $url, $options);


        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            throw new Exception(isset($content['message']) ? $content['message'] : 'Hubo un error');
        }
        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['details']['output'];
    }

    
    public function desempaquetarCrearNuevosPaquetes($parametros)
    {
        
        $query = [
            'auth_type' => $this->authType,
            'zapikey'   => $this->zapikey
        ];

        $ordenes = [];

        $formFields = [
            'arguments' => json_encode([
                'id_envio' => '',
                'proveedor_envio' => '',
                'ordenes' => $ordenes
            ])
        ];

        $formData = new FormDataPart($formFields);

        $url = $this->urlRequest . "/functions/desempaquetar_y_crear_nuevos_paquetes/actions/execute";
        $method = "POST";
        $options = [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'query' => $query,
            'body'  => $formData->bodyToIterable(),
        ];

        $response = $this->client->request($method, $url, $options);


        if ($response->getStatusCode() >= 300) {
            $content = $response->toArray(false);
            throw new Exception(isset($content['message']) ? $content['message'] : 'Hubo un error');
        }
        if (empty($response->getContent())) {
            return null;
        }

        $content = $response->toArray();

        return $content['details']['output'];

        return "ok";
    }

}
