<?php

namespace App\Services;

use App\Entity\Configuraciones;
use App\Entity\Direccion;
use App\Entity\OrdenDet;
use App\Repository\DireccionRepository;
use App\Repository\OrdenDetRepository;
use App\Services\Externo\ServientregaService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Security;

class PagosService
{

    private $em;
    private $serService;
    private $utiService;
    private $picService;
    private $security;
    private $contactosService;

    public function __construct(
        EntityManagerInterface $em,
        ServientregaService $serService,
        UtilsService $utiService,
        PickerService $picService,
        Security $security,
        ContactosService $contactosService
    ) {
        $this->em = $em;
        $this->serService = $serService;
        $this->utiService = $utiService;
        $this->picService = $picService;
        $this->security   = $security;
        $this->contactosService = $contactosService;
    }

    public function calcularTarifas($parametros)
    {
        $items       = $parametros['items'];
        $metodoEnvio = (isset($parametros['metodoEnvio'])) ? $parametros['metodoEnvio'] : null;

        // Calcular valor pagado por orden
        /**@var null|OrdenDet $ordenDet */
        $ordenDet = $items[0];
        /**@var null|OrdenCab $ordenCab */
        $ordenCab = $ordenDet->getOrdenCab();
        $totalEnvioOrden = $ordenCab->getTotalEnvio();

        // Calcular Saldo de Envios a Favor generado por Paquetes
        $totalEnviosFavor = 0; //CALCULAR

        // Peso Total de los 
        // Cambio de SERVIENTREGA PICKER A NORMAL Y EXPRESS
        // $totalPesoItems = $this->calcularPesoTotalItems(['items' => $items]);
        $totalPesoItems = 0; // No se calcula ahora por pesos
        $parametros['totalPesoItems'] = $totalPesoItems;
        $parametros['items']          = $items;
        
        // Calculo de descuento
        //$subtotal = $ordenCab->getTotalItems(); // PREGUNTAS A KEVIN SI COJO EL TOTAL DE LA ORDEN O DE LOS TICKETS SELECCIONADOS
        $subtotal = $this->getSubtotalItemsSeleccionados($items);
        $parametros['subtotal'] = $subtotal;


        // Costo de envio por tickets
        $totalEnvioItems = $this->calcularEnvioItems($parametros);
        
        // Cambio de SERVIENTREGA PICKER A NORMAL Y EXPRESS
        // $totalEnviosDescuento = $this->calcularDescuentos([
        //     'totalEnvio' => $totalEnvioItems,
        //     'subtotal'   => $subtotal
        // ]);

        $totalEnviosDescuento = 0;

        // Total a pagar
        $totalEnviosPagar = $totalEnvioItems - $totalEnviosDescuento;

        // Calcular rubros generales
        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);

        $rubroIva = $confRepo->getConfigDouble('PORCENTAJE_IVA');

        $subtotalConIva = $subtotal * ($rubroIva / 100);
        $subtotalItemsEnvios = $subtotal + $totalEnviosPagar;
        $totalIva = $subtotalItemsEnvios * ($rubroIva / 100);
        $totalPagar = $subtotalItemsEnvios;
        $totalPagarItemsEnvios = $subtotal + $subtotalConIva;

        // $requiereEnvio = ($metodoEnvio == 'SERVIENTREGA' || $metodoEnvio == 'PICKER') ? 'S' : 'N';
        $requiereEnvio = ($metodoEnvio == 'NORMAL' || $metodoEnvio == 'EXPRESS') ? 'S' : 'N';

        $tarifas = [
            'subtotalItems' => $subtotal,
            'subtotalItemsIva' => $subtotalConIva,
            'rubrosEnvios' => [
                // 'totalPesoItems' => $totalPesoItems,
                // 'totalFavor'     => $totalEnviosFavor,
                // 'totalItems'     => $totalEnvioItems,
                // 'totalDescuento' => $totalEnviosDescuento,
                'totalPagar'     => $totalEnviosPagar
            ],
            'subtotalItemsEnvios'    => $subtotalItemsEnvios,
            'subtotalItemsEnviosIva' => $totalIva,

            'totalPagarItemsEnvios'  => $totalPagarItemsEnvios,
            'requiereEnvio'          => $requiereEnvio
        ];
        return $tarifas;
    }

    function calcularPesoTotalItems($parametros)
    {
        $items = $parametros['items'];
        $user  = $this->security->getUser();
        /**@var null|OrdenDetRepository $ordenDetRepo */
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
        $totalPesoItems = $ordenDetRepo->getPesoTotalPorItems([
            'items' => $items,
            'user'  => $user
        ]);
        $totalPesoItems = round($totalPesoItems, 2);
        return $totalPesoItems;
    }

    /**
     * Calcula el valor del Envio para:
     * SERVIENTREGA
     * PICKER
     */
    function calcularEnvioItems($parametros)
    {
        $metodoEnvio = (isset($parametros['metodoEnvio'])) ? $parametros['metodoEnvio'] : null;

        if ($metodoEnvio == 'SERVIENTREGA') {
            $totalEnvioItems = $this->calcularEnvioItemsServientrega($parametros);
        } elseif ($metodoEnvio == 'PICKER') {
            $totalEnvioItems = $this->calcularEnvioItemsPicker($parametros);
        } elseif ($metodoEnvio == 'NORMAL') {
            $totalEnvioItems = $this->calcularEnvioItemsNormal($parametros);
        } elseif ($metodoEnvio == 'EXPRESS') {
            $totalEnvioItems = $this->calcularEnvioItemsPicker($parametros);
        } else {
            $totalEnvioItems = 0;
        }

        return $totalEnvioItems;
    }

    function calcularEnvioItemsNormal($parametros)
    {
        $subtotal = $parametros['subtotal'];
        $validaOrdenesCompletas = $parametros['validaOrdenesCompletas'];

        if ($validaOrdenesCompletas == 'S' && $subtotal >= 20) {
            return 0;
        }
        return 5;

    }


    /**
     * Calcula el valor del Envio por Picker
     */
    function calcularEnvioItemsPicker($parametros)
    {
        $user        = $this->security->getUser();
        $contactoId  = $parametros['contactoId'];
        $direccionId = $parametros['direccionId'];

        $contactos = $this->contactosService->obtenerContactos();
        $direccion = $contactos[$contactoId]['direcciones'][$direccionId];

        if (empty($direccion)) {
            throw new Exception('No existe direccion asociada a la cuenta');
        }

        $datos = [
            'latitude'  => explode(',', $direccion['Coordenadas'])[0],
            'longitude' => explode(',', $direccion['Coordenadas'])[1]
        ];

        $totalEnvioItems = $this->picService->preCheckout($datos);

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        $tasaIvaPicker = $confRepo->getConfigDouble('PORCENTAJE_IVA'); //0.12

        $totalEnvioItems = $totalEnvioItems * (1 + $tasaIvaPicker/100);

        if (empty($totalEnvioItems)) {
            throw new Exception('Hubo un problema con Picker, por favor intentar más tarde');
        }

        $totalNormal = 0;
        //$totalNormal = $this->calcularEnvioItemsNormal($parametros);

        return $totalEnvioItems + $totalNormal;
    }

    /**
     * Calcula el valor del Envio por Servientrega
     */
    function calcularEnvioItemsServientrega($parametros)
    {
        $contactoId     = $parametros['contactoId'];
        $direccionId    = $parametros['direccionId'];
        $totalPesoItems = $parametros['totalPesoItems'];

        // Calcular valor de Servientrega
        $tarifas = $this->getPrecioServientrega([
            'contactoId'  => $contactoId,
            'direccionId' => $direccionId,
        ]);

        $precioInicial   = $tarifas['precioInicial'];
        $precioAdicional = $tarifas['precioAdicional'];

        /**@var null|ConfiguracionesRepository $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        $arranqueServientregaKg = $confRepo->getConfigDouble('ARRANQUE_SERVIENTREGA_KG'); // Defaul 2
        $arranqueServientregaKg = (empty($arranqueServientregaKg)) ? 2 : $arranqueServientregaKg;

        $vecesPeso = $totalPesoItems / $arranqueServientregaKg;

        if ($vecesPeso < 1) {
            $totalEnvioItems = $precioInicial;
        } else {
            $vecesPesoCeil = ceil($vecesPeso);
            $totalEnvioItems = $precioInicial + ($vecesPesoCeil - 1) * $precioAdicional;
        }

        return $totalEnvioItems;
    }


    /**
     * Otiene las tarifas de Servientrega, a partir del id de la ciudad
     */
    public function getPrecioServientrega($parametros)
    {

        $contactoId  = $parametros["contactoId"];
        $direccionId = $parametros["direccionId"];

        /** @var DireccionRepository|null $dirRepo */
        $dirRepo = $this->em->getRepository(Direccion::class);

        $contactos = $this->contactosService->obtenerContactos();


        $direccion = $contactos[$contactoId]['direcciones'][$direccionId];

        //$direccion = $dirRepo->find($direccionId);

        $provincia = $direccion['Provincia_Destino'];
        $ciudad    = $direccion['Ciudad_Destino1'];
        $region    = $this->utiService->getRegionPorProvincia($provincia);

        return $this->serService->getTarifasServientrega([
            'region'    => $region,
            'provincia' => $provincia,
            'ciudad'    => $ciudad
        ]);
    }

    /**
     * Obtiene el precio de los items Seleccionados
     */
    function getSubtotalItemsSeleccionados($items)
    {
        $subtotal = 0;

        /**@var null|OrdenDet $item */
        foreach ($items as $item) {
            $subtotal += $item->getPrecio();
        }
        return $subtotal;
    }



    /**
     * Calcula el valor de descuento por envio, en base del total del envio
     * y el subtotal de los items seleccionados
     */
    function calcularDescuentos($parametros)
    {

        $totalEnvio = $parametros['totalEnvio'];
        $subtotal   = $parametros['subtotal'];

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);

        $rubroIva          = $confRepo->getConfigDouble('PORCENTAJE_IVA');
        $umbralSup         = $confRepo->getConfigDouble('UMBRAL_SUPERIOR_VECES'); //8
        $umbralInf         = $confRepo->getConfigDouble('UMBRAL_INFERIOR_VECES'); //4
        $tasaEnvioGratis   = $confRepo->getConfigDouble('TASA_ENVIO_GRATIS'); //1
        $tasaEnvioParcial  = $confRepo->getConfigDouble('TASA_ENVIO_PARCIAL'); //0.5
        $tasaEnvioCompleto = $confRepo->getConfigDouble('TASA_ENVIO_COMPLETO'); //0

        $subtotal = $subtotal * (($rubroIva / 100) + 1);

        if ($subtotal > ($umbralSup * $totalEnvio)) {
            $descuento = $totalEnvio * $tasaEnvioGratis;
        } elseif ($subtotal <= ($umbralSup * $totalEnvio) && $subtotal > (4 * $totalEnvio)) {
            $descuento = $totalEnvio * $tasaEnvioParcial;
        } elseif ($subtotal <= ($umbralInf * $totalEnvio)) {
            $descuento = $totalEnvio * $tasaEnvioCompleto;;
        }

        return $descuento;
    }
}
