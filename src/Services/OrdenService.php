<?php

namespace App\Services;

use App\Entity\Caracteristica;
use App\Entity\Configuraciones;
use App\Entity\DatafastTrx;
use App\Entity\OrdenCab;
use App\Entity\OrdenDet;
use App\Entity\OrdenDetCaracteristica;
use App\Entity\Producto;
use App\Entity\ProductoCaracteristica;
use App\Entity\ProductoFavorito;
use App\Entity\SubcategoriaProducto;
use App\Entity\TarifarioElectrocorteXerox;
use App\Entity\TarifarioMimaki;
use App\Entity\TarifarioXerox;
use App\Entity\TipoProducto;
use App\Entity\UserPi;
use App\Entity\UserTc;
use App\Exceptions\OrdenException;
use App\Exceptions\TransferFileException;
use App\Repository\CaracteristicaRepository;
use App\Repository\ConfiguracionesRepository;
use App\Repository\FilesOrdenCabRepository;
use App\Repository\OrdenCabRepository;
use App\Repository\OrdenDetCaracteristicaRepository;
use App\Repository\OrdenDetRepository;
use App\Repository\ProductoCaracteristicaRepository;
use App\Repository\ProductoRepository;
use App\Repository\TarifarioElectrocorteXeroxRepository;
use App\Repository\TarifarioMimakiRepository;
use App\Repository\TarifarioXeroxRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use NumberFormatter;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class OrdenService
{

    private $tokenService;

    private $zohoBooksService;

    private $zohoWDService;

    private $zohoDeskService;

    private $pickerService;

    private $em;

    private $security;

    /** @var SessionInterface|null $session */
    private $session;

    private $dirService;

    private $logger;

    private $util;

    private $dataFastService;

    private $pagService;

    private $errorService;

    private $productoService;

    private $flashBagService;

    private $entityId;

    private $env;

    private $shopperMid;

    private $shopperTid;

    private $shopperEci;

    private $shopperPserv;

    private $parentIdTkFinalizadas;

    private $successCodes = array('000.100.112', '000.100.110', '000.000.000', '000.200.100');

    private $estadoOrdenCab = 'A';

    private $comprobanteDirectory;

    private $slugger;

    public function __construct(
        TokenService $tokenService,
        ZohoBooksService $zohoBooksService,
        ZohoWorkDriveService $zohoWDService,
        ZohoDeskService $zohoDeskService,
        PickerService $pickerService,
        EntityManagerInterface $em,
        Security $security,
        SessionInterface $session,
        DireccionesService $dirService,
        LoggerInterface $logger,
        DataFastService $dataFastService,
        UtilsService $util,
        ErrorService $errorService,
        ProductoService $productoService,
        FlashBagInterface $flashBagService,
        PagosService $pagService,
        SluggerInterface $slugger,
        $entityId,
        $appEnv,
        $shopperMid,
        $shopperTid,
        $shopperEci,
        $shopperPserv,
        $parentIdTkFinalizadas,
        $comprobanteDirectory
    ) {
        $this->tokenService          = $tokenService;
        $this->zohoBooksService      = $zohoBooksService;
        $this->zohoWDService         = $zohoWDService;
        $this->zohoDeskService       = $zohoDeskService;
        $this->pickerService         = $pickerService;
        $this->em                    = $em;
        $this->security              = $security;
        $this->session               = $session;
        $this->dirService            = $dirService;
        $this->logger                = $logger;
        $this->dataFastService       = $dataFastService;
        $this->errorService          = $errorService;
        $this->productoService       = $productoService;
        $this->pagService            = $pagService;
        $this->util                  = $util;
        $this->env                   = $appEnv;
        $this->entityId              = $entityId;
        $this->shopperMid            = $shopperMid;
        $this->shopperTid            = $shopperTid;
        $this->shopperEci            = $shopperEci;
        $this->shopperPserv          = $shopperPserv;
        $this->parentIdTkFinalizadas = $parentIdTkFinalizadas;
        $this->flashBagService       = $flashBagService;
        $this->comprobanteDirectory  = $comprobanteDirectory;
        $this->slugger               = $slugger;
    }

    function prepareOrdenInit()
    {
        $esProforma = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $user = $this->security->getUser();

        $prodRepo = $this->em->getRepository(Producto::class);

        // Obtener Productos Padres
        $productos = $prodRepo->findBy([
            'estado' => 'A',
            'producto' => null
        ]);

        $this->refreshMateriales();

        $ordenRepo = $this->em->getRepository(OrdenCab::class);
        $orden = $ordenRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user'   => $user
        ]);

        $itemsOrden = [];
        $tarifas = [];
        if (empty($orden)) {
            $orden = new OrdenCab();
            $orden->setUser($user);
            $orden->setEstado($estadoOrdenCab);
            $this->em->persist($orden);
            $this->em->flush();
        } else {

            $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
            $itemsOrden = $ordenDetRepo->findBy([
                'estado' => 'A',
                'ordenCab' => $orden
            ], [
                'id' => 'DESC'
            ]);
            if (!empty($itemsOrden)) {
                $this->actualizarPrecios($orden);
                $this->calcularTarifas([
                    'items' => $itemsOrden
                ]);
            } else {
                $orden->resetPrecios();
            }
        }

        $tarifas = $orden->getArrayData();

        return [
            'productos'  => $productos,
            'itemsOrden' => $itemsOrden,
            'tarifas'    => $tarifas
        ];
    }

    public function refreshMateriales()
    {
        $materiales = $this->productoService->getMateriales();
        $this->session->set('materiales', $materiales);
    }

    public function getZohoProductos()
    {

        $token = $this->tokenService->getBooksToken();

        $productosTodos = [];

        $page = 1;
        do {
            $productos = $this->zohoBooksService->getAllItem($token, $page);
            $productosTodos = array_merge($productosTodos, $productos['items']);
            $page++;
        } while ($productos['page_context']['has_more_page']);

        return $productosTodos;
    }

    public function categorizarProductos($productosZoho)
    {

        $subcatRepo = $this->em->getRepository(SubcategoriaProducto::class);
        $catRepo    = $this->em->getRepository(TipoProducto::class);
        $proFavRepo = $this->em->getRepository(ProductoFavorito::class);
        $subcategorias = $subcatRepo->findBy(['estado' => 'A']);
        $categorias    = $catRepo->findBy(['estado' => 'A']);
        $user = $this->security->getUser();

        $favoritos = $proFavRepo->findBy(['user' => $user, 'estado' => 'A']);

        $categorizacion = [];
        foreach ($subcategorias as $subcategoria) {
            $categorizacion[$subcategoria->getCategoriaProducto()->getTipoProducto()->getCategoria()]['subcategorias'][] = $subcategoria->getSubcategoria();
        }

        $categorizacionFav = [];
        foreach ($favoritos as $fav) {
            $categorizacionFav[] = $fav->getProductoId();
        }

        $productosCategorizados = [];
        foreach ($productosZoho as $producto) {



            if ($producto['status'] == 'active') {

                foreach ($categorias as $categoria) {
                    if (in_array($producto['category_name'], $categorizacion[$categoria->getCategoria()]['subcategorias'])) {
                        if (in_array($producto['item_id'], $categorizacionFav)) {
                            $categorizacion[$categoria->getCategoria()]['productosFav'][] = $producto;
                        } else {
                            $categorizacion[$categoria->getCategoria()]['productos'][] = $producto;
                        }
                    }
                }
            }
        }

        return $categorizacion;
    }

    public function existProductZoho($item, $productos)
    {
        foreach ($productos as $producto) {
            if ($producto['item_id'] == $item->getProductoId() && $producto['sku'] == $item->getSku()) {
                return $producto;
            }
        }
        return false;
    }

    public function filterActiveProducts($productos)
    {

        $tarMimRepo = $this->em->getRepository(TarifarioMimaki::class);
        $itemsMimaki = $tarMimRepo->findBy([
            'estado' => 'A'
        ]);

        $productosMimaki = [];
        $productosMimakiFav = [];
        foreach ($itemsMimaki as $item) {
            $productoFound = $this->existProductZoho($item, $productos["MIMAKI"]["productos"]);
            if ($productoFound) {
                $productosMimaki[] = $productoFound;
            }

            if (isset($productos["MIMAKI"]["productosFav"])) {
                $productoFoundFav = $this->existProductZoho($item, $productos["MIMAKI"]["productosFav"]);
                if ($productoFoundFav) {
                    $productosMimakiFav[] = $productoFoundFav;
                }
            }
        }

        $productos["MIMAKI"]["productos"] = $productosMimaki;
        $productos["MIMAKI"]["productosFav"] = $productosMimakiFav;
        return $productos;
    }

    public function getProductos()
    {
        // obtiene productos de Zoho
        $productosZoho = $this->getZohoProductos();

        // categoriza los productos
        $productos = $this->categorizarProductos($productosZoho);
        // categoriza por favoritos

        $productos = $this->filterActiveProducts($productos);

        return $productos;
    }

    /**
     * 
     * Calcula todas las tarifas de los productos a analizar
     * 
     * Configuraciones :
     * PROMO_XEROX Porcentaje de reducción al precio del tarifario
     * 
     */
    public function actualizarPrecios(OrdenCab $orden)
    {
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
        $itemsOrden = $ordenDetRepo->findBy([
            'estado' => 'A',
            'ordenCab' => $orden
        ]);

        $msg = '';

        $dataRef = [];
        $laminado = [
            'TIRO' => 0,
            'T/R'  => 0
        ];
        $electrocorte = [
            'adhesivo' => 0,
            'cartulina' => 0
        ];
        foreach ($itemsOrden as $item) {
            // Logica para XEROX
            $tipoProducto = $item->getProducto()->getMaquina();
            if ($tipoProducto == "XEROX") {
                // Logica Base
                // $dataRef[$item->getSubcategoriaProducto()->getId()][$item->getTipoImpresion()] = (isset($dataRef[$item->getSubcategoriaProducto()->getId()][$item->getTipoImpresion()])) ? $dataRef[$item->getSubcategoriaProducto()->getId()][$item->getTipoImpresion()] + $item->getCantidad() : $item->getCantidad();
                $dataRef[$item->getProducto()->getCategoriaProducto()->getId()] = (isset($dataRef[$item->getProducto()->getCategoriaProducto()->getId()])) ? $dataRef[$item->getProducto()->getCategoriaProducto()->getId()] + $item->getCantidad() : $item->getCantidad();
                // Logica Laminado
                if ($item->getLaminado() == 'S') {
                    $laminado[$item->getTipoImpresion()] += $item->getCantidad();
                }
                // Logica Electrocorte
                if ($item->getTipoCorte() == 'ELECTROCORTE') {
                    if ($item->getProducto()->getCategoriaProducto()->getCategoria() == 'Adhesivos') {
                        $electrocorte['adhesivo'] += $item->getCantidad();
                    } else {
                        $electrocorte['cartulina'] += $item->getCantidad();
                    }
                }
            }
        }

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        // calculo de $laminado XEROX
        $laminadoTiro   = $confRepo->getConfigDouble('PRECIO_LAMINADO_TIRO');
        $laminadoTR     = $confRepo->getConfigDouble('PRECIO_LAMINADO_TR');
        $limiteLaminado = $confRepo->getConfigDouble('VALOR_LIMITE_LAMINADO');
        $precioPorRecto = $confRepo->getConfigDouble('PRECIO_CORTE_RECTO');
        $precioPorPlano = $confRepo->getConfigDouble('PRECIO_CORTE_PLANAS');
        $rubroIva       = $confRepo->getConfigDouble('PORCENTAJE_IVA');
        $promoXerox     = $confRepo->getConfigDouble('PROMO_XEROX');

        // calculo de $laminado MIMAKI
        $precioElecMimaki   = $confRepo->getConfigDouble('PRECIO_ELECTROCORTE_MIMAKI');
        $precioOjalMimaki   = $confRepo->getConfigDouble('PRECIO_OJALES_MIMAKI');
        $precioLamBriMimaki = $confRepo->getConfigDouble('PRECIO_LAMINADO_BRILLO_MIMAKI');
        $precioLamMatMimaki = $confRepo->getConfigDouble('PRECIO_LAMINADO_MATE_MIMAKI');
        $precioLamFloMimaki = $confRepo->getConfigDouble('PRECIO_LAMINADO_FLOORGRAPHIC_MIMAKI');

        // Precio minimo Mimaki
        $precioMinMimaki = $confRepo->getConfigDouble('PRECIO_MINIMO_MIMAKI');

        $totalLaminado = $laminado['TIRO'] * $laminadoTiro + $laminado['T/R'] * $laminadoTR;

        $banderaLaminado = false;
        if ($totalLaminado < $limiteLaminado) {
            $totalLaminado = $limiteLaminado;
            $banderaLaminado = true;
        }

        $laminadoPorUnidad = ($laminado['TIRO'] + $laminado['T/R'] == 0) ? $totalLaminado : $totalLaminado / ($laminado['TIRO'] + $laminado['T/R']);

        /** @var TarifarioXeroxRepository|null $tarXeroxRepo */
        $tarXeroxRepo = $this->em->getRepository(TarifarioXerox::class);

        /** @var TarifarioMimakiRepository|null $tarMimamkiRepo */
        $tarMimamkiRepo = $this->em->getRepository(TarifarioMimaki::class);

        /** @var TarifarioElectrocorteXeroxRepository|null $tarElecXeroxRepo */
        $tarElecXeroxRepo = $this->em->getRepository(TarifarioElectrocorteXerox::class);
        foreach ($itemsOrden as $item) {
            $precioBase = 0.00;
            $precioCorte = 0.00;
            $precioLaminado = 0.00;
            $precioTotal = 0.00;
            $precioOjales = 0.00;
            $precioRecto = 0.00;

            $tipoProducto = $item->getProducto()->getMaquina();
            if ($tipoProducto == "XEROX") {
                $tarifaBase = $tarXeroxRepo->findTarifa($item, $dataRef);
                $precioBase = $tarifaBase * $item->getCantidad();
            } elseif ($tipoProducto == "MIMAKI") {
                $tarifaBase = $tarMimamkiRepo->findTarifa($item);
                $precioBase = ($item->getAncho() * $item->getAlto()) * $tarifaBase * $item->getCantidad();
            }

            if ($item->getLaminado() == 'S') {
                if ($tipoProducto == "XEROX") {
                    if ($banderaLaminado) {
                        $precioLaminado = $laminadoPorUnidad * $item->getCantidad();
                    } else {
                        if ($item->getTipoImpresion() == 'TIRO') {
                            $precioLaminado = $laminadoTiro * $item->getCantidad();
                        } elseif ($item->getTipoImpresion() == 'T/R') {
                            $precioLaminado = $laminadoTR * $item->getCantidad();
                        }
                    }
                } elseif ($tipoProducto == "MIMAKI") {
                    if ($item->getTipoLaminado() == "BRILLO") {
                        $precioLaminado = $precioLamBriMimaki;
                    } elseif ($item->getTipoLaminado() == "MATE") {
                        $precioLaminado = $precioLamMatMimaki;
                    } elseif ($item->getTipoLaminado() == "FLOOR GRAPHIC") {
                        $precioLaminado = $precioLamFloMimaki;
                    } else {
                        //error de tipo de laminado
                    }
                    $precioLaminado = ($item->getAncho() * $item->getAlto()) * $precioLaminado;
                }
            }

            if ($item->getOjales() == 'S') {
                if ($tipoProducto == "MIMAKI") {
                    $precioOjales = $item->getCantidadOjales() * $precioOjalMimaki;
                }
            }

            if ($tipoProducto == "XEROX") {
                if ($item->getTipoCorte() == 'RECTO') {
                    $precioCorte = $precioPorRecto;
                } elseif ($item->getTipoCorte() == 'PLANAS') {
                    $precioCorte = $precioPorPlano;
                } elseif ($item->getTipoCorte() == 'ELECTROCORTE') {
                    $tarifaElectrocorte = $tarElecXeroxRepo->findTarifa($item, $electrocorte);
                    // Validar Subscripcion Profesional y Profesional Plus
                    $precioCorte = $tarifaElectrocorte * $item->getCantidad();
                }
            } elseif ($tipoProducto == "MIMAKI") {
                if ($item->getElectrocorte() == 'S') {
                    $precioCorte = ($item->getAncho() * $item->getAlto()) * $precioElecMimaki;
                }
            }

            // Calculo precio corte RECTO Rigidos
            if ($tipoProducto == "MIMAKI") {
                if ($item->getCorteRigido() == 'S') {
                    $precioCorteRigido = $tarMimamkiRepo->findTarifaCorteRecto($item);
                    $precioRecto = ($item->getAncho() * $item->getAlto()) * $precioCorteRigido * $item->getCantidad();
                }
            }

            // Promociones XEROX
            $precioBaseReal = $precioBase;
            if ($tipoProducto == "XEROX") {
                $precioBase = round($precioBase * ((100 - $promoXerox) / 100), 2);
            } else {
                $precioBaseReal = 0;
            }
            $item->setPrecioBase($precioBase);
            $item->setPrecioBaseReal($precioBaseReal);

            // Precios para XEROX
            $precioBase     = round($precioBase, 2);                                    // Precio base
            $precioCorte    = round($precioCorte, 2);                                   // Precio base CORTE
            $precioLaminado = round($precioLaminado, 2);                                // Precio base LAMINADO

            $item->setPrecioBase($precioBase);
            $item->setPrecioLaminado($precioLaminado);
            $item->setPrecioCorte($precioCorte);

            if ($tipoProducto == "MIMAKI") {                                            // Precios para MIMAKI
                $precioOjales = round($precioOjales, 2);
                $precioRecto  = round($precioRecto, 2);                                 // Precio base RECTO

                $precioBase     = round($precioBase, 2);                                // Precio base
                $precioLaminado = round($precioLaminado * $item->getCantidad(), 2);     // Precio base LAMINADO

                $precioCorte = round($precioCorte * $item->getCantidad(), 2);           // Precio base CORTE
                $precioOjales = round($precioOjales * $item->getCantidad(), 2);         // Precio base OJALES

                $item->setPrecioBase($precioBase);
                $item->setPrecioOjales($precioOjales);
                $item->setPrecioCorteRigido($precioRecto);
                $item->setPrecioLaminado($precioLaminado);
            }

            $precioTotalReal = $precioBase +  $precioCorte + $precioLaminado + $precioOjales + $precioRecto;

            // Se ajusta precio base para enviarlo a Zoho igual
            $precioTotal = round(round($precioTotalReal / $item->getCantidad(), 2) * $item->getCantidad(), 2);

            // Ajuste por excedente o faltante
            $precioBase = $precioBase + ($precioTotal - $precioTotalReal);
            $item->setPrecioBase($precioBase);





            $item->setPrecio($precioTotal);

            if ($tipoProducto == "MIMAKI" && $item->getPrecio() < $precioMinMimaki) {
                $item->setPrecioBase($precioMinMimaki);
                $item->setPrecioLaminado(0.00);
                $item->setPrecioCorte(0.00);
                $item->setPrecioOjales(0.00);
                $item->setPrecioCorteRigido(0.00);
                $item->setPrecio($precioMinMimaki);
                $msg = 'Usted ha hecho una compra de $ ' . $precioTotal . ', '
                    . 'sin embargo el costo mìnimo es de $' . $precioMinMimaki . ' + IVA, '
                    . 'se ha ajustado al precio mínimo';
            }
        }

        $this->em->flush();

        return $msg;
    }

    /**
     * @deprecated
     */
    public function calcularTarifaPicker($direccion, $subtotal)
    {
        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);

        $rubroIva          = $confRepo->getConfigDouble('PORCENTAJE_IVA');
        $topeContraEntrega = $confRepo->getConfigDouble('VALOR_TOPE_CONTRA_ENTREGA_PICKER');

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenRepo = $this->em->getRepository(OrdenCab::class);

        /** @var OrdenCab|null $orden */
        $orden = $ordenRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user'   => $this->security->getUser()
        ]);

        $orden->resetPrecios();
        $tarifaPicker = 0;
        $tarifaDescuento = 0;
        $requiereEnvio = 'N';
        $direccionId = '';
        if ($direccion != null) {
            $direccionId = $direccion['address_id'];
            $requiereEnvio = 'S';
            $street2 = $direccion['street2'];
            $street2 = preg_replace('/\s+/', ' ', $street2);
            $street2 = explode(" ", $street2);
            $latitud  = str_replace(',', '', $street2[2]);
            $longitud = str_replace(',', '', $street2[3]);

            $datos = [
                'latitude'  => $latitud,
                'longitude' => $longitud
            ];

            $tarifaPicker = $this->pickerService->preCheckout($datos);

            // calculos de descuento
            $umbralSup         = $confRepo->getConfigDouble('UMBRAL_SUPERIOR_VECES'); //8
            $umbralInf         = $confRepo->getConfigDouble('UMBRAL_INFERIOR_VECES'); //4
            $tasaEnvioGratis   = $confRepo->getConfigDouble('TASA_ENVIO_GRATIS'); //1
            $tasaEnvioParcial  = $confRepo->getConfigDouble('TASA_ENVIO_PARCIAL'); //0.5
            $tasaEnvioCompleto = $confRepo->getConfigDouble('TASA_ENVIO_COMPLETO'); //0
            $tasaIncPicker     = $confRepo->getConfigDouble('TASA_INCREMENTO_PICKER'); //0.15

            $tarifaPicker = $tarifaPicker * (1 + $tasaIncPicker);

            $valorRefIva      = $subtotal * (($rubroIva / 100) + 1);

            if ($valorRefIva > ($umbralSup * $tarifaPicker)) {
                $tarifaDescuento = $tarifaPicker * $tasaEnvioGratis;
            } elseif ($valorRefIva <= ($umbralSup * $tarifaPicker) && $valorRefIva > (4 * $tarifaPicker)) {
                $tarifaDescuento = $tarifaPicker * $tasaEnvioParcial;
            } elseif ($valorRefIva <= ($umbralInf * $tarifaPicker)) {
                $tarifaDescuento = $tarifaPicker * $tasaEnvioCompleto;;
            }
        }

        $valorSubtotal = $subtotal + $tarifaPicker - $tarifaDescuento;
        $valorIva      = $valorSubtotal * ($rubroIva / 100);
        $totalPagar    = $valorSubtotal + $valorIva;
        $valorSubIva   = $subtotal * (($rubroIva / 100) + 1);

        $esAutomatico = $valorSubIva > $topeContraEntrega;

        $fmt = new NumberFormatter('en_EC', NumberFormatter::CURRENCY);


        $orden->setTotalItems($subtotal);
        $orden->setTotalEnvio($tarifaPicker - $tarifaDescuento);
        $orden->setSubtotal($valorSubtotal);
        $orden->setTotalIva($valorIva);
        $orden->setTotalPagar($totalPagar);
        $orden->setRequiereEnvio($requiereEnvio);
        $orden->setDireccionId($direccionId);

        $this->em->flush();

        $precioMinimoCompra = $confRepo->getConfigDouble('PRECIO_MINIMO_COMPRA');; //0
        $ordenPermitida = ($subtotal < $precioMinimoCompra);
        $msgOrdenPermitida = '';

        $fmt = numfmt_create('en_US', NumberFormatter::CURRENCY);
        $currency = "USD";

        $fmtSubtotal = numfmt_format_currency($fmt, $subtotal, $currency);
        $fmtPrecioMinimoCompra = numfmt_format_currency($fmt, $precioMinimoCompra, $currency);

        if ($ordenPermitida) {
            $msgOrdenPermitida = 'Usted ha hecho una compra global de ' . $fmtSubtotal . ' '
                . 'sin embargo el costo mìnimo es de ' . $fmtPrecioMinimoCompra . ' + IVA, '
                . 'edite la orden y cumpla el mínimo de compra general';
        }


        $tarifas = [
            'valorSubtotalItems'   => $fmt->formatCurrency($subtotal, 'USD'),
            'valorSubtotal'        => $fmt->formatCurrency($valorSubtotal, 'USD'),
            'valorPicker'          => $fmt->formatCurrency($tarifaPicker, 'USD'),
            'valorDescuentoPicker' => ($tarifaDescuento == 0) ? $fmt->formatCurrency($tarifaDescuento, 'USD') : '-' . $fmt->formatCurrency($tarifaDescuento, 'USD'),
            'valorTotalEnvio'      => $fmt->formatCurrency($tarifaPicker - $tarifaDescuento, 'USD'),
            'valorIva'             => $fmt->formatCurrency($valorIva, 'USD'),
            'totalPagar'           => $fmt->formatCurrency($totalPagar, 'USD'),
            'esAutomatico'         => $esAutomatico,
            'ordenPermitida'       => $ordenPermitida,
            'msgOrdenPermitida'    => $msgOrdenPermitida
        ];

        return $tarifas;
    }


    public function generarOrden($datos)
    {
        unset($datos['direccionId']);
        $formaPagoCode = $datos['formaPagoCode'];

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        /**@var null|OrdenCabRepository $ordenRepo */
        $ordenRepo = $this->em->getRepository(OrdenCab::class);
        $orden = $ordenRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user'   => $this->security->getUser()
        ]);

        $userBooksZoho = $this->session->get('userBooksZoho');

        $datos['customerId']  = $userBooksZoho['contact_id'];
        $datos['contactName'] = $userBooksZoho['contact_name'];

        $mensaje = "";

        if ($orden->getRequiereEnvio() == "S") {
            $datos['requiereEnvio'] = 'Picker Programado';
            //$datos['direccionId'] = $orden->getDireccionId();
            //$dir = $this->dirService->searchDireccion($datos['direccionId']);
            $dir = $this->dirService->obtenerDireccion();
            $mensaje .= 'El motorizado dejará su pedido en la dirección "' . $dir . '"<br>';
        } else {
            $datos['requiereEnvio'] = 'Retiro en Oficina';
            //$dir = $this->dirService->searchDireccionPorNombre(strtoupper($datos['requiereEnvio']));
            $dir = $this->dirService->obtenerDireccion();
            if (!empty($dir)) {
                //$datos['direccionId']   = $dir['address_id'];
            }
            $mensaje .= 'Por favor acercarse a nuestras oficinas, para poder retirar su orden. <br>';
        }

        if ($datos['pagoMotorizado'] == "true") {
            $datos['pagoMotorizado'] = 'SI - MAXIMO $50 USD';
            $mensaje .= 'Por favor cancelar el valor total al motorizado.<br>';
        } else {
            $datos['pagoMotorizado'] = 'NO';
        }

        $datos['crearTicket']   = "Si";
        $datos['usrIdAsignado'] = "2244816000000069001";


        $itemsOrden = [];
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
        $itemsOrden = $ordenDetRepo->findBy([
            'estado' => 'A',
            'ordenCab' => $orden
        ]);

        $datos['items'] = [];

        $idx = 0;

        /** @var OrdenDet|null $item */
        foreach ($itemsOrden as $item) {
            //$datos['items'][$idx]['item_id']     = $item->getProductoId();
            $datos['items'][$idx]['name']        = $item->getProducto()->getProducto()->getNombreProducto();
            $datos['items'][$idx]['description'] = $item->getProducto()->getNombreProducto() . '\n' . $item->getDescripcion();
            $datos['items'][$idx]['quantity']    = $item->getCantidad();
            $datos['items'][$idx]['rate']        = round($item->getPrecio() / $item->getCantidad(), 2);
            $datos['items'][$idx]['item_order']  = $idx + 1;
            $item->setItemOrder($idx + 1);
            $idx++;
        }



        $datos['payment_terms']       = $userBooksZoho['payment_terms'];
        $datos['payment_terms_label'] = $userBooksZoho['payment_terms_label'];

        $orden->setMetodoPago($formaPagoCode);

        $tokenBooks    = $this->tokenService->getBooksToken();
        $ordenZoho = $this->zohoBooksService->generarOrden($tokenBooks, $datos);
        if (!empty($orden->getComprobantePath())) {
            $attOrderZoho = $this->zohoBooksService->addFileToOrder($tokenBooks, [
                'orderId' => $ordenZoho['salesorder_id'],
                'filePath' => $orden->getComprobantePath(),
            ]);
        }

        if (!empty($formaPagoCode) && $formaPagoCode != 'transfbanc') {
            $confOrderZoho = $this->zohoBooksService->confirmarOrden($tokenBooks, [
                'orderId' => $ordenZoho['salesorder_id'],
            ]);
        }

        // Actualizar line_item_id
        /** @var OrdenDet|null $item */
        foreach ($itemsOrden as $item) {
            $item->setLineItemId($this->findLineItemIdInOrden([
                'ordenZoho' => $ordenZoho,
                'itemOrder' => $item->getItemOrder()
            ]));
        }

        $this->em->flush();


        return [
            'ordenZoho' => $ordenZoho,
            'mensaje'   => $mensaje
        ];
        //return null;
    }

    public function findLineItemIdInOrden($parametros)
    {
        $ordenZoho = $parametros['ordenZoho'];
        $itemOrder = $parametros['itemOrder'];

        foreach ($ordenZoho['line_items'] as $item) {
            if ($item['item_order'] == $itemOrder) {
                return $item['line_item_id'];
            }
        }
        return null;
    }

    public function cerrarOrden($saleOrder)
    {
        $ordenZoho = $saleOrder['ordenZoho'];
        $noOrden   = $ordenZoho['salesorder_number'];

        $user = $this->security->getUser();

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);

        /** @var OrdenCab|null $orden */
        $orden = $ordenCabRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user'   => $user
        ]);
        $orden->setDescripcion($saleOrder['mensaje']);
        $orden->setNoOrden($noOrden);
        $orden->setEstado('P');
        $this->em->flush();
    }

    /**
     * @deprecated Por nueva forma de subir archivos
     */
    public function transferirArchivos($data)
    {
        $noOrden  = $data['orden'];
        $noTicket = $data['ticket'];

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        /** @var OrdenCab|null $ordenCab */
        $ordenCab = $ordenCabRepo->findOneBy([
            'estado'  => 'P',
            'noOrden' => $noOrden
        ]);

        if (empty($ordenCab)) {
            throw new TransferFileException("Orden ya procesada, o inexistente");
        }

        $ordenDet = $ordenDetRepo->findBy([
            'estado' => 'A',
            'ordenCab' => $ordenCab
        ]);

        if (empty($ordenDet)) {
            throw new TransferFileException("Detall de la Orden no encontrada");
        }

        $data = [
            'rutas' => [],
            'ticket' => $noTicket
        ];
        $idx = 1;
        /** @var OrdenDet|null $item */
        foreach ($ordenDet as $item) {
            $data['idx'] = $idx;
            $rutaImpresion = $item->getRutaImpresion();
            if (!empty($rutaImpresion)) {
                // $rutas[]['rutaApp'] = $rutaImpresion;
                $data = $this->transferirArchivo($item, $data);
            }

            $data['idx'] = $idx;
            $rutaElectrocorte = $item->getRutaElectrocorte();
            if (!empty($rutaElectrocorte)) {
                // $rutas[]['rutaApp'] = $rutaElectrocorte;
                $data['tipo'] = 'ELECTROCORTE';
                $data = $this->transferirArchivo($item, $data);
            }
            $idx++;
        }

        $ordenCab->setNoTicket($noTicket);
        $ordenCab->setEstado('I');
        $ordenCab->setFechaActualizacion();
        $this->em->flush();
    }

    /**
     * @deprecated por nuevo formato del api
     */
    public function renameFiles($data)
    {
        $noOrden  = $data['orden'];
        $noTicket = $data['ticket'];

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        /** @var OrdenCab|null $ordenCab */
        $ordenCab = $ordenCabRepo->findOneBy([
            'estado'  => 'P',
            'noOrden' => $noOrden
        ]);

        if (empty($ordenCab)) {
            throw new TransferFileException("Orden ya procesada, o inexistente");
        }

        $ordenDet = $ordenDetRepo->findBy([
            'estado' => 'A',
            'ordenCab' => $ordenCab
        ]);

        if (empty($ordenDet)) {
            throw new TransferFileException("Detall de la Orden no encontrada");
        }

        $data = [
            'rutas' => [],
            'ticket' => $noTicket
        ];

        $tokenWD = $this->tokenService->getWorkDriveToken();

        // se renombran carpetas de items
        $idx = 1;
        $contElectro = 0;
        $contMimaki  = 0;
        $contXerox   = 0;
        /** @var OrdenDet|null $item */
        foreach ($ordenDet as $item) {
            $newNameFolder = "ITEM " . $idx . " - " . $item->getSku();

            if (!empty($item->getFiles()->getParentIdElectroTk())) {
                $this->zohoWDService->renameFolder($tokenWD, $newNameFolder, $item->getFiles()->getParentIdElectroTk());
                $contElectro++;
            }

            if (!empty($item->getFiles()->getParentIdImpresionTk())) {
                $this->zohoWDService->renameFolder($tokenWD, $newNameFolder, $item->getFiles()->getParentIdImpresionTk());
            }
            $tipoCategoria = $item->getSubcategoriaProducto()->getCategoriaProducto()->getTipoProducto()->getCategoria();
            if ($tipoCategoria == 'XEROX') {
                $contXerox++;
            }
            if ($tipoCategoria == 'MIMAKI') {
                $contMimaki++;
            }
            $idx++;
        }

        // se eliminan carpetas cabecera de categoria adicionales
        $tokenWD = $this->tokenService->getWorkDriveToken();
        if (!empty($ordenCab->getFiles()->getParentIdElectroTk()) && $contElectro == 0) {
            try {
                $this->zohoWDService->deleteFolders($tokenWD, [$ordenCab->getFiles()->getParentIdElectroTk()]);
            } catch (Exception $e) {
                $this->logger->info("TRACKING TRANSFERIR >>> " . get_class($this));
            }
            $ordenCab->getFiles()->setParentIdElectroTk(null);
        }

        if (!empty($ordenCab->getFiles()->getParentIdXeroxTk()) && $contXerox == 0) {
            try {
                $this->zohoWDService->deleteFolders($tokenWD, [$ordenCab->getFiles()->getParentIdXeroxTk()]);
            } catch (Exception $e) {
                $this->logger->info("TRACKING TRANSFERIR >>> " . get_class($this));
            }
            $ordenCab->getFiles()->setParentIdXeroxTk(null);
        }

        if (!empty($ordenCab->getFiles()->getParentIdMimakiTk()) && $contMimaki == 0) {
            try {
                $this->zohoWDService->deleteFolders($tokenWD, [$ordenCab->getFiles()->getParentIdMimakiTk()]);
            } catch (Exception $e) {
                $this->logger->info("TRACKING TRANSFERIR >>> " . get_class($this));
            }
            $ordenCab->getFiles()->setParentIdMimakiTk(null);
        }

        $this->em->flush();

        // se renombran carpetas cabecera de categoriza
        $tokenWD = $this->tokenService->getWorkDriveToken();
        $newNameFolder = "#" . $noTicket . " - " . $ordenCab->getUser()->getRazonSocial();

        if (!empty($ordenCab->getFiles()->getParentIdTk())) {
            $this->zohoWDService->renameFolder($tokenWD, $newNameFolder, $ordenCab->getFiles()->getParentIdTk());
        }

        // if (!empty($ordenCab->getParentIdWdElectro())) {
        //     $this->zohoWDService->renameFolder($tokenWD, $newNameFolder, $ordenCab->getParentIdWdElectro());
        // }
        // if (!empty($ordenCab->getParentIdWdXerox())) {
        //     $this->zohoWDService->renameFolder($tokenWD, $newNameFolder, $ordenCab->getParentIdWdXerox());
        // }
        // if (!empty($ordenCab->getParentIdWdMimaki())) {
        //     $this->zohoWDService->renameFolder($tokenWD, $newNameFolder, $ordenCab->getParentIdWdMimaki());
        // }

        $ordenCab->setNoTicket($noTicket);
        $ordenCab->setEstado('I');
        $ordenCab->setFechaActualizacion();
        $this->em->flush();
    }

    /**
     * @deprecated por nuevo formato del api
     */
    public function movePendienteToFinalizadas($data)
    {
        $noOrden  = $data['orden'];
        $noTicket = $data['ticket'];

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        /** @var OrdenCab|null $ordenCab */
        $ordenCab = $ordenCabRepo->findOneBy([
            'estado'  => 'I',
            'noOrden' => $noOrden
        ]);

        if (empty($ordenCab)) {
            throw new TransferFileException("Orden no se encuentra finaizada");
        }

        $ordenDet = $ordenDetRepo->findBy([
            'estado' => 'A',
            'ordenCab' => $ordenCab
        ]);

        if (empty($ordenDet)) {
            throw new TransferFileException("Detall de la Orden no encontrada");
        }

        $data = [
            'rutas' => [],
            'ticket' => $noTicket
        ];

        // ELiminar esta carpeta al final
        $parentIdBaseFolder = $ordenCab->getFiles()->getParentIdTk();

        $tokenWD = $this->tokenService->getWorkDriveToken();

        $folderName = "#" . $noTicket . " - " . $ordenCab->getUser()->getRazonSocial();
        // CARPETA BASE DEL CLIENTE EN PRINTO TICKETS
        $responseBase = $this->zohoWDService->createFolder($tokenWD, $folderName, $this->parentIdTkFinalizadas);
        $ordenCab->getFiles()->setParentIdTk($responseBase);

        //para electrocorte
        if (!empty($ordenCab->getFiles()->getParentIdElectroTk())) {
            //carpeta en TICKETS
            $responseElec = $this->zohoWDService->createFolder($tokenWD, 'ELECTROCORTE', $responseBase);
            $ordenCab->getFiles()->setParentIdElectroTk($responseElec);

            //carpeta en WORKSPACE
            $responseElecWs = $this->zohoWDService->createFolder($tokenWD, $folderName, 'ELECTROCORTE');
            $ordenCab->getFiles()->setParentIdElectroWs($responseElecWs);
        }

        //para xerox
        if (!empty($ordenCab->getFiles()->getParentIdXeroxTk())) {
            //carpeta en TICKETS
            $responseXM = $this->zohoWDService->createFolder($tokenWD, 'XEROX', $responseBase);
            $ordenCab->getFiles()->setParentIdXeroxTk($responseXM);

            //carpeta en WORKSPACE
            $responseXMWs = $this->zohoWDService->createFolder($tokenWD, $folderName, 'XEROX');
            $ordenCab->getFiles()->setParentIdXeroxWs($responseXMWs);
        }

        //para mimaki
        if (!empty($ordenCab->getFiles()->getParentIdMimakiTk())) {
            //carpeta en TICKETS
            $responseXM = $this->zohoWDService->createFolder($tokenWD, 'MIMAKI', $responseBase);
            $ordenCab->getFiles()->setParentIdMimakiTk($responseXM);

            //carpeta en WORKSPACE
            $responseXMWs = $this->zohoWDService->createFolder($tokenWD, $folderName, 'MIMAKI');
            $ordenCab->getFiles()->setParentIdMimakiWs($responseXMWs);
        }

        $this->em->flush();

        foreach ($ordenDet as $item) {
            $tipoCategoria = $item->getSubcategoriaProducto()->getCategoriaProducto()->getTipoProducto()->getCategoria();

            if ($tipoCategoria == 'XEROX') {
                //carpeta en TICKETS
                $finParentId = $this->zohoWDService->copyFolder($tokenWD, [
                    'folderSourceId' => $item->getFiles()->getParentIdImpresionTk(),
                    'folderTargetId' => $ordenCab->getFiles()->getParentIdXeroxTk(),
                ]);
                $item->getFiles()->setParentIdImpresionTk($finParentId);

                //carpeta en WORKSPACE
                $finParentIdWs = $this->zohoWDService->copyFolder($tokenWD, [
                    'folderSourceId' => $item->getFiles()->getParentIdImpresionTk(),
                    'folderTargetId' => $ordenCab->getFiles()->getParentIdXeroxWs(),
                ]);
                $item->getFiles()->setParentIdImpresionWs($finParentIdWs);

                $this->em->flush();
            }

            if ($tipoCategoria == 'MIMAKI') {
                //carpeta en TICKETS
                $finParentId = $this->zohoWDService->copyFolder($tokenWD, [
                    'folderSourceId' => $item->getFiles()->getParentIdImpresionTk(),
                    'folderTargetId' => $ordenCab->getFiles()->getParentIdMimakiTk(),
                ]);
                $item->getFiles()->setParentIdImpresionTk($finParentId);

                //carpeta en WORKSPACE
                $finParentIdWs = $this->zohoWDService->copyFolder($tokenWD, [
                    'folderSourceId' => $item->getFiles()->getParentIdImpresionTk(),
                    'folderTargetId' => $ordenCab->getFiles()->getParentIdMimakiWs(),
                ]);
                $item->getFiles()->setParentIdImpresionWs($finParentIdWs);

                $this->em->flush();
            }

            if ($tipoCategoria == 'XEROX' || $tipoCategoria == 'MIMAKI') {
                //Se actualizan archivos
                $fileTk = $this->zohoWDService->getListFilesFolders($tokenWD, [
                    'folderId' => $item->getFiles()->getParentIdImpresionTk()
                ]);
                if (count($fileTk) >= 1) {
                    $item->getFiles()->setFileIdImpresionTk($fileTk[0]['id']);
                }
                $fileWs = $this->zohoWDService->getListFilesFolders($tokenWD, [
                    'folderId' => $item->getFiles()->getParentIdImpresionWs()
                ]);
                if (count($fileWs) >= 1) {
                    $item->getFiles()->setFileIdImpresionWs($fileWs[0]['id']);
                }

                $this->em->flush();
            }

            if ($item->getElectrocorte() == 'S') {
                //carpeta en TICKETS
                $finParentId = $this->zohoWDService->copyFolder($tokenWD, [
                    'folderSourceId' => $item->getFiles()->getParentIdElectroTk(),
                    'folderTargetId' => $ordenCab->getFiles()->getParentIdElectroTk(),
                ]);
                $item->getFiles()->setParentIdElectroTk($finParentId);

                //carpeta en WORKSPACE
                $finParentIdWs = $this->zohoWDService->copyFolder($tokenWD, [
                    'folderSourceId' => $item->getFiles()->getParentIdElectroTk(),
                    'folderTargetId' => $ordenCab->getFiles()->getParentIdElectroWs(),
                ]);
                $item->getFiles()->setParentIdElectroWs($finParentIdWs);

                //Se actualizan archivos
                $fileTk = $this->zohoWDService->getListFilesFolders($tokenWD, [
                    'folderId' => $item->getFiles()->getParentIdElectroTk()
                ]);
                if (count($fileTk) >= 1) {
                    $item->getFiles()->setFileIdElectroTk($fileTk[0]['id']);
                }
                $fileWs = $this->zohoWDService->getListFilesFolders($tokenWD, [
                    'folderId' => $item->getFiles()->getParentIdElectroWs()
                ]);
                if (count($fileWs) >= 1) {
                    $item->getFiles()->setFileIdElectroWs($fileWs[0]['id']);
                }

                $this->em->flush();
            }
        }

        $this->zohoWDService->deleteFolders($tokenWD, [
            $parentIdBaseFolder
        ], true);

        $this->em->flush();
    }

    /**
     * @deprecated Por nueva forma de subir archivos
     */
    public function transferirArchivo(OrdenDet $item, $data)
    {

        $token = $this->tokenService->getWorkDriveToken();

        $rutas  = $data['rutas'];
        $ticket = $data['ticket'];
        $idx    = $data['idx'];

        $folderName = '#' . $ticket . ' - ' . $item->getOrdenCab()->getUser()->getRazonSocial();
        $rutasTmp = $rutas;
        $tipo = '';
        if (isset($data['tipo']) && $data['tipo'] == 'ELECTROCORTE') {
            $tipo = 'ELECTROCORTE';
            if (!isset($rutas['electrocorteUser'])) {
                $response = $this->zohoWDService->createFolder($token, $folderName, 'ELECTROCORTE');
                $rutasTmp['electrocorteUser'] = $response;
            }
        } else {
            $tipoCategoria = $item->getSubcategoriaProducto()->getCategoriaProducto()->getTipoProducto()->getCategoria();
            $tipo = $tipoCategoria;
            if ($tipoCategoria == 'XEROX' &&  !isset($rutas['xeroxUser'])) {
                $response = $this->zohoWDService->createFolder($token, $folderName, $tipoCategoria);
                $rutasTmp['xeroxUser'] = $response;
            }

            if ($tipoCategoria == 'MIMAKI' &&  !isset($rutas['mimakiUser'])) {
                $response = $this->zohoWDService->createFolder($token, $folderName, $tipoCategoria);
                $rutasTmp['mimakiUser'] = $response;
            }
        }
        $idParentUserId = '';
        $filePath = '';
        if (isset($data['tipo']) && $data['tipo'] == 'ELECTROCORTE') {
            $idParentUserId = $rutasTmp['electrocorteUser'];
            $filePath = $item->getRutaElectrocorte();
        } else {
            $tipoCategoria = $item->getSubcategoriaProducto()->getCategoriaProducto()->getTipoProducto()->getCategoria();
            $filePath = $item->getRutaImpresion();
            if ($tipoCategoria == 'XEROX') {
                $idParentUserId = $rutasTmp['xeroxUser'];
            } elseif ($tipoCategoria == 'MIMAKI') {
                $idParentUserId = $rutasTmp['mimakiUser'];
            }
        }
        $folderNameItem = 'ITEM ' . $idx . ' - ' . $item->getSku();
        $token = $this->tokenService->getWorkDriveToken();
        $folderItem = $this->zohoWDService->createFolder($token, $folderNameItem, $idParentUserId);

        // validar segundo nivel de carpetas
        $token = $this->tokenService->getWorkDriveToken();
        $this->zohoWDService->uploadFile($token, $filePath, $folderItem);

        $res = [
            'rutas' => $rutasTmp,
            'token' => $token,
            'ticket' => $ticket
        ];

        return $res;
    }

    public function getOpenTickets($parametros)
    {
        $token = $this->tokenService->getDeskToken();
        $parametros['status'] = 'EN PROCESO';
        $tickets = $this->zohoDeskService->getTicketsByStatus($token, $parametros);

        $responseTickets = [];

        if (!empty($tickets)) {
            foreach ($tickets as $ticket) {
                if ($ticket['statusType'] == 'Open') {
                    $responseTickets[] = $ticket;
                }
            }
        }

        return $responseTickets;
    }


    public function getZohoOrdenes($parametros)
    {
        $token = $this->tokenService->getBooksToken();
        $ordenesTodos = [];
        $page = 1;
        do {
            $ordenes = $this->zohoBooksService->getAllOrdenesPorCliente($token, $parametros);
            $ordenesTodos = array_merge($ordenesTodos, $ordenes['salesorders']);
            $page++;
        } while ($ordenes['page_context']['has_more_page']);
        return $ordenesTodos;
    }

    public function getRubrosCreditoDirecto()
    {

        $userBooksZoho = $this->session->get('userBooksZoho');
        $userCrmZoho   = $this->session->get('userCrmZoho');

        $parametros = [
            'customer_id'  => $userBooksZoho['contact_id'],
            'status'       => 'open',
            'cf_printo_fp' => 'Credito Directo'
        ];

        $ordenes = $this->getZohoOrdenes($parametros);

        $totalOrdenes = 0;
        foreach ($ordenes as $orden) {
            $totalOrdenes += $orden['total'];
        }

        $totalCtasCobrar = (isset($userBooksZoho['outstanding_receivable_amount'])) ? $userBooksZoho['outstanding_receivable_amount'] : 0;
        $totalCredAprob  = (isset($userCrmZoho['L_mite_de_Cr_dito'])) ? $userCrmZoho['L_mite_de_Cr_dito'] : 0;
        $totalCredDispo  = $totalCredAprob - $totalCtasCobrar - $totalOrdenes;
        $condicionPago   = (isset($userCrmZoho['Condici_n_de_Pago'])) ? $userCrmZoho['Condici_n_de_Pago'] : '-';
        $diasCredito     = (isset($userCrmZoho['D_as_Cr_dito'])) ? $userCrmZoho['D_as_Cr_dito'] : 0;

        $total = [
            'cuentasxcobrar'    => $totalCtasCobrar,
            'creditoaprobado'   => $totalCredAprob,
            'creditodisponible' => $totalCredDispo,
            'condicionPago'     => $condicionPago,
            'diascredito'       => $diasCredito
        ];

        return $total;
    }

    public function getRubrosOrdenActual()
    {
        $user = $this->security->getUser();

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);

        $ordenCab = $ordenCabRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user' => $user
        ]);

        $valores = [
            'items'    => $ordenCab->getTotalItems(),
            'envio'    => $ordenCab->getTotalEnvio(),
            'subtotal' => $ordenCab->getSubtotal(),
            'iva'      => $ordenCab->getTotalIva(),
            'pagar'    => $ordenCab->getTotalPagar(),
        ];

        return $valores;
    }

    public function prepareUpload($arrayParametros)
    {
        $intIdDetalle = $arrayParametros['idDetalle'];
        $isEdit       = $arrayParametros['isEdit'];
        /** @var UserPi|null $user */
        $user         = $this->security->getUser();

        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
        /** @var OrdenDet|null $itemDet */
        $itemDet      = $ordenDetRepo->find($intIdDetalle);

        $token = $this->tokenService->getWorkDriveToken();

        if ($isEdit == 'true') {
            // eliminar carpeta en Workdrive
            $folders = array();
            if (!empty($itemDet->getFiles()->getFileIdImpresion())) {
                $folders[] = $itemDet->getFiles()->getFileIdImpresion();
                $itemDet->getFiles()->SetFileIdImpresion(null);
                $itemDet->setSubidaImpresion('N');
            }

            if (!empty($itemDet->getFiles()->getFileIdElectrocorte())) {
                $folders[] = $itemDet->getFiles()->getFileIdElectrocorte();
                $itemDet->getFiles()->SetFileIdElectrocorte(null);
                $itemDet->setSubidaElectro('N');
            }

            $this->zohoWDService->deleteFolders($token, $folders);
        }


        $orden = $itemDet->getOrdenCab();

        $token = $this->tokenService->getWorkDriveToken();

        // VALIDAR QUE EXISTA CARPETA PADRE
        $folderName = '#' . uniqid() . ' - ' . $user->getRazonSocial() . ' - ' . $orden->getNoOrden();

        // CARPETA BASE DEL CLIENTE EN PRINTO TICKETS
        if (empty($itemDet->getFiles()->getFolderId())) {
            $responseBase = $this->zohoWDService->createFolder($token, $folderName, 'TICKETS_PENDIENTES');
            //$orden->setParentIdTickets($responseBase);
            $itemDet->getFiles()->setFolderId($responseBase);
        }

        $responseBase = $itemDet->getFiles()->getFolderId();

        $folderItemElec = '';
        if ($itemDet->getElectrocorte() == 'S') {
            $folderItemElec = $responseBase;
        }

        $tipoCategoria = $itemDet->getProducto()->getMaquina();
        if ($tipoCategoria == 'XEROX' || $tipoCategoria == 'MIMAKI') {
            $folderItemXeMi = $responseBase;
        }

        $this->em->flush();

        return [
            'parentIdElec' => $folderItemElec,
            'parentIdXeMi' => $folderItemXeMi,
        ];
    }


    public function deleteItem($idItem)
    {
        $error = false;
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        /** @var OrdenDet|null $ordenDet */
        $ordenDet = $ordenDetRepo->findOneBy([
            'estado' => 'A',
            'id'     => $idItem
        ]);

        if (empty($ordenDet)) {
            $error = true;
            $mensaje = 'Item no existe';
            return [
                'error'       => $error,
                'mensaje'     => $mensaje
            ];
        }

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        if ($ordenDet->getOrdenCab()->getUser()->getId() != $user->getId()) {
            $error = true;
            $mensaje = 'Item no permitido para eliminar';
            return [
                'error'       => $error,
                'mensaje'     => $mensaje
            ];
        }

        $ordenDet->setEstado('E');
        $this->em->flush();

        $token = $this->tokenService->getWorkDriveToken();

        $folders = array();

        $folders = [
            $ordenDet->getFiles()->getFolderId(),
        ];

        try {
            $this->zohoWDService->deleteFolders($token, $folders, true);
        } catch (Exception $e) {
            $this->logger->info("TRACKING TRANSFERIR >>> " . get_class($this));
        }

        return [
            'error'       => $error,
            'mensaje'     => 'Se ha eliminado correctamente'
        ];
    }

    public function getTicketsEnviadas()
    {
        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $parametros = [
            'accountName' => $user->getRazonSocial()
        ];

        $tickets = $this->getOpenTickets($parametros);


        return $tickets;
    }

    public function getDetalleTicket($ticket)
    {

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenCab = $ordenCabRepo->findOneBy([
            'estado' => 'I',
            'user' => $this->security->getUser(),
            'noTicket' => $ticket
        ]);

        if (empty($ordenCab)) {
            return [
                'error' => true,
                'mensaje' => 'Hubo un problema con la orden consultada'
            ];
        }

        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        $ordenDets = $ordenDetRepo->findBy([
            'ordenCab' => $ordenCab,
            'estado' => 'A'
        ]);

        return [
            'error'   => false,
            'orden'   => $ordenCab,
            'detalle' => $ordenDets
        ];
    }

    public function uploadFile($parametros)
    {
        $idDetalle = $parametros['idDetalle'];
        $idFile    = $parametros['idFile'];
        $tipoFile  = $parametros['tipoFile'];

        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        /** @var OrdenDet|null $ordenDet */
        $ordenDet = $ordenDetRepo->findOneBy([
            'id' => $idDetalle,
            'estado' => 'A'
        ]);

        $ordenCab = $ordenDet->getOrdenCab();

        if (empty($ordenDet) || $ordenCab->getUser() != $this->security->getUser()) {
            return [
                'error'        => true,
                'mensaje'      => 'Hubo un error actualizando archivo'
            ];
        }

        $tokenWD = $this->tokenService->getWorkDriveToken();

        $tipoCategoria = $ordenDet->getProducto()->getMaquina();

        $filesDelete = [];

        if ($tipoFile == 'electro') {

            if (!empty($ordenDet->getFiles()->getFileIdElectrocorte())) {
                // Eliminar archivo
                $filesDelete[] = $ordenDet->getFiles()->getFileIdElectrocorte();
                $ordenDet->getFiles()->setFileIdElectrocorte(null);
                // if (!empty($filesDelete) && !empty($ordenDet->getFiles()->getParentIdElectroWs())) {
                //     $filesDelete[] = $ordenDet->getFiles()->getParentIdElectroWs();
                // }
            }

            $ordenDet->setSubidaElectro('S');
            $ordenDet->getFiles()->setFileIdElectrocorte($idFile);
        } elseif ($tipoFile == 'impresion') {

            if (!empty($ordenDet->getFiles()->getFileIdImpresion())) {
                // Eliminar archivo
                $filesDelete[] = $ordenDet->getFiles()->getFileIdImpresion();
                $ordenDet->getFiles()->setFileIdImpresion(null);
                // if (!empty($filesDelete) && !empty($ordenDet->getFiles()->getParentIdImpresionWs())) {
                //     $filesDelete[] = $ordenDet->getFiles()->getParentIdImpresionWs();
                // }
            }
            $ordenDet->setSubidaImpresion('S');
            $ordenDet->getFiles()->setFileIdImpresion($idFile);
        }

        if (!empty($filesDelete)) {
            try {
                $tokenWD = $this->tokenService->getWorkDriveToken();
                $this->zohoWDService->deleteFolders($tokenWD, $filesDelete, true);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }

        // sleep(3);
        // SE EJECUTA CUANDO QUIEREN EDITAR UNA ORDEN YA ENVIADA
        // if (!empty($ordenCab->getNoTicket())) {
        //     if ($tipoCategoria == 'XEROX') {
        //         $finParentIdWs = $this->zohoWDService->copyFolder($tokenWD, [
        //             'folderSourceId' => $ordenDet->getFiles()->getParentIdImpresionTk(),
        //             'folderTargetId' => $ordenCab->getFiles()->getParentIdXeroxWs(),
        //         ]);
        //         $ordenDet->getFiles()->setParentIdImpresionWs($finParentIdWs);
        //     }
        //     if ($tipoCategoria == 'MIMAKI') {
        //         $finParentIdWs = $this->zohoWDService->copyFolder($tokenWD, [
        //             'folderSourceId' => $ordenDet->getFiles()->getParentIdImpresionTk(),
        //             'folderTargetId' => $ordenCab->getFiles()->getParentIdMimakiWs(),
        //         ]);
        //         $ordenDet->getFiles()->setParentIdImpresionWs($finParentIdWs);
        //     }
        //     if ($ordenDet->getElectrocorte() == 'S') {
        //         $finParentIdWs = $this->zohoWDService->copyFolder($tokenWD, [
        //             'folderSourceId' => $ordenDet->getFiles()->getParentIdElectroTk(),
        //             'folderTargetId' => $ordenCab->getFiles()->getParentIdElectroWs(),
        //         ]);
        //         $ordenDet->getFiles()->setParentIdElectroWs($finParentIdWs);
        //     }
        // }


        $this->em->flush();
    }

    public function deleteFilesFolders($filesDelet)
    {
        $tokenWD = $this->tokenService->getWorkDriveToken();
        $this->zohoWDService->deleteFolders($tokenWD, $filesDelet);
    }

    public function getCheckoutId($parametros)
    {
        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $parametros['tarjetas'] = $this->dataFastService->getTarjetasPorUsuario([
            'user' => $user
        ]);

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenCab = $ordenCabRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user' => $user
        ]);

        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
        $ordenDets = $ordenDetRepo->findBy([
            'ordenCab' => $ordenCab,
            'estado' => 'A'
        ]);

        $userCrmZoho = $this->session->get('userCrmZoho');
        $userBooksZoho = $this->session->get('userBooksZoho');
        $phone = $userBooksZoho['mobile'];
        $addressBilling = ($ordenCab->requiereEnvio() == 'S') ? $userCrmZoho['Direcci_n_de_facturaci_n'] : 'Retiro en Oficina';

        if (isset($userCrmZoho['Direcci_n_de_facturaci_n']) && !empty($userCrmZoho['Direcci_n_de_facturaci_n'])) {
            $addressBilling = $userCrmZoho['Direcci_n_de_facturaci_n'];
        } else {
            $addressBilling = "Sin Dirección de Facturación";
        }

        if ($ordenCab->getRequiereEnvio() == 'S') {
            //$direccion = $this->dirService->searchDireccion($ordenCab->getDireccionId());
            $direccion = $this->dirService->obtenerDireccion();
            if (!empty($direccion)) {
                $addressShipping = $direccion['address'];
            } else {
                $addressShipping = "Sin Dirección de entrega";
            }
        } else {
            $addressShipping = "Retiro en Oficina";
        }

        $addressShipping = substr($addressShipping, 0, 90) . '...';

        $trxId = md5(uniqid(rand(), true));
        $identificationDocType = "IDCARD";
        $shippingCountry = "EC";
        $billingCountry = "EC";
        $businessName = "PRINTO.CLOUD";


        $totalPagar = $ordenCab->getTotalPagar();
        $totalPagar = ($this->env == 'prod') ? $totalPagar : 1;
        $totalPagar = number_format($totalPagar, 2, '.', '');

        $data = array();

        $data["entityId"] = $this->entityId;
        $data["amount"] = $totalPagar;
        $data["currency"] = 'USD';
        $data["paymentType"] = 'DB';

        $data["merchantTransactionId"] = $trxId;
        $data["customer.givenName"] = $user->getRazonSocial();
        $data["customer.middleName"] = '';
        $data["customer.surname"] = $user->getRazonSocial();
        $data["customer.ip"] = $this->util->getUserIP();
        $data["customer.merchantCustomerId"] = $user->getCrmUserId();
        $data["customer.email"] = $user->getEmail();
        $data["customer.phone"] = $phone;
        $data["shipping.street1"] = $addressShipping;
        $data["shipping.country"] = $shippingCountry;
        $data["billing.street1"] = $addressBilling;
        $data["billing.country"] = $billingCountry;
        $data["customer.identificationDocType"] = $identificationDocType;
        $data["customer.identificationDocId"] = substr($user->getRuc(), 0, 10);
        $idx = 0;
        foreach ($ordenDets as $item) {
            $nombreProducto = substr($item->getNombreProducto(), 0, 200);
            $descriProducto = substr($item->getDescripcion(), 0, 200);
            $prePrice = round($item->getPrecio() / $item->getCantidad(), 2);
            $price = number_format(round($prePrice, 2), 2, '.', '');
            $quantity = $item->getCantidad();

            $data["cart.items[" . $idx . "].name"] = $nombreProducto;
            $data["cart.items[" . $idx . "].description"] = $descriProducto;
            $data["cart.items[" . $idx . "].price"] = $price;
            $data["cart.items[" . $idx . "].quantity"] = "" . $quantity . "";
            $idx++;
        }

        $subtotal = number_format(round($ordenCab->getSubtotal(), 2), 2, '.', '');
        $valoriva = number_format(round($ordenCab->getTotalIva(), 2), 2, '.', '');

        $subtotal = ($this->env == 'prod') ? $subtotal : "0.89";
        $valoriva = ($this->env == 'prod') ? $valoriva : "0.11";

        $data["customParameters[SHOPPER_VAL_BASE0]"] = "0.00";
        $data["customParameters[SHOPPER_VAL_BASEIMP]"] = $subtotal;
        $data["customParameters[SHOPPER_VAL_IVA]"] = $valoriva;

        $data["customParameters[SHOPPER_MID]"]   = $this->shopperMid;
        $data["customParameters[SHOPPER_TID]"]   = $this->shopperTid;
        $data["customParameters[SHOPPER_ECI]"]   = $this->shopperEci;
        $data["customParameters[SHOPPER_PSERV]"] = $this->shopperPserv;

        $data["customParameters[SHOPPER_VERSIONDF]"] = "2";

        $data["risk.parameters[USER_DATA2]"] = $businessName;

        if (($this->env != 'prod')) {
            $data["testMode"] = "EXTERNAL";
        }

        if (isset($parametros['tarjetas']) && !empty($parametros['totalPagar'])) {
            $data = array_merge($data, $parametros['tarjetas']);
        }

        $parametros["data"] = $data;

        try {

            // Crear TRX
            $dfTrx = new DatafastTrx();
            $dfTrx->setTipo("checkout");
            $dfTrx->setTransactionId($trxId);
            $dfTrx->setOrdenCab($ordenCab);
            $dfTrx->setNombreTransaccion("COMPRA ORDEN");
            $dfTrx->setRequest(json_encode($data));
            $this->em->persist($dfTrx);
            $this->em->flush();

            $checkout = $this->dataFastService->getCheckoutId($parametros);

            $resultCode = 'NO CODE ERROR';
            if (isset($checkout['result']) && isset($checkout['result']['code'])) {
                $resultCode = $checkout['result']['code'];
            }
            //Actualizar TRX
            $dfTrx->setResponse(json_encode($checkout));
            $dfTrx->setResultCode($resultCode);
            $dfTrx->setEstadoTransaccion("Generado");
            $dfTrx->setFechaActualizacion();
            $this->em->flush();

            if (!in_array($resultCode, $this->successCodes)) {
                $errorMessage = "";
                if (isset($responsePurchase['result']) && isset($responsePurchase['result']['description'])) {
                    $errorMessage = " - " . $responsePurchase['result']['description'];
                }
                return [
                    'error' => true,
                    'message' => 'Hubo un problema con la transacción' . $errorMessage
                ];
            }
        } catch (Exception $e) {
            $dfTrx->setFechaActualizacion();
            $dfTrx->setEstadoTransaccion("Error");
            $dfTrx->setError(substr($e->getMessage(), 0, 300));
            $this->em->flush();
            $this->logger->error(get_class($this) . ' - method getCheckoutId - Hubo un error checout inicial PRINTO - ' . $e->getMessage());
            return [
                'error' => true,
                'message' => 'Hubo un problema con Datafast'
            ];
        }

        return $checkout['id'];
    }

    public function purchaseOrder($parametros)
    {
        $user = $this->security->getUser();

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);

        $ordenCab = $ordenCabRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user' => $user
        ]);


        $data = array();
        $data["entityId"] = $this->entityId;
        $parametros["data"] = $data;

        try {

            // Buscar TRX
            $dfTrxRepo = $this->em->getRepository(DatafastTrx::class);
            $dfTrxCheckout = $dfTrxRepo->findOneBy([
                'estado' => 'A',
                'ordenCab' => $ordenCab
            ]);
            // Crear TRX
            $dfTrx = new DatafastTrx();
            $dfTrx->setTipo("purchase");
            $dfTrx->setTransactionId($dfTrxCheckout->getTransactionId());
            $dfTrx->setOrdenCab($ordenCab);
            $dfTrx->setRequest(json_encode($data));
            $this->em->persist($dfTrx);
            $this->em->flush();

            $responsePurchase = $this->dataFastService->purchaseOrder($parametros);

            $resultCode = 'NO CODE ERROR';
            if (isset($responsePurchase['result']) && isset($responsePurchase['result']['code'])) {
                $resultCode = $responsePurchase['result']['code'];
            }

            //Actualizar TRX
            $dfTrx->setResponse(json_encode($responsePurchase));
            $dfTrx->setResultCode($resultCode);
            $dfTrx->setEstadoTransaccion("Generado");
            $dfTrx->setFechaActualizacion();
            $this->em->flush();

            if (!in_array($resultCode, $this->successCodes)) {
                $errorMessage = "";
                if (isset($responsePurchase['result']) && isset($responsePurchase['result']['description'])) {
                    $errorMessage = " - " . $responsePurchase['result']['description'] . " - Código : " . $resultCode;
                }
                return [
                    'error' => true,
                    'message' => 'Hubo un problema al procesar su compra' . $errorMessage
                ];
            }

            // Registro de tarjetas de credito
            if (isset($responsePurchase['registrationId'])) {
                $userTcRepo = $this->em->getRepository(UserTc::class);
                $user = $this->security->getUser();
                $userTc = $userTcRepo->findOneBy([
                    'user' => $user,
                    'estado' => 'A',
                    'registrationid' => $responsePurchase['registrationId']
                ]);
                if (empty($userTc)) {
                    $userTc = new UserTc();
                    $userTc->setRegistrationid($responsePurchase['registrationId']);
                    $userTc->setUser($user);
                    $this->em->persist($userTc);
                    $this->em->flush();
                }
            }

            // OK para DATAFAST

            // Generación de la Orden en Zoho
            $ordenVenta = null;
            try {

                $datos = [
                    'requiereEnvio'  => $ordenCab->getRequiereEnvio(),
                    'direccionId'    => $ordenCab->getDireccionId(),
                    'pagoMotorizado' => 'false',
                    'formapago'      => 'Tarjeta de Crédito / Debito',
                    'formaPagoCode'  => 'tc'
                ];

                $ordenVenta = $this->generarOrden($datos);
                //$ordenVenta['ordenZoho']['salesorder_number'] = "SO-02641";

                if ($ordenVenta == null) {
                    return [
                        'error' => true,
                        'message' => 'Hubo un problema con la transacción'
                    ];
                } else {
                    $this->cerrarOrden($ordenVenta);
                }
            } catch (OrdenException $e) {
                $this->logger->error(get_class($this) . ' - method purchaseOrder - Hubo un error Creacion de orden ZOHO - ' . $e->getMessage());
                return [
                    'error' => true,
                    'message' => 'Hubo un problema con la transacción : ' . $e->getMessage()
                ];
            } catch (Exception $e) {
                $this->logger->error(get_class($this) . ' - method purchaseOrder - Hubo un error Creacion de orden PRINTO - ' . $e->getMessage());
                return [
                    'error' => true,
                    'message' => 'Hubo un problema con la transacción'
                ];
            }
        } catch (Exception $e) {
            $dfTrx->setFechaActualizacion();
            $dfTrx->setEstadoTransaccion("Error");
            $dfTrx->setError(substr($e->getMessage(), 0, 300));
            $this->em->flush();
            $this->logger->error(get_class($this) . ' - method purchaseOrder - Hubo un error Creacion de orden DATAFAST - ' . $e->getMessage());
            return [
                'error' => true,
                'message' => 'Hubo un problema con Datafast'
            ];
        }

        return [
            'error' => false,
            'ordenVenta' => $ordenVenta
        ];
    }

    public function deleteOrder()
    {
        $user = $this->security->getUser();

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        $ordenCab = $ordenCabRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user' => $user
        ]);

        $ordenDet = $ordenDetRepo->findBy([
            'estado' => 'A',
            'ordenCab' => $ordenCab
        ]);

        $foldersDelete = [];
        /** @var OrdenDet|null $item */
        foreach ($ordenDet as $item) {
            $item->setEstado('E');
            if (!empty($item->getFiles()->getFolderId())) {
                $foldersDelete[] = $item->getFiles()->getFolderId();
            }
        }
        $ordenCab->setEstado('E');
        $this->em->flush();

        $tokenWD = $this->tokenService->getWorkDriveToken();

        if (!empty($foldersDelete)) {
            $this->zohoWDService->deleteFolders($tokenWD, $foldersDelete, true);
        }
    }

    public function addProductToOrder($parametros)
    {
        $idItem        = $parametros['idItem'];
        $productoId    = $parametros['productoId'];

        $alias         = $parametros['alias'];

        $tipoCorte     = $parametros['tipoCorte'];
        $tipoLaminado  = $parametros['tipoLaminado'];
        $tipoImpresion = $parametros['tipoImpresion'];
        $observacion   = $parametros['observacion'];

        $cantidad      = $parametros['cantidad'];
        $cantOjales    = $parametros['cantOjales'];
        $ancho         = $parametros['ancho'];
        $alto          = $parametros['alto'];

        $laminar       = $parametros['laminar'];
        $ojales        = $parametros['ojales'];
        $electrocorte  = $parametros['electrocorte'];
        $corteRigido   = $parametros['corteRigido'];

        $filePrint     = $parametros['filePrint'];
        $fileElect     = $parametros['fileElect'];
        $conservar     = $parametros['conservar'];

        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        if (empty($idItem)) {
            $itemOrden = new OrdenDet();
        } else {

            /** @var OrdenDet|null $orden */
            $itemOrden = $ordenDetRepo->find($idItem);

            if (!empty($itemOrden) && $itemOrden->getOrdenCab()->getUser()->getId() != $user->getId()) {
                return [
                    'error' => true,
                    'html'  => '',
                    'msg'   => 'Transacción no permitida'
                ];
            }
        }

        /** @var ProductoRepository|null $prodRepo */
        $prodRepo = $this->em->getRepository(Producto::class);

        /** @var Producto|null $producto */
        $producto = $prodRepo->find($productoId);

        $cantidad     = ($cantidad == null) ? 0 : $cantidad;
        $cantOjales   = ($cantOjales == null) ? 0 : $cantOjales;
        $ancho        = ($ancho == null) ? 0 : $ancho;
        $alto         = ($alto == null) ? 0 : $alto;

        $laminar      = (!empty($laminar) && $laminar == 'on') ? true : false;
        $ojales       = (!empty($ojales) && $ojales == 'on') ? true : false;
        $electrocorte = (!empty($electrocorte) && $electrocorte == 'on') ? true : false;
        $corteRigido  = (!empty($corteRigido) && $corteRigido == 'on') ? true : false;

        $descripcion = $this->productoService->getDescripcionProducto([
            'producto'      => $producto,

            'alias'         => $alias,

            'tipoCorte'     => $tipoCorte,
            'tipoLaminado'  => $tipoLaminado,
            'tipoImpresion' => $tipoImpresion,
            'observacion'   => $observacion,

            'cantidad'      => $cantidad,
            'cantOjales'    => $cantOjales,
            'ancho'         => $ancho,
            'alto'          => $alto,

            'laminar'       => $laminar,
            'ojales'        => $ojales,
            'electrocorte'  => $electrocorte,
            'corteRigido'   => $corteRigido,
        ]);

        $itemOrden->setNombreProducto($producto->getNombreProducto());
        $itemOrden->setProducto($producto);
        $itemOrden->setDescripcion($descripcion);
        $itemOrden->setObservacion($observacion);
        $itemOrden->setCantidad($cantidad);
        $itemOrden->setPrecio(0);
        $itemOrden->setAncho($ancho);
        $itemOrden->setAlto($alto);
        $itemOrden->setAlias($alias);

        $itemOrden->setElectrocorte($electrocorte == 'on' ? 'S' : 'N');

        if ($tipoCorte) {
            $itemOrden->setTipoCorte($tipoCorte);
            if ($tipoCorte == 'ELECTROCORTE') {
                $itemOrden->setElectrocorte("S");
            }
        } elseif ($electrocorte) {
            $itemOrden->setTipoCorte("ELECTROCORTE");
        }


        $itemOrden->setLaminado($laminar ? 'S' : 'N');
        $itemOrden->setCorteRigido($corteRigido ? 'S' : 'N');
        $itemOrden->setOjales($ojales ? 'S' : 'N');
        $itemOrden->setCantidadOjales($cantOjales);

        $itemOrden->setTipoLaminado($tipoLaminado);
        $itemOrden->setTipoImpresion($tipoImpresion);

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenRepo = $this->em->getRepository(OrdenCab::class);
        $ordenCab = $ordenRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user'   => $user
        ]);
        $itemOrden->setOrdenCab($ordenCab);

        $msg = "Se actualizaron los datos correctamente";
        $action = "edititem";
        if (empty($idItem)) {
            $this->em->persist($itemOrden);
            $msg = "Se agrego un item correctamente";
            $action = "newitem";
        }

        $fl = $this->em->flush();

        // Calcular Pesos
        // $result = $this->actualizarPesos([
        //     'item' => $itemOrden
        // ]);

        $itemsOrden = [];
        $detalle = [];
        $msgWarning = '';
        $tarifas = [];
        if (!empty($ordenCab)) {
            $itemsOrden = $ordenDetRepo->findBy([
                'estado' => 'A',
                'ordenCab' => $ordenCab
            ]);
            if (!empty($itemsOrden)) {
                $msgWarning = $this->actualizarPrecios($ordenCab);
                if ($producto->getMaquina() == 'MIMAKI') {
                    if (!empty($msg)) {
                        $msg .= '<br>' . $msgWarning;
                    }
                }

                $this->calcularTarifas([
                    'items' => $itemsOrden
                ]);

                $tarifas = $ordenCab->getArrayData();
            }
        }

        foreach ($itemsOrden as $key => $item) {
            $detalle[] = $item->getArrayData();
        }

        return [
            'item'    => $itemOrden,
            'detalle' => $detalle,
            'msg'     => $msg,
            'action'  => $action,
            'tarifas' => $tarifas,

            // Nuevo esquema de mensajes
            // 'warning' => $result['warning'],
            // 'msgW'    => $result['msgW']
        ];
    }
    
    /**
     * 
     * Metodo que calcula pesos por configuraciones
     * 
     * @deprecated
     */
    public function actualizarPesos($parametros)
    {
        $result = [
            'error' => false,
            'msgE'  => '',
            'warning' => false,
            'msgW' => '',
        ];

        /** @var null|OrdenDet $item */
        $item = $parametros['item'];
        $producto = $item->getProducto();

        /** @var null|ProductoCaracteristicaRepository $prodCaracRepo */
        $prodCaracRepo = $this->em->getRepository(ProductoCaracteristica::class);

        /** @var null|CaracteristicaRepository $caracRepo */
        $caracRepo = $this->em->getRepository(Caracteristica::class);

        $pesoReferencial = 0;

        $caracteristica = $caracRepo->findOneBy([
            'estado' => 'A',
            'val2'   => $producto->getMaquina(),
            'nombre' => 'peso'
        ]);

        $prodCaract = $prodCaracRepo->findOneBy([
            'producto'       => $producto,
            'caracteristica' => $caracteristica,
            'estado'         => 'A'
        ]);


        if (empty($prodCaract)) {
            $valorProdCarac = 0.05; // valor default si no esta configurado
            $result = [
                'warning' => true,
                'msgW'     => 'Peso no configurado para el peso, default 0.05, solo por desarrollo'
            ];
            //throw new Exception("Producto no tiene configurado un peso referencial");
        } else {
            $valorProdCarac = $prodCaract->getValor();
        }

        $cantidadReferencial = $caracteristica->getVal1();

        $pesoReferencial = $valorProdCarac;

        $dimensiones = ($item->getAncho() * $item->getAlto());
        $dimensiones = ($dimensiones == 0) ? 1 : $dimensiones;

        $cantidadItem = $item->getCantidad() * $dimensiones;

        $pesoTotal = round(($cantidadReferencial * $pesoReferencial) / $cantidadItem, 5);

        /** @var null|OrdenDetCaracteristicaRepository $ordenDetCaracRepo */
        $ordenDetCaracRepo = $this->em->getRepository(OrdenDetCaracteristica::class);

        $ordenDetCarac = $ordenDetCaracRepo->findOneBy([
            'caracteristica' => $caracteristica,
            'ordenDet'       => $item,
            'estado'         => 'A'
        ]);

        if (empty($ordenDetCarac)) {
            $ordenDetCarac = new OrdenDetCaracteristica();
            $ordenDetCarac->setCaracteristica($caracteristica);
            $ordenDetCarac->setOrdenDet($item);
            $ordenDetCarac->setValor($pesoTotal);
            $ordenDetCarac->setVal1($cantidadItem);
            $this->em->persist($ordenDetCarac);
        }

        $ordenDetCarac->setVal1($cantidadItem);
        $ordenDetCarac->setValor($pesoTotal);

        $this->em->flush();

        return $result;
    }

    /**
     * 
     */
    public function procesarOrden()
    {
        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenRepo = $this->em->getRepository(OrdenCab::class);
        $orden = $ordenRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user'   => $user
        ]);

        $itemsOrden = [];
        if (empty($orden)) {
            return [
                'error' => true,
                'msg'   => 'go_home'
            ];
        } else {
            $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
            $itemsOrden = $ordenDetRepo->findBy([
                'estado' => 'A',
                'ordenCab' => $orden
            ]);
        }


        $contact = $this->dirService->getContactZohoBook();

        //$this->session->set('userBooksZoho', $contact);

        $tarifas = $this->calcularTarifas([
            'items' => $itemsOrden
        ]);

        if ($tarifas['ordenPermitida']) {
            $this->flashBagService->add('error', $tarifas['msgOrdenPermitida']);
        }

        return [
            'itemsOrden'  => $itemsOrden,
            'tarifas'     => $tarifas,
            //'direcciones' => $contact['addresses'],
        ];
    }

    public function pagoOrden()
    {
        /** @var UserPi|null $user */
        $user = $this->security->getUser();

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);

        $ordenCab = $ordenCabRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user' => $user
        ]);

        if (is_null($ordenCab)) {
            $this->flashBagService->add('error', 'Orden no encontrada');
            //$this->session->getFlashBag()->set('error', 'Orden no encontrada');
            return [
                'error'    => true,
                'to_route' => 'admin_orden',
            ];
        }

        /** Validacion de total minimo */
        $confRepo = $this->em->getRepository(Configuraciones::class);
        $subtotal = $ordenCab->getTotalItems();
        $precioMinimoCompra = $confRepo->getConfigDouble('PRECIO_MINIMO_COMPRA');; //0
        $ordenPermitida = ($subtotal < $precioMinimoCompra);
        if ($ordenPermitida) {
            return [
                'error'    => true,
                'to_route' => 'admin_procesar_orden',
            ];
        }

        /** Validacion de numero de facturas vencidad */
        $userBooksZoho = $this->session->get("userBooksZoho");

        $numFactVenc = isset($userBooksZoho['cf_num_fac_vencidas']) ? (int)$userBooksZoho['cf_num_fac_vencidas'] : 0;
        if ($numFactVenc > 0) {
            $this->flashBagService->add('error', 'Usted posee ' . $numFactVenc . ' facturas vencidas, por favor contactese con soporte.');
            return [
                'error'    => true,
                'to_route' => 'admin_procesar_orden',
            ];
        }

        $pagoEfecMen = ($ordenCab->getRequiereEnvio() == 'S' && $ordenCab->getTotalPagar() <= 50);
        $pagoOfic    = ($ordenCab->getRequiereEnvio() == 'N' && $ordenCab->getTotalPagar() <= 50);
        $credDirecto = true;
        $tarjetaCred = true;

        $fp = [
            'pagoEfecMen' => $pagoEfecMen,
            'pagoOfic'    => $pagoOfic,
            'credDirecto' => $credDirecto,
            'tarjetaCred' => $tarjetaCred
        ];

        $valores = [
            'items'    => $ordenCab->getTotalItems(),
            'envio'    => $ordenCab->getTotalEnvio(),
            'subtotal' => $ordenCab->getSubtotal(),
            'iva'      => $ordenCab->getTotalIva(),
            'pagar'    => $ordenCab->getTotalPagar(),
        ];

        return [
            'valores' => $valores,
            'fp'      => $fp
        ];
    }

    public function finalizarCompra($parametros)
    {
        $formaPago = $parametros['formaPago'];

        $printo_fp = '';

        $pagoMotorizado = 'false';

        switch ($formaPago) {
            case 'mensajero':
                $pagoMotorizado = 'true';
                $printo_fp = 'Efectivo al Mensajero';
                break;
            case 'oficina':
                $pagoMotorizado = 'false';
                $printo_fp = 'Pago en Oficina';
                break;
            case 'creditodirecto':
                $printo_fp = 'Credito Directo';
                break;
            case 'transfbanc':
                $printo_fp = 'Transferencia Bancaria';
                break;
            case 'tc':
                $printo_fp = 'Tarjeta de Crédito / Debito';
                break;
            default:
                # code...
                break;
        }

        $datos = [
            'pagoMotorizado' => $pagoMotorizado,
            'formapago'      => $printo_fp,
            'formaPagoCode'  => $formaPago,
        ];

        // crear orden

        $ordenVenta = $this->generarOrden($datos);

        if ($ordenVenta == null) {
            $error = true;
            $mensaje = 'Hubo un error al crear la Orden';
        } else {
            $this->cerrarOrden($ordenVenta);
            $error = false;
            $mensaje = 'Se creo satisfactoriamente la orden';
        }

        return [
            'error'       => $error,
            'noOrden'     => $ordenVenta['ordenZoho']['salesorder_number'],
            'mensaje'     => $mensaje
        ];
    }

    public function calcularTarifas($parametros)
    {
        $metodoEnvio = (isset($parametros['metodo'])) ? $parametros['metodo'] : null;
        $direccionId = (isset($parametros['direccionId'])) ? $parametros['direccionId'] : null;

        $user    = $this->security->getUser();

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        /**@var null|OrdenCabRepository $ordenCabRepo */
        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
        $ordenCab = $ordenCabRepo->findOneBy([
            'user'   => $user,
            'estado' => $estadoOrdenCab
        ]);

        if (empty($ordenCab)) {
            throw new Exception('No existe orden vigente activa');
        }

        /**@var null|OrdenDetRepository $ordenDetRepo */
        $ordenDetRepo = $this->em->getRepository(OrdenDet::class);
        $items = $ordenDetRepo->findBy([
            'ordenCab' => $ordenCab,
            'estado'   => 'A'
        ]);

        if (empty($items)) {
            throw new Exception('No existen items que procesar');
        }

        $tarifas = $this->pagService->calcularTarifas([
            'items'       => $items,
            'metodoEnvio' => $metodoEnvio,
            'direccionId' => $direccionId
        ]);

        // Validar tarifas
        $validaTarifas = $this->validarTarifasOrden($tarifas);

        // Asignar tarifas
        $ordenCab->resetPrecios();
        $ordenCab->setTotalItems($tarifas['subtotalItems']);
        //$ordenCab->setTotalEnvio($tarifas['rubrosEnvios']['totalPagar']);
        $ordenCab->setSubtotal($tarifas['subtotalItems']);
        $ordenCab->setTotalIva($tarifas['subtotalItemsIva']);
        $ordenCab->setTotalPagar($tarifas['totalPagarItemsEnvios']);
        $ordenCab->setRequiereEnvio($tarifas['requiereEnvio']);
        $ordenCab->setDireccionId($direccionId);
        $this->em->flush();

        $tarifas = array_merge($tarifas, $validaTarifas);

        $this->session->set('tarifas', $tarifas);

        return $tarifas;
    }

    public function validarTarifasOrden($tarifas)
    {
        $subtotal    = $tarifas['subtotalItems'];
        $valorSubIva = $tarifas['subtotalItemsIva'];

        /** @var ConfiguracionesRepository|null $confRepo */
        $confRepo = $this->em->getRepository(Configuraciones::class);


        // Validación Minimo de compra $2
        $precioMinimoCompra = $confRepo->getConfigDouble('PRECIO_MINIMO_COMPRA');; //0
        $ordenPermitida = ($subtotal < $precioMinimoCompra);
        $msgOrdenPermitida = '';

        if ($ordenPermitida) {

            $fmt = numfmt_create('en_US', NumberFormatter::CURRENCY);
            $currency = "USD";

            $fmtSubtotal = numfmt_format_currency($fmt, $subtotal, $currency);
            $fmtPrecioMinimoCompra = numfmt_format_currency($fmt, $precioMinimoCompra, $currency);


            $msgOrdenPermitida = 'Usted ha hecho una compra global de ' . $fmtSubtotal . ' '
                . 'sin embargo el costo mìnimo es de ' . $fmtPrecioMinimoCompra . ' + IVA, '
                . 'edite la orden y cumpla el mínimo de compra general';
        }

        // Validacion minimo contra entrega $50
        $topeContraEntrega = $confRepo->getConfigDouble('VALOR_TOPE_CONTRA_ENTREGA_PICKER');
        $esAutomatico = $valorSubIva > $topeContraEntrega;

        return [
            'error'             => $ordenPermitida,
            'esAutomatico'      => $esAutomatico,
            'ordenPermitida'    => $ordenPermitida,
            'msgOrdenPermitida' => $msgOrdenPermitida
        ];
    }

    public function uploadImgProd($parametros)
    {
        $file = $parametros['file'];

        $user    = $this->security->getUser();

        if ($file) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->comprobanteDirectory,
                    $newFilename
                );

                /** Se actualiza Item en el Detalle de la Orden */
                $esProforma     = $this->session->get('esProforma');
                $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

                /**@var null|OrdenCabRepository $ordenCabRepo */
                $ordenCabRepo = $this->em->getRepository(OrdenCab::class);
                $ordenCab = $ordenCabRepo->findOneBy([
                    'user'   => $user,
                    'estado' => $estadoOrdenCab
                ]);
                $ordenCab->setComprobantePath("/" . $this->comprobanteDirectory . $newFilename);

                $this->em->flush();

                return $ordenCab->getComprobantePath();
            } catch (FileException $e) {
                dump($e);
            }
        }

        return;
    }
}
