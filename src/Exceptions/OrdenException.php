<?php

namespace App\Exceptions;

use Exception;

class OrdenException extends \Exception
{

    const CODE_ERROR         = 400;
    const CODE_ERROR_API     = 401;
    const CODE_ERROR_ZOHO_BOOKS = 402;

    const MESSAGE_ERROR            = 'Error General del Aplicativo';
    const MESSAGE_ERROR_API        = 'Error de Api Generado';
    const MESSAGE_ERROR_ZOHO_BOOKS = 'Error de Zoho Books';

    public function __construct(
        String $message = self::MESSAGE_ERROR,
        int $code = self::CODE_ERROR
    ) {
        parent::__construct($message, $code);
    }
}
