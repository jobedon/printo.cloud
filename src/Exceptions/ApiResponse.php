<?php

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse extends JsonResponse
{

    const OK = 200;
    const GENERAL_ERROR = 400;
    const ZOHO_WD_ERROR = 401;


    /**
     * ApiResponse constructor.
     *
     * @param string $message
     * @param mixed  $data
     * @param array  $errors
     * @param int    $status
     * @param array  $headers
     * @param bool   $json
     */
    public function __construct(string $message, int $status = self::OK, array $headers = [], bool $json = false)
    {
        parent::__construct($this->format($status, $message), $status, $headers, $json);
    }

    /**
     * Format the API response.
     *
     * @param string $message
     * @param mixed  $data
     * @param array  $errors
     *
     * @return array
     */
    private function format(int $status, string $message)
    {

        $response = [
            'status'  => $status,
            'message' => $message,
        ];

        return $response;
    }
}
