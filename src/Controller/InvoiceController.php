<?php

namespace App\Controller;

use App\Services\InvoiceService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * InvoiceController.
 * @Route("/admin/Facturas", name="admin_invoice_")
 */
class InvoiceController extends AbstractController
{

    private $session;

    public function __construct(
        SessionInterface $session
    )
    {
        $this->session = $session;
    }
    /**
     * @Route("/", name="index")
     */
    public function index(InvoiceService $invoiceService)
    {

        $invoices = $invoiceService->getUnpaidInvoices();

        return $this->render('admin/invoice/index_invoice.html.twig', [
            'invoices' => $invoices
        ]);
    }

    /**
     * @Route("/checkout", name="checkout", methods={"POST"})
     */
    public function gectCheckoutId(Request $request, InvoiceService $invoiceService)
    {

        $token  = $request->get('token');
        $rubros = $request->get('rubros');

        $parametros = [
            'token' => $token,
            'rubros' => $rubros
        ];

        $checkoutId = $invoiceService->getCheckoutId($parametros);

        $html = $this->renderView('admin/invoice/tc.html.twig', [
            'checkoutId' => $checkoutId,
        ]);

        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @Route("/shopper-result", name="shopper_result", methods={"GET"})
     */
    public function shopperResult(Request $request, InvoiceService $invoiceService)
    {
        $resourcePath = $request->get('resourcePath');
        $idPayment = $request->get('id');

        try {

            $responsePurchase = $invoiceService->purchaseOrder([
                'resourcePath' => $resourcePath,
                'idPayment'    => $idPayment
            ]);
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
            return $this->redirectToRoute('admin_invoice_index');
        }

        $this->session->set('purchase', $responsePurchase);
        return $this->redirectToRoute('admin_invoice_payment_complete');
    }
    
    /**
     * @Route("/pago-completado", name="payment_complete", methods={"GET"})
     */
    public function paymentComplete()
    {

        if (!$this->session->has('purchase')) {
            return $this->redirectToRoute('admin_invoice_index');
        } 

        $purchase = $this->session->get('purchase');
        $this->session->remove('purchase');

        return $this->render("admin/invoice/payment_complete.html.twig", [
            'purchase' => $purchase
        ]);
    }

}
