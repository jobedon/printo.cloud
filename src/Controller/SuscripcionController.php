<?php

namespace App\Controller;

use App\Entity\Plan;
use App\Services\AuthenticationService;
use App\Services\SubscriptionService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * SuscripcionController.
 * @Route("/admin/subscription", name="suscripcion")
 */
class SuscripcionController extends AbstractController
{
    /** @var SessionInterface|null $session */
    private $session;

    private $em;

    private $logger;

    private $authService;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        AuthenticationService $authService
    ) {
        $this->session     = $session;
        $this->em          = $em;
        $this->logger      = $logger;
        $this->authService = $authService;
    }

    /**
     * @Route("/", name="_index")
     */
    public function index(SubscriptionService $subsService)
    {
        $this->authService->refreshSubscription();

        $mySubscription = $this->session->get('userSubscription');
        $planRepo = $this->em->getRepository(Plan::class);

        $myPlan = null;

        if (!empty($mySubscription)) {
            $myPlan = $planRepo->findOneBy([
                'status' => 'A',
                'zohoPlanCode' => $mySubscription['plan_code']
            ]);
        }

        $suscripciones = $subsService->getAllSubscriptions();

        return $this->render('admin/suscripcion/index.html.twig', [
            'suscripciones' => $suscripciones,
            'myPlan' => $myPlan
        ]);
    }


    /**
     * @Route("/buy/{printoPlanCode}", name="_custom")
     */
    public function custom($printoPlanCode = null, SubscriptionService $subsService)
    {
        $this->authService->refreshSubscription();

        $mySubscription = $this->session->get('userSubscription');
        $planRepo = $this->em->getRepository(Plan::class);

        $myPlan = null;

        if (!empty($mySubscription)) {
            $myPlan = $planRepo->findOneBy([
                'status' => 'A',
                'zohoPlanCode' => $mySubscription['plan_code']
            ]);
        }


        $suscripciones = $subsService->getAllSubscriptions();

        /* CODIGO TEMPORAL */
        // dump($suscripciones);
        // $planSelected = $suscripciones[$printoPlanCode];
        // dump($myPlan);
        // dump($mySubscription);
        // $amount = $subsService->calcularRubro($planSelected);
        // dump($planSelected);
        /*******************/

        return $this->render('admin/suscripcion/custom.html.twig', [
            'suscripciones' => $suscripciones,
            'printoPlanCode' => $printoPlanCode,
            'myPlan' => $myPlan
        ]);
    }


    /**
     * @Route("/checkout", name="_checkout")
     */
    public function checkout(SubscriptionService $subsService, Request $r)
    {

        $planKey = $r->get('planKey');

        $response = [
            'error' => false,
            'message' => 'Transacción exitosa',
            'html' => ''
        ];

        $mySubscription = $this->session->get('userSubscription');
        $planRepo = $this->em->getRepository(Plan::class);

        $myPlan = null;
        if (!empty($mySubscription)) {
            $myPlan = $planRepo->findOneBy([
                'status' => 'A',
                'zohoPlanCode' => $mySubscription['plan_code']
            ]);
        }

        $suscripciones = $subsService->getAllSubscriptions();
        $planSelected = $suscripciones[$planKey];
        $prices = $subsService->calcularRubro($planSelected);

        if (!empty($myPlan) && $myPlan->getPriority() >= $planSelected['priority']) {
            return new JsonResponse([
                'error' => true,
                'message' => 'No esta permitido contratar un plan de menor denominación, por favor contactarse con soporte'
            ]);
        }

        $checkoutId = $subsService->getCheckoutId([
            'amount' => $prices['finalPriceIvaC'],
            'planKey' => $planKey
        ]);

        if (isset($checkoutId['error']) && $checkoutId['error']) {
            return new JsonResponse($checkoutId);
        }

        $newPlan = $planRepo->findOneBy([
            'status' => 'A',
            'printoPlanCode' => $planKey
        ]);

        $checkout = [
            'checkoutId' => $checkoutId,
            'prices'     => $prices,
            'myPlan'     => $myPlan,
            'newPlan'    => $newPlan
        ];

        $this->session->set("checkout", $checkout);


        $userBooksZoho = $this->session->get('userBooksZoho');

        $html = $this->renderView('admin/suscripcion/tc.html.twig', $checkout);

        $response['html'] = $html;

        return new JsonResponse($response);
    }

    /**
     * @Route("/Shopper-Result", name="_shopper_result")
     */
    public function shopperResultPayment(Request $r, SubscriptionService $subsService)
    {
        $resourcePath = $r->get('resourcePath');
        $checkoutId   = $r->get('id');

        if (!$this->session->has("checkout")) {
            //Enviarlo a error
        }

        $checkout = $this->session->get("checkout");
        $this->session->remove("checkout");

        try {
            $responsePurchase = $subsService->purchaseOrder([
                'resourcePath' => $resourcePath,
                'checkoutId'   => $checkoutId,
                'checkout'     => $checkout
            ]);
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
            // return $this->redirectToRoute('admin_pago_order');
        }

        if ($responsePurchase['error']) {
            $this->addFlash('error', $responsePurchase['message']);
            // return $this->redirectToRoute('admin_pago_order');
        }

        return $this->redirectToRoute('suscripcion_finalizar_orden', [
            //'subscription' => $responsePurchase
        ]);
    }

    /**
     * @Route("/compra-plan", name="_finalizar_orden")
     */
    public function ordenFinalizada()
    {
        $this->authService->refreshSubscription();

        return $this->render('admin/suscripcion/finaliza_orden.html.twig', []);
    }
}
