<?php

namespace App\Controller;

use App\Services\AuthenticationService;
use App\Services\CatalogoService;
use App\Services\OrdenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * ProformaController.
 * @Route("/admin/Proforma", name="admin_proforma_")
 */
class ProformaController extends AbstractController
{

    private $authService;
    private $session;
    private $catService;
    private $ordService;

    public function __construct(
        AuthenticationService $authService,
        CatalogoService $catService,
        OrdenService $ordService,
        SessionInterface $session
    ) {
        $this->authService = $authService;
        $this->session     = $session;
        $this->catService  = $catService;
        $this->ordService  = $ordService;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $esProforma = true;
        $this->session->set('esProforma' , $esProforma);

        $this->authService->refreshSubscription();
        $productos = $this->catService->initCatalogo();
        $result = $this->ordService->prepareOrdenInit();

        return $this->render('admin/catalogo/index.html.twig', [
            'productos' => $productos,
            'categorias' => $result['productos'],
            'itemsOrden' => $result['itemsOrden'],
            'tarifas'    => $result['tarifas'],
        ]);
    }
}
