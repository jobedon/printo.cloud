<?php
namespace App\Controller;

use App\Services\ContactosService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin/Contactos", name="admin_contactos_")
 */
class ContactosController extends AbstractController
{
    private $session;

    public function __construct(
        SessionInterface $session
    ) {
        $this->session = $session;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(ContactosService $contactosService)
    {

        $contactos = $contactosService->obtenerContactos();

        return $this->render('admin/contactos/index_contactos.html.twig', [
            'contactos' => $contactos
        ]);
    }

    /**
     * @Route("/Guardar", name="guardar", methods={"POST"})
     */
    public function guardarContacto(Request $r, ContactosService $contactosService)
    {
        $contactoId = $r->get('id');

        $datos = [
            'contactoId' => $contactoId,
            'Name'     => $r->get('Name'),
            'Apellidos_de_Contacto'   => $r->get('Apellidos_de_Contacto'),
            'Celular' => $r->get('Celular'),
            'Identificaci_n'  => $r->get('Identificaci_n')
        ];

        if (empty($contactoId)) {
            $contacto = $contactosService->saveContacto($datos);
        }else{
            $contacto = $contactosService->actualizarContacto($datos);
        }     

        return new JsonResponse([
            'error' => false
        ]);
    }

    /**
     * @Route("/Eliminar/{id}", name="delete")
     */
    public function deleteDireccion(ContactosService $contactosService, $id)
    {
        try {
            $contactosService->deleteContacto([
                'contactoId' => $id
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'error' => true,
                'mensaje' => $e->getMessage()
            ]);
        }

        return new JsonResponse([
            'error' => false,
            'mensaje' => 'El contacto ha sido eliminada',
            'urlRedirect' => $this->generateUrl('admin_contactos_index')
        ]);
    }

}