<?php

namespace App\Controller;

use App\Entity\UserPi;
use App\Services\AuthenticationService;
use App\Services\UtilsService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     * @Route("/", name="home")
     */
    public function login(Request $request, AuthenticationUtils $utils, UserPasswordEncoderInterface $encoder)
    {

        $error = $utils->getLastAuthenticationError();
        $lastUsername = $utils->getLastUsername();

        // $pwd = $encoder->encodePassword(new UserPi(), '0927355909001');

        return $this->render('security/login.html.twig', [
            'error'         => $error,
            'last_username' => $lastUsername
        ]);
    }
    /**
     * @Route("/Recuperar-Conrasena", name="pwd_recovery")
     */
    public function forgotePwd(Request $r, AuthenticationService $authService, UtilsService $utilsService)
    {

        if ($r->getMethod() == 'POST') {
            $ruc = $r->get('ruc');
            $user = $authService->forgotPwd($ruc);

            $email = $utilsService->hideEmailAddress($user->getEmail());

            return $this->render('security/recovery-pwd.html.twig', [
                'email' => $email
            ]);
        }

        // $pwd = $encoder->encodePassword(new UserPi(), '0927355909001');

        return $this->render('security/recovery-pwd.html.twig', []);
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, AuthenticationService $auth)
    {

        $error = "";
        $success = "";

        if ($request->getMethod() == 'POST') {
            $ruc = $request->get('_username');

            try {
                $user = $auth->registerUser($ruc);
                $success = "Se registro el usuario " . $user->getRazonSocial() . " correctamente.";
            } catch (Exception $e) {
                $error = $e->getMessage();
            }
        }

        return $this->render('security/register.html.twig', [
            'error'   => $error,
            'success' => $success,
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
    }
}
