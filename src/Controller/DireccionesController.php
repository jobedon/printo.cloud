<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use App\Services\DireccionesService;
use App\Services\OrdenService;
use App\Services\TokenService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/Direcciones", name="admin_direcciones_")
 */
class DireccionesController extends AbstractController
{

    private $tokenService;

    private $session;

    private $em;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $em,
        TokenService $tokenService
    ) {
        $this->session = $session;
        $this->em      = $em;
        $this->tokenService = $tokenService;
    }

    /**
     * @Route("/Contacto/{id}", name="index")
     */
    public function index(DireccionesService $dirService, $id)
    {

        $provincias = $dirService->getCiudades();
        $contacto = $dirService->getDirecciones([
            'contactoId' => $id
        ]);
        
        return $this->render('admin/direcciones/index_direcciones.html.twig', [
            'contacto' => $contacto,
            'provincias'    => $provincias
        ]);
    }

    /**
     * @Route("/Cambiar-Provincia", name="change_provincia")
     */
    public function changeProvincia(DireccionesService $dirService, Request $r)
    {
        $provincia = $r->get('provincia');
        $provincias = $dirService->getCiudades();
        $ciudades = $provincias[$provincia];

        return new JsonResponse([
            'data' => $ciudades
        ]);
    }

    /**
     * @Route("/Guardar-Direccion", name="guardar", methods={"POST"})
     */
    public function guardarDireccion(Request $r, DireccionesService $dirService)
    {
        $direccionId = $r->get('id');

        $datos = [
            'direccionId'       => $direccionId,
            'contactoId'        => $r->get('contactoId'),
            'Name'              => $r->get('Name'),
            'Coordenadas'       => $r->get('Coordenadas'),
            'Provincia_Destino' => $r->get('Provincia_Destino'),
            'Ciudad_Destino1'   => $r->get('Ciudad_Destino1'),
        ];

        if (empty($direccionId)) {
            $direccion = $dirService->saveDireccion($datos);
        }else{
            $direccion = $dirService->actualizarDireccion($datos);
        }     

        return new JsonResponse([
            'error' => false
        ]);
    }

    /**
     * @Route("/Eliminar/{contactoId}/{direccionId}", name="delete")
     */
    public function deleteDireccion(DireccionesService $dirService, $direccionId, $contactoId)
    {
        try {
            $response = $dirService->deleteDireccion([
                'direccionId' => $direccionId,
                'contactoId'  => $contactoId,
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'error' => true,
                'mensaje' => $e->getMessage()
            ]);
        }

        return new JsonResponse([
            'error' => false,
            'mensaje' => 'La direccion ha sido eliminada',
            'urlRedirect' => $this->generateUrl('admin_direcciones_index',[
                'id' => $contactoId
            ])
        ]);
    }
}
