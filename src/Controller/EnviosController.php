<?php

namespace App\Controller;

use App\Services\AuthenticationService;
use App\Services\ContactosService;
use App\Services\DireccionesService;
use App\Services\EnviosService;
use App\Services\Externo\ServientregaService;
use App\Services\TokenService;
use App\Services\ZohoApiService;
use App\Services\ZohoDeskService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * OrdenController.
 * @Route("/admin/Envios", name="admin_envios_")
 */
class EnviosController extends AbstractController
{

    private $enviosService;
    private $session;

    public function __construct(
        EnviosService $enviosService,
        SessionInterface $session
    ) {
        $this->enviosService = $enviosService;
        $this->session       = $session;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(
        ServientregaService $se
    ) {

        $ordenes = $this->enviosService->getListadoTicketsListos();

        return $this->render('admin/envios/listado_ordenes.html.twig', [
            'ordenes' => $ordenes
        ]);
    }

    /**
     * @Route("/Preparar-Envio", name="preparar")
     */
    public function prepararEnvio(Request $r, EnviosService $enviosService)
    {
        $tickets = $r->get('tickets');
        $validaOrdenesCompletas = $r->get('validaOrdenesCompletas');

        $this->session->set("ticketsEnvio", $tickets);
        $this->session->set("validaOrdenesCompletas", $validaOrdenesCompletas);

        $urlRedirect = $this->generateUrl('admin_envios_metodo');

        return new JsonResponse([
            'urlRedirect' => $urlRedirect
        ]);
    }

    /**
     * @Route("/Generar-Pdf/{guia}", name="generar", methods={"GET"})
     */
    public function generatePdf(EnviosService $enviosService, $guia)
    {

        $response = $enviosService->generatePdf([
            'guia' => $guia
        ]);

        $pdfDecode = base64_decode($response['archivoEncriptado']);

        return new Response($pdfDecode, 200, [
            'Content-Type' => 'application/pdf'
        ]);
    }

    /**
     * @Route("/Metodo", name="metodo", methods={"GET"})
     */
    public function metodoEnvio(
        DireccionesService $dirService,
        ContactosService $contactosService,
        EnviosService $enviosService,
        AuthenticationService $auth
    ) {

        $contactos = $contactosService->obtenerContactos();

        $tickets = $this->session->get("ticketsEnvio");

        $data = $auth->getUser($this->getUser()->getRuc());

        //try {
        // $valores = $enviosService->calcularEnvio([
        //     'tickets' => $tickets
        // ]);
        // } catch (Exception $e) {
        //     dump('error');
        // }

        return $this->render('admin/envios/metodos_envio.html.twig', [
            'contactos' => $contactos,
            'tickets' => $tickets,
        ]);
    }

    /**
     * @Route("/Calcular-Metodo", name="calcular_metodo", methods={"POST"})
     */
    public function calcularMetodoEnvio(EnviosService $enviosService, Request $r)
    {
        $contactoId  = $r->get('contactoId');
        $direccionId = $r->get('direccionId');
        $metodo      = $r->get('metodo');

        $tickets = $this->session->get("ticketsEnvio");
        $validaOrdenesCompletas = $this->session->get("validaOrdenesCompletas");

        $valores = null;

        if (!empty($direccionId) && !empty($metodo)) {
            // try {
            $valores = $enviosService->calcularTarifas([
                'tickets' => $tickets,
                'validaOrdenesCompletas' => $validaOrdenesCompletas,
                'contactoId' => $contactoId,
                'direccionId' => $direccionId,
                'metodo' => $metodo
            ]);
            // } catch (Exception $e) {
            //     return new JsonResponse([
            //         'error' => true,
            //         'msg'   => 'Error en el calculo ' .  $e->getMessage()
            //     ]);
            // }
        }

        $html = $this->renderView('admin/envios/calculo_envio.html.twig', [
            'valores' => $valores,
            'tickets' => $tickets
        ]);

        return new JsonResponse([
            'error' => false,
            'msg'   => '',
            'html'  => $html
        ]);
    }

    /**
     * @Route("/Preparar-Pago", name="preparar_pago", methods={"POST"})
     */
    public function prepararPago(EnviosService $enviosService, Request $r)
    {   
        $contactoId  = $r->get('contactoId');
        $direccionId = $r->get('direccionId');
        $metodo      = $r->get('metodo');

        $tickets = $this->session->get("ticketsEnvio");
        $validaOrdenesCompletas = $this->session->get("validaOrdenesCompletas");

        $valores = $enviosService->calcularTarifas([
            'tickets' => $tickets,
            'validaOrdenesCompletas' => $validaOrdenesCompletas,
            'contactoId' => $contactoId,
            'direccionId' => $direccionId,
            'metodo' => $metodo
        ]);

        $checkoutId = $enviosService->getCheckoutId([
            'valorPagar' => $valores['totalPagar']
        ]);

        $html = $this->renderView('admin/envios/formapago/tarjetacredito.html.twig', [
            'checkoutId' => $checkoutId,
            'tickets' => implode(',', $tickets)
        ]);

        return new JsonResponse([
            'error' => false,
            'html'  => $html
        ]);

    }


    /**
     * @Route("/Shopper-Result", name="shopper_result")
     */
    public function shopperResultPayment(Request $r, EnviosService $ordenService)
    {

        $resourcePath = $r->get('resourcePath');
        $tickets      = $r->get('tickets');

        $tickets = $this->session->get("ticketsEnvio");

        // try {
            $responsePurchase = $ordenService->purchaseOrder([
                'resourcePath' => $resourcePath,
                'tickets'      => $tickets
            ]);
        // } catch (Exception $e) {
        //     $this->addFlash('error', $e->getMessage());
        //     return $this->redirectToRoute('admin_envios_metodo');
        // }

        if ($responsePurchase['error']) {
            $this->addFlash('error', $responsePurchase['message']);
            $this->addFlash('toOption', true);
            return $this->redirectToRoute('admin_envios_metodo');
        }

        return $this->redirectToRoute('admin_envios_finalizar_envio', [

        ]);
    }


    /**
     * @Route("/Transaccion-Finalizada", name="finalizar_envio")
     */
    public function envioFinalizado()
    {

        return $this->render('admin/envios/finaliza_envio.html.twig', [
        ]);
    }


}
