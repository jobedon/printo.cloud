<?php

namespace App\Controller;

use App\Services\AuthenticationService;
use App\Services\CatalogoService;
use App\Services\FileUploader;
use App\Services\OrdenService;
use App\Services\ProductoService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Routing\Annotation\Route;

/**
 * CatalogoController.
 * @Route("/admin/Catalago", name="admin_catalogo_")
 */
class CatalogoController extends AbstractController
{

    private $authService;
    private $catService;
    private $ordService;
    private $session;

    public function __construct(
        AuthenticationService $authService,
        CatalogoService $catService,
        OrdenService $ordService,
        SessionInterface $session
    ) {
        $this->authService = $authService;
        $this->catService  = $catService;
        $this->ordService  = $ordService;
        $this->session     = $session;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $esProforma = false;
        $this->session->set('esProforma' , $esProforma);

        $this->authService->refreshSubscription();
        $productos = $this->catService->initCatalogo();
        $result = $this->ordService->prepareOrdenInit();

        return $this->render('admin/catalogo/index.html.twig', [
            'productos' => $productos,
            'categorias' => $result['productos'],
            'itemsOrden' => $result['itemsOrden'],
            'tarifas'    => $result['tarifas'],
        ]);
    }


    /**
     * @Route("/Obtener-Informacion-Producto", name="obtener_info_producto")
     */
    public function obtenerInfoProducto(Request $r, ProductoService $productoService)
    {

        $idProducto = $r->get('idProducto');
        $idItem     = $r->get('idItem');

        $result = $productoService->obtenerInfoProducto([
            'idProducto' => $idProducto,
            'idItem'     => $idItem
        ]);

        if (isset($result['error']) && $result['error']) {
            return new JsonResponse($result);
        }

        // codigo mudado a changeCategory

        $html = $this->renderView('admin/catalogo/productos/detalle_nuevo_producto.html.twig', $result);

        return new JsonResponse([
            'error' => false,
            'html'  => $html
        ]);
    }

    /**
     * @Route("/Cambio-Producto", name="change_product")
     */
    public function changeProducto(Request $r, ProductoService $productoService)
    {

        $nombreProducto = $r->get("tipoProducto");
        $idMaterial     = $r->get("productoId");
        $idItem         = $r->get('idItem');

        $result = $productoService->cambioMaterial([
            'nombreProducto' => $nombreProducto,
            'idMaterial' => $idMaterial,
            'idItem'     => $idItem
        ]);

        if (isset($result['error']) && $result['error']) {
            return new JsonResponse($result);
        }

        $flujoHtml = $result['htmlProducto'];

        $favoriteHtml =  $this->renderView('admin/catalogo/productos/favorito_swtich.html.twig', [
            'favorito' => $result['favorito'],
        ]);

        return new JsonResponse([
            'error'        => false,
            'favoriteHtml' => $favoriteHtml,
            'flujoHtml'    => $flujoHtml,
        ]);
    }

    /**
     * @Route("/Edit-Prodcut", name="edit_product")
     */
    public function editProduct(Request $r, ProductoService $productoService)
    {
        $idItem = $r->get('idItem');

        $result = $productoService->editProduct([
            'idItem' => $idItem
        ]);

        if (isset($result['error']) && $result['error']) {
            return new JsonResponse($result);
        }

        return new JsonResponse([
            'error' => false,
            'item'  => $result
        ]);

    }

    /**
     * @Route("/Add-Product", name="add_product")
     */
    public function addProductToOrder(Request $r, FileUploader $fileUploader, OrdenService $ordenService)
    {

        if ($r->getMethod() == 'POST') {

            $idItem    = $r->get('idItem');
            $conservar = $r->get('conservar');

            $alias     = $r->get('alias');

            //$tipoProducto  = $r->get('categoria'); // Se reemplaza con el producto->getMaquina
            $productoId    = $r->get('producto');
            //$prodName      = $r->get('productoname'); // Se reemplza con el producto
            $tipoCorte     = $r->get('tipocorte');
            $laminar       = $r->get('necesitalaminar');
            $ojales        = $r->get('necesitaojales');
            $cantOjales    = $r->get('cantojales');
            $tipoLaminado  = $r->get('laminado');
            $cantidad      = $r->get('cantidad');
            $tipoImpresion = $r->get('impresion');
            $observacion   = $r->get('observacion');
            $observacion   = substr($observacion, 0, 300);

            $corteRigido   = $r->get('necesitacorterecto');

            $ancho         = $r->get('ancho');
            $alto          = $r->get('alto');
            $electrocorte  = $r->get('necesitaelectrocorte');

            //$subcategoria  = $r->get('subcategoria'); // dato innecesario

            $filePrint = $r->files->get('archivoimpresion');
            $fileElect = $r->files->get('archivoelectrocorte');

            try
            {

                $result = $ordenService->addProductToOrder([
                    'idItem'        => $idItem,
                    'conservar'     => $conservar,

                    'alias'         => $alias,

                    'productoId'    => $productoId,
                    'tipoCorte'     => $tipoCorte,
                    'laminar'       => $laminar,
                    'ojales'        => $ojales,
                    'cantOjales'    => $cantOjales,
                    'tipoLaminado'  => $tipoLaminado,
                    'cantidad'      => $cantidad,
                    'tipoImpresion' => $tipoImpresion,
                    'observacion'   => $observacion,
                    'corteRigido'   => $corteRigido,
                    'ancho'         => $ancho,
                    'alto'          => $alto,
                    'electrocorte'  => $electrocorte,

                    'filePrint'     => $filePrint,
                    'fileElect'     => $fileElect,
                    
                ]);
            }catch(Exception $e)
            {
                return new JsonResponse([
                    'error' => true,
                    'msg' => $e->getMessage()
                ]);
            }

            $item = $result['item'];

            $html = $this->renderView('admin/catalogo/productos/item_producto.html.twig', [
                'item'   => $item,
                'action' => 'newitem'
            ]);

            return new JsonResponse([
                'error'     => false,
                'html'      => $html,
                'detalle'   => $result['detalle'],
                'msg'       => $result['msg'],
                'action'    => $result['action'],
                'idDetalle' => $item->getId(),
                'tarifas'   => $result['tarifas'],

                // Nuevo esquema de mensajes
                // 'warning' => $result['warning'],
                // 'msgW'    => $result['msgW']
            ]);
        }

        return new JsonResponse([
            'error' => true,
            'html'  => '',
            'msg'   => 'Hubo un problema, intentar más tarde'
        ]);
    }

    /**
     * @Route("/Eliminar-Item", name="eliminar_item")
     */
    public function deleteItem(OrdenService $ordenService, Request $r)
    {
        $idItem = $r->get('idItem');

        $result = $ordenService->deleteItem($idItem);

        if ($result['error']) {
            return new JsonResponse($result);
        }


        $urlRedirect = $this->generateUrl('admin_orden');
        $mensaje = 'Se ha eliminado correctamente';

        return new JsonResponse([
            'error'       => $result['error'],
            'mensaje'     => $result['mensaje'],
            'urlRedirect' => $urlRedirect
        ]);
    }

    /**
     * @Route("/Upload-Image-Product", name="upload_img_prod")
     */
    public function uploadImgProd(Request $r, ProductoService $prodService)
    {
        $file      = $r->files->get('file');
        $idDetalle = $r->get('idDetalle');
        $isEdit    = $r->get('isEdit');

        $result = $prodService->uploadImgProd([
            'file' =>      $file,
            'idDetalle' => $idDetalle,
            'idEdit' =>    $isEdit,
        ]);

        return new JsonResponse([
            'imgPathRef' => $result
        ]);
    }


    /**
     * @Route("/Eliminar-Orden", name="delete_order")
     */
    public function deleteOrder(OrdenService $ordenService)
    {
        $ordenService->deleteOrder();
        $esProforma = $this->session->get('esProforma');
        if ($esProforma) {
            return $this->redirectToRoute('admin_proforma_index');
        }
        return $this->redirectToRoute('admin_catalogo_index');
    }

}
