<?php

namespace App\Controller;

use App\Exceptions\ApiResponse;
use App\Exceptions\TransferFileException;
use App\Services\OrdenRestService;
use App\Services\OrdenService;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * OrdenRestController.
 * @Route("/api", name="api")
 */
class OrdenRestController extends AbstractController
{

    /**
     * @Route("/transferfiles", name="_transfer_files")
     */
    public function transferFilesOrder(Request $r, OrdenRestService $ordenRestService, LoggerInterface $logger)
    {

        try {
            $data = json_decode($r->getContent(), true);

            $logger->info("RASTREO REQUEST : " . json_encode($data));

            $response = $ordenRestService->transferFilesOrder($data);


        } catch (TransferFileException $e) {
            return new ApiResponse($e->getMessage(), $e->getCode());
        } catch (Exception $e) {
            return new ApiResponse($e->getMessage(), ApiResponse::GENERAL_ERROR);
        }

        $logger->info("RASTREO RESPONSE : " . json_encode($response));

        return new JsonResponse($response);

    }
}
