<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/legal", name="legal")
 */
class LegalController extends AbstractController
{
    /**
     * @Route("/legal", name="index")
     */
    public function index()
    {
        return $this->render('legal/index.html.twig', [
            'controller_name' => 'LegalController',
        ]);
    }

    /**
     * @Route("/terminos-condiciones", name="_terminos")
     */
    public function terminos()
    {
        return $this->render('legal/terminos_condiciones.html.twig', [
            
        ]);
    }

    /**
     * @Route("/politicas-de-privacidad", name="_politicas_privacidad")
     */
    public function politicasPrivacidad()
    {
        return $this->render('legal/politica_privacidad.html.twig', [
            
        ]);
    }

    /**
     * @Route("/politicas-de-entrega", name="_politicas_entrega")
     */
    public function politicasEntrega()
    {
        return $this->render('legal/politica_entrega.htmlt.twig', [
            
        ]);
    }
}
