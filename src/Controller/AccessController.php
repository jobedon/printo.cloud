<?php

namespace App\Controller;

use App\Entity\UserPi;
use App\Services\TokenService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * OrdenController.
 * @Route("/admin", name="admin")
 */
class AccessController extends AbstractController
{

    /** @var SessionInterface|null $session */
    private $session;

    private $encoder;

    private $encoderFactory;

    private $em;

    public function __construct(
        SessionInterface $session,
        UserPasswordEncoderInterface $encoder,
        EncoderFactoryInterface $encoderFactory,
        EntityManagerInterface $em
    ) {
        $this->session = $session;
        $this->encoder = $encoder;
        $this->encoderFactory = $encoderFactory;
        $this->em             = $em;
    }

    /**
     * @Route("/Cambiar-Contraseña", name="_change_password")
     */
    public function changePassword(Request $r)
    {

        if ($r->getMethod() == 'POST') {
            $contraseñaActual  = $r->get('current_password');
            $contraseñaNueva   = $r->get('new_password');
            $contraseñaRepetir = $r->get('repeat_password');

            if ($contraseñaNueva != $contraseñaRepetir) {
                $this->addFlash('error', 'Nueva contraseña no coincide con la repetida');
                return $this->redirectToRoute("admin_change_password");
            }

            $regex = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*[^a-zA-Z\d]).{8,}$/';
            if (!preg_match($regex, $contraseñaNueva)) {
                $this->addFlash('error', 'Hubo un error al actualizar las contraseñas');
                $this->addFlash('errormsg', 'La contraseña es debil, debe contener al menos un caracter mayúscula, uno minúscula, un dígito y un caracter especial');
                return $this->redirectToRoute("admin_change_password");
            }

            if (!$this->encoder->isPasswordValid($this->getUser(), $contraseñaActual)) {
                $this->addFlash('error', 'Hubo un error al actualizar las contraseñas');
                $this->addFlash('errormsg', 'La contraseña ingresada con coincide con la actual');
                return $this->redirectToRoute("admin_change_password");
            }

            $pwd = $this->encoder->encodePassword($this->getUser(), $contraseñaNueva);

            $this->getUser()->setPassword($pwd);
            $this->getUser()->setEstado('A');
            $this->em->flush();

            $this->addFlash('success', 'La contraseña se ha cambiado satisfactoriamente');
        }

        return $this->render('admin/user/change_password.html.twig', []);
    }
}
