<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use App\Entity\Configuraciones;
use App\Entity\OrdenCab;
use App\Entity\OrdenDet;
use App\Entity\ProductoFavorito;
use App\Exceptions\OrdenException;
use App\Services\AuthenticationService;
use App\Services\DireccionesService;
use App\Services\EnviosService;
use App\Services\FileUploader;
use App\Services\OrdenRestService;
use App\Services\OrdenService;
use App\Services\ProductoService;
use App\Services\TokenService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use NumberFormatter;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * OrdenController.
 * @Route("/admin", name="admin")
 */
class OrdenController extends AbstractController
{

    /** @var SessionInterface|null $session */
    private $session;

    private $em;

    private $serializer;

    private $logger;

    private $authService;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        AuthenticationService $authService
    ) {
        $this->session = $session;
        $this->em      = $em;
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
        $this->logger = $logger;
        $this->authService = $authService;
    }

    /**
     * @Route("/test", name="_test")
     */
    public function testFile()
    {
        return $this->render('admin/orden/test.html.twig', []);
    }

    /**
     * @Route("/Orden", name="_orden")
     */
    public function index(OrdenService $productoService, LoggerInterface $logger): Response
    {

        $this->authService->refreshSubscription();

        $result = $productoService->prepareOrdenInit();

        return $this->render('admin/orden/index.html.twig', [
            'categorias' => $result['productos'],
            'itemsOrden' => $result['itemsOrden'],
        ]);
    }


    /**
     * @Route("/Orden-Procesar", name="_procesar_orden")
     */
    public function procesarOrden(OrdenService $ordenService, DireccionesService $dirService): Response
    {

        $result = $ordenService->procesarOrden();

        if (isset($result['error']) && $result['error']) {
            return $this->redirectToRoute('admin_orden');
        }

        return $this->render('admin/orden/proceso_orden.html.twig', [
            'itemsOrden'  => $result['itemsOrden'],
            'valores'     => $result['tarifas']
        ]);
    }

    /**
     * @Route("/Cambio-Categoria", name="_change_category")
     */
    public function changeCategory(Request $r, ProductoService $productoService)
    {
        $categoria = $r->get('categoria');
        $idItem    = $r->get('idItem');

        $result = $productoService->changeCategory([
            'categoria' => $categoria,
            'idItem'    => $idItem
        ]);

        if (isset($result['error']) && $result['error']) {
            return new JsonResponse($result);
        }

        // codigo mudado a changeCategory

        $html = $this->renderView('admin/orden/productos_select.html.twig', $result);

        return new JsonResponse([
            'error' => false,
            'html'  => $html
        ]);
    }

    /**
     * @Route("/Cambio-Producto-Favorito", name="_change_favorite_product")
     */
    public function changeProductFavorite(Request $request, OrdenService $ordenService, ProductoService $productoService)
    {
        $productoId = $request->get("productoId");

        $result = $productoService->changeProductFavorite([
            'productoId' => $productoId,
        ]);

        $ordenService->refreshMateriales();

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @Route("/Cambio-Producto", name="_change_product")
     */
    public function changeProducto(Request $r, ProductoService $productoService)
    {

        $nombreProducto = $r->get("tipoProducto");
        $idMaterial     = $r->get("productoId");
        $idItem         = $r->get('idItem');

        $result = $productoService->cambioMaterial([
            'nombreProducto' => $nombreProducto,
            'idMaterial' => $idMaterial,
            'idItem'     => $idItem
        ]);

        if (isset($result['error']) && $result['error']) {
            return new JsonResponse($result);
        }

        $flujoHtml = $this->renderView('admin/orden/flujo.html.twig', [
            'producto'          => $result['producto'],
            'extAllowImpresion' => $result['extAllowImpresion'],
            'extAllowElectroco' => $result['extAllowElectroco'],
            'item'              => $result['item'],
            'features'          => $result['features'],
        ]);

        $favoriteHtml =  $this->renderView('admin/orden/favorito_swtich.html.twig', [
            'favorito' => $result['favorito'],
        ]);

        return new JsonResponse([
            'error'        => false,
            'favoriteHtml' => $favoriteHtml,
            'flujoHtml'    => $flujoHtml,
        ]);
    }

    /**
     * 
     * Subida de archivos desde el servidor a WorkDrive
     * 
     * @deprecated Se subira directamente desde el frontend al Workdrive
     * 
     * @Route("/Add-File", name="_add_file")
     */
    public function addFile(Request $r, FileUploader $fileUploader)
    {
        if ($r->getMethod() == 'POST') {

            $filePrint = $r->files->get('archivoimpresion');
            $fileElect = $r->files->get('archivoelectrocorte');
            $idDetalle = $r->get('idDetalle');


            $ordenRepo = $this->em->getRepository(OrdenDet::class);
            $ordenDet = $ordenRepo->findOneBy([
                'estado' => 'A',
                'id'     => $idDetalle
            ]);

            if (!empty($idDetalle)) {
                $path = $ordenDet->getSubcategoriaProducto()->getCategoriaProducto()->getTipoProducto()->getCategoria() .
                    '/' .
                    $this->getUser()->getId() .
                    '/' .
                    $ordenDet->getOrdenCab()->getId() .
                    '/';

                //throw new Exception("error de caida");

                if ($filePrint) {
                    $filePrintFileName = $fileUploader->upload($filePrint, $path);
                    $ordenDet->setRutaImpresion($filePrintFileName);
                }

                if ($fileElect) {
                    $fileElectFileName = $fileUploader->upload($fileElect, $path);
                    $ordenDet->setRutaElectrocorte($fileElectFileName);
                }
                $ordenDet->setSubidaImpresion('S');
                $this->em->flush();
            } else {
                return new JsonResponse([
                    'error' => true,
                    'msg'   => 'Error al procesar item ' . $idDetalle . '<',
                ]);
            }


            return new JsonResponse([
                'error' => false,
                'msg'   => 'ok',
            ]);
        }

        return new JsonResponse([
            'error'     => true,
            'msg'       => 'Hubo un problema, intentar más tarde',
        ]);
    }

    /**
     * @Route("/Add-Product", name="_add_product")
     */
    public function addProductToOrder(Request $r, FileUploader $fileUploader, OrdenService $ordenService)
    {

        if ($r->getMethod() == 'POST') {

            $idItem    = $r->get('idItem');
            $conservar = $r->get('conservar');

            //$tipoProducto  = $r->get('categoria'); // Se reemplaza con el producto->getMaquina
            $productoId    = $r->get('producto');
            //$prodName      = $r->get('productoname'); // Se reemplza con el producto
            $tipoCorte     = $r->get('tipocorte');
            $laminar       = $r->get('necesitalaminar');
            $ojales        = $r->get('necesitaojales');
            $cantOjales    = $r->get('cantojales');
            $tipoLaminado  = $r->get('laminado');
            $cantidad      = $r->get('cantidad');
            $tipoImpresion = $r->get('impresion');
            $observacion   = $r->get('observacion');
            $observacion   = substr($observacion, 0, 300);

            $corteRigido   = $r->get('necesitacorterecto');

            $ancho         = $r->get('ancho');
            $alto          = $r->get('alto');
            $electrocorte  = $r->get('necesitaelectrocorte');

            //$subcategoria  = $r->get('subcategoria'); // dato innecesario

            $filePrint = $r->files->get('archivoimpresion');
            $fileElect = $r->files->get('archivoelectrocorte');

            try {

                $result = $ordenService->addProductToOrder([
                    'idItem'        => $idItem,
                    'conservar'     => $conservar,

                    'productoId'    => $productoId,
                    'tipoCorte'     => $tipoCorte,
                    'laminar'       => $laminar,
                    'ojales'        => $ojales,
                    'cantOjales'    => $cantOjales,
                    'tipoLaminado'  => $tipoLaminado,
                    'cantidad'      => $cantidad,
                    'tipoImpresion' => $tipoImpresion,
                    'observacion'   => $observacion,
                    'corteRigido'   => $corteRigido,
                    'ancho'         => $ancho,
                    'alto'          => $alto,
                    'electrocorte'  => $electrocorte,

                    'filePrint'     => $filePrint,
                    'fileElect'     => $fileElect,

                ]);
            } catch (Exception $e) {
                return new JsonResponse([
                    'error' => true,
                    'msg' => $e->getMessage()
                ]);
            }

            $item = $result['item'];

            $html = $this->renderView('admin/orden/item_producto.html.twig', [
                'item'   => $item,
                'action' => 'newitem'
            ]);

            return new JsonResponse([
                'error'     => false,
                'html'      => $html,
                'detalle'   => $result['detalle'],
                'msg'       => $result['msg'],
                'action'    => $result['action'],
                'idDetalle' => $item->getId(),

                // Nuevo esquema de mensajes
                'warning' => $result['warning'],
                'msgW'    => $result['msgW']
            ]);
        }

        return new JsonResponse([
            'error' => true,
            'html'  => '',
            'msg'   => 'Hubo un problema, intentar más tarde'
        ]);
    }

    /**
     * @Route("/Eliminar-Item/{idItem}", name="_eliminar_item")
     */
    public function deleteItem($idItem, OrdenService $ordenService)
    {

        $result = $ordenService->deleteItem($idItem);

        if ($result['error']) {
            return new JsonResponse($result);
        }

        $urlRedirect = $this->generateUrl('admin_orden');
        $mensaje = 'Se ha eliminado correctamente';

        return new JsonResponse([
            'error'       => $result['error'],
            'mensaje'     => $result['mensaje'],
            'urlRedirect' => $urlRedirect
        ]);
    }

    /**
     * @Route("/Calcular-Tarifas", name="_calcular_tarifas")
     */
    public function getTarifas(OrdenService $ordenService, DireccionesService $dirService, Request $r)
    {
        $requiereEnvio = $r->get('requiereEnvio');
        $direccionId   = $r->get('direccionId');

        $tarifas   = $this->session->get('tarifas');
        $subtotal = $tarifas['valorSubtotal'];

        $direccion = null;
        if ($requiereEnvio == 'true') {
            $direccion = $dirService->searchDireccion($direccionId);
        }

        $tarifas = $ordenService->calcularTarifaPicker($direccion, $subtotal);

        return new JsonResponse($tarifas);
    }

    /**
     * @Route("/Orden/{noOrden}", name="_finalizar_orden")
     */
    public function ordenFinalizada($noOrden)
    {

        $ordenCabRep = $this->em->getRepository(OrdenCab::class);
        $orden = $ordenCabRep->findOneBy([
            'user'    => $this->getUser(),
            'noOrden' => $noOrden
        ]);


        return $this->render('admin/orden/finaliza_orden.html.twig', [
            'orden' => $orden
        ]);
    }

    /**
     * @Route("/Edit-Prodcut", name="_edit_product")
     */
    public function editProduct(Request $r, ProductoService $productoService)
    {
        $idItem = $r->get('idItem');

        $result = $productoService->editProduct([
            'idItem' => $idItem
        ]);

        if (isset($result['error']) && $result['error']) {
            return new JsonResponse($result);
        }

        return new JsonResponse([
            'error' => false,
            'item'  => $result
        ]);
    }

    /**
     * @Route("/Eliminar-Orden", name="_delete_order")
     */
    public function deleteOrder(OrdenService $ordenService)
    {
        $ordenService->deleteOrder();
        return $this->redirectToRoute('admin_orden');
    }

    /**
     * @Route("/Pago-Orden", name="_pago_order")
     */
    public function pagoOrden(Request $r, OrdenService $ordenService)
    {

        if ($r->getMethod() == 'POST' || $r->getMethod() == 'GET') {

            $result = $ordenService->pagoOrden();

            if (isset($result['error']) && $result['error']) {
                return $this->redirectToRoute($result['to_route']);
            }

            return $this->render('admin/orden/pago_orden.html.twig', [
                'valores' => $result['valores'],
                'fp'      => $result['fp']
            ]);
        }

        return $this->redirectToRoute('admin_procesar_orden');
    }

    /**
     * @Route("/Finalizar-Orden", name="_finalizar_order")
     */
    public function finalizarCompra(Request $r, OrdenService $ordenService)
    {

        $formaPago = $r->get('formaPago');
        try {
            $result = $ordenService->finalizarCompra([
                'formaPago' => $formaPago
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'error'       => true,
                'mensaje'     => $e->getMessage()
            ]);
        }

        $urlRedirect = $this->generateUrl('admin_finalizar_orden', [
            'noOrden' => $result['noOrden']
        ]);

        return new JsonResponse([
            'error'       => $result['error'],
            'mensaje'     => $result['mensaje'],
            'urlRedirect' => $urlRedirect,
        ]);
    }

    /**
     * @Route("/Cambio-Forma-Pago", name="_change_method_payment")
     */
    public function cambiaFormaPago(Request $r, OrdenService $ordenService)
    {
        $fp = $r->get('formapago');

        $html = '';


        switch ($fp) {
            case 'creditodirecto':
                $rubrosOrden = $ordenService->getRubrosOrdenActual();
                $total = $ordenService->getRubrosCreditoDirecto();

                $html = $this->renderView('admin/orden/formapago/creditodirecto.html.twig', [
                    'total'       => $total,
                    'rubrosOrden' => $rubrosOrden
                ]);
                break;
            case 'mensajero':
                $html = $this->renderView('admin/orden/formapago/mensajero.html.twig', []);
                break;
            case 'oficina':
                $html = $this->renderView('admin/orden/formapago/pagooficina.html.twig', []);
                break;
            case 'transfbanc':
                $html = $this->renderView('admin/orden/formapago/transfbanc.html.twig', []);
                break;
            case 'tc':
                $esProforma     = $this->session->get('esProforma');
                $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

                $ordenCabRepo = $this->em->getRepository(OrdenCab::class);

                $ordenCab = $ordenCabRepo->findOneBy([
                    'estado' => $estadoOrdenCab,
                    'user' => $this->getUser()
                ]);

                if (is_null($ordenCab)) {
                    return new JsonResponse([
                        'error' => true,
                        'message' => 'Orden no encontrada',
                        'urlRedirect' => $this->generateUrl('admin_orden')
                    ]);
                }

                $checkoutId = $ordenService->getCheckoutId([
                    'totalPagar' => $ordenCab->getTotalPagar()
                ]);

                if (isset($checkoutId["error"]) && $checkoutId["error"]) {
                    return new JsonResponse([
                        'error' => true,
                        'urlRedirect' => $this->generateUrl('admin_pago_order'),
                        'message' => isset($checkoutId["message"]) ? $checkoutId["message"] : 'Hubo un problema',
                    ]);
                }

                $html = $this->renderView('admin/orden/formapago/tarjetacredito.html.twig', [
                    'checkoutId' => $checkoutId
                ]);
                break;
            default:
                # code...
                break;
        }
        $arrayParametros = '';
        return new JsonResponse([
            'html' => $html
        ]);
    }

    /**
     * @Route("/Preparar-Subida", name="_prepare_upload")
     */
    public function prepararSubida(Request $r, TokenService $tokenService, OrdenService $ordenService)
    {

        if ($r->getMethod() == 'POST') {

            $tipoToken = $r->get('tipotoken');
            $idDetalle = $r->get('idDetalle');
            $isEdit    = $r->get('isEdit');

            $arrayParametros = [
                'tipotoken' => (isset($tipotoken)) ? $tipotoken : '',
                'idDetalle' => $idDetalle,
                'isEdit'    => $isEdit
            ];

            $parentIds = $ordenService->prepareUpload($arrayParametros);

            $token = $tokenService->getToken($tipoToken);
            return new JsonResponse([
                'error'        => false,
                'token'        => $token,
                'parentIds'    => $parentIds,
                'datetime_ini' => date("Y-m-d H:i:s")
            ]);
        }

        return new JsonResponse([
            'error'        => true,
            'mensaje'      => 'Hubo un error solicitando token!'
        ]);
    }

    /**
     * @Route("/Actualizar-Archivo", name="_update_file")
     */
    public function updateFile(Request $r, OrdenService $ordenService)
    {

        if ($r->getMethod() == 'POST') {

            $idDetalle = $r->get('idDetalle');
            $idFile    = $r->get('idFile');
            $tipoFile  = $r->get('tipo');

            $ordenService->uploadFile([
                'idDetalle' => $idDetalle,
                'idFile'    => $idFile,
                'tipoFile'  => $tipoFile
            ]);

            return new JsonResponse([
                'error'        => false,
                'mensaje'      => 'Archivo actualizado correctamente'
            ]);
        }

        return new JsonResponse([
            'error'        => true,
            'mensaje'      => 'Hubo un error actualizando archivo'
        ]);
    }

    /**
     * @Route("/Ordenes-Enviadas", name="_orders_sent")
     */
    public function ordenesTramite(OrdenService $ordenService)
    {


        $tickets = $ordenService->getTicketsEnviadas();

        return $this->render('admin/orden/historial/enviadas.html.twig', [
            'tickets' => $tickets
        ]);
    }

    /**
     * @Route("/Detalle-Orden/{ticket}", name="_order_detail")
     */
    public function detalleOrden($ticket, OrdenService $ordenService)
    {

        $ordenDetalle = $ordenService->getDetalleTicket($ticket);

        if ($ordenDetalle['error']) {
            $this->addFlash('error', $ordenDetalle['mensaje']);
            //$this->session->getFlashBag()->set('error', $ordenDetalle['mensaje']);
            return $this->redirectToRoute('admin_orders_sent');
        }

        return $this->render('admin/orden/historial/detalle_orden.html.twig', [
            'ordenDetalle' => $ordenDetalle
        ]);
    }


    /**
     * @Route("/Editar-Item-File", name="_edit_item_file")
     */
    public function editItemFile(Request $r)
    {

        if ($r->getMethod() == 'POST') {
            $idDetalle = $r->get('idItem');

            $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

            $ordenDet = $ordenDetRepo->findOneBy([
                'id' => $idDetalle,
                'estado' => 'A'
            ]);

            if (empty($ordenDet)) {
                return new JsonResponse([
                    'error' => true,
                    'mensaje' => 'No existe Item'
                ]);
            }

            if ($ordenDet->getOrdenCab()->getUser()->getId() != $this->getUser()->getId()) {
                return new JsonResponse([
                    'error' => true,
                    'mensaje' => 'No existe Orden'
                ]);
            }


            /** @var ConfiguracionesRepository|null $confRepo */
            $confRepo = $this->em->getRepository(Configuraciones::class);

            $tipoProducto = $ordenDet->getSubcategoriaProducto()->getCategoriaProducto()->getTipoProducto()->getCategoria();

            if ($tipoProducto == 'XEROX') {
                $extAllowImpresion = $confRepo->getConfigString('FILES_ALLOW_XEROX_IMPRESION');
                $extAllowElectroco = $confRepo->getConfigString('FILES_ALLOW_XEROX_ELECTROCORTE');
            } elseif ($tipoProducto == 'MIMAKI') {
                $extAllowImpresion = $confRepo->getConfigString('FILES_ALLOW_MIMAKI_IMPRESION');
                $extAllowElectroco = $confRepo->getConfigString('FILES_ALLOW_MIMAKI_ELECTROCORTE');
            }

            $html = $this->renderView('admin/orden/historial/edit_item_file.html.twig', [
                'item' => $ordenDet,
                'extAllowElectroco' => $extAllowElectroco,
                'extAllowImpresion' => $extAllowImpresion
            ]);

            return new JsonResponse([
                'error' => false,
                'html'  => $html
            ]);
        }


        return new JsonResponse([
            'error' => true,
            'mensaje' => 'Huno un problema con al edición'
        ]);
    }

    /**
     * @Route("/Preparar-Edicion-Archivo", name="_prepare_edit_file_upload")
     */
    public function prepareFileEditItem(Request $r, TokenService $tokenService, OrdenService $ordenService)
    {
        if ($r->getMethod() == 'POST') {

            $tipoToken = $r->get('tipotoken');
            $idDetalle = $r->get('idDetalle');

            $ordenDetRepo = $this->em->getRepository(OrdenDet::class);

            $ordenDet = $ordenDetRepo->findOneBy([
                'id' => $idDetalle,
                'estado' => 'A'
            ]);

            if (empty($ordenDet)) {
                return new JsonResponse([
                    'error' => true,
                    'mensaje' => 'No existe Item'
                ]);
            }

            if ($ordenDet->getOrdenCab()->getUser()->getId() != $this->getUser()->getId()) {
                return new JsonResponse([
                    'error' => true,
                    'mensaje' => 'No existe Orden'
                ]);
            }

            $parentIds = [
                'parentIdXeMi' => $ordenDet->getFiles()->getParentIdImpresionTk(),
                'parentIdElec' => $ordenDet->getFiles()->getParentIdElectroTk(),
            ];

            $token = $tokenService->getToken($tipoToken);
            return new JsonResponse([
                'error'        => false,
                'token'        => $token,
                'parentIds'    => $parentIds,
                'datetime_ini' => date("Y-m-d H:i:s")
            ]);
        }

        return new JsonResponse([
            'error'        => true,
            'mensaje'      => 'Hubo un error solicitando token!'
        ]);
    }

    /**
     * @Route("/Shopper-Result", name="_shopper_result")
     */
    public function shopperResultPayment(Request $r, OrdenService $ordenService)
    {

        $resourcePath = $r->get('resourcePath');

        $esProforma     = $this->session->get('esProforma');
        $estadoOrdenCab = ($esProforma) ? 'PRO' : 'A';

        $ordenCabRepo = $this->em->getRepository(OrdenCab::class);

        $ordenCab = $ordenCabRepo->findOneBy([
            'estado' => $estadoOrdenCab,
            'user' => $this->getUser()
        ]);

        try {
            $responsePurchase = $ordenService->purchaseOrder([
                'resourcePath' => $resourcePath
            ]);
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
            //$this->session->getFlashBag()->set('error', $e->getMessage());
            return $this->redirectToRoute('admin_pago_order');
        }

        if ($responsePurchase['error']) {
            // $this->session->getFlashBag()->set('error', $responsePurchase['message']);
            // $this->session->getFlashBag()->set('toOption', true);
            $this->addFlash('error', $responsePurchase['message']);
            $this->addFlash('toOption', true);
            return $this->redirectToRoute('admin_pago_order');
        }

        return $this->redirectToRoute('admin_finalizar_orden', [
            'noOrden' => $responsePurchase['ordenVenta']['ordenZoho']['salesorder_number']
        ]);
    }


    /**
     * @Route("/Requiere-Envio", name="_cambio_requiere_envio", methods={"POST"})
     */
    public function cambioRequiereEnvio(
        DireccionesService $dirService,
        OrdenService $ordService,
        Request $r
    ) {
        $requiereEnvio = ($r->get('requiereenvio') != null && $r->get('requiereenvio') == "true") ? true : false;

        if ($requiereEnvio) {
            $direcciones = $dirService->getDirecciones();
            $html = $this->renderView('admin/orden/envio/metodo_envio.html.twig', [
                'direcciones' => $direcciones
            ]);
        } else {
            $valores = $ordService->calcularTarifas([]);
            $error = $valores['error'];
            $html = $this->renderView('admin/orden/envio/calculo_envio.html.twig', [
                'valores' => $valores
            ]);
        }

        return new JsonResponse([
            'error' => false,
            'msg'   => '',
            'html'  => $html
        ]);
    }

    /**
     * @Route("/Calcular-Metodo", name="_calcular_metodo", methods={"POST"})
     */
    public function calcularMetodoEnvio(OrdenService $ordService, Request $r)
    {

        $direccionId = $r->get('direccionId');
        $metodo      = $r->get('metodo');

        $valores = null;
        $error = false;

        if (!empty($direccionId)) {
            // try {
            $valores = $ordService->calcularTarifas([
                'direccionId' => $direccionId,
                'metodo' => $metodo
            ]);
            $error = $valores['error'];
            // } catch (Exception $e) {
            //     return new JsonResponse([
            //         'error' => true,
            //         'msg'   => 'Error en el calculo ' .  $e->getMessage()
            //     ]);
            // }
        }

        $html = $this->renderView('admin/orden/envio/calculo_envio.html.twig', [
            'valores' => $valores
        ]);

        return new JsonResponse([
            'error' => $error,
            'msg'   => '',
            'html'  => $html
        ]);
    }

    /**
     * @Route("/Upload-Comprobante", name="_upload_comprobante")
     */
    public function uploadComprobante(Request $r, OrdenService $ordService)
    {
        $file      = $r->files->get('file');

        $result = $ordService->uploadImgProd([
            'file' =>      $file
        ]);

        return new JsonResponse([
            'error'      => false
        ]);
    }
}
