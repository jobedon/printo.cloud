<?php

namespace App\DataFixtures;

use App\Entity\UserPi;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        
        $user = new UserPi();
        $user->setEmail('jbedonsanchez92@gmail.com');
        $user->setPassword(
            $this->encoder->encodePassword($user, '0000')
        );
        $manager->persist($user);
        $manager->flush();
    }
}
