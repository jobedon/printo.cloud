$(document).ready(function () {
  toastr.options.positionClass = 'toast-bottom-left';

  function updateProgress(label, percentage, color) {
    let elemento = $("#progressComprobante .progress-bar");
    elemento.attr("aria-valuenow", percentage);
    elemento.css("width", percentage + "%");
    elemento.removeClass("bg-primary bg-danger bg-success");
    elemento.addClass("bg-" + color);
    let labelElemento = $(
      "#progressComprobante .progress-bar span"
    );
    labelElemento.text(percentage + "% subiendo");

    let tipo = label == "progressComprobante" ? "Comprobante" : tipo;

    if (color == "danger") {
      labelElemento.text(percentage + "% " + tipo + " error");
    } else if (color == "success") {
      labelElemento.text(percentage + "% " + tipo);
    }
  }

  $("#panelFormaPago").on('change', "input[name='archivocomprobante']", function () {
    bsCustomFileInput.init();

    $("#finalizarOrden").removeClass('disabled').removeAttr('disabled');

    let filename = $(this).val().split('\\').pop();
    let extension = filename.split('.').pop().toUpperCase();
    let extAllowed = $(this).data('extallowed');
    let messageFile = "Seleccione archivo " + extAllowed;

    if (!extAllowed.includes(extension)) {
      toastr.error('Extension no permitida, solo ' + extAllowed);
      $(this).val('');
      $(this).next('label').html(messageFile);
      return false;
    }

    if (filename.length <= 0) {
      $("#finalizarOrden").addClass('disabled').attr('disabled', true);
      toastr.error('Debe ingresar el comprobante');
      $(this).val('');
      $(this).next('label').html(messageFile);
      return false;
    }


  });

  $('#panelFormaPago').on('click', '#finalizarOrden', function () {
    $('#loadingopciones').removeClass('d-none')
    $('#loadingformapago').removeClass('d-none')
    let requiereEnvio = $("input[name='requiereenvio']").val()
    let direccionId = $("input[name='direccion']").val()
    let formaPago = $("input[name='formapago']:checked").val()
    let data = {
      formaPago: formaPago,
    }
    let url = $(this).data('href')

    // Logica del comprobante
    let tipo = $("input[name='formapago']:checked").val();
    if (tipo == 'transfbanc') {
      let label = "progressComprobante";
      var inputFile = document.getElementById('archivocomprobante');
      var file = inputFile.files[0];
      var dataFile = new FormData();
      dataFile.append('file', file, file.name);

      if (file == null) {
        toastr.error('Debe ingresar el comprobante');
      }

      $.ajax({
        url: urlUploadComprobante,
        type: "POST",
        data: dataFile,
        timeout: 0,
        processData: false,
        contentType: false,
        cache: false,
        xhr: function () {
          var xhr = new window.XMLHttpRequest();
          xhr.upload.addEventListener(
            "progress",
            function (evt) {
              if (evt.lengthComputable) {
                let percentComplete = (evt.loaded / evt.total) * 100;
                updateProgress(
                  label,
                  Math.ceil(percentComplete),
                  "primary"
                );
              }
            },
            false
          );
          return xhr;
        },
        success: function (result) {
          updateProgress(label, 100, "success");
          $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (response) {
              if (response.error) {
                $('#loadingopciones').addClass('d-none')
                $('#loadingformapago').addClass('d-none')
                toastr.error(response.mensaje)
              } else {
                toastr.success(response.mensaje)
                setTimeout(function () {
                  location.href = response.urlRedirect
                }, 3000)
              }
            },
            error: function (xhr, status, error) {
              toastr.error(
                'Hubo un error generando la orden, volver a intentar',
              )
            },
          })


        }
      });

    } else {

      $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (response) {
          if (response.error) {
            $('#loadingopciones').addClass('d-none')
            $('#loadingformapago').addClass('d-none')
            toastr.error(response.mensaje)
          } else {
            toastr.success(response.mensaje)
            setTimeout(function () {
              location.href = response.urlRedirect
            }, 3000)
          }
        },
        error: function (xhr, status, error) {
          toastr.error(
            'Hubo un error generando la orden, volver a intentar',
          )
        },
      })

    }




  })

})
