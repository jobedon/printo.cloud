$(document).ready(function () {
  
  toastr.options.positionClass = 'toast-bottom-left';

  $('#checkout').on('click', function (e) {
    $('#loadingPage').removeClass('d-none')
    let planKey = $("select[name='plan'] option:selected").val()
    let data = {
      planKey: planKey,
    }
    $.ajax({
      type: 'POST',
      url: urlCheckout,
      data: data,
      success: function (response) {
        if (response.error) {
          $('#loadingPage').addClass('d-none')
          toastr.error(response.message)
        } else {
          $('#selectPlan').hide(300)
          $('#cancelPlan').removeClass('d-none')
          $('#checkoutAction').addClass('d-none')
          $('#checkoutPanel').html(response.html)
        }
      },
      error: function (xhr, status, error) {
        toastr.error('Hubo un error generando la orden, volver a intentar')
        $('#loadingPage').addClass('d-none')
        $('#checkoutPanel').html('')
        $('#selectPlan').show(300)
        $('#cancelPlan').addClass('d-none')
        $('#checkoutAction').removeClass('d-none')
      },
    })
  })
  $('#changePlan').on('click', function (e) {
    $('#checkoutPanel').html('')
    $('#selectPlan').show(300)
    $('#cancelPlan').addClass('d-none')
    $('#checkoutAction').removeClass('d-none')
  })
})
