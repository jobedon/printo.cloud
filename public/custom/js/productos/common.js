
bsCustomFileInput.init();
$('.select2bs4').select2({
    theme: 'bootstrap4'
})
$("input[data-bootstrap-switch]").each(function () {
    $(this).bootstrapSwitch();
});

function conservar() {
    $("#groupfile").addClass("d-none");
    $("input[name='archivoimpresion']").removeAttr("required");
    $("input[name='archivoelectrocorte']").removeAttr("required");
}

function noConservar() {
    $("#groupfile").removeClass("d-none");
    $("input[name='archivoesEditimpresion']").attr("required", true);
    if ($("select[name='tipocorte']").val() == 'ELECTROCORTE') {
        $("input[name='archivoelectrocorte']").attr("required", true);
    }
}

function habilitaElectrocorte() {
    $("#electofile").removeClass("d-none");
    $("input[name='archivoelectrocorte']").val("");
    $("input[name='archivoelectrocorte']").attr("required", true);
}

function deshabilitaElectrocorte() {
    $("#electofile").addClass("d-none");
    $("input[name='archivoelectrocorte']").val("");
    $("input[name='archivoelectrocorte']").removeAttr("required");
}

function habilitaImpresion() {
    $("#imprefile").removeClass("d-none");
    $("input[name='archivoimpresion']").val("");
    $("input[name='archivoimpresion']").attr("required", true);
}

function deshabilitaImpresion() {
    $("#imprefile").addClass("d-none");
    $("input[name='archivoimpresion']").val("");
    $("input[name='archivoimpresion']").removeAttr("required");
}

var esEdit = false;
var esCorteSinCorteAElectro = false;
var esElectroAElecto = false;
var item = null;