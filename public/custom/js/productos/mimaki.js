$(document).ready(function () {
    toastr.options.positionClass = 'toast-bottom-left';
    
    $("input[name='necesitalaminar']").on("switchChange.bootstrapSwitch", function () {
        if ($(this).is (':checked')) {
            $("select[name='laminado']").val("").change();
            $("select[name='laminado']").attr("required", true);
            $(".laminadosection").removeClass("d-none");
        } else {
            $(".laminadosection").addClass("d-none");
            $("select[name='laminado']").removeAttr("required");
        }
    });

    $("input[name='necesitaelectrocorte']").on("switchChange.bootstrapSwitch", function () {
        if ($(this).is (':checked')) {
            $("#electofile").removeClass("d-none");
            $("input[name='archivoelectrocorte']").attr("required", true);
        } else {
            $("#electofile").addClass("d-none");
            $("input[name='archivoelectrocorte']").removeAttr("required");
        }
    });

    $("input[name='necesitaojales']").on("switchChange.bootstrapSwitch", function () {
        if ($(this).is (':checked')) {
            $(".ojalessection").removeClass("d-none");
            $("input[name='cantojales']").attr("required", true);
            $("input[name='cantojales']").val("");
            //$("textarea[name='especifojales']").val("Sin especificación");
        } else {
            $(".ojalessection").addClass("d-none");
            $("input[name='cantojales']").removeAttr("required");
        }
    });

    $("input.fileProduct").on("change", function (){
        bsCustomFileInput.init();
        let filename = $(this).val().split('\\').pop();
        let extension = filename.split('.').pop().toUpperCase();
        let extAllowed = $(this).data('extallowed');
        let messageFile = "Seleccione archivo " + extAllowed;

        if (!extAllowed.includes(extension)) {
            toastr.error('Extension no permitida, solo ' + extAllowed);
            $(this).val('');
            $(this).next('label').html(messageFile);
            return false;
        }
    });


    if (esEdit) {

        $("input[name='alias']").val(item.alias);

        $("input[name='cantidad']").val(item.cantidad);
        $("select[name='ancho']").val(item.ancho).change();
        $("input[name='alto']").val(item.alto);
        $("input[name='necesitalaminar']").bootstrapSwitch('state', item.laminado).change();
        $("input[name='necesitacorterecto']").bootstrapSwitch('state', item.corteRecto).change();
        $("select[name='laminado']").val(item.tipolaminado).change();
        $("input[name='necesitaojales']").bootstrapSwitch('state', item.ojales).change();
        $("input[name='necesitaelectrocorte']").bootstrapSwitch('state', item.electrocorte).change();
        $("input[name='cantojales']").val(item.cantidadojales);
        $("textarea[name='observacion']").val(item.observacion.replace("\n","\\n").replace("\r",""));
        $("select[name='impresion']").val(item.tipoimpresion).change();
        
        if (item.subidaImpresion || item.subidaElectro) {
            $("#conservarfiles").removeClass("d-none");
            $("#groupfile").addClass("d-none");
            $("input[name='archivoimpresion']").removeAttr("required");
            $("input[name='archivoelectrocorte']").removeAttr("required");
        }
        $("input[name='conservar']").on("change", function () {
            if ($(this).is(':checked')) {
                conservar();
            } else {
                noConservar();
            }

        });

        $('#loadingproduct').addClass('d-none')

    }

});