$(document).ready(function () {
    toastr.options.positionClass = 'toast-bottom-left';

    $("select[name='tipocorte']").on("change", function () {


        if (esEdit && !item.subidaElectro && !item.subidaImpresion ) {
            $("#conservarfiles").addClass("d-none");

            habilitaImpresion();

            if ($(this).val() == 'ELECTROCORTE') {
                habilitaElectrocorte();
            }
        } else {

            if ($(this).val() == 'ELECTROCORTE') {
                habilitaElectrocorte();

                // Si tengo recto o sin corte y cambio electrocorte
                if (esCorteSinCorteAElectro) {
                    $("input[name='conservar']").removeAttr("checked");
                    $("input[name='conservar']").attr("disabled", true);
                    // Se copia de conservar NO
                    noConservar();
                }

                // Si tengo electrocorte y cambio a electrocorte
                if (esElectroAElecto) {
                    $("input[name='conservar']").attr("checked", true);
                    $("input[name='conservar']").removeAttr("disabled");
                    // Se copia de conservar SI
                    conservar();
                }


            } else {
                deshabilitaElectrocorte();
                // Si tengo recto o sin corte y cambio entre ellos
                if (esCorteSinCorteAElectro) {
                    $("input[name='conservar']").attr("checked", true);
                    $("input[name='conservar']").removeAttr("disabled");
                    // Se copia de conservar SI
                    conservar();
                }

                // Si tengo electrocorte y cambio a recto o sin corte
                if (esElectroAElecto) {
                    $("input[name='conservar']").removeAttr("checked");
                    $("input[name='conservar']").attr("disabled", true);
                    // Se copia de conservar NO
                    noConservar()
                }
            }
        }
    });

    $("input[name='necesitalaminar']").on("switchChange.bootstrapSwitch", function () {
        if ($(this).is(':checked')) {
            $("select[name='laminado']").val("").change();
            $("select[name='laminado']").attr("required", true);
            $(".laminadosection").removeClass("d-none");
        } else {
            $(".laminadosection").addClass("d-none");
            $("select[name='laminado']").removeAttr("required");
        }
    });

    $("input.fileProduct").on("change", function () {
        bsCustomFileInput.init();
        let filename = $(this).val().split('\\').pop();
        let extension = filename.split('.').pop().toUpperCase();
        let extAllowed = $(this).data('extallowed');
        let messageFile = "Seleccione archivo " + extAllowed;

        if (!extAllowed.includes(extension)) {
            toastr.error('Extension no permitida, solo ' + extAllowed);
            $(this).val('');
            $(this).next('label').html(messageFile);
            return false;
        }
    });



    if (esEdit) {

        $("input[name='alias']").val(item.alias);

        $("select[name='tipocorte']").val(item.tipocorte).change();
        $("input[name='cantidad']").val(item.cantidad);
        $("select[name='impresion']").val(item.tipoimpresion).change();

        $("input[name='necesitalaminar']").bootstrapSwitch('state', item.laminado).change();
        $("select[name='laminado']").val(item.tipolaminado).change();

        $('#loadingproduct').addClass('d-none')
        let observacion = item.observacion.replace("\n", "\\n");
        observacion = observacion.replace("\r", "");
        $("textarea[name='observacion']").val(observacion);

        $("select[name='impresion']").val(item.tipoimpresion).change();

        if (item.subidaImpresion || item.subidaElectro) {
            $("#conservarfiles").removeClass("d-none");
            $("#groupfile").addClass("d-none");
            $("input[name='archivoimpresion']").removeAttr("required");
            $("input[name='archivoelectrocorte']").removeAttr("required");
        }

        $("input[name='conservar']").on("change", function () {
            if ($(this).is(':checked')) {
                conservar();
            } else {
                noConservar();
            }

        });

    }

});