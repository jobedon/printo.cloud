toastr.options.positionClass = 'toast-bottom-left';

function actualizarInfoProductos(detalle) {
  let selector1 = "#productDetails table tbody tr#item";
  let preciosubtotal = "";
  let desc = "";
  $.each(detalle, function (idx, item) {
    $(selector1 + item.id + " td span.nombreproducto").text(
      item.nombreproducto
    );
    desc = "";
    desc = item.descripcion.replace(/\n/g, "<br />");
    $(selector1 + item.id + " td span.descripcion").html(desc);
    $(selector1 + item.id + " td span.subcategoria").text(item.subcategoria);
    $(selector1 + item.id + " td span.categoria").text(item.categoria);
    $(selector1 + item.id + " td span.cantidad").text(item.cantidad);
    $(selector1 + item.id + " td span.preciounit").text(item.preciounit);

    preciosubtotal = "";
    preciosubtotal += "<span>" + item.subtotal + "</span>";
    preciosubtotal += '<p class=" stripHr">Base : ' + item.preciobase + "<br>";
    preciosubtotal +=
      item.preciolaminado == 0
        ? ""
        : "Laminado : " + item.preciolaminado + "<br>";
    preciosubtotal +=
      item.preciocorte == 0 ? "" : "Corte : " + item.preciocorte + "<br>";
    preciosubtotal +=
      item.precioojales == 0 ? "" : "Ojales : " + item.precioojales + "<br>";
    preciosubtotal +=
      item.preciorigido == 0
        ? ""
        : "Corte Recto : " + item.preciorigido + "</p>";

    $(selector1 + item.id + " td.preciosubtotal").html(preciosubtotal);
  });
}

function checkFilesUploads() {
  // > Logica inicial para revisar boton procesar
  let incomplete = false;
  let numItems = 0;
  $.each($(".progress-bar"), function (idx, item) {
    if ($(item).attr("aria-valuenow") != "100") {
      incomplete = true;
    }
    numItems++;
  });
  if (!incomplete && numItems != 0) {
    $(".btnCrearOrden").removeClass("disabled");
  }
  // > Logica final para revisar boton procesar
}

function updateProgress(label, percentage, idDetalle, color) {
  let elemento = $("#item" + idDetalle + " #" + label + " .progress-bar");
  elemento.attr("aria-valuenow", percentage);
  elemento.css("width", percentage + "%");
  elemento.removeClass("bg-primary bg-danger bg-success");
  elemento.addClass("bg-" + color);
  let labelElemento = $(
    "#item" + idDetalle + " #" + label + " .progress-bar span"
  );
  labelElemento.text(percentage + "% subiendo");
  if (color == "danger") {
    let tipo = label == "progressElectrocorte" ? "Electrocorte" : "Impresion";
    labelElemento.text(percentage + "% " + tipo + " error");
  } else if (color == "success") {
    let tipo = label == "progressElectrocorte" ? "Electrocorte" : "Impresion";
    labelElemento.text(percentage + "% " + tipo);
  }
}

function uploadFileWD(parentId, token, file, respAddProduct, label) {
  let uniqueid = new Date().getUTCMilliseconds();

  $.ajax({
    url: "https://upload.zoho.com/workdrive-api/v1/stream/upload",
    method: "POST",
    timeout: 0,
    processData: false,
    headers: {
      "x-filename": file.name,
      "x-parent_id": parentId,
      "upload-id": uniqueid,
      "x-streammode": "1",
      Authorization: "Bearer " + token,
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
      "Access-Control-Allow-Origin":
        "https://sistema.printo.cloud.dev:8000, https://upload.zoho.com",
    },
    xhr: function () {
      var xhr = new window.XMLHttpRequest();
      xhr.upload.addEventListener(
        "progress",
        function (evt) {
          if (evt.lengthComputable) {
            let percentComplete = (evt.loaded / evt.total) * 100;
            updateProgress(
              label,
              Math.ceil(percentComplete),
              respAddProduct.idDetalle,
              "primary"
            );
          }
        },
        false
      );
      return xhr;
    },
    data: file,
    success: function (result, status, xhr) {
      updateProgress(label, 100, respAddProduct.idDetalle, "success");
      checkFilesUploads();

      let idFile = result.data[0].attributes.resource_id;

      let dataUpdateFile = {
        idDetalle: respAddProduct.idDetalle,
        idFile: idFile,
        tipo: label == "progressElectrocorte" ? "electro" : "impresion",
      };

      $.ajax({
        type: "POST",
        url: urlUpdateFile,
        data: dataUpdateFile,
        success: function (respUpdateFile) {
          if (respUpdateFile.error) {
            toastr.error(
              "Hubo un error en la subida de archivos, volver a intentar"
            );
          }
        },
      });
    },
    error: function (xhr, status, error) {
      toastr.error("Hubo un error en la subida de archivos, volver a intentar");
      updateProgress(label, 99, respAddProduct.idDetalle, "danger");
    },
  });
}

$(document).ready(function () {
  checkFilesUploads();

  $("select[name='categoria']").on("change", function () {
    let data = {
      categoria: $(this).val(),
      idItem: $("input[name='idItem']").val(),
    };
    let target = $(this).data("target");
    $.ajax({
      type: $(this).data("method"),
      url: $(this).data("url"),
      data: data,
      success: function (r) {
        if (r.error) {
        } else {
          $("#" + target).html(r.html);
        }
      },
    });
  });

  $("#formProduct").on("submit", function () {
    $("#loadingproduct").removeClass("d-none");
    $(".btnCrearOrden").addClass("disabled");
    var data = $(this).serialize();

    let url = $(this).attr("action");
    let method = $(this).attr("method");

    let idItem = $("input[name='idItem']").val();
    let isEdit = idItem.length != 0;

    data += "&idItem=" + idItem;

    $.ajax({
      type: method,
      url: url,
      data: data,
      success: function (respAddProduct) {
        if (respAddProduct.error) {
          toastr.error(respAddProduct.msg);
          $("#loadingproduct").addClass("d-none");
        } else {

          if (respAddProduct.warning){
            toastr.warning(respAddProduct.msgW);
          }

          toastr.success(respAddProduct.msg);

          actualizarInfoProductos(respAddProduct.detalle);

          let datos = {
            idDetalle: respAddProduct.idDetalle,
          };

          let conservar = $("input[name='conservar']:checked").val();
          let isConservar = conservar == "on";

          let errorSubida = $("input[name='errorSubida']").val();
          let hasErrorSubida = errorSubida == "1";

          if (
            !isEdit ||
            (isEdit && hasErrorSubida) ||
            (isEdit && !isConservar)
          ) {
            $.ajax({
              type: "POST",
              url: urlPrepareUpload,
              data: {
                tipotoken: "UPLOAD",
                idDetalle: respAddProduct.idDetalle,
                isEdit: isEdit,
              },
              success: function (respPrepareUpload) {
                if (!respPrepareUpload.error) {
                  if (
                    typeof respPrepareUpload.parentIds.parentIdElec !==
                      "undefined" &&
                    respPrepareUpload.parentIds.parentIdElec.length > 0
                  ) {
                    let electFile = document.getElementById(
                      "archivoelectrocorte"
                    ).files[0];
                    uploadFileWD(
                      respPrepareUpload.parentIds.parentIdElec,
                      respPrepareUpload.token,
                      electFile,
                      respAddProduct,
                      "progressElectrocorte"
                    );
                  }
                  if (
                    typeof respPrepareUpload.parentIds.parentIdXeMi !==
                      "undefined" &&
                    respPrepareUpload.parentIds.parentIdXeMi.length > 0
                  ) {
                    let impreFile =
                      document.getElementById("archivoimpresion").files[0];
                    uploadFileWD(
                      respPrepareUpload.parentIds.parentIdXeMi,
                      respPrepareUpload.token,
                      impreFile,
                      respAddProduct,
                      "progressImpresion"
                    );
                  }

                  if (respAddProduct.action == "edititem") {
                    $("tr#item" + respAddProduct.idDetalle).remove();
                  }

                  $("#productDetails table tbody").prepend(respAddProduct.html);

                  $("#modal-producto").modal("hide");
                  $("#formProduct").trigger("reset");
                  $("#changeCategory").html("");
                  $("tr#itemEmpty").remove();
                } else {
                  toastr.error(respPrepareUpload.mensaje);
                }
                $("#loadingproduct").addClass("d-none");
              },
            });
          } else {
            $("#loadingproduct").addClass("d-none");
            checkFilesUploads();
            $("#modal-producto").modal("hide");
            $("#formProduct").trigger("reset");
            $("#changeCategory").html("");
            $("tr#itemEmpty").remove();
          }
        }
      },
    });

    return false;
  });

  $("#productDetails").on("click", ".edit", function () {
    $("button#addProduct").text("Editar");
    $("#formProduct").trigger("reset");
    $("#changeCategory").html("");
    $("#loadingproduct").removeClass("d-none");
    let data = {
      idItem: $(this).data("iditem"),
    };
    $.ajax({
      type: "POST",
      url: urlEditItem,
      data: data,
      success: function (r) {
        $("input[name='idItem']").val(r.item.id);
        $("select[name='categoria']").val(r.item.categoriaName).change();
      },
    });
  });
  $("#addNewProduct").on("click", function () {
    $("button#addProduct").text("Agregar");
    $("#formProduct").trigger("reset");
    $("#changeCategory").html("");
    $("input[name='idItem']").val("");
  });
});
