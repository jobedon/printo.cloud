toastr.options.positionClass = 'toast-bottom-left';

function updateProgress(label, percentage, idDetalle, color) {
  let elemento = $('#item' + idDetalle + ' #' + label + ' .progress-bar')
  elemento.attr('aria-valuenow', percentage)
  elemento.css('width', percentage + '%')
  elemento.removeClass('bg-primary bg-danger bg-success')
  elemento.addClass('bg-' + color)
  let labelElemento = $(
    '#item' + idDetalle + ' #' + label + ' .progress-bar span',
  )
  labelElemento.text(percentage + '% subiendo')
  if (color == 'danger') {
    let tipo = label == 'progressElectrocorte' ? 'Electrocorte' : 'Impresion'
    labelElemento.text(percentage + '% ' + tipo + ' error')
  } else if (color == 'success') {
    let tipo = label == 'progressElectrocorte' ? 'Electrocorte' : 'Impresion'
    labelElemento.text(percentage + '% ' + tipo)
  }
}

function uploadFileWD(parentId, token, file, idDetalle, label) {
  let uniqueid = new Date().getUTCMilliseconds()

  $.ajax({
    url: 'https://upload.zoho.com/workdrive-api/v1/stream/upload',
    method: 'POST',
    timeout: 0,
    processData: false,
    headers: {
      'x-filename': file.name,
      'x-parent_id': parentId,
      'upload-id': uniqueid,
      'x-streammode': '1',
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Access-Control-Allow-Origin':
        'https://sistema.printo.cloud.dev:8000, https://upload.zoho.com',
    },
    xhr: function () {
      var xhr = new window.XMLHttpRequest()
      xhr.upload.addEventListener(
        'progress',
        function (evt) {
          if (evt.lengthComputable) {
            let percentComplete = (evt.loaded / evt.total) * 100
            updateProgress(
              label,
              Math.ceil(percentComplete),
              idDetalle,
              'primary',
            )
          }
        },
        false,
      )
      return xhr
    },
    data: file,
    success: function (result, status, xhr) {
      updateProgress(label, 100, idDetalle, 'success')

      let idFile = result.data[0].attributes.resource_id

      let dataUpdateFile = {
        idDetalle: idDetalle,
        idFile: idFile,
        tipo: label == 'progressElectrocorte' ? 'electro' : 'impresion',
      }

      $.ajax({
        type: 'POST',
        url: urlUpdateFile,
        data: dataUpdateFile,
        success: function (respUpdateFile) {
          if (respUpdateFile.error) {
            toastr.error(
              'Hubo un error en la subida de archivos, volver a intentar',
            )
          }
        },
      })
    },
    error: function (xhr, status, error) {
      toastr.error('Hubo un error en la subida de archivos, volver a intentar')
      updateProgress(label, 99, idDetalle, 'danger')
    },
  })
}

$(document).ready(function () {
  //editFile
  $('#productDetails').on('click', '.editFile', function () {
    $('#loadingproduct').removeClass('d-none')
    $('#panelEdicionArchivo').text('')
    let data = {
      idItem: $(this).data('iditem'),
    }
    $("input[name='idItem']").val(data.idItem)

    $.ajax({
      type: 'POST',
      url: urlEditItemFile,
      data: data,
      success: function (respEditItemFile) {
        if (respEditItemFile.error) {
        } else {
          $('#panelEdicionArchivo').html(respEditItemFile.html)
        }
        $('#loadingproduct').addClass('d-none')
      },
    })
  })

  $('body').on('change', "input[name='conservarImp']", function () {
    let conservar = $(this).is(':checked')
    if (conservar) {
      $('#conservarImpFile').addClass('d-none')
      $("input[name='archivoimpresion']").removeAttr('required')
    } else {
      $('#conservarImpFile').removeClass('d-none')
      $("input[name='archivoimpresion']").attr('required', true)
    }
  })

  $('body').on('change', "input[name='conservarElec']", function () {
    let conservar = $(this).is(':checked')
    if (conservar) {
      $('#conservarElecFile').addClass('d-none')
      $("input[name='archivoelectrocorte']").removeAttr('required')
    } else {
      $('#conservarElecFile').removeClass('d-none')
      $("input[name='archivoelectrocorte']").attr('required', true)
    }
  })

  $('body').on('change', 'input.fileProduct', function () {
    bsCustomFileInput.init()
    let filename = $(this).val().split('\\').pop()
    let extension = filename.split('.').pop().toUpperCase()
    let extAllowed = $(this).data('extallowed')
    let messageFile = 'Seleccione archivo ' + extAllowed

    if (!extAllowed.includes(extension)) {
      toastr.error('Extension no permitida, solo ' + extAllowed)
      $(this).val('')
      $(this).next('label').html(messageFile)
      return false
    }
  })

  $('#formProduct').on('submit', function () {
    $('#loadingproduct').removeClass('d-none')

    let conservaImpresion = $("input[name='conservarImp']").is(':checked')
    let conservaElectro = $("input[name='conservarElec']").is(':checked')

    let parentIdWdImpresion = $("input[name='parentIdWdImpresion']").val()
    let parentIdWdElectro = $("input[name='parentIdWdElectro']").val()

    let idDetalle = $("input[name='idItem']").val()

    // ajax para el token

    $.ajax({
      type: 'POST',
      url: urlPrepareUpload,
      data: {
        tipotoken: 'UPLOAD',
        idDetalle: idDetalle,
      },
      success: function (respPrepareUpload) {
        if (
          !conservaImpresion &&
          respPrepareUpload.parentIds.parentIdXeMi != null
        ) {
          let impreFile = document.getElementById('archivoimpresion').files[0]
          uploadFileWD(
            respPrepareUpload.parentIds.parentIdXeMi,
            respPrepareUpload.token,
            impreFile,
            idDetalle,
            'progressImpresion',
          )
        }
        if (
          !conservaElectro &&
          respPrepareUpload.parentIds.parentIdElec != null
        ) {
          let impreFile = document.getElementById('archivoelectrocorte')
            .files[0]
          uploadFileWD(
            respPrepareUpload.parentIds.parentIdElec,
            respPrepareUpload.token,
            impreFile,
            idDetalle,
            'progressElectrocorte',
          )
        }

        $('#loadingproduct').addClass('d-none')
        $('#modal-producto').modal('hide')
        $('#formProduct').trigger('reset')
        $('#panelEdicionArchivo').html('')
      },
    })

    return false
  })
})
