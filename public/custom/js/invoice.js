$(document).ready(function () {
	toastr.options.newestOnTop = false;
	toastr.options.positionClass = "toast-top-full-width";

	function getTotal() {
		let total = 0.0;
		let response = [];
		let detalle = [];
		$.each($("input.totalInvoice"), function (idx, item) {
			total = (parseFloat(total) + parseFloat(item.value)).toFixed(2);
			detalle.push({
				invoiceid: $(this).attr("data-invoiceid"),
				value: item.value,
			});
		});

		response = {
			total: total,
			detalle: detalle,
		};

		return response;
	}

	$("input.currency").inputmask({
		alias: "currency",
		prefix: "",
		groupSeparator: "",
	});

	$("input.totalInvoice").on("change", function () {
		let maxval = $(this).attr("data-max");

		if (this.value > parseFloat(maxval)) {
			this.value = maxval;
			toastr.error("Valor maximo de la factura $ " + maxval, "Error!");
		}

		total = getTotal().total;

		console.log(total);
		$("input#totalPagar").val(total);
	});

	$("input#totalPagar").on("change", function () {
		let totalPagar = $(this).val();
        $.each($("input.totalInvoice"), function (idx, item) {
			if (totalPagar >= 0) {
				let valMax = $(this).data('max');
				if (totalPagar < valMax) {
					this.value = totalPagar
				}else{
					this.value = valMax;	
				}
				totalPagar -= valMax;
			}else{
				this.value = 0;
			}
		});
	});

	$("#nextPayment").on("click", function (e) {
		e.preventDefault();
		$("#loading").addClass("show");
		$(".facturas").fadeOut(300);
		let total = getTotal();

		let data = {
			rubros: total,
			token: $('input[name="token"]').val(),
		};
		console.log($("#form-payment").attr("data-url"));
		$.ajax({
			type: "POST",
			url: $("#form-payment").attr("data-url"),
			data: data,
			success: function (response) {
				$("input#totalPagar").attr('disabled', 'disabled');
				console.log(response);
				$("div#tc").html(response.html);
				$(".original").removeClass("d-none");
				$("#loading").removeClass("show");
			},
			error: function (e) {
				$(".original").removeClass("d-none");
				$("#loading").removeClass("show");
			},
		});
	});
});
