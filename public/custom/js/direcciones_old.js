let map, infoWindow, marker, geoloccontrol, pos

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    streetViewControl: false,
    tilt: 0,
    center: { lat: -34.397, lng: 150.644 },
    zoom: 16,
  })

  defaultCurrentPosition()

  marker = new google.maps.Marker({
    position: pos,
    map,
    title: 'Printo Cloud!',
  })

  infoWindow = new google.maps.InfoWindow()

  const locationButton = document.createElement('a')
  locationButton.textContent = 'Mi Ubicación'
  locationButton.classList.add('custom-map-control-button')
  locationButton.classList.add('btn')
  locationButton.classList.add('btn-orange')
  locationButton.classList.add('text-white')
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton)

  locationButton.addEventListener('click', () => {
    defaultCurrentPosition()
  })

  // Try HTML5 geolocation.

  // map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
  // geoloccontrol = new klokantech.GeolocationControl(map, 16)

  map.addListener('click', function (mapsMouseEvent) {
    marker.setPosition(mapsMouseEvent.latLng)
    $("input[name='coords']").val(
      mapsMouseEvent.latLng.lat() + ', ' + mapsMouseEvent.latLng.lng(),
    )
  })
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos)
  infoWindow.setContent(
    browserHasGeolocation
      ? 'Error: The Geolocation service failed.'
      : "Error: Your browser doesn't support geolocation.",
  )
  infoWindow.open(map)
}

function defaultCurrentPosition() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        }
        map.setCenter(pos)
        marker.setPosition(pos)
        $("input[name='coords']").val(pos.lat + ', ' + pos.lng)
      },
      () => {
        handleLocationError(true, infoWindow, map.getCenter())
      },
    )
  } else {
    //Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter())
  }
}

$(document).ready(function () {
  $('.select2').select2()
  $('.select2bs4').select2({
    theme: 'bootstrap4',
  })
  $("input[name='coords']").bind('input', function () {
    let reg = /^(-?\d+(\.\d+)?),\s*(-?\d+(\.\d+)?)$/

    let data = $(this).val().trim()

    if (reg.test(data)) {
      //si es coordenada
      let arrayData = data.split(',')
      let latLng = new google.maps.LatLng(
        arrayData[0].trim(),
        arrayData[1].trim(),
      )
      marker.setPosition(latLng)
      map.setCenter(latLng)
    } else {
      //no es coordenada
      toastr.error('No tiene formato de coordenadas de google maps')
      $("input[name='coords']").val('')
    }
  })
  $('#dirform').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which
    if (keyCode === 13) {
      e.preventDefault()
      return false
    }
  })
  $('#crearDir').on('click', function (e) {
    $(this).attr('disabled', true)
    let dir = $('input[name="direccion"]').val()
    let cords = $('input[name="coords"]').val()
    if (dir.length == 0) {
      e.preventDefault()
      toastr.error('Por favor ingresar una dirección')
      $(this).removeAttr('disabled')
      return false
    }
    if (cords.length == 0) {
      e.preventDefault()
      toastr.error('Por favor ingresar las coordenadas, puede usar el mapa')
      $(this).removeAttr('disabled')
      return false
    }
    $('#dirform').submit()
  })
})
