$(document).ready(function () {
	let ciudad = $('select[name="Ciudad_Destino1"]').select2({
		theme: "bootstrap4",
		placeholder: "Seleccione una Ciudad",
	});

	$('select[name="Provincia_Destino"]').select2({
		theme: "bootstrap4",
		placeholder: "Selecccione una Provincia",
	});

	$('select[name="Provincia_Destino"]').on("change", () => {
		let data = {
			provincia: $('select[name="Provincia_Destino"]')
				.children("option:selected")
				.val(),
		};
		$.ajax({
			type: "POST",
			url: urlChangeProvincia,
			data: data,
			dataType: "json",
			success: function (r) {
				$('select[name="Ciudad_Destino1"]').html("");

				$('select[name="Ciudad_Destino1"]')
					.select2({
						theme: "bootstrap4",
						placeholder: "Seleccione una Ciudad",
						data: r.data,
					})
					.removeAttr("disabled");
			},
		});
	});

	$("#formRegistro").on("submit", function () {
		let data = {};
		$("#formRegistro input, #formRegistro select").each(function (
			idx,
			item
		) {
			data[$(item).attr("name")] = $(item).val();
		});

		$.ajax({
			type: "POST",
			url: $(this).attr("action"),
			data: data,
			dataType: "json",
			success: function (r) {
				if (r.error) {
				} else {
					location.reload();
				}
			},
		});

		return false;
	});

	// $("#formDireccion").on("submit", function () {
	// 	let data = {
	// 		nombres: $("input[name='nombres']").val(),
	// 		apellidos: $("input[name='apellidos']").val(),
	// 		razonsocial: $("input[name='razonsocial']").val(),
	// 		direccion1: $("input[name='direccion1']").val(),
	// 		telefono1: $("input[name='telefono1']").val(),
	// 		provincia: $("select[name='provincia']")
	// 			.children("option:selected")
	// 			.val(),
	// 		ciudad: $("select[name='ciudad']")
	// 			.children("option:selected")
	// 			.text(),
	// 		ciudadId: $("select[name='ciudad']")
	// 			.children("option:selected")
	// 			.val(),
	// 	};

	// 	$.ajax({
	// 		type: "POST",
	// 		url: $(this).attr("action"),
	// 		data: data,
	// 		dataType: "json",
	// 		success: function (r) {
	// 			if (r.error) {
	// 			} else {
	// 				location.reload();
	// 			}
	// 		},
	// 	});

	// 	return false;
	// });

	$("#registrosDetails").on("click", ".confirmdelete", function () {
		let url = $(this).attr("data-url");
		$("#confirmacionEliminacion").attr("data-url", url);
	});

	async function changeData(data) {
		for (let clave in data) {
			await new Promise((resolve) => {
				if ($(`[name='${clave}']`)[0].nodeName == "SELECT") {
					$.when().done(function () {});
					$(`[name='${clave}']`).val(data[clave]);
					$(`[name='${clave}']`).change();
					setTimeout(() => {
						resolve();
					}, 300);
				} else {
					$(`[name='${clave}']`).val(data[clave]);
					resolve();
				}
			});
		}
	}

	$("#registrosDetails").on("click", ".edit", function (event) {
		event.preventDefault();
		let data = $(this).data("info");
		changeData(data);
		return false;
	});
	$(".new").on("click", function (event) {
		event.preventDefault();
		$("#formRegistro").trigger("reset");
		$("#formRegistro input[type=hidden].resetInput").val("");
		return false;
	});
});
