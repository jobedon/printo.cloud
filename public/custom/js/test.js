$(function () {
  let filename,
    parentId = 'g3lp7732580f821e047a99b4379bcef4796c3'

  let uniqueid = new Date().getUTCMilliseconds()

  $('#sent').on('click', function () {
    var file = document.getElementById('fileupload').files[0]

    var formData = new FormData()
    formData.append('file', file)

    $.ajax({
      url: 'https://upload.zoho.com/workdrive-api/v1/stream/upload',
      method: 'POST',
      timeout: 0,
      processData: false, // tell jQuery not to process the data
      headers: {
        'x-filename': $('input[name="namefile"]').val(),
        'x-parent_id': parentId,
        'upload-id': uniqueid,
        'x-streammode': '1',
        Authorization: 'Bearer ' + $('input[name="token"]').val(),
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Access-Control-Allow-Origin': '*',
      },
      xhr: function () {
        var xhr = new window.XMLHttpRequest()
        xhr.upload.addEventListener(
          'progress',
          function (evt) {
            if (evt.lengthComputable) {
              var percentComplete = (evt.loaded / evt.total) * 100
            }
          },
          false,
        )
        return xhr
      },
      data: file,
      success: function (r) {
        console.log(r)
      },
    })
    return false
  })
})
