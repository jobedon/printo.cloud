$(function () {
    toastr.options.positionClass = 'toast-bottom-left';

    function actualizarInfoProductos(detalle) {
        let selector1 = "#cartSideBar div#item";
        let preciosubtotal = "";
        let desc = "";
        $.each(detalle, function (idx, item) {
            $(selector1 + item.id + " .nombreproducto").text(item.nombreproducto);
            $(selector1 + item.id + " .nombresubproducto").text(item.nombresubproducto);
            desc = "";
            desc = item.descripcion.replace(/\n/g, "<br />");
            $(selector1 + item.id + " span.descripcion").html(desc);
            $(selector1 + item.id + " span.cantidad").text(item.cantidad);
            $(selector1 + item.id + " span.preciounit").text(item.preciounit);

            preciosubtotal = "";
            preciosubtotal += "<span class='precio badge badge-info' >" + item.subtotal + "</span>";
            preciosubtotal += '<p class=" stripHr">Base : ' + item.preciobase + "<br>";
            preciosubtotal +=
                item.preciolaminado == 0
                    ? ""
                    : "Laminado : " + item.preciolaminado + "<br>";
            preciosubtotal +=
                item.preciocorte == 0 ? "" : "Corte : " + item.preciocorte + "<br>";
            preciosubtotal +=
                item.precioojales == 0 ? "" : "Ojales : " + item.precioojales + "<br>";
            preciosubtotal +=
                item.preciorigido == 0
                    ? ""
                    : "Corte Recto : " + item.preciorigido + "</p>";

            $(selector1 + item.id + " .preciosubtotal").html(preciosubtotal);
        });
    }

    function checkFilesUploads() {
        // > Logica inicial para revisar boton procesar
        let incomplete = false;
        let numItems = 0;
        $.each($(".progress-bar"), function (idx, item) {
            if ($(item).attr("aria-valuenow") != "100") {
                incomplete = true;
            }
            numItems++;
        });
        if (!incomplete && numItems != 0) {
            $(".btnCrearOrden").removeClass("disabled");
        }
        // > Logica final para revisar boton procesar
    }

    function updateProgress(label, percentage, idDetalle, color) {
        let elemento = $("#item" + idDetalle + " #" + label + " .progress-bar");
        elemento.attr("aria-valuenow", percentage);
        elemento.css("width", percentage + "%");
        elemento.removeClass("bg-primary bg-danger bg-success");
        elemento.addClass("bg-" + color);
        let labelElemento = $(
            "#item" + idDetalle + " #" + label + " .progress-bar span"
        );
        labelElemento.text(percentage + "% subiendo");

        let tipo = label == "progressElectrocorte" ? "Electrocorte" : "Impresion";
        tipo = label == "progressImgRef" ? "Imagen" : tipo;

        if (color == "danger") {
            labelElemento.text(percentage + "% " + tipo + " error");
        } else if (color == "success") {
            labelElemento.text(percentage + "% " + tipo);
        }
    }

    function uploadFileWD(parentId, token, file, respAddProduct, label) {
        let uniqueid = new Date().getUTCMilliseconds();

        $.ajax({
            url: "https://upload.zoho.com/workdrive-api/v1/stream/upload",
            method: "POST",
            timeout: 0,
            processData: false,
            headers: {
                "x-filename": file.name,
                "x-parent_id": parentId,
                "upload-id": uniqueid,
                "x-streammode": "1",
                Authorization: "Bearer " + token,
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                "Access-Control-Allow-Origin":
                    "https://sistema.printo.cloud.dev:8000, https://upload.zoho.com",
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener(
                    "progress",
                    function (evt) {
                        if (evt.lengthComputable) {
                            let percentComplete = (evt.loaded / evt.total) * 100;
                            updateProgress(
                                label,
                                Math.ceil(percentComplete),
                                respAddProduct.idDetalle,
                                "primary"
                            );
                        }
                    },
                    false
                );
                return xhr;
            },
            data: file,
            success: function (result, status, xhr) {
                updateProgress(label, 100, respAddProduct.idDetalle, "success");
                checkFilesUploads();

                let idFile = result.data[0].attributes.resource_id;

                let dataUpdateFile = {
                    idDetalle: respAddProduct.idDetalle,
                    idFile: idFile,
                    tipo: label == "progressElectrocorte" ? "electro" : "impresion",
                };

                $.ajax({
                    type: "POST",
                    url: urlUpdateFile,
                    data: dataUpdateFile,
                    success: function (respUpdateFile) {
                        if (respUpdateFile.error) {
                            toastr.error(
                                "Hubo un error en la subida de archivos, volver a intentar"
                            );
                        }
                    },
                });
            },
            error: function (xhr, status, error) {
                toastr.error("Hubo un error en la subida de archivos, volver a intentar");
                updateProgress(label, 99, respAddProduct.idDetalle, "danger");
            },
        });
    }

    function openSideBarCart() {
        if ($("#cartSideBar").css('display') == 'none') {
            $("#btnCart").click();
        }
    }

    function resetForm(title) {
        $("button#addProduct").text(title);
        $("#formProduct").trigger("reset");
        $("#changeCategory").html("");
        $("input[name='idItem']").val("");
        $("#panelProductoModal").html("");
        $("#loadingproduct").removeClass("d-none");
    }

    function subirImagenProducto(isEdit, idDetalle) {

        $("#item" + idDetalle + " #progressImgRef").removeClass("d-none");

        let imgRefFile = document.getElementById("imgRef").files[0];
        let label = "progressImgRef";

        var inputFile = document.getElementById('imgRef');
        var file = inputFile.files[0];
        var data = new FormData();
        data.append('file', file, file.name);
        data.append('idDetalle', idDetalle);
        data.append('isEdit', isEdit);

        $.ajax({
            url: urlUploadImageProducto,
            type: "POST",
            data: data,
            timeout: 0,
            processData: false,
            contentType: false,
            cache: false,
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener(
                    "progress",
                    function (evt) {
                        if (evt.lengthComputable) {
                            let percentComplete = (evt.loaded / evt.total) * 100;
                            updateProgress(
                                label,
                                Math.ceil(percentComplete),
                                idDetalle,
                                "primary"
                            );
                        }
                    },
                    false
                );
                return xhr;
            },
            success: function (result) {
                updateProgress(label, 100, idDetalle, "success");
                $("#item" + idDetalle + " #progressImgRef").remove();
                $("#item" + idDetalle + " img").attr("src", result.imgPathRef);
            }
        });
    }


    checkFilesUploads();
    if (tarifas.totalPagar) {
        $("span.totalPagarPreview").text(tarifas.totalPagar);
    }


    $(".itemCatalogo").on("click", function () {
        esEdit = false;

        resetForm("Agregar");

        let title = $(this).data('title');
        $("#modal-producto .modal-title").text(title);

        let idProducto = $(this).data("id");

        let data = {
            idProducto,
        };

        $.ajax({
            type: "POST",
            url: urlObtenerInfoProducto,
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.error) {
                } else {
                    $("#panelProductoModal").html(r.html);
                    $(".select2bs4").select2({ theme: "bootstrap4" });
                }
                $("#loadingproduct").addClass("d-none");
            },
        });
    });



    $("#modal-producto").on("change", "select[name='producto']", () => {
        $("#loadingproduct").removeClass("d-none");
        let url = urlCambiarMaterial;
        let data = {
            productoId: $("select[name='producto']").val(),
            idItem: $("input[name='idItem']").val(),
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response) {
                $("#favorito-swtich").html(response.favoriteHtml);
                $("#flujo").html(response.flujoHtml);

                $("#loadingproduct").addClass("d-none");
            },
        });
    });

    $("#cartSideBar").on("click", ".edit", function () {

        resetForm("Editar");

        $("#loadingproduct").removeClass("d-none");

        let title = $(this).data('title');
        $("#modal-producto .modal-title").text(title);

        let idProducto = $(this).data("id");
        let idItem = $(this).data("iditem");

        let data = {
            idProducto,
            idItem
        };

        $.ajax({
            type: "POST",
            url: urlObtenerInfoProducto,
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.error) {
                } else {
                    $("#panelProductoModal").html(r.html);
                    $(".select2bs4").select2({ theme: "bootstrap4" });
                }
            },
        });


    });



    $("#formProduct").on("submit", function (e) {
        e.preventDefault();

        $("#loadingproduct").removeClass("d-none");
        $(".btnCrearOrden").addClass("disabled");

        $("button#addProduct").text("Agregar");

        var data = $(this).serialize();

        let url = $(this).attr("action");
        let method = $(this).attr("method");

        let idItem = $("input[name='idItem']").val();
        let isEdit = idItem.length != 0;

        data += "&idItem=" + idItem;

        $.ajax({
            type: method,
            url: url,
            data: data,
            success: function (respAddProduct) {
                if (respAddProduct.error) {
                    toastr.error(respAddProduct.msg);
                    $("#loadingproduct").addClass("d-none");
                } else {

                    if (respAddProduct.warning) {
                        toastr.warning(respAddProduct.msgW);
                    }

                    toastr.success(respAddProduct.msg);

                    actualizarInfoProductos(respAddProduct.detalle);

                    let datos = {
                        idDetalle: respAddProduct.idDetalle,
                    };

                    let conservar = $("input[name='conservar']:checked").val();
                    let isConservar = conservar == "on";

                    let errorSubida = $("input[name='errorSubida']").val();
                    let hasErrorSubida = errorSubida == "1";

                    $("span.totalPagarPreview").text(respAddProduct.tarifas.totalPagar);

                    if (esProforma) {
                        openSideBarCart();
                        if (respAddProduct.action == "edititem") {
                            $("#cartSideBar div#item" + respAddProduct.idDetalle).remove();
                        }
                        $("#cartSideBar #contentItems").prepend(respAddProduct.html);
                        $("#modal-producto").modal("hide");
                        $("#formProduct").trigger("reset");
                        $("#changeCategory").html("");
                        $("#itemEmpty").remove();


                    } else {
                        if (
                            !isEdit ||
                            (isEdit && hasErrorSubida) ||
                            (isEdit && !isConservar)
                        ) {
                            $.ajax({
                                type: "POST",
                                url: urlPrepareUpload,
                                data: {
                                    tipotoken: "UPLOAD",
                                    idDetalle: respAddProduct.idDetalle,
                                    isEdit: isEdit,
                                },
                                success: function (respPrepareUpload) {
                                    if (!respPrepareUpload.error) {
                                        openSideBarCart();

                                        if (
                                            typeof respPrepareUpload.parentIds.parentIdElec !==
                                            "undefined" &&
                                            respPrepareUpload.parentIds.parentIdElec.length > 0
                                        ) {
                                            let electFile = document.getElementById(
                                                "archivoelectrocorte"
                                            ).files[0];
                                            uploadFileWD(
                                                respPrepareUpload.parentIds.parentIdElec,
                                                respPrepareUpload.token,
                                                electFile,
                                                respAddProduct,
                                                "progressElectrocorte"
                                            );
                                        }
                                        if (
                                            typeof respPrepareUpload.parentIds.parentIdXeMi !==
                                            "undefined" &&
                                            respPrepareUpload.parentIds.parentIdXeMi.length > 0
                                        ) {
                                            let impreFile =
                                                document.getElementById("archivoimpresion").files[0];
                                            uploadFileWD(
                                                respPrepareUpload.parentIds.parentIdXeMi,
                                                respPrepareUpload.token,
                                                impreFile,
                                                respAddProduct,
                                                "progressImpresion"
                                            );
                                        }

                                        if (respAddProduct.action == "edititem") {
                                            $("#cartSideBar div#item" + respAddProduct.idDetalle).remove();
                                        }

                                        $("#cartSideBar #contentItems").prepend(respAddProduct.html);

                                        // Subir imagen
                                        if (document.getElementById('imgRef').files.length != 0) {
                                            subirImagenProducto(isEdit, respAddProduct.idDetalle);
                                        } else {
                                            $("#item" + respAddProduct.idDetalle + " #progressImgRef").remove();
                                        }

                                        $("#modal-producto").modal("hide");
                                        $("#formProduct").trigger("reset");
                                        $("#changeCategory").html("");
                                        $("#itemEmpty").remove();
                                    } else {
                                        toastr.error(respPrepareUpload.mensaje);
                                    }
                                    $("#loadingproduct").addClass("d-none");
                                },
                            });
                        } else {

                            // Subir imagen
                            if (document.getElementById('imgRef').files.length != 0) {
                                subirImagenProducto(isEdit, respAddProduct.idDetalle);
                            } else {
                                $("#item" + respAddProduct.idDetalle + " #progressImgRef").remove();
                            }

                            $("#loadingproduct").addClass("d-none");
                            checkFilesUploads();
                            $("#modal-producto").modal("hide");
                            $("#formProduct").trigger("reset");
                            $("#changeCategory").html("");
                            $("#itemEmpty").remove();
                        }
                    }


                }
            },
        });

        return false;
    });

    $('#cartSideBar').on('click', '.confirmdelete', function () {
        $('input[name="idItemDelete"]').val($(this).data('iditem'));
    });


    $('#confirmacionEliminacion').on('click', function () {
        $("#loadingConfirmacion").removeClass("d-none");
        let url = urlDeleteProducto;
        let data = {
            idItem: $('input[name="idItemDelete"]').val()
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response) {
                if (response.error) {
                    $("#loadingConfirmacion").addClass("d-none");
                    toastr.error(response.mensaje)
                } else {
                    toastr.success(response.mensaje)
                    setTimeout(function () {
                        //location.href = response.urlRedirect;
                        location.reload();
                    }, 1500);

                }
            },
        });
    });

    var cssSideBar;

    $(".expandCart").on("click", function (e) {

        $("#bodyContent").addClass('d-none');

        cssSideBar = $("#cartSideBar").attr('style');
        $("#cartSideBar").attr('style', '');

        $("#cartSideBar").addClass('expand');
        $("#cartSideBar").removeClass('control-sidebar');

        let itemDetails = $(".itemDetail");
        $.each(itemDetails, function (idx, ele) {
            let classcontract = $(ele).data('classcontract');
            let classexpand = $(ele).data('classexpand');
            $(ele).removeClass(classcontract);
            $(ele).addClass(classexpand);
        });


        $(".itemAccionAside").addClass('d-none ');
        $(".itemAccion").removeClass('d-none');

        $(this).addClass('d-none');
        $(".contractCart").removeClass('d-none');

        //e.preventDefault();
    });

    $(".contractCart").on("click", function (e) {
        e.preventDefault();

        $("#bodyContent").removeClass('d-none');
        $("#cartSideBar").attr('style', cssSideBar);
        $("#cartSideBar").removeClass('expand');
        $("#cartSideBar").addClass('control-sidebar');

        let itemDetails = $(".itemDetail");
        $.each(itemDetails, function (idx, ele) {
            let classcontract = $(ele).data('classcontract');
            let classexpand = $(ele).data('classexpand');
            $(ele).removeClass(classexpand);
            $(ele).addClass(classcontract);
        });


        $(".itemAccionAside").removeClass('d-none ');
        $(".itemAccion").addClass('d-none');

        $(this).addClass('d-none');
        $(".expandCart").removeClass('d-none');


    });

    $("#btnCart").on("click", function () {
        $(".contractCart").click();
    });

});
