$(document).ready(function () {
  'use strict'
  if (typeof i18next !== 'undefined') {
    i18next.init(
      {
        lng: 'es',
        resources: {
          es: {
            translation: {
              wordLength: 'Tu contraseña es demasiado corta',
              wordNotEmail: 'No uses tu email como tu contraseña',
              wordSimilarToUsername:
                'Tu contraseña no puede contener tu nombre de usuario',
              wordTwoCharacterClasses: 'Mezcla diferentes clases de caracteres',
              wordRepetitions: 'Demasiadas repeticiones',
              wordSequences: 'Tu contraseña contiene secuencias',
              errorList: 'Errores:',
              veryWeak: 'Muy Débil',
              weak: 'Débil',
              normal: 'Intermedia',
              medium: 'Media',
              strong: 'Fuerte',
              veryStrong: 'Muy Fuerte',
              start: 'Comience a escribir la contraseña',
              label: 'Contraseña',
              pageTitle:
                'Bootstrap 4 Password Strength Meter - Ejemplo de Traducciones',
              goBack: 'Atrás',
            },
          },
        },
      },
      function () {
        var options = {}
        options.ui = {
          container: '#pwd-container',
          showVerdictsInsideProgressBar: true,
          viewports: {
            progress: '.pwstrength_viewport_progress',
          },
          progressBarExtraCssClasses:
            'progress-bar-striped progress-bar-animated',
        }
        options.common = {
          debug: true,
          onLoad: function () {
            $('#messages').text('Start typing password')
          },
        }
        $('input[name="new_password"]').pwstrength(options)
      },
    )
  }

  $('.toggle-pwd .toggle-eye').on('click', function () {
    let input = $(this).parent().closest('.toggle-pwd').find('input')
    const type = $(input).attr('type') === 'password' ? 'text' : 'password'
    $(input).attr('type', type)
    if (type === 'password') {
      $(this).removeClass('fa-eye')
      $(this).addClass('fa-eye-slash')
    } else {
      $(this).addClass('fa-eye')
      $(this).removeClass('fa-eye-slash')
    }
  })
})
