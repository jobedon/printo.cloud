$(document).ready(function () {
  toastr.options.positionClass = 'toast-bottom-left';
  $(".select2").select2();
  $(".select2bs4").select2({
    theme: "bootstrap4",
  });

  function getTarifasPorMetodo() {
    $("#loadingTarifas").removeClass("d-none");

    let metodo = $("input[name='metodoEnvio']:checked").val();
    let direccionId = $("select[name='direccion'] option:selected").val();

    let data = {
      metodo,
      direccionId,
    };

    $.ajax({
      type: "POST",
      url: urlCalcularMetodo,
      data: data,
      dataType: "json",
      success: function (r) {
        if (r.error) {
          toastr.error(r.msg);
        } else {
          $("#panelTarifas").html(r.html);
        }
        $("#loadingTarifas").addClass("d-none");
      },
    });




  }

  $("input[name='requiereenvio']").on("change", function () {
    $("#loadingPagos").removeClass("d-none");

    let requiereenvio = $("input[name='requiereenvio']:checked").val();
    if (requiereenvio == "si") {
      $("#panelMetodoEnvio").removeClass("d-none");
    } else {
      $("#panelMetodoEnvio").addClass("d-none");
    }

    let data = {
      requiereenvio: requiereenvio == "si"
    };

    $.ajax({
      type: "POST",
      url: urlCambioRequiereEnvio,
      data: data,
      dataType: "json",
      success: function (r) {
        if (r.error) {
          toastr.error(res.message);
        } else {
          if (requiereenvio == "si") {
            $("#panelMetodoEnvio").html(r.html);
          } else {
            $("#panelTarifas").html(r.html);
          }

        }
        $("#loadingPagos").addClass("d-none");
      },
    });
  });

  $("#panelMetodoEnvio").on("change", "select[name='direccion']", function () {
    let nombre = $("select[name='direccion'] option:selected").attr(
      "data-dest"
    );
    let ciud = $("select[name='direccion'] option:selected").attr("data-ciu");
    let prov = $("select[name='direccion'] option:selected").attr("data-prov");
    let dir = $("select[name='direccion'] option:selected").attr("data-dir");

    $("#metodosEnvio")
      .find("label")
      .removeClass("active")
      .end()
      .find('[type="radio"]')
      .prop("checked", false);

    if (
      ciud != "GUAYAQUIL" &&
      ciud != "DURAN" &&
      ciud != "ELOY ALFARO - DURAN"
    ) {
      $("input#metodoServientrega").parent().removeClass("disabled");
    } else {
      $("input#metodoPicker, input#metodoServientrega")
        .parent()
        .removeClass("disabled");
    }

    getTarifasPorMetodo();
  });

  $("#panelMetodoEnvio").on("change", "input[name='metodoEnvio']", function () {
    getTarifasPorMetodo();
  });


});
