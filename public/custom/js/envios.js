$(document).ready(function () {
    toastr.options.positionClass = 'toast-bottom-left';
    
    function getTarifasPorMetodo()
    {
        $("#loadingPagos").removeClass("d-none");

        let metodo = $("input[name='metodoEnvio']:checked").val();
        let direccionId = $("select[name='direccion'] option:selected").val();
        let contactoId = $("select[name='contacto'] option:selected").val();

        let data = {
            metodo,
            direccionId,
            contactoId
        };

        $.ajax({
            type: 'POST',
            url: urlCalcularMetodo,
            data: data,
            dataType: "json",
            success: function (r) {
                if (r.error) {
                    toastr.error(r.msg);
                }else{
                    $("#panelTarifas").html(r.html);
                }
                $("#loadingPagos").addClass("d-none");
            },
        });
    }

    if (jQuery().select2) {

        $('select[name="contacto"]').select2({
            theme: 'bootstrap4',
            placeholder: "Seleccciones un Contacto",
        });

        $('select[name="direccion"]').select2({
            theme: 'bootstrap4',
            placeholder: "Seleccciones una Dirección",
        });
    }

    $("#panelEnvios").on("change", "input[name='ticketsSelected']", () => {
        let numPaquetes = $("input[name='ticketsSelected']:checked").length;
        if (numPaquetes == 0) {
            $("#btnCrearEnvio").addClass("disabled");
        } else {
            $("#btnCrearEnvio").removeClass("disabled");
        }
    });

    function validaDatos(ordenes, ordenesSeleccionadas)
    {
        for (const ordenNumber in ordenes)
        {
            if (ordenesSeleccionadas[ordenNumber] != ordenes[ordenNumber]) {
                return 'N';
            }
        }
        return 'S';
    }

    $("#panelEnvios").on("click", "#btnCrearEnvio", function(event) {
        
        let tickets = [];
        let orden, cant;

        let ordenes = {};
        let ordenesSeleccionadas = {};
        $("input[name='ticketsSelected']:checked").each((idx, item) => {
            tickets.push(item.value);
            orden = $(item).data('orden');
            cant  = $(item).data('cant');
            console.log(orden, cant);
            ordenes[orden]  = cant;
            ordenesSeleccionadas[orden] = (ordenesSeleccionadas[orden]||0) + 1;
        });
        console.log(ordenes);
        console.log(ordenesSeleccionadas);

        let validaOrdenesCompletas = validaDatos(ordenes, ordenesSeleccionadas);


        let data = {
            tickets,
            validaOrdenesCompletas
        };

        console.log(data);

        $.ajax({
            type: 'POST',
            url: urlPrepararEnvio,
            data: data,
            dataType: "json",
            success: function (r) {
                location.href = r.urlRedirect;
            },
        });

    });

    $("#panelEnvios").on("change","input[name='politicas']", function() {
        if (this.checked) {
            $(".realizarTrx").removeClass("disabled").removeAttr("disabled");
        }else{
            $(".realizarTrx").addClass("disabled").attr("disabled", true);
        }
    });


    $("input[name='metodoEnvio']").on("change", function(){
        getTarifasPorMetodo();
    });

    $("select[name='contacto']").on("change", function(){
        let jsonContactos = JSON.parse(contactos);
        let idContacto = $(this).val();
        let direcciones = jsonContactos[idContacto]['direcciones'];
        console.log(direcciones);

        let data = [];

        Object.entries(direcciones).forEach(function(item){
            data.push({
                id : item[1].id,
                text : item[1].Name
            });
        });

        console.log(data);

        $('select[name="direccion"]').empty();
        $('select[name="direccion"]').select2({
            theme: 'bootstrap4',
            data: data,
        }).removeAttr('disabled');

    });

    $("select[name='direccion']").on("change", function(){

        let jsonContactos = JSON.parse(contactos);
        let idContacto = $("select[name='contacto'] option:selected").val();
        let idDireccion = $(this).val();

        let contacto = jsonContactos[idContacto];
        let direccion = jsonContactos[idContacto]['direcciones'][idDireccion];

        let nombre = contacto.Name;
        let ciud = direccion.Ciudad_Destino1;
        let prov = direccion.Provincia_Destino;
        let dir = direccion.Name;

        $("input[name='dest']").val(nombre);
        $("input[name='ciuprov']").val(ciud + ", " + prov);
        $("input[name='dir']").val(dir);

        $('#metodosEnvio').find('label')
            .removeClass('active')
            .addClass('disabled')
            .end().find('[type="radio"]')
            .prop('checked', false);

        if (ciud != 'GUAYAQUIL' && ciud != 'DURAN' && ciud != 'ELOY ALFARO - DURAN') {
            $("input#metodoServientrega").parent().removeClass('disabled');
        } else {
            $("input#metodoPicker, input#metodoServientrega").parent().removeClass('disabled');
        }

        getTarifasPorMetodo();

    });

    $("#panelEnvios").on("click", "#realizarPago", function(){

        $("#loadingṔagos").removeClass("d-none");

        let metodo = $("input[name='metodoEnvio']:checked").val();
        let direccionId = $("select[name='direccion'] option:selected").val();
        let contactoId = $("select[name='contacto'] option:selected").val();

        let data = {
            metodo,
            contactoId,
            direccionId
        };

        $.ajax({
            type: "POST",
            url: urlPrepararPago,
            data: data,
            success: function (res) {

                if (res.error) {
                    toastr.error(res.message)
                    // setTimeout(function () {
                    //     location.href = res.urlRedirect
                    // }, 3000)
                }else{
                    $("#panelFormaPago").html(res.html)
                }
                
                $("#loadingṔagos").addClass("d-none");
            },
        });


    });


});
