<?php

namespace App\Tests;

use App\Entity\DatafastTrx;
use App\Entity\UserPi;
use App\Services\EnviosService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class EnviosTest extends KernelTestCase
{

    public function testPaquetes()
    {
        self::bootKernel();
        //$container = self::$kernel->getContainer();

        $container = self::$container;

        $em = $container->get(EntityManagerInterface::class);
        $es = $container->get(EnviosService::class);

        $user = $em->getRepository(UserPi::class)->findOneBy([
            'ruc' => '0927355909001'
        ]);

        $dfTrx = $em->getRepository(DatafastTrx::class)->findOneBy([
            'transactionId' => 'ff886a7116583f96be2729b6a95b8315'
        ]);

        $es->empacarTickets([
            'tickets' => [11556, 11557],
            'dfTrx' => $dfTrx,
            'user'  => $user
        ]);
    }
}
