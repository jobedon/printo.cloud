<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113052158 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE caracteristica (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(50) NOT NULL, descripcion VARCHAR(100) DEFAULT NULL, estado VARCHAR(3) DEFAULT \'A\' NOT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, fecha_actualizacion DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE producto_caracteristica (id INT AUTO_INCREMENT NOT NULL, producto_id INT DEFAULT NULL, caracteristica_id INT DEFAULT NULL, valor VARCHAR(100) NOT NULL, estado VARCHAR(3) DEFAULT \'A\' NOT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, fecha_actualizacion DATETIME DEFAULT NULL, INDEX IDX_8677B3B87645698E (producto_id), INDEX IDX_8677B3B8A7300D78 (caracteristica_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE producto_caracteristica ADD CONSTRAINT FK_8677B3B87645698E FOREIGN KEY (producto_id) REFERENCES producto (id)');
        $this->addSql('ALTER TABLE producto_caracteristica ADD CONSTRAINT FK_8677B3B8A7300D78 FOREIGN KEY (caracteristica_id) REFERENCES caracteristica (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE producto_caracteristica DROP FOREIGN KEY FK_8677B3B8A7300D78');
        $this->addSql('DROP TABLE caracteristica');
        $this->addSql('DROP TABLE producto_caracteristica');
    }
}
