<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113040150 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE producto (id INT AUTO_INCREMENT NOT NULL, producto_id INT DEFAULT NULL, categoria_producto_id INT DEFAULT NULL, nombre_producto VARCHAR(150) DEFAULT NULL, descripcion VARCHAR(255) DEFAULT NULL, sku VARCHAR(50) DEFAULT NULL, estado VARCHAR(3) DEFAULT \'A\' NOT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, fecha_actualizacion DATETIME DEFAULT NULL, INDEX IDX_A7BB06157645698E (producto_id), INDEX IDX_A7BB061569022511 (categoria_producto_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE producto ADD CONSTRAINT FK_A7BB06157645698E FOREIGN KEY (producto_id) REFERENCES producto (id)');
        $this->addSql('ALTER TABLE producto ADD CONSTRAINT FK_A7BB061569022511 FOREIGN KEY (categoria_producto_id) REFERENCES categoria_producto (id)');
        $this->addSql('ALTER TABLE orden_cab DROP parent_id_wd_tickets');
        $this->addSql('ALTER TABLE orden_det CHANGE descripcion descripcion VARCHAR(600) DEFAULT NULL');
        $this->addSql('ALTER TABLE tarifario_mimaki CHANGE precio_recto_metro2 precio_recto_metro2 DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE producto DROP FOREIGN KEY FK_A7BB06157645698E');
        $this->addSql('DROP TABLE producto');
        $this->addSql('ALTER TABLE orden_cab ADD parent_id_wd_tickets VARCHAR(100) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE orden_det CHANGE descripcion descripcion VARCHAR(300) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE tarifario_mimaki CHANGE precio_recto_metro2 precio_recto_metro2 DOUBLE PRECISION DEFAULT \'0\'');
    }
}
