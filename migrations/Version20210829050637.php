<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210829050637 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE files_orden_cab (id INT AUTO_INCREMENT NOT NULL, parent_id_xerox_ws VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta Xerox en mesa de trabajo\', parent_id_mimaki_ws VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta Mimaki en mesa de trabajo\', parent_id_electro_ws VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta Electrocorte en mesa de trabajo\', parent_id_xerox_tk VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta Xerox en carpeta Tickets\', parent_id_mimaki_tk VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta Mimaki en carpeta Tickets\', parent_id_electro_tk VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta Electrocorte en carpeta Tickets\', parent_id_tk VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta principal en carpeta Tickets\', description VARCHAR(100) DEFAULT NULL, status VARCHAR(3) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orden_cab ADD files_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orden_cab ADD CONSTRAINT FK_8CC1C31AA3E65B2F FOREIGN KEY (files_id) REFERENCES files_orden_cab (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CC1C31AA3E65B2F ON orden_cab (files_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orden_cab DROP FOREIGN KEY FK_8CC1C31AA3E65B2F');
        $this->addSql('DROP TABLE files_orden_cab');
        $this->addSql('DROP INDEX UNIQ_8CC1C31AA3E65B2F ON orden_cab');
        $this->addSql('ALTER TABLE orden_cab DROP files_id');
    }
}
