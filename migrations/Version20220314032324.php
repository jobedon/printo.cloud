<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220314032324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE catalogo (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) DEFAULT NULL, descripcion VARCHAR(100) DEFAULT NULL, estado VARCHAR(3) NOT NULL, fecha_creacion DATETIME NOT NULL, fecha_actualizacion DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE producto ADD catalogo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE producto ADD CONSTRAINT FK_A7BB06154979D753 FOREIGN KEY (catalogo_id) REFERENCES catalogo (id)');
        $this->addSql('CREATE INDEX IDX_A7BB06154979D753 ON producto (catalogo_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE producto DROP FOREIGN KEY FK_A7BB06154979D753');
        $this->addSql('DROP TABLE catalogo');
        $this->addSql('DROP INDEX IDX_A7BB06154979D753 ON producto');
        $this->addSql('ALTER TABLE producto DROP catalogo_id');
    }
}
