<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210829052937 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orden_det ADD files_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orden_det ADD CONSTRAINT FK_1936A5CAA3E65B2F FOREIGN KEY (files_id) REFERENCES files_orden_det (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1936A5CAA3E65B2F ON orden_det (files_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orden_det DROP FOREIGN KEY FK_1936A5CAA3E65B2F');
        $this->addSql('DROP INDEX UNIQ_1936A5CAA3E65B2F ON orden_det');
        $this->addSql('ALTER TABLE orden_det DROP files_id');
    }
}
