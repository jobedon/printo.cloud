INSERT INTO `caracteristica` (`id`, `nombre`, `descripcion`, `estado`, `fecha_creacion`, `fecha_actualizacion`, `val1`, `val2`) VALUES 
(NULL, 'peso', 'Total de peso por X cantidad de hojas para Xerox', 'A', CURRENT_TIMESTAMP, NULL, 100, 'XEROX'), 
(NULL, 'peso', 'Total de peso por X m2 para Mimaki', 'A', CURRENT_TIMESTAMP, NULL, 100, 'MIMAKI');


INSERT INTO `producto_caracteristica` 
(`id`, `producto_id`, `caracteristica_id`, `valor`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES 
(NULL, '4', '7', '0.1', 'A', CURRENT_TIMESTAMP, NULL), 
(NULL, '40', '8', '0.01', 'A', CURRENT_TIMESTAMP, NULL);


INSERT INTO `configuraciones` 
(`id`, `codigo`, `mapping_name`, `valorDouble`, `valorString`, `descripcion`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES 
(NULL, 'ARRANQUE_SERVIENTREGA_KG', NULL, '2', NULL, NULL, 'A', CURRENT_TIMESTAMP, NULL);


INSERT INTO `catalogo` 
(`id`, `nombre`, `descripcion`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(NULL, 'General', NULL, 'A', CURRENT_TIMESTAMP, NULL);

UPDATE `producto` SET `catalogo_id` = '1' WHERE `producto`.`id` in (1,2);


UPDATE plan SET status = 'I';

INSERT INTO plan
(zoho_plan_code, name, status, created_at, printo_plan_code, priority) VALUES
('PRO-01','Profesional Mensual','A',CURRENT_TIMESTAMP,'PRO-01',10),
('PRO-03','Profesional Trimestral','A',CURRENT_TIMESTAMP,'PRO-03',20),
('PRO-06','Profesional Semestral','A',CURRENT_TIMESTAMP,'PRO-06',30),
('PRO-12','Profesional Anual','A',CURRENT_TIMESTAMP,'PRO-12',40);


INSERT INTO `caracteristica` 
(`id`, `nombre`, `descripcion`, `estado`, `fecha_creacion`, `fecha_actualizacion`, `val1`, `val2`, `val3`) VALUES 
(NULL, 'imagen_orden_det', 'Imagen asociada al item de al Orden', 'A', CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);


UPDATE `producto` SET `img_path` = '/uploads/productos/formato_super_a3.jpeg' WHERE `producto`.`id` = 1;
UPDATE `producto` SET `img_path` = '/uploads/productos/gigantografias.jpeg' WHERE `producto`.`id` = 2;
