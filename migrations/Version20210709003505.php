<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210709003505 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE plan (id INT AUTO_INCREMENT NOT NULL, zoho_plan_code VARCHAR(50) DEFAULT NULL, name VARCHAR(100) DEFAULT NULL, description VARCHAR(150) DEFAULT NULL, status VARCHAR(3) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE categoria_producto CHANGE tipo_producto_id tipo_producto_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orden_cab CHANGE user_id user_id INT DEFAULT NULL, CHANGE total_items total_items DOUBLE PRECISION DEFAULT NULL, CHANGE total_envio total_envio DOUBLE PRECISION DEFAULT NULL, CHANGE subtotal subtotal DOUBLE PRECISION DEFAULT NULL, CHANGE total_iva total_iva DOUBLE PRECISION DEFAULT NULL, CHANGE total_pagar total_pagar DOUBLE PRECISION DEFAULT NULL, CHANGE requiere_envio requiere_envio VARCHAR(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE orden_det CHANGE orden_cab_id orden_cab_id INT DEFAULT NULL, CHANGE subcategoria_producto_id subcategoria_producto_id INT DEFAULT NULL, CHANGE sku sku VARCHAR(15) NOT NULL, CHANGE precio_base precio_base DOUBLE PRECISION DEFAULT NULL, CHANGE precio_corte precio_corte DOUBLE PRECISION DEFAULT NULL, CHANGE precio_laminado precio_laminado DOUBLE PRECISION DEFAULT NULL, CHANGE precio_ojales precio_ojales DOUBLE PRECISION DEFAULT NULL, CHANGE corte_rigido corte_rigido VARCHAR(1) DEFAULT \'N\' NOT NULL, CHANGE precio_corte_rigido precio_corte_rigido DOUBLE PRECISION DEFAULT NULL, CHANGE parent_id_wd_impresion parent_id_wd_impresion VARCHAR(150) DEFAULT NULL, CHANGE parent_id_wd_electro parent_id_wd_electro VARCHAR(150) DEFAULT NULL, CHANGE file_id_wd_impresion file_id_wd_impresion VARCHAR(150) DEFAULT NULL, CHANGE file_id_wd_electro file_id_wd_electro VARCHAR(150) DEFAULT NULL');
        $this->addSql('ALTER TABLE producto_favorito CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subcategoria_producto CHANGE categoria_producto_id categoria_producto_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tarifario_electrocorte_xerox CHANGE cantidad_min cantidad_min INT NOT NULL, CHANGE cantidad_max cantidad_max INT NOT NULL');
        $this->addSql('ALTER TABLE tarifario_mimaki CHANGE subcategoria_producto_id subcategoria_producto_id INT DEFAULT NULL, CHANGE precio precio DOUBLE PRECISION NOT NULL, CHANGE precio_plus precio_plus DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE tarifario_xerox CHANGE subcategoria_producto_id subcategoria_producto_id INT DEFAULT NULL, CHANGE cantidad_min cantidad_min INT NOT NULL, CHANGE cantidad_max cantidad_max INT NOT NULL');
        $this->addSql('ALTER TABLE user_tc CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE zoho_tokens CHANGE tipo_zoho_id tipo_zoho_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE plan');
        $this->addSql('ALTER TABLE categoria_producto CHANGE tipo_producto_id tipo_producto_id INT NOT NULL');
        $this->addSql('ALTER TABLE orden_cab CHANGE user_id user_id INT NOT NULL, CHANGE total_items total_items DOUBLE PRECISION DEFAULT \'0\', CHANGE total_envio total_envio DOUBLE PRECISION DEFAULT \'0\', CHANGE subtotal subtotal DOUBLE PRECISION DEFAULT \'0\', CHANGE total_iva total_iva DOUBLE PRECISION DEFAULT \'0\', CHANGE total_pagar total_pagar DOUBLE PRECISION DEFAULT \'0\', CHANGE requiere_envio requiere_envio VARCHAR(1) CHARACTER SET utf8 DEFAULT \'N\' NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE orden_det CHANGE orden_cab_id orden_cab_id INT NOT NULL, CHANGE subcategoria_producto_id subcategoria_producto_id INT NOT NULL, CHANGE sku sku VARCHAR(15) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, CHANGE precio_base precio_base DOUBLE PRECISION DEFAULT \'0\', CHANGE precio_corte precio_corte DOUBLE PRECISION DEFAULT \'0\', CHANGE precio_laminado precio_laminado DOUBLE PRECISION DEFAULT \'0\', CHANGE precio_ojales precio_ojales DOUBLE PRECISION DEFAULT \'0\', CHANGE corte_rigido corte_rigido VARCHAR(1) CHARACTER SET utf8 DEFAULT \'N\' COLLATE `utf8_general_ci`, CHANGE precio_corte_rigido precio_corte_rigido DOUBLE PRECISION DEFAULT \'0\', CHANGE parent_id_wd_impresion parent_id_wd_impresion VARCHAR(100) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, CHANGE parent_id_wd_electro parent_id_wd_electro VARCHAR(100) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, CHANGE file_id_wd_impresion file_id_wd_impresion VARCHAR(100) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, CHANGE file_id_wd_electro file_id_wd_electro VARCHAR(100) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE producto_favorito CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE subcategoria_producto CHANGE categoria_producto_id categoria_producto_id INT NOT NULL');
        $this->addSql('ALTER TABLE tarifario_electrocorte_xerox CHANGE cantidad_min cantidad_min INT DEFAULT 0 NOT NULL, CHANGE cantidad_max cantidad_max INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE tarifario_mimaki CHANGE subcategoria_producto_id subcategoria_producto_id INT NOT NULL, CHANGE precio precio DOUBLE PRECISION DEFAULT \'0\' NOT NULL, CHANGE precio_plus precio_plus DOUBLE PRECISION DEFAULT \'0\'');
        $this->addSql('ALTER TABLE tarifario_xerox CHANGE subcategoria_producto_id subcategoria_producto_id INT NOT NULL, CHANGE cantidad_min cantidad_min INT DEFAULT 0 NOT NULL, CHANGE cantidad_max cantidad_max INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE user_tc CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE zoho_tokens CHANGE tipo_zoho_id tipo_zoho_id INT NOT NULL');
    }
}
