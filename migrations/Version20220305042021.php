<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220305042021 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE orden_det_caracteristica (id INT AUTO_INCREMENT NOT NULL, orden_det_id INT DEFAULT NULL, caracteristica_id INT DEFAULT NULL, estado VARCHAR(3) DEFAULT \'A\' NOT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, fecha_actualizacion DATETIME DEFAULT NULL, valor VARCHAR(25) DEFAULT NULL, val1 VARCHAR(25) DEFAULT NULL, val2 VARCHAR(25) DEFAULT NULL, val3 VARCHAR(25) DEFAULT NULL, INDEX IDX_5A0E19DEE6B30B8A (orden_det_id), INDEX IDX_5A0E19DEA7300D78 (caracteristica_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orden_det_caracteristica ADD CONSTRAINT FK_5A0E19DEE6B30B8A FOREIGN KEY (orden_det_id) REFERENCES orden_det (id)');
        $this->addSql('ALTER TABLE orden_det_caracteristica ADD CONSTRAINT FK_5A0E19DEA7300D78 FOREIGN KEY (caracteristica_id) REFERENCES caracteristica (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE orden_det_caracteristica');
    }
}
