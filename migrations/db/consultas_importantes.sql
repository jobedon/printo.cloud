-- Consulta de transacciones de clientes por tarjeta de credito
select
    dt.id,
    up.razon_social,
    dt.fecha_creacion,
    dt.response,
    oc.total_pagar,
    oc.no_orden
from
    datafast_trx dt,
    orden_cab oc,
    user_pi up
where
    dt.orden_cab_id = oc.id
    and oc.user_id = up.id
    and up.razon_social = 'Exproduce S.a.'
    and dt.tipo = 'purchase'
    and dt.result_code = '000.000.000'
order by
    dt.fecha_creacion desc;