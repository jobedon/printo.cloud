<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309051920 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE datafast_trx ADD user_id INT DEFAULT NULL, ADD observacion VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE datafast_trx ADD CONSTRAINT FK_4C8182EAA76ED395 FOREIGN KEY (user_id) REFERENCES user_pi (id)');
        $this->addSql('CREATE INDEX IDX_4C8182EAA76ED395 ON datafast_trx (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE datafast_trx DROP FOREIGN KEY FK_4C8182EAA76ED395');
        $this->addSql('DROP INDEX IDX_4C8182EAA76ED395 ON datafast_trx');
        $this->addSql('ALTER TABLE datafast_trx DROP user_id, DROP observacion');
    }
}
