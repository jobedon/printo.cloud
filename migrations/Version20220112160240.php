<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112160240 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tarifario_xerox ADD categoria_producto_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tarifario_xerox ADD CONSTRAINT FK_1C75369E69022511 FOREIGN KEY (categoria_producto_id) REFERENCES categoria_producto (id)');
        $this->addSql('CREATE INDEX IDX_1C75369E69022511 ON tarifario_xerox (categoria_producto_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tarifario_xerox DROP FOREIGN KEY FK_1C75369E69022511');
        $this->addSql('DROP INDEX IDX_1C75369E69022511 ON tarifario_xerox');
        $this->addSql('ALTER TABLE tarifario_xerox DROP categoria_producto_id');
    }
}
