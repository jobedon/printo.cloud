<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210714024249 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_plan_history (id INT AUTO_INCREMENT NOT NULL, plan_inicial_id INT DEFAULT NULL, plan_final_id INT DEFAULT NULL, user_id INT DEFAULT NULL, status VARCHAR(3) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_19C9444121179EF2 (plan_inicial_id), INDEX IDX_19C944411DB12C57 (plan_final_id), INDEX IDX_19C94441A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_plan_history ADD CONSTRAINT FK_19C9444121179EF2 FOREIGN KEY (plan_inicial_id) REFERENCES plan (id)');
        $this->addSql('ALTER TABLE user_plan_history ADD CONSTRAINT FK_19C944411DB12C57 FOREIGN KEY (plan_final_id) REFERENCES plan (id)');
        $this->addSql('ALTER TABLE user_plan_history ADD CONSTRAINT FK_19C94441A76ED395 FOREIGN KEY (user_id) REFERENCES user_pi (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_plan_history');
    }
}
