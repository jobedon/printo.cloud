<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309054657 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE paquete_envio (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, datafast_trx_id INT DEFAULT NULL, observacion VARCHAR(100) DEFAULT NULL, estado VARCHAR(3) DEFAULT \'A\' NOT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, fecha_actualizacion DATETIME DEFAULT NULL, INDEX IDX_80669BA7A76ED395 (user_id), INDEX IDX_80669BA7513ED975 (datafast_trx_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE paquete_envio ADD CONSTRAINT FK_80669BA7A76ED395 FOREIGN KEY (user_id) REFERENCES user_pi (id)');
        $this->addSql('ALTER TABLE paquete_envio ADD CONSTRAINT FK_80669BA7513ED975 FOREIGN KEY (datafast_trx_id) REFERENCES datafast_trx (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE paquete_envio');
    }
}
