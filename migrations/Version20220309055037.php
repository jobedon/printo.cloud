<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309055037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orden_det ADD paquete_envio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orden_det ADD CONSTRAINT FK_1936A5CA8C1219E9 FOREIGN KEY (paquete_envio_id) REFERENCES paquete_envio (id)');
        $this->addSql('CREATE INDEX IDX_1936A5CA8C1219E9 ON orden_det (paquete_envio_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orden_det DROP FOREIGN KEY FK_1936A5CA8C1219E9');
        $this->addSql('DROP INDEX IDX_1936A5CA8C1219E9 ON orden_det');
        $this->addSql('ALTER TABLE orden_det DROP paquete_envio_id');
    }
}
