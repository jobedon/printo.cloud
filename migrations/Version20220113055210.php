<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113055210 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tarifario_mimaki CHANGE producto_id producto_id_zoho VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL');
        $this->addSql('ALTER TABLE tarifario_mimaki ADD producto_id INT NULL DEFAULT NULL AFTER subcategoria_producto_id');
        $this->addSql('ALTER TABLE tarifario_mimaki ADD CONSTRAINT FK_427A87C7645698E FOREIGN KEY (producto_id) REFERENCES producto (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_427A87C7645698E ON tarifario_mimaki (producto_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tarifario_mimaki DROP producto_id');
        $this->addSql('ALTER TABLE tarifario_mimaki CHANGE producto_id_zoho producto_id VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL');
        $this->addSql('ALTER TABLE tarifario_mimaki CHANGE producto producto_id VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL');
        $this->addSql('ALTER TABLE tarifario_mimaki DROP FOREIGN KEY FK_427A87C7645698E');
        $this->addSql('DROP INDEX UNIQ_427A87C7645698E ON tarifario_mimaki');
    }
}
