INSERT INTO `caracteristica` (`nombre`, `descripcion`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
('anchos_disponibles', NULL, 'A', '2022-01-13 00:23:26', NULL),
('altos_disponibles', NULL, 'A', '2022-01-13 00:23:26', NULL),
('corte_recto', NULL, 'A', '2022-01-18 01:37:24', NULL),
('electrocorte', NULL, 'A', '2022-01-18 01:37:24', NULL),
('ojales', NULL, 'A', '2022-01-18 01:37:46', NULL),
('tipo_impresion', 'Puede ser T o T/R', 'A', '2022-01-18 01:43:26', NULL);


INSERT INTO `producto` (`producto_id`, `categoria_producto_id`, `nombre_producto`, `descripcion`, `sku`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(NULL, NULL, 'Formatos Super A3', 'XEROX', NULL, 'A', '2022-01-12 23:16:44', NULL),
(NULL, NULL, 'Gigantografias', 'MIMAKI', NULL, 'A', '2022-01-12 23:16:44', NULL),
(1, 1, 'Papel Bond de 75 gr 32.5x44.5', NULL, 'INV-X01', 'A', '2022-01-12 23:21:45', NULL),
(1, 1, 'Papel Bond de 90 gr 32.5x44.5', NULL, 'INV-X02', 'A', '2022-01-12 23:21:45', NULL),
(1, 1, 'Papel Bond de 120 gr 32.5x44.5', NULL, 'INV-X03', 'A', '2022-01-12 23:21:45', NULL),
(1, 1, 'Papel Couche Brillo de 115 gr 32.5x44.5', NULL, 'INV-X04', 'A', '2022-01-12 23:21:45', NULL),
(1, 1, 'Papel Couche Brillo de 150 gr 32.5x44.5', NULL, 'INV-X05', 'A', '2022-01-12 23:21:45', NULL),
(1, 1, 'Papel Couche Mate de 115 gr 32.5x44.5', NULL, 'INV-X06', 'A', '2022-01-12 23:21:45', NULL),
(1, 1, 'Papel Couche Mate de 150 gr 32.5x44.5', NULL, 'INV-X07', 'A', '2022-01-12 23:21:45', NULL),
(1, 1, 'Earth Pact Natural de 90 gr. 32.5x44.5', NULL, 'INV-X08', 'A', '2022-01-12 23:21:45', NULL),
(1, 1, 'Earth Pact Natural de 150 gr. 32.5x44.5', NULL, 'INV-X09', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Papel Couche Brillo de 200 gr 32.5x44.5', NULL, 'INV-X10', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Papel Couche Brillo de 250 gr 32.5x44.5', NULL, 'INV-X11', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Papel Couche Brillo de 300 gr 32.5x44.5', NULL, 'INV-X12', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Papel Couche Mate de 200 gr 32.5x44.5', NULL, 'INV-X13', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Papel Couche Mate de 250 gr 32.5x44.5', NULL, 'INV-X14', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Papel Couche Mate de 300 gr 32.5x44.5', NULL, 'INV-X15', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Earth Pact Natural de 200 gr. 32.5x44.5', NULL, 'INV-X16', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Plegable 012 33x48.8', NULL, 'INV-X17', 'A', '2022-01-12 23:21:45', NULL),
(1, 2, 'Plegable 014 33x48.8', NULL, 'INV-X18', 'A', '2022-01-12 23:21:45', NULL),
(1, 3, 'Adhesivo P3H 33x48.8', NULL, 'INV-X19', 'A', '2022-01-12 23:21:45', NULL),
(1, 3, 'Adhesivo P4H 33x48.8', NULL, 'INV-X20', 'A', '2022-01-12 23:21:45', NULL),
(1, 3, 'Adhesivo Pre-Cortar 33x48.8', NULL, 'INV-X21', 'A', '2022-01-12 23:21:45', NULL),
(1, 3, 'Adhesivo Vinil Transparente Brillante 48.8x33', NULL, 'INV-X22', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Polipropileno Vinil Adhesivo Blanco Mate 48.8x33', NULL, 'INV-X23', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Papel Adhesivo Metalizado Plata 33x48.8', NULL, 'INV-X24', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Majestic Dig Marble White de 250 gr. 46.4x32', NULL, 'INV-X25', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Oro Intenso de 250 gr. 46.4x32', NULL, 'INV-X26', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'INV - Esse Digital Silver de 284 gr - 45.7x30.5', NULL, 'INV-X27', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Linen Digital White 311 gr 45.7x30.5', NULL, 'INV-X28', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Bianco Flash Linen de 250 gr 33x48.8', NULL, 'INV-X29', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Classic Linen Avalancha White de 270 gr. 33x48.8', NULL, 'INV-X30', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Classic Linen Natural White de 270 gr. 33x48.8', NULL, 'INV-X31', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'StarWhite Flash Blue de 227 gr.33x48.8', NULL, 'INV-X32', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Shiro Alga White de 300 gr 33x48.8', NULL, 'INV-X33', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Crush Uva de 250 gr. 33x48.8', NULL, 'INV-X34', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Splendorlux Premium White fotografico 33x48.8', NULL, 'INV-X35', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Tintoretto Gesso de 240 gr 33x48.8', NULL, 'INV-X36', 'A', '2022-01-12 23:21:45', NULL),
(1, 4, 'Astrosilver Orion 148 de 220 gr. 33x48.8', NULL, 'INV-X37', 'A', '2022-01-12 23:21:45', NULL),
(2, 5, 'Lona Brillante de 13 onzas', NULL, 'PMI-0002', 'A', '2022-01-13 00:28:26', NULL),
(2, 5, 'Lona Mate de 13 onzas', NULL, 'PMI-0003', 'A', '2022-01-13 00:28:26', NULL),
(2, 5, 'Lona Translúcida de 13 onzas', NULL, 'PMI-0004', 'A', '2022-01-13 00:28:26', NULL),
(2, 7, 'Vinil Blanco Brillante respaldo blanco', NULL, 'PMI-0005', 'A', '2022-01-13 00:28:26', NULL),
(2, 7, 'Vinil Blanco Brillante respaldo negro/gris', NULL, 'PMI-0006', 'A', '2022-01-13 00:28:26', NULL),
(2, 7, 'Vinil Blanco Mate respaldo blanco', NULL, 'PMI-0007', 'A', '2022-01-13 00:28:26', NULL),
(2, 7, 'Vinil Blanco Mate respaldo negro/gris', NULL, 'PMI-0008', 'A', '2022-01-13 00:28:26', NULL),
(2, 7, 'Vinil transparente brillante para impresión', NULL, 'PMI-0009', 'A', '2022-01-13 00:28:26', NULL),
(2, 7, 'Vinil transparente Mate para impresión', NULL, 'PMI-0010', 'A', '2022-01-13 00:28:26', NULL),
(2, 7, 'Vinil Microperforado', NULL, 'PMI-0011', 'A', '2022-01-13 00:28:26', NULL),
(2, 7, 'Vinil reflectivo panal de abeja', NULL, 'PMI-0012', 'A', '2022-01-13 00:28:26', NULL),
(2, 5, 'Pet Banner', NULL, 'PMI-0015', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Lienzo Americano Premium', NULL, 'PMI-0016', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Papel Blue Back para planos', NULL, 'PMI-0017', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Back Light Film Brillante', NULL, 'PMI-0020', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Sintra de 2 mm + Vinil Brillante', NULL, 'PMI-0023', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Sintra de 2 mm + Vinil Mate', NULL, 'PMI-0024', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Sintra de 3 mm + Vinil Brillante', NULL, 'PMI-0025', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Sintra de 3 mm + Vinil Mate', NULL, 'PMI-0026', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Foam Board de 5 mm + Vinil Brillante ', NULL, 'PMI-0027', 'A', '2022-01-13 00:28:26', NULL),
(2, 8, 'Foam Board de 5 mm + Vinil Mate', NULL, 'PMI-0028', 'A', '2022-01-13 00:28:26', NULL);


UPDATE tarifario_xerox
SET estado = 'I'
WHERE estado = 'A';


INSERT INTO `tarifario_xerox` (`nivel`, `descripcion_nivel`, `descripcion_cantidad`, `cantidad_min`, `cantidad_max`, `precio_tiro`, `precio_tiro_retiro`, `subcategoria_producto_id`, `estado`, `fecha_creacion`, `fecha_actualizacion`, `categoria_producto_id`) VALUES
(1, 'NIVEL 1', 'De 1 a 3', 1, 3, 1.56, 2.44, 1, 'A', '2020-09-30 11:59:58', NULL, 1),
(2, 'NIVEL 2', 'De 4 a 10', 4, 10, 1.19, 1.99, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(3, 'NIVEL 3', 'De 11 a 20', 11, 20, 1.06, 1.69, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(4, 'NIVEL 4', 'De 21 a 40', 21, 40, 0.94, 1.55, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(5, 'NIVEL 5', 'De 41 a 80', 41, 80, 0.91, 1.32, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(6, 'NIVEL 6', 'De 81 a 150', 81, 150, 0.66, 1.12, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(7, 'NIVEL 7', 'De 151 a 175', 151, 175, 0.53, 0.93, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(8, 'NIVEL 8', 'De 176 a 200', 176, 200, 0.47, 0.88, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(9, 'NIVEL 9', 'De 201 a 250', 201, 250, 0.44, 0.78, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(10, 'NIVEL 10', 'De 251 a 300', 251, 300, 0.38, 0.71, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(11, 'NIVEL 11', 'De 301 en adelante', 301, 999999999, 0.34, 0.62, 1, 'A', '2020-09-30 12:06:01', NULL, 1),
(1, 'NIVEL 1', 'De 1 a 3', 1, 3, 2.5, 3.75, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(2, 'NIVEL 2', 'De 4 a 10', 4, 10, 1.38, 2.31, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(3, 'NIVEL 3', 'De 11 a 20', 11, 20, 1.12, 2.09, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(4, 'NIVEL 4', 'De 21 a 40', 21, 40, 0.97, 1.82, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(5, 'NIVEL 5', 'De 41 a 80', 41, 80, 0.94, 1.55, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(6, 'NIVEL 6', 'De 81 a 150', 81, 150, 0.79, 1.3, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(7, 'NIVEL 7', 'De 151 a 175', 151, 175, 0.6, 1.07, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(8, 'NIVEL 8', 'De 176 a 200', 176, 200, 0.55, 1.04, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(9, 'NIVEL 9', 'De 201 a 250', 201, 250, 0.53, 0.97, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(10, 'NIVEL 10', 'De 251 a 300', 251, 300, 0.45, 0.9, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(11, 'NIVEL 11', 'De 301 en adelante', 301, 999999999, 0.38, 0.8, 3, 'A', '2020-09-30 12:23:34', NULL, 2),
(1, 'NIVEL 1', 'De 1 a 3', 1, 3, 4.12, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(2, 'NIVEL 2', 'De 4 a 10', 4, 10, 1.62, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(3, 'NIVEL 3', 'De 11 a 20', 11, 20, 1.6, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(4, 'NIVEL 4', 'De 21 a 40', 21, 40, 1.47, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(5, 'NIVEL 5', 'De 41 a 80', 41, 80, 1.37, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(6, 'NIVEL 6', 'De 81 a 150', 81, 150, 1.24, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(7, 'NIVEL 7', 'De 151 a 175', 151, 175, 1.13, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(8, 'NIVEL 8', 'De 176 a 200', 176, 200, 1.04, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(9, 'NIVEL 9', 'De 201 a 250', 201, 250, 0.94, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(10, 'NIVEL 10', 'De 251 a 300', 251, 300, 0.81, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(11, 'NIVEL 11', 'De 301 en adelante', 301, 999999999, 0.74, NULL, 6, 'A', '2020-09-30 12:36:10', NULL, 3),
(1, 'NIVEL 1', 'De 1 a 3', 1, 3, 5.5, 6.25, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(2, 'NIVEL 2', 'De 4 a 10', 4, 10, 2.1, 3.09, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(3, 'NIVEL 3', 'De 11 a 20', 11, 20, 2.06, 2.8, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(4, 'NIVEL 4', 'De 21 a 40', 21, 40, 1.91, 2.63, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(5, 'NIVEL 5', 'De 41 a 80', 41, 80, 1.76, 2.3, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(6, 'NIVEL 6', 'De 81 a 150', 81, 150, 1.63, 1.95, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(7, 'NIVEL 7', 'De 151 a 175', 151, 175, 1.49, 1.85, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(8, 'NIVEL 8', 'De 176 a 200', 176, 200, 1.46, 1.74, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(9, 'NIVEL 9', 'De 201 a 250', 201, 250, 1.4, 1.65, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(10, 'NIVEL 10', 'De 251 a 300', 251, 300, 1.34, 1.6, 9, 'A', '2020-09-30 12:44:49', NULL, 4),
(11, 'NIVEL 11', 'De 301 en adelante', 301, 999999999, 1.24, 1.54, 9, 'A', '2020-09-30 12:44:49', NULL, 4);


UPDATE tarifario_mimaki
SET estado = 'I'
WHERE estado = 'A';


INSERT INTO `tarifario_mimaki` (`descripcion_producto`, `producto_id_zoho`, `subcategoria_producto_id`, `producto_id`, `precio`, `precio_plus`, `precio_recto_metro2`, `estado`, `sku`, `anchos_disponibles`, `altos_disponibles`, `fecha_creacion`, `fecha_actualizacion`, `precio_cliente_final`, `precio_printo_free`, `precio_printo_profesional`) VALUES
(NULL, NULL, NULL, 40, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:18:11', NULL, 7.14, 5.8, 5.36),
(NULL, NULL, NULL, 41, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:19:51', NULL, 7.14, 5.8, 5.36),
(NULL, NULL, NULL, 42, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:19:51', NULL, 13.19, 8.93, 8.04),
(NULL, NULL, NULL, 43, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:21:27', NULL, 8.93, 6.7, 6.25),
(NULL, NULL, NULL, 44, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:21:27', NULL, 10.71, 7.59, 7.14),
(NULL, NULL, NULL, 45, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 8.93, 6.7, 6.25),
(NULL, NULL, NULL, 46, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 10.71, 7.59, 7.14),
(NULL, NULL, NULL, 47, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 8.93, 6.7, 6.25),
(NULL, NULL, NULL, 48, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 8.93, 6.7, 6.25),
(NULL, NULL, NULL, 49, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 11.16, 8.48, 7.59),
(NULL, NULL, NULL, 50, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 22.32, 20),
(NULL, NULL, NULL, 51, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 17, 16),
(NULL, NULL, NULL, 52, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 22.32, 20),
(NULL, NULL, NULL, 53, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 12, 9.5, 8.5),
(NULL, NULL, NULL, 54, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 17, 16),
(NULL, NULL, NULL, 55, NULL, NULL, 2, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 16.96, 16.07),
(NULL, NULL, NULL, 56, NULL, NULL, 2, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 16.96, 16.07),
(NULL, NULL, NULL, 57, NULL, NULL, 2, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 17.86, 16.96),
(NULL, NULL, NULL, 58, NULL, NULL, 2, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 17.86, 16.96),
(NULL, NULL, NULL, 59, NULL, NULL, 3, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 25, 19.64, 18.75),
(NULL, NULL, NULL, 60, NULL, NULL, 3, 'A', NULL, NULL, NULL, '2022-01-13 01:26:55', NULL, 26.79, 19.64, 18.75);


INSERT INTO `producto_caracteristica` (`producto_id`, `caracteristica_id`, `valor`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(40, 1, '90,101,125,160', 'A', '2022-01-13 01:36:53', NULL),
(40, 2, 'Todos', 'A', '2022-01-13 01:36:53', NULL),
(41, 1, '90,101,125,160', 'A', '2022-01-13 01:40:23', NULL),
(42, 1, '90,101,125,160', 'A', '2022-01-13 01:40:23', NULL),
(43, 1, '152,122,127,101,76', 'A', '2022-01-13 01:40:23', NULL),
(44, 1, '76,101,122,127,152', 'A', '2022-01-13 01:40:23', NULL),
(45, 1, '76,101,122,127,152', 'A', '2022-01-13 01:40:23', NULL),
(46, 1, '76,101,122,127,152', 'A', '2022-01-13 01:40:23', NULL),
(47, 1, '76,101,122,127,152', 'A', '2022-01-13 01:40:23', NULL),
(48, 1, '76,101,122,127,152', 'A', '2022-01-13 01:40:23', NULL),
(49, 1, '101,122,127,152', 'A', '2022-01-13 01:40:23', NULL),
(50, 1, '124', 'A', '2022-01-13 01:40:23', NULL),
(51, 1, '91', 'A', '2022-01-13 01:40:23', NULL),
(52, 1, '51,101,152', 'A', '2022-01-13 01:40:23', NULL),
(53, 1, '127,152', 'A', '2022-01-13 01:40:23', NULL),
(54, 1, '51,101,152', 'A', '2022-01-13 01:40:23', NULL),
(55, 1, '120', 'A', '2022-01-13 01:40:23', NULL),
(56, 1, '120', 'A', '2022-01-13 01:40:23', NULL),
(57, 1, '120', 'A', '2022-01-13 01:40:23', NULL),
(58, 1, '120', 'A', '2022-01-13 01:40:23', NULL),
(59, 1, '120', 'A', '2022-01-13 01:40:23', NULL),
(60, 1, '120', 'A', '2022-01-13 01:40:23', NULL),
(41, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(42, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(43, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(44, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(45, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(46, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(47, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(48, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(49, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(50, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(51, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(52, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(53, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(54, 2, 'Todos', 'A', '2022-01-13 01:40:23', NULL),
(55, 2, '240', 'A', '2022-01-13 01:40:23', NULL),
(56, 2, '240', 'A', '2022-01-13 01:40:23', NULL),
(57, 2, '240', 'A', '2022-01-13 01:40:23', NULL),
(58, 2, '240', 'A', '2022-01-13 01:40:23', NULL),
(59, 2, '240', 'A', '2022-01-13 01:40:23', NULL),
(60, 2, '240', 'A', '2022-01-13 01:40:23', NULL),
(24, 6, 'TIRO', 'A', '2022-01-18 01:44:25', NULL);



UPDATE tarifario_electrocorte_xerox
SET precio_adhesivo = ROUND((precio_adhesivo / 1.12),2), precio_cartulina = ROUND((precio_cartulina / 1.12),2);


UPDATE tarifario_mimaki
SET precio_recto_metro2 = ROUND((precio_recto_metro2 / 1.12),2)
WHERE estado = 'A'
AND precio_recto_metro2 is not null;


UPDATE configuraciones
SET valorDouble = ROUND((valorDouble / 1.12),2)
WHERE codigo IN (
    'PRECIO_CORTE_PLANAS',
    'PRECIO_CORTE_RECTO', 
    'PRECIO_ELECTROCORTE_MIMAKI',
    'VALOR_LIMITE_LAMINADO',
    'PRECIO_LAMINADO_TIRO',
    'PRECIO_LAMINADO_TR',
    'PRECIO_LAMINADO_BRILLO_MIMAKI',
    'PRECIO_LAMINADO_MATE_MIMAKI',
    'PRECIO_LAMINADO_FLOORGRAPHIC_MIMAKI',
    'PRECIO_OJALES_MIMAKI'
);


INSERT INTO `configuraciones` (`id`, `codigo`, `mapping_name`, `valorDouble`, `valorString`, `descripcion`, `estado`, `fecha_creacion`, `fecha_actualizacion`) VALUES (NULL, 'PROMO_XEROX', NULL, '21', NULL, 'porcentaje de promocion', 'A', CURRENT_TIMESTAMP, NULL);


