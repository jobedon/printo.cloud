<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113061458 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tarifario_mimaki ADD precio_cliente_final DOUBLE PRECISION DEFAULT NULL, ADD precio_printo_free DOUBLE PRECISION DEFAULT NULL, ADD precio_printo_profesional DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tarifario_mimaki DROP precio_cliente_final, DROP precio_printo_free, DROP precio_printo_profesional');
    }
}
