<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220118074308 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orden_det CHANGE producto_id producto_id_zoho VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL');
        $this->addSql('ALTER TABLE orden_det ADD producto_id INT NULL DEFAULT NULL');

        $this->addSql('ALTER TABLE orden_det ADD CONSTRAINT FK_1936A5CA7645698E FOREIGN KEY (producto_id) REFERENCES producto (id)');
        $this->addSql('CREATE INDEX IDX_1936A5CA7645698E ON orden_det (producto_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orden_det DROP producto_id');
        $this->addSql('ALTER TABLE orden_det CHANGE producto_id_zoho producto_id VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL');

        $this->addSql('ALTER TABLE orden_det DROP FOREIGN KEY FK_1936A5CA7645698E');
        $this->addSql('DROP INDEX IDX_1936A5CA7645698E ON orden_det');
    }
}
