<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220220080530 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE direccion (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, nombres VARCHAR(100) DEFAULT NULL, apellidos VARCHAR(100) NOT NULL, razon_social VARCHAR(100) DEFAULT NULL, direccion1 VARCHAR(200) DEFAULT NULL, direccion2 VARCHAR(200) DEFAULT NULL, telefono1 VARCHAR(20) DEFAULT NULL, telefono2 VARCHAR(20) DEFAULT NULL, ciudad VARCHAR(50) DEFAULT NULL, provincia VARCHAR(50) DEFAULT NULL, ciudad_id INT DEFAULT NULL, correo VARCHAR(50) DEFAULT NULL, estado VARCHAR(3) DEFAULT \'A\' NOT NULL, fecha_creacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, fecha_actualizacion DATETIME DEFAULT NULL, tipo VARCHAR(15) DEFAULT \'DESTINO\', INDEX IDX_F384BE95A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE direccion ADD CONSTRAINT FK_F384BE95A76ED395 FOREIGN KEY (user_id) REFERENCES user_pi (id)');
        $this->addSql('ALTER TABLE orden_det DROP producto_id_zoho');
        $this->addSql('ALTER TABLE producto_favorito DROP producto_id_zoho');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE direccion');
        $this->addSql('ALTER TABLE orden_det ADD producto_id_zoho VARCHAR(50) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE producto_favorito ADD producto_id_zoho VARCHAR(50) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`');
    }
}
