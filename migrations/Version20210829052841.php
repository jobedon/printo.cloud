<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210829052841 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE files_orden_det (id INT AUTO_INCREMENT NOT NULL, parent_id_impresion_ws VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta item Xerox/Mimaki en mesa de trabajo\', parent_id_electro_ws VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta item Electrocorte en mesa de trabajo\', file_id_impresion_ws VARCHAR(100) DEFAULT NULL COMMENT \'file Id item Xerox/Mimaki en mesa de trabajo\', file_id_electro_ws VARCHAR(100) DEFAULT NULL COMMENT \'file Id item Electrocorte en mesa de trabajo\', parent_id_impresion_tk VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta item Xerox/Mimaki en carpeta Tickets\', parent_id_electro_tk VARCHAR(100) DEFAULT NULL COMMENT \'Parent Id carpeta item Electrocorte en carpeta Tickets\', file_id_impresion_tk VARCHAR(100) DEFAULT NULL COMMENT \'file Id item Xerox/Mimaki en carpeta Tickets\', file_id_electro_tk VARCHAR(100) DEFAULT NULL COMMENT \'file Id item Electrocorte en carpeta Tickets\', description VARCHAR(100) DEFAULT NULL, status VARCHAR(3) DEFAULT \'A\' NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE files_orden_cab CHANGE status status VARCHAR(3) DEFAULT \'A\' NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE files_orden_det');
        $this->addSql('ALTER TABLE files_orden_cab CHANGE status status VARCHAR(3) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
